export class Manufactures {
    id: number;
    name: string;
    createdTime: number;
    slug: string;
    description: string;
    logoImage: string;
    address: string;
    website: string;
    seoTitle: string;
    seoKeywords: string;
    seoDescription: string;
    seoIndex: number;
    blSeoIndex: boolean;

    blname: boolean = false;
    blSeoTitle: boolean = false;
    blSeoKeywork: boolean = false;
    blSeoDepcription: boolean = false;
}

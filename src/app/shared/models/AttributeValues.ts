export class AttributeValues {
    id: number;
    attributeId: number;
    value: string;
    blSelected: boolean = false;
}

import { BaseEntity } from './base-entity';
export class Accessary  extends BaseEntity {
    accessaryURN: string;
    nameVN: string;
    nameEN: string;
}

export  class SelectItem {
    label?: string;
    value?: number;
    styleClass?: string;
    icon?: string;
    title?: string;
    disabled?: boolean;
}

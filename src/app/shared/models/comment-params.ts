export class CommentParams {
  claimId: number = null ;
  beneficiaryId: number = null;
  classificationExchange: string = '';
  classificationSending: string = '';
  content: string = '';
}

import {Attributes} from './Attributes';
import {AttributeValues} from './AttributeValues';
export class ListAttributeAndValues {
    attribute: Attributes;
    listAttributeValues: AttributeValues[] = [];
    blValidate: boolean;
}

export class CustomerOrderProduct {
    id: number;
    productId: number;
    customerOrderId: number;
    quantity: number;
    unitPrice: number;
    subTotal: number;
    discountTotal: number;
    discountId: number;
    discountCode: number;
    total: number;
    createdAt: number;
    updatedAt: number;
    blProduct: boolean;
    blQuantity: boolean;
    orginSku: string;
    sku: string;
    productName: string;
    pathBarCode: string;
}

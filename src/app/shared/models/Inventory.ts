export class Inventory  {
    id: number;
    sku: string;
    productId: number;
    productName: string;
    stock: number;
    currentStock: number;
    createdAt: number;
    updatedAt: number;
    export: number;
    blExport: boolean;
    inventoryArea: string;
    inventoryAreaId: number;
}

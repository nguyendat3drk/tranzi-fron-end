import {BaseEntity} from "./base-entity";

export class Incident extends BaseEntity {
  nameEn: string;
  nameVn: string;
}

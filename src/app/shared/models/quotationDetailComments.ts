import { BaseEntity } from './base-entity';
export class QuotationDetailComments extends BaseEntity {
    quotationDetailId: number;
    employeeName: string;
    beneficiaryName: string;
    content: string;
    createdDate: string;
}

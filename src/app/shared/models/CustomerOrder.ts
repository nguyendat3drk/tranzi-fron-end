import { CustomerOrderProduct } from "./CustomerOrderProduct";

export class CustomerOrder {
    id: number;
    orderCode: string;
    shippingAddress: string;
    shippingName: string;
    shippingMobile: string;
    shippingDistrict: string;
    shippingCity: string;
    shippingEmail: string;
    customerId: number;
    customerName: number;
    subTotal: number;
    discountTotal: number;
    discountId: number;
    discountCode: string;
    shippingFee: number;
    total: number;
    status: number;
    paymentMethod: number;
    paymentStatus: string;
    shipmentStatus: number;
    note: string;
    createdAt: number;
    updatedAt: number;
    listProductOrder: CustomerOrderProduct [] = [];
    listProductOrderDelete: CustomerOrderProduct [] = [];
}

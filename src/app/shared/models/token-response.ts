import {RoleType} from './role-type.enum';

export class TokenResponse {
  type: number;
  access_token: string;
  token_type: string;
  refresh_token: string;
  expires_in: number;
  scope: string;
  roles: RoleType[];
  employeeId: string;
  username: string;
  beneficiaryId: number;
  id: number;
  isAuth: boolean;
  isRememberPassword: boolean;
  isExternalAccess: boolean;
  userRole:  number;
  token: string;
  firstLogin:  number;
  state: string;
  email: string;
  activationCode: string;
  rememberCode: string;
  firstName: string;
  lastName: string;
  company: string;
  phone: string;
  flags:number;
}

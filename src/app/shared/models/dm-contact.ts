import {BaseEntity} from './base-entity';

export class DmContact extends BaseEntity {
  accountNumber: string;
  address: string;
  bankName: string;
  birthday: string;
  branchName: string;
  email: string;
  identityCard: string;
  nameEN: string;
  nameVN: string;
  numberPassport: string;
  phoneNumber: string;
  taxCode: string;
  typeContact: string;
  commonName: string;
  commune: string;
  district: string;
  city: string;
  postCode: string;
  bankId: number;
  parentBankId: number;
  bankAccountNumber: string;
  allowQuotation: boolean;
}

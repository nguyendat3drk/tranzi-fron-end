import { BaseEntity } from './base-entity';

export class Document extends BaseEntity {
    id: number;
    nameVN: String;
    status: String;
    updatedDate: Date;
    quantity: number;
    typeUpload: number;
    require: number;
    orderNumber: number;
}

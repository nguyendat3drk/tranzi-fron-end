export class Tags {
    id: number;
    displayValue: string;
    slug: string;
    productId: number;
    articleId: number;
}

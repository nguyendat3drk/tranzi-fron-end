import {BaseEntity} from './base-entity';

export class GaraDetailTablePay extends BaseEntity {
  claimId: number;
  plateNumber: string;
  dateAssignWork: Date;
  compensationId: number;
  dateSentInvoice: Date;
  datePaid: Date;
  numberWaiting: number;
  amount: number;
  employeeId: number;
  employeeName: string;
}

import { AttributeValues } from './AttributeValues';

export class Attributes {
  id: number;
  name: string;
  nameEn: string;
  description: string;
  blSelectedRow: boolean = false;
  disabled: boolean;
  listCategoryName: string;
  type: number;
  orderby: number;
  selectListCategory: number[] = [];
}

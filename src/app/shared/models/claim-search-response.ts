import { Insuranceamount } from './insuranceamount'

export class ClaimSearchResponse {
  claimId: number;
  registrationNo: string;
  policyInforPlateNumber: string;
  policyNumber: string;
  policyInsurancePeriodFromDate: string;
  policyInsurancePeriodTodate: string;
  dateInsurance: string;
  dateOfLoss: string;
  claimNumber: string;
  claimStatus: string;
  claimOfficerName: string;
  description: string;
  claimOfficerId: number;
  coinsurance: string;
  siteSurveyorId: number;
  workSurveyorId: number;
  zoneSiteSurveyorId: number;
  zoneWorkSurveyorId: number;
  insuranceamountFlag: Insuranceamount;
  additionaldata: any

}

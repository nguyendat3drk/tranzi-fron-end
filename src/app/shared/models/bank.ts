import { BaseEntity } from './base-entity';

export class Bank extends BaseEntity {
  accountNumber: string;
  address: string;
  bankName: string;
  birthday: string;
  branchname: string;
  email: string;
  identityCard: string;
  nameEn: string;
  nameVn: string;
  numberPassport: string;
  phoneNumber: string;
  taxCode: string;
  typeContact: string;
}

import { BaseEntity } from './base-entity';
export class HistoryExchangeApproves extends BaseEntity {
    amount: number;
    bossComment: string;
    bossFeedbackTime: string;
    claimOfficeComment: string;
    claimOfficeSendTime: string;
    compensationProposalId: number;
    status: number;
    times: number;
    type: number;
    quotationId: number;
}

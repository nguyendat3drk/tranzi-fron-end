import {BaseEntity} from "./base-entity";

export class Branch extends BaseEntity {
  branchNameEN: string;
  branchNameVN: string;
  companyNameEN: string;
  companyNameVN: string;
  createdBy: number;
  createdDate: string;
  location: string;
  status: number;
  updatedBy: number;
  updatedDate: string;
}

import { BaseEntity } from './base-entity';
export class AuthorizationModel extends BaseEntity {
    access_token: string;
    token_type: string;
    refresh_token: string;
    expires_in: number;
    scope: string;
    roles: any;
    employeeId: number;
    type: number;
    roleId: number;
    username: string;
    beneficiaryId: number;
    fullName: string;
}

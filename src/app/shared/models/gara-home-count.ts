import {BaseEntity} from './base-entity';

export class GaraHomeCount extends BaseEntity {
  claimRequestQuotation: number;
  claimWaitingApprovalQuotation: number;
  claimWaitingGuarantee: number;
  claimNotExportInvoice: number;
  claimWaitingPayment: number;
  claimPaid: number;
}

export class ClaimSearchParams {
  claimId: number;
  buId: number;
  claimNumber: number;
  claimOfficer: string ;
  claimStatus: string ;
  classisNo: string ;
  dateOffLostFrom: string ;
  dateOffLostTo: string ;
  identityCard: string ;
  insuredName: string ;
  plateNumber: string ;
  policyNumber: string ;
  taxCode: string ;

  registrationNo: string;
  // policyNumber: string;
  policyInsurancePeriodFromDate: string;
  policyInsurancePeriodTodate: string;
  dateInsurance: string;
  dateOfLoss: string;
  // claimNumber: string;
  // claimStatus: string;
  claimOfficerName: string;
}

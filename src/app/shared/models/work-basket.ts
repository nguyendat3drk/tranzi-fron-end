export class WorkBasket {
    claimId: number;
    updatedDate: Date;
    taskType: number;
    group: number;
    summary: String;
    claimNumber: number;
    statusBossConfirm: String;
    taskStatus: number;
    garaId: number;
    garaNameVN: String;
    policyInforPlateNumber: string;
    insuredName: string;
    inspectionPlace: string;
    flag: string;
    employeeResolve: string;
    buName: string;
    plateNumber: number;
}

export class Setting {
    id: number;
    key: string;
    type: string;
    value: string;
    url: string;
    shortDescription: string;
    description: string;
    createdAt: number;
    updatedAt: number;
    title: string
}
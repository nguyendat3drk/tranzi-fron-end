
export class Media {
    id: number;
    createdTime: number = 0;
    serverPath: string;
    urlPath: string;
    mimeType: string;
    type: string;
    selectImage: boolean;
}

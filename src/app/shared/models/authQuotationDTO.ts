export class AuthQuotationDTO {
    // Cho phép comment dòng chi tiết báo giá
    allowCommentLine;
    // Cho phép sửa giá trị dòng chi tiết báo giá
    allowEditLine: boolean;
    // Cho phép phê duyệt
    allowAppove: boolean;
    // Cho phép hủy bỏ
    allowCancel: boolean;
    // Cho phép thông qua
    allowPass: boolean;
    // Cho phép lãnh đạo confirm
    allowConfirmByLeader: boolean;
    // Cho phép GDV confirm
    allowConfirmByGDV: boolean;
}

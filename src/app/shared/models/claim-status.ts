export enum ClaimStatus {
  // Ghi nhận, vừa tạo hồ sơ
  CREATE_NEW = '1',
  // Lập dự phòng
  SET_BACKUP = '2',
  // Hoàn thành gán giám định viên hiện trường
  ASSIGNED_EMPLOYEE_SCENCE = '3',
  // Hoàn thành gán giám định viên tại xưởng
  ASSIGNED_EMPLOYEE_GARA = '4',
  // Hoàn thành giám định hiện trường
  COMPLETED_INSPECTION_SCENCE = '5',
  // Hoàn thành giám định tại xưởng
  COMPLETED_INSPECTION_GARA = '6',
  // Hoàn thành chứng từ
  COMPLETED_DOCUMENT = '7',
  // Hoàn thành báo giá
  COMPLETED_PRICE = '8',
  // Hoàn thành giám định
  COMPLETED_INSPECTION = '9',
  // Đề xuất bồi thường
  OFFER_INDEMNIFY = '10',
  // Đóng hoàn thành hồ sơ
  CLOSED = '11',
  // Đóng với lý do từ chối
  CLOSED_BY_REJECT = '12',
}

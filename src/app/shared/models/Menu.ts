
export class Menu {
id: number;
menuCode: String;
menuName: String;
status: String;
description: String;
path: String;
title: String;
icon: String;
selected: number;
expanded: number;
order: number;
parentCode: String;
checkSelected: Boolean = true;
isSubMenu: Boolean;
}

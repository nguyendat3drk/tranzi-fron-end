import {BaseEntity} from './base-entity';

export class EstimatedSalvage extends BaseEntity {
  claimId: number;
  claimScopeId: number;
  paymentSalvageId: string;
  typePaymentSalvage: number;
  nameEN: string;
  nameVN: string;
  estimateAmount: number = 0;
  estimateAmount1: string;
  tax: number = 0;
  taxAmount: number = 0;
  blHidden: boolean;
}

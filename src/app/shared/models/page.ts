export class Page<E> {
  contents: E[];
  link: {
    first: string,
    last: string,
    next: string,
    prev: string
  };
  metadata: {
    currentPage: number,
    size: number,
    total: number,
    totalPage: number
  }
}

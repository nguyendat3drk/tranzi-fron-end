export class EclaimHttpError {
  code: number;
  error: any;
  message: string;
  type: string;
}

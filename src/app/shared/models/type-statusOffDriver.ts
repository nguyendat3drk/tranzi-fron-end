export enum TypeStatusOffDriver {
  WAITINGFORSURVEYOR = '1',
  ISREPAIRING = '2',
  REPAIRED = '3',
}

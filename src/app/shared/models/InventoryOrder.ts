export class InventoryOrder {
  sku: string;
  productId: number;
  productName: string;
  id: number;
  inventoryId: number;
  type: number;
  reasonId: number;
  quantity: number;
  unitPrice: number;
  totalPrice: number;
  serialNumbers: string;
  comment: string;
  createdAt: number;
  updatedAt: number;
  blQuantity: boolean;
  blUnitPrice: boolean;
  maxQuatity: boolean;
}

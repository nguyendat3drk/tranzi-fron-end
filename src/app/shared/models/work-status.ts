export enum WorkStatus {
  STATUS_CCN = 1, // * 1. Chờ chấp nhận</br>
  STATUS_CN = 2, // * 2. Chấp nhận</br>
  STATUS_KCN = 3, // * 3. không chấp nhận</br>
  STATUS_CPD = 4, // * 4. Chờ phê duyệt</br>
  STATUS_KPD = 5, // * 5. Không phê duyệt</br>
  STATUS_HT = 6, // * 6. Hoàn thành</br>
  STATUS_CHT = 7, // * 7. Chưa hoàn thành</br>
  STATUS_CBVPH = 8, // * 8. Chờ bảo việt phản hồi</br>
  STATUS_CGAPH = 9, // * 9. Chờ Gara phàn hồi</br>
  STATUS_PD = 10, // * 10. Phê duyệt
}

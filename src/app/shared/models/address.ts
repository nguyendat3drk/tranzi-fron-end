export class Address {
  district: string;
  postCode: string;
  city: string;
  commune: string;
  fullAddress: string;
}

export class MenuItem {
  // id: number;
  // menuCode: string;
  // menuName: string;
  // path: string;
  // parentCode: string;
  // insertPri: number;
  // updatePri: number;
  // deletePri: number;
  // queryPri: number;
  title: string;
  isSubMenu: boolean;
  icon: string;
  pathMatch: string;
  selected: boolean;
  expanded: boolean;
  order: number;
}

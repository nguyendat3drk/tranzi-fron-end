export enum TypeDamageLevel {
  DamageSmall = '1',
  DamageMedium = '2',
  DamageLarge = '3',
}

import {BaseEntity} from './base-entity';

export class GaraDetailTable extends BaseEntity {
  claimId: number;
  plateNumber: string;
  dateAssignWork: Date;
  quotationId: number;
  dateRequestQuotation: Date;
  dateQuotation: Date;
  numberWaiting: number;
  amount: number;
  employeeId: number;
  employeeName: string;
}

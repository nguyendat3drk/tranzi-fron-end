import { ArticleLinkUpload } from "./ArticleLinkUpload";

export class Articles {
    id: number;
    authorId: number;
    createdTime: number;
    name: string;
    slug: string;
    description: string;
    content: string;
    bannerImage: string;
    url: string;
    seoTitle: string;
    seoKeywords: string;
    seoDescription: string;
    seoIndex: number;
    blSeoIndex: boolean
    productName: string;
    productId: number;
    termsid: number;
    type: number;
    listTerms: number []=[];
    listArticleLinkUpload: ArticleLinkUpload[] = [];
}

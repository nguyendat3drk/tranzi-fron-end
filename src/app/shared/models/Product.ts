import { Media } from './Media';
import { AttributeValues } from './AttributeValues';
import { Tags } from './Tags';
import { ListAttributeAndValues } from './ListAttributeAndValues';
import { ArticleLinkUpload } from './ArticleLinkUpload';

export class Product {
    id: number;
    authorId: number;
    name: string;
    slug: string;
    shortDescription: string;
    description: string;
    createdTime: number;
    normalPrice: number;
    salePrice: number;
    saleStart: number;
    saleEnd: number;
    sku: string;
    orginSku: string;
    manufactureId: number;
    inStock: number;
    featured: number;
    blFeatured: boolean;
    orginFrom: string;
    packageTypeId: number;
    minOrder: number;
    shippingTime: string;
    thumbnailImage: string;
    actived: number;
    blActived: boolean;
    seoTitle: string;
    seoKeywords: string;
    seoDescription: string;
    seo_index: number;
    seoIndex: number;
    blSeoIndex: boolean;
    categoriesName: string;
    categoryId: number;
    listMedia: Media[] = []
    listAttributeValues: AttributeValues [] = [];
    listTags: Tags []= []
    listAttributeAndValues: ListAttributeAndValues[] = [];
    listMediaThumbnail: Media[] = []
    listCategory: number []= []
    blCheckDel: string;
    productStatus: string;
    listArticleLinkUpload: ArticleLinkUpload[] = [];
}       

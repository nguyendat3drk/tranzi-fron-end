export class Announcer {
  announcerEmail: string = '';
  announcerId: number ;
  announcerName: string = '';
  announcerPhone: string = '';
  isPassRequireValid: boolean;
}

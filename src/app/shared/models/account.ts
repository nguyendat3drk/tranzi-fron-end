import {BaseEntity} from './base-entity';
import {DmContact} from './dm-contact';

export class Account extends BaseEntity {
  accountType: number;
  contactId: number;
  dmContact: DmContact;
  employeeId: number;
  id: number;
  roleId:  number;
  userName: string;
  password: string;

  ipAddress: string;
  username: string;
  salt: string;
  email: string;
  activationCode: string;
  forgottenPasswordCode: string;
  forgottenPasswordTime: string;
  rememberCode: string;
  createdOn: number;
  last_login: number;
  active: number;
  firstName: string;
  lastName: string;
  company: string;
  phone: string;
  currentSession: string;
  blAction: boolean = false;
}

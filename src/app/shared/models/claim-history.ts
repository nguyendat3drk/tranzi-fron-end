import {BaseEntity} from './base-entity';

export class ClaimHistory extends BaseEntity {
  claimId: number;
  claimStatus: string;
  updatedBy: string;
  updatedDate: string;
}

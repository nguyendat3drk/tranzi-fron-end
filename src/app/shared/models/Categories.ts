export class Categories {
    id: number;
    name: string;
    slug: string;
    description: string;
    createdTime: number;
    iconUrl: string;
    bannerImage: string;
    bannerLink: string;
    order: number;
    parentId: number;
    seoTitle: string;
    seoKeywords: string;
    seoDescription: string;
    seoIndex: number;
    parentName: string;
    blSeoIndex: boolean;
    countProduct: number
    nameEn: string;
    type: number;
}

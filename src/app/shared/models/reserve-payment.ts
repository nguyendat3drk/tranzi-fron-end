import { BaseEntity } from './base-entity';
import { TypePayment } from './type-payment';

export class ReservePayment extends BaseEntity {
  claimId: number;
  claimScopeId: number;
  reserveAmount: number;
  typePayment: TypePayment;
  checksave: boolean = false;
  insuranceAmount: number;
}

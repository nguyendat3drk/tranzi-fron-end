export class Beneficiary {
    id: number;
    code: number;
    nameVN: string;
    nameEN: string;
    phoneNumber: string;
    address: string;
    email: string;
    identityCard: string;
    numberPassport: string;
    birthday: string;
    taxCode: string;
    accountNumber: string;
    bankName: string;
    branchName: string;
    commune: string;
    district: string;
    city: string;
    postCode: string
  }

import {BaseEntity} from './base-entity';
import {Branch} from './branch';

export class AdminBom extends BaseEntity {
  id: number;
  description: string;
  name: string;
  createdTime: number;
  updateTime: number;
  userId: number;
  userName: string ;
  listDetail: any [] = []
}

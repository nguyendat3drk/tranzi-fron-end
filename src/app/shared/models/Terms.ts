export class Terms {
    id: number;
    name: string;
    slug: string;
    description: string;
    bannerImage: string;
    bannerUrl: string;
    seoTitle: string;
    seoKeywords: string;
    seoDescription: string;
    seoIndex: number;
    blSeoIndex: boolean
    listCategory: number []=[];
    blSelectedRow:boolean;
    parentId:number;
}

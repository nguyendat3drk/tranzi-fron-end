export class GaraSearchParams {
  plateNumber: string = '';
  dateAssignWork: Date = null;
  dateRequestQuotation: Date = null;
  dateQuotation: Date = null;
  employeeName: string = '';
  dateSentInvoice: Date = null;
  datePaid: Date = null;
  format: string;
}

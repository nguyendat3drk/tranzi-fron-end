import {BaseEntity} from "./base-entity";

export class Guarantee extends BaseEntity {
    id: number;
    time: number;
	approvedAmount: number;
	totalSquarePart: number;
	totalLabout: number;
	agreeGuaranteeDate: Date;
	amountGuaranteedBaoviet: number;
	approverDate: Date;
	cancellerDate: Date;
	checkerDate: Date;
	deductible: number;
	depreciationRate: number;
	iscountAmount: number;
	discountRate: number;
	flag: string;
	flagApprove: string;
	flagCancellation: string;
	flagCheck: string;
	marketValue: number;
	paymentCustomer: number;
	status: number;
	sumInsurance: number;

	claimId: number;
	cateBeneficiaryId: number;
	cateBuId: number;
	cancellerId: number;
	approverId: number;
	agreeGuaranteeId: number;
	checkerId: number;
    garaName: string;
}
import { BaseEntity } from './base-entity';

export class ClaimCustomer extends BaseEntity {
    insuredName: string;
    policyInforPlateNumber: string;
    workSurveyorId: number;
    siteSurveyorId: number;
    claimOfficerId: number;
    zoneSiteSurveyorId: number;
    zoneWorkSurveyorId: number;
}

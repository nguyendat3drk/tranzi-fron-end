export class Customer {
  id: number;
  fullName: string;
  password: string = '';
  email: string = '';
  secretKey: string = '';
  createdAt: number;
  updatedAt: number;
  isEmailVerified: number;
  status: number;
  blSelectedRow: boolean;
}

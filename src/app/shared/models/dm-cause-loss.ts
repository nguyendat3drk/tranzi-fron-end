import {BaseEntity} from './base-entity';

export class DmCauseLoss extends BaseEntity {
  nameEN: string;
  nameVN: string;
}

export enum UserTypes {
  ADMIN = 1, // Admin
  TANDPTG = 2, // Tổng, phó tổng giám đốc công ty
  GD_GDX = 3, // Giám đóc,phó giám đóc giám định xe
  TP_GDX = 4, // Trưởng, phó phòng giám định xe
  TZ = 5, // Trưởng zone
  GDV = 6, // Giám định viên
  GDGDBT = 7, // Giám đốc,phó giám đốc giám định bồi thường
  TP_GDBT = 8, // Trưởng,phó phòng giám định bồi thường
  BTV = 9, // Bồi thường viên
  GARAGE = 10, // garage
  BL_BT = 11, // Bộ phận duyệt bảo lãnh và bồi thường 
}

import {Directive} from '@angular/core';
import {AbstractControl, ValidationErrors, Validator} from '@angular/forms';

@Directive({
  selector: '[appTime]'
})
export class TimeDirective implements Validator {

  constructor() {
  }

  registerOnValidatorChange(fn: () => void): void {
  }

  validate(c: AbstractControl): ValidationErrors | null {
    let isValid = false;
    const arrTime = c.value.split(':');
    const hour = parseInt(arrTime[0]);
    const minute = parseInt(arrTime[1]);
    const second = parseInt(arrTime[2]);
    if (hour >= 0 && hour <= 12 && minute >= 0 && minute <= 59 && second >= 0 && second <= 59) {
      isValid = true;
    }

    return isValid ? null : {'appTime': {value: c.value}};
  }
}

import {AbstractControl} from '@angular/forms';

export class EclaimValidators {
  static timeValidate = (c: AbstractControl) => {
    let isValid = false;
    if (!c.value) {
      return null;
    }
    const arrTime = c.value.split(':');
    // tslint:disable-next-line:radix
    const hour = parseInt(arrTime[0]);
    // tslint:disable-next-line:radix
    const minute = parseInt(arrTime[1]);
    // tslint:disable-next-line:radix
    const second = parseInt(arrTime[2]);
    if (hour >= 0 && hour <= 24 && minute >= 0 && minute <= 59 && second >= 0 && second <= 59) {
      isValid = true;
    }

    return isValid ? null : {'appTime': {value: c.value}};
  }
}

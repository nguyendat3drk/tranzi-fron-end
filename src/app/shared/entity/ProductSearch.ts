export class ProductSearch{
    page: number;
	pageSize: number;
	columnName: string;
	typeOrder: string;
	search: string;
	blSale: boolean;
	priceStart: number;
	priceEnd: number;
	listCategoriID: number[] = [];
	blInsufficient: boolean = false;
}
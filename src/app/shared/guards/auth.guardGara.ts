import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable()
export class AuthGuardGara implements CanActivate {
  constructor(private router: Router) {

  }
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    if (JSON.parse(localStorage.getItem('token'))) {
      // 1 = BV, 2 = Gara
      let typeLogin = 0;
      let ob = JSON.parse(localStorage.getItem('token'));
      typeLogin = (ob && ob.type) ? Number(ob.type) : 0;
      if (typeLogin !== 2) {
        localStorage.clear();
        this.router.navigate(['/auth/login']);
      }
      return true;
    } else {
      this.router.navigate(['/auth/login']);
    }
  }
}

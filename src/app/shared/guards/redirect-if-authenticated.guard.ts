import { Injectable } from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router} from '@angular/router';
import { Observable } from 'rxjs';
import {Oauth2Service} from '../services/http/oauth2.service';

@Injectable()
export class RedirectIfAuthenticatedGuard implements CanActivate {
  constructor(private router: Router) {

  }
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    if (JSON.parse(localStorage.getItem('token'))) {
      // TODO garage
      this.router.navigate(['/tranzi-admin']);
      return false;
    } else {
      return true;
    }
  }
}

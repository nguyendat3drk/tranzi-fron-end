import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
import { PageForbiddenComponent } from './components/page-forbidden/page-forbidden.component';
import { BvMenuService } from './services/bv-menu.service';
import { AppTranslationModule } from '../app.translation.module';
import { CalendarModule, ConfirmDialogModule, InputMaskModule } from 'primeng/primeng';
import { FormsModule, NG_VALIDATORS } from '@angular/forms';
import { GrowlModule } from 'primeng/growl';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { TimeDirective } from './directives/time.directive';

import { MessagesModule } from 'primeng/messages';
import { MessageModule } from 'primeng/message';
import { ProgressBarModule } from 'primeng/progressbar';
import { NgSelectModule } from '@ng-select/ng-select';

import { DataTableModule } from 'primeng/datatable';
import { ContextMenuModule } from 'primeng/contextmenu';
import { FormatDatePipe } from './pipes/format-date.pipe';
import { BvGaraComponent } from './components/bv-gara/bv-gara.component';
import { BvGaraDirective } from './components/bv-gara/bv-gara.directive';
import { BvGaraCreateComponent } from './components/bv-gara/bv-gara-create/bv-gara-create.component';

@NgModule({
  imports: [
    CommonModule,
    AppTranslationModule,
    FormsModule,
    InputMaskModule,
    GrowlModule,
    NgbModule,
    ConfirmDialogModule,
    MessageModule,
    MessagesModule,
    ProgressBarModule,
    NgSelectModule,
    CalendarModule,
    DataTableModule,
    ContextMenuModule,
  ],
  declarations: [
    PageNotFoundComponent,
    PageForbiddenComponent,
    TimeDirective,
    FormatDatePipe,
    BvGaraComponent,
    BvGaraDirective,
    BvGaraCreateComponent,
  ],
  exports: [
    PageNotFoundComponent,
    PageForbiddenComponent,
    InputMaskModule,
    GrowlModule,
    FormsModule,
    AppTranslationModule,
    NgbModule,
    ConfirmDialogModule,
    TimeDirective,
    CalendarModule,
    FormatDatePipe,
   
  ],
  providers: [
    BvMenuService,
    { provide: NG_VALIDATORS, useExisting: TimeDirective, multi: true },
  ],
})
export class SharedModule { }

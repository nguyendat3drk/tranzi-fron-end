export const ECLAIM_FLAG = {
	idFlag1: 1,
	nameFlag1: 'Bảo hiểm vật chất ô tô',
  idFlag2: 2,
  nameFlag2: 'Bảo hiểm bắt buộc TNDS chủ xe ô tô',
  idFlag3: 3,
  nameFlag3: 'Bảo hiểm tự nguyện TNDS chủ xe ô tô',
  idFlag4: 4,
  nameFlag4: 'Bảo hiểm tai nạn lái xe và người trên xe',
  idFlag5: 5,
  nameFlag5: 'Bảo hiểm TNDS chủ xe đối với hàng hóa trên xe'
}
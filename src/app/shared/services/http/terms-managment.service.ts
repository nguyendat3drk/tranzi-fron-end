import { Injectable } from '@angular/core';
import { BaseHttpService } from './base-http.service';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from '../../../../environments/environment';
import { Terms } from '../../models/Terms';

@Injectable()
export class TermsManagmentService extends BaseHttpService<Terms> {
    protected base = 'terms';
    constructor(protected http: HttpClient) {
        super(http);
    }
    getAlls() {
        return this.http.get(`${environment.endpoint}/terms/getAlls`).map((response: Response) => response);
    }
    
    getAllRespont() {
        return this.http.get(`${environment.endpoint}/terms/getAllRespont`).map((response: Response) => response);
    }
    getAllsPadding(page: number, pageSize: number, columnName: string, typeOrder: string, search: string) {
        const url = `${environment.endpoint}/terms/pagerAllTerms`
            + '?page=' + page + '&pageSize=' + pageSize +
             '&columnName=' + columnName + '&typeOrder=' + typeOrder + '&search=' + search
        return this.http.get(url).map((response: Response) => response);
    }
    
    getTermsID(id) {
        return this.http.get(`${environment.endpoint}/terms/getTermsId/${id}`).map((response: Response) => response);
    }
    create(data) {
        return this.http.post(`${environment.endpoint}/terms/create`, data).map((response: Response) => response);
    }
    update(Terms) {
        return this.http.put(`${environment.endpoint}/terms/update`, Terms).map((response: Response) => response);
    }
    deleteId(id) {
        return this.http.delete(`${environment.endpoint}/terms/delete/${id}`).map((response: Response) => response);
    }
}


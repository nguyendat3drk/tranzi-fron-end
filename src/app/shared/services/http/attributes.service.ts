import { Injectable } from '@angular/core';
import { BaseEntity } from '../../models/base-entity';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs/Rx';
import { DmContact } from '../../models/dm-contact';
import { environment } from '../../../../environments/environment';
import { Subject } from 'rxjs/Subject';
import { Attributes } from '../../models/Attributes';
import { BaseHttpService } from './base-http.service';

@Injectable()
export class AttributesService extends BaseHttpService<Attributes> {
    protected base = 'attributes';
    constructor(protected http: HttpClient) {
        super(http);
    }
    getAll() {
        return this.http.get(`${environment.endpoint}/attributes/getAlls`).map((response: Response) => response);
    }
    getAllsPadding(page: number, pageSize: number, columnName: string, typeOrder: string, search: string) {
        const url = `${environment.endpoint}/attributes/pagerAllAttributes`
            + '?page=' + page + '&pageSize=' + pageSize +
            '&columnName=' + columnName + '&typeOrder=' + typeOrder + '&search=' + search
        return this.http.get(url).map((response: Response) => response);
    }
    getAttributeBuCategory(id){
        return this.http.get(`${environment.endpoint}/attributes/getAttributeByCategory/${id}`).map((response: Response) => response);
    }
    updateOrderAttributes(data) {
        return this.http.put(`${environment.endpoint}/attributes/updateOrder`, data).map((response: Response) => response);
    }
    deleteAttributeId(id) {
        return this.http.delete(`${environment.endpoint}/attributes/delete/${id}`).map((response: Response) => response);
    }
    create(attributes) {
        if (attributes && (attributes.id) > 0) {
            return this.http.put(`${environment.endpoint}/attributes/update`, attributes).map((response: Response) => response);
        } else {
            return this.http.post(`${environment.endpoint}/attributes/create`, attributes).map((response: Response) => response);
        }
    }
    update(attributes) {
        return this.http.put(`${environment.endpoint}/attributes/update`, attributes).map((response: Response) => response);
    }
    //
    getListCategoryByAttribute(id) {
        return this.http.get(`${environment.endpoint}/attributes/getListCategoryByAttribute/${id}`).map((response: Response) => response);
    }
    // AttributeValues
    getAllsPaddingValuesByAttributeId(page: number, pageSize: number, columnName: string, typeOrder: string, search: string, id: number) {
        const url = `${environment.endpoint}/attributevalues/pagerAllAttributesByID`
            + '?page=' + page + '&pageSize=' + pageSize +
            '&columnName=' + columnName + '&typeOrder=' + typeOrder + '&search=' + search + '&id=' + id
        return this.http.get(url).map((response: Response) => response);
    }
    getAllsPaddingValues(page: number, pageSize: number, columnName: string, typeOrder: string, search: string) {
        const url = `${environment.endpoint}/attributevalues/pagerAllAttributes`
            + '?page=' + page + '&pageSize=' + pageSize +
            '&columnName=' + columnName + '&typeOrder=' + typeOrder + '&search=' + search
        return this.http.get(url).map((response: Response) => response);
    }
    deleteAttributeValuesId(id) {
        return this.http.delete(`${environment.endpoint}/attributevalues/delete/${id}`).map((response: Response) => response);
    }
    createValues(attributes) {
        if (attributes && (attributes.id) > 0) {
            return this.http.put(`${environment.endpoint}/attributevalues/update`, attributes).map((response: Response) => response);
        } else {
            return this.http.post(`${environment.endpoint}/attributevalues/create`, attributes).map((response: Response) => response);
        }
    }
    updateValues(attributes) {
        return this.http.put(`${environment.endpoint}/attributevalues/update`, attributes).map((response: Response) => response);
    }

    // get list id AttributeValue by id product
    getListAttributeValueID(id) {
        return this.http.get(`${environment.endpoint}/product/getListIDAttributeValue/${id}`).map((response: Response) => response);
    }
}

import {Injectable} from '@angular/core';
import {environment} from '../../../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {Address} from '../../models/address';
import {Observable} from 'rxjs/Rx';

@Injectable()
export class AddressService {

  constructor(private http: HttpClient) {
  }

  search(query: string): Observable<Address[]> {
    return this.http.get<Address[]>(`${environment.endpoint}/address/search?searchKey=${encodeURI(query)}`);
  }

  searchAddress(query: string): Observable<Address[]> {
    const address = query.replace(/,/g, ', ');
    return this.http.get<Address[]>(`${environment.endpoint}/address/search?searchKey=${encodeURI(address)}`);
  }
}

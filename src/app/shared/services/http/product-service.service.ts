import { Injectable } from '@angular/core';
import { BaseHttpService } from './base-http.service';
import { HttpClient } from '@angular/common/http';
import { Claim } from '../../models/claim';
import { environment } from '../../../../environments/environment';
import { catchError, map } from 'rxjs/internal/operators';
import { Observable } from 'rxjs/Rx';
import { Page } from '../../models/page';
import { ProductsPrices } from '../../models/ProductsPrices';

@Injectable()
export class ProductPriceService extends BaseHttpService<ProductsPrices> {

    protected base = 'productprice';

    constructor(protected http: HttpClient) {
        super(http);
    }
    getProductPriceByProductID(id: number): Observable<any> {
        // tslint:disable-next-line:max-line-length
        const url = `${environment.endpoint}/${this.base}`;
        return this.http.get(url + '/getAllProduct/' + id).map((response: Response) => response);
    }
    deleteProductId(id: number): Observable<any> {
        // tslint:disable-next-line:max-line-length
        const url = `${environment.endpoint}/${this.base}`;
        return this.http.delete(url + '/deleteByProductID/' + id).map((response: Response) => response);
    }
    deleteListProductId(listId): Observable<any> {
        // tslint:disable-next-line:max-line-length
        const url = `${environment.endpoint}/product`;
        return this.http.post(url + '/deleteListID/', listId).map((response: Response) => response);
    }
    addListProductPrice(list): Observable<any> {
        // tslint:disable-next-line:max-line-length
        const url = `${environment.endpoint}/${this.base}`;
        return this.http.post(url + '/createList' ,list).map((response: Response) => response);
    }
    
}

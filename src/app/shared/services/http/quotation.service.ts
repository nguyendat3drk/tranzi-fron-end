import { Injectable } from '@angular/core';
import { BaseHttpService } from './base-http.service';
import { HttpClient } from '@angular/common/http';
import { Claim } from '../../models/claim';
import { environment } from '../../../../environments/environment';
import { catchError, map } from 'rxjs/internal/operators';
import { Observable } from 'rxjs/Rx';
import { Page } from '../../models/page';

@Injectable()
export class QuotationService extends BaseHttpService<any> {

    protected base = 'quotation';

    constructor(protected http: HttpClient) {
        super(http);
    }
    getQuotationByClaimId(claimId: number): Observable<any> {
        // tslint:disable-next-line:max-line-length
        const url = `${environment.endpoint}/${this.base}`;
        return this.http.get(url + '/getQuotationByBaoViet/' + claimId).map((response: Response) => response);
    }
    getQuotationGaraByClaimId(claimId: number): Observable<any> {
        // tslint:disable-next-line:max-line-length
        const url = `${environment.endpoint}/${this.base}`;
        return this.http.get(url + '/getQuotationByGara/' + claimId).map((response: Response) => response);
    }
    setComment(event): Observable<any> {
        const url = `${environment.endpoint}/${this.base}`;
        return this.http.post(url + '/createQuotationDetail/comment', event).map((response: Response) => response);
    }
    setComments(event): Observable<any> {
        const url = `${environment.endpoint}/${this.base}`;
        return this.http.post(url + '/createQuotationDetail/comments', event).map((response: Response) => response);
    }
    setHistoryExchange(event): Observable<any> {
        const url = `${environment.endpoint}/${this.base}`;
        return this.http.post(url + '/updateApprove', event).map((response: Response) => response);
    }
    updateQuotation(event): Observable<any> {
        const url = `${environment.endpoint}/${this.base}`;
        return this.http.put(url + '/update', event).map((response: Response) => response);
    }
    garaRefuseRepair(claimId): Observable<any> {
        const url = `${environment.endpoint}/${this.base}`;
        return this.http.put(url + '/changeStatus?claimId=' + claimId + '&status=1', {}).map((response: Response) => response);
    }
    garaAdditionalQuotation(claimId, data): Observable<any> {
        const url = `${environment.endpoint}/${this.base}`;
        return this.http.post(url + '/garaAdditionalQuotation/' + claimId, data).map((response: Response) => response);
    }
    getJobList(id: number): Observable<any> {
        // tslint:disable-next-line:max-line-length
        const url = `${environment.endpoint}/${this.base}`;
        return this.http.get(url + '/loadCoreQuotionDetail/' + id).map((response: Response) => response);
    }
    getQuotationForNew(id: number): Observable<any> {
        const url = `${environment.endpoint}/${this.base}`;
        return this.http.get(url + '/loadCoreQuotionDetail/' + id).map((response: Response) => response);
    }
    createQuotation(event): Observable<any> {
        const url = `${environment.endpoint}/${this.base}`;
        return this.http.post(url + '/create', event).map((response: Response) => response);
    }
    searchAccessary(text: string): Observable<any> {
        const url = `${environment.endpoint}`;
        return this.http.get(url + '/accessary/list?searchKey=' + encodeURI(text)).map((response: Response) => response);
    }
}

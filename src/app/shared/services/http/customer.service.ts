import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs/Rx';
import { environment } from '../../../../environments/environment';
import { Subject } from 'rxjs/Subject';
import { Customer } from '../../models/Customer';
import { BaseHttpService } from './base-http.service';

@Injectable()
export class CustomerService extends BaseHttpService<Customer> {
    protected base = 'customers';
    constructor(protected http: HttpClient) {
        super(http);
    }
    getAll() {
        return this.http.get(`${environment.endpoint}/customers/getAlls`).map((response: Response) => response);
    }
    getAllsPadding(page: number, pageSize: number, columnName: string, typeOrder: string, search: string) {
        const url = `${environment.endpoint}/customers/pagerAllCustomer`
            + '?page=' + page + '&pageSize=' + pageSize +
            '&columnName=' + columnName + '&typeOrder=' + typeOrder + '&search=' + search
        return this.http.get(url).map((response: Response) => response);
    }
    deleteAttributeId(id) {
        return this.http.delete(`${environment.endpoint}/customers/delete/${id}`).map((response: Response) => response);
    }
    
    getCustomerNameId(id) {
        return this.http.get(`${environment.endpoint}/customers/getCustomerNameId/${id}`).map((response: Response) => response);
    }
    create(customers:Customer) {
        if (customers && (customers.id) > 0) {
            return this.http.put(`${environment.endpoint}/customers/update`, customers).map((response: Response) => response);
        } else {
            return this.http.post(`${environment.endpoint}/customers/create`, customers).map((response: Response) => response);
        }
    }
    update(customers) {
        return this.http.put(`${environment.endpoint}/customers/update`, customers).map((response: Response) => response);
    }
    // AttributeValues
    getAllsPaddingValuesByAttributeId(page: number, pageSize: number, columnName: string, typeOrder: string, search: string, id: number) {
        const url = `${environment.endpoint}/attributevalues/pagerAllAttributesByID`
            + '?page=' + page + '&pageSize=' + pageSize +
            '&columnName=' + columnName + '&typeOrder=' + typeOrder + '&search=' + search + '&id=' + id
        return this.http.get(url).map((response: Response) => response);
    }
    getAllsPaddingValues(page: number, pageSize: number, columnName: string, typeOrder: string, search: string) {
        const url = `${environment.endpoint}/attributevalues/pagerAllAttributes`
            + '?page=' + page + '&pageSize=' + pageSize +
            '&columnName=' + columnName + '&typeOrder=' + typeOrder + '&search=' + search
        return this.http.get(url).map((response: Response) => response);
    }
    deleteAttributeValuesId(id) {
        return this.http.delete(`${environment.endpoint}/attributevalues/delete/${id}`).map((response: Response) => response);
    }
    createValues(customers) {
        if (customers && (customers.id) > 0) {
            return this.http.put(`${environment.endpoint}/attributevalues/update`, customers).map((response: Response) => response);
        } else {
            return this.http.post(`${environment.endpoint}/attributevalues/create`, customers).map((response: Response) => response);
        }
    }
    updateValues(customers) {
        return this.http.put(`${environment.endpoint}/attributevalues/update`, customers).map((response: Response) => response);
    }

    // get list id AttributeValue by id product
    getListAttributeValueID(id) {
        return this.http.get(`${environment.endpoint}/product/getListIDAttributeValue/${id}`).map((response: Response) => response);
    }
    // Trang Chu
    countCustomer() {
        return this.http.get(`${environment.endpoint}/customers/countCustomer`).map((response: Response) => response);
    }
    // export file
    exxportOrderCustomer(id) {
        return this.http.get(`${environment.endpoint}/customerorder/exportOrder/${id}`).map((response: Response) => response);
      }
}

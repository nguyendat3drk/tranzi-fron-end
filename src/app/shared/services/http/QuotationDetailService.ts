import { Injectable } from '@angular/core';
import { BaseHttpService } from './base-http.service';
import { HttpClient } from '@angular/common/http';
import { Claim } from '../../models/claim';
import { environment } from '../../../../environments/environment';
import { catchError, map } from 'rxjs/internal/operators';
import { Observable } from 'rxjs/Rx';
import { Page } from '../../models/page';

@Injectable()
export class QuotationDetailService extends BaseHttpService<any> {

    protected base = 'quotation';

    constructor(protected http: HttpClient) {
        super(http);
    }
    getQuotationById(id: number): Observable<any> {
        // tslint:disable-next-line:max-line-length
        const url = `${environment.endpoint}/${this.base}`;
        return this.http.get(url + '/getQuotionDetail/' + id).map((response: Response) => response);
    }
    getAuthQuotationDTOById(id: number): Observable<any> {
        const url = `${environment.endpoint}`;
        return this.http.get(url + '/auth/bussiness/quotation?quotationId=' + id).map((response: Response) => response);
    }


}

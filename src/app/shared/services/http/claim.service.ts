import { Injectable } from '@angular/core';
import { BaseHttpService } from './base-http.service';
import { HttpClient } from '@angular/common/http';
import { Claim } from '../../models/claim';
import { environment } from '../../../../environments/environment';
import { catchError, map } from 'rxjs/internal/operators';
import { Observable } from 'rxjs/Rx';
import { Page } from '../../models/page';

@Injectable()
export class ClaimService extends BaseHttpService<Claim> {

  protected base = 'claim';

  constructor(protected http: HttpClient) {
    super(http);
  }

  private toInternalClaim = (claim: any): Claim => {
    const internalClaim = new Claim();
    // tslint:disable-next-line:prefer-const
    for (let property in claim) {
      if (property === 'flag1' || property === 'flag2' || property === 'flag3'
        || property === 'flag4' || property === 'flag5' || property === 'flagApplicable') {
        internalClaim[property] = claim[property] === '1';
      } else {
        internalClaim[property] = claim[property];
      }
    }
    return internalClaim;
  };
  private toExternalClaim = (claim: Claim): any => {
    const data: any = {};
    for (const item in claim) {
      if (claim[item] === true) {
        // console.log(item);
        data[item] = '1';
      } else if (claim[item] === false) {
        data[item] = '0';
      } else {
        data[item] = claim[item];
      }
    }

    return data;
  }

  create(claim: Claim): Observable<any> {
    const data = this.toExternalClaim(claim);
    // console.log(data);
    // return this.http.post(`${environment.endpoint}/${this.base}/create`, data).pipe(catchError(this.handleError));
    return this.http.post(`${environment.endpoint}/${this.base}/create`, data).map((response: Response) => response);
  }

  updateClaim(claim: Claim): Observable<any> {
    const data = this.toExternalClaim(claim);
    return this.http.put(`${environment.endpoint}/${this.base}/update`, data).map((response: Response) => response);
  }

  load(id: number): Observable<Claim> {
    return this.http.get<any>(`${environment.endpoint}/${this.base}/load/${id}`)
      .pipe(
      map(this.toInternalClaim),
      );
  }

  getDetail(id: number): Observable<Claim> {
    return super.getDetail(id)
      .pipe(
      map(this.toInternalClaim),
      );
  }

  getList(page = 0, size = 20, params?: Object): Observable<Page<Claim>> {
    return super.getList(page, size, params).pipe(
      map(x => {
        x.contents.map(this.toInternalClaim);
        return x;
      }),
    );
  }



  update(claim: Claim): Observable<any> {
    const data = this.toExternalClaim(claim);
    return this.http.put(`${environment.endpoint}/${this.base}/update`, data)
      .pipe(catchError(this.handleError));
  }

  searchBv(listSearchBv): Observable<any> {
    return this.http.post(`${environment.endpoint}/${this.base}/search`, listSearchBv).map((response: Response) => response);
  }
  getClaimCustomer(id: number) {
    return this.http.get(`${environment.endpoint}/${this.base}/getClaimCustomer/${id}`).map((response: Response) => response);
  }
  getlimitPriceRole() {
    return this.http.get(`${environment.endpoint}/account/getLimitation`).map((response: Response) => response);
  }
  getClaimInfoZoneSite(id: number) {
    return this.http.get(`${environment.endpoint}/${this.base}/findClaimInForByIdZone/${id}`).map((response: Response) => response);
  }
}

import { Injectable } from '@angular/core';
import { BaseHttpService } from './base-http.service';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from '../../../../environments/environment';
import { catchError, map } from 'rxjs/internal/operators';
import { Observable } from 'rxjs/Rx';
import { Accessary } from '../../models/Accessary';
import { Page } from '../../models/page';
import {DmContact} from '../../models/dm-contact';


@Injectable()
export class AccessaryService extends BaseHttpService<Accessary> {

    protected base = 'accessary';

    constructor(protected http: HttpClient) {
        super(http);
    }
    searchAccessary(text: string): Observable<any> {
        const url = `${environment.endpoint}/${this.base}`;
        return this.http.get(url + '/list?searchKey=' + encodeURI(text)).map((response: Response) => response);
    }
}


import { Injectable } from '@angular/core';
import { BaseHttpService } from './base-http.service';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from '../../../../environments/environment';
import { Manufactures } from '../../models/Manufactures';

@Injectable()
export class ManufactService extends BaseHttpService<Manufactures> {
    protected base = 'manufactures';
    constructor(protected http: HttpClient) {
        super(http);
    }
    getAlls() {
        return this.http.get(`${environment.endpoint}/manufactures/getAlls`).map((response: Response) => response);
    }
    getAllsPadding(page: number, pageSize: number, columnName: string, typeOrder: string, search: string) {
        const url = `${environment.endpoint}/manufactures/pagerAllManufactures`
            + '?page=' + page + '&pageSize=' + pageSize +
            '&columnName=' + columnName + '&typeOrder=' + typeOrder + '&search=' + search
        return this.http.get(url).map((response: Response) => response);
    }
    getManufacturesID(id) {
        return this.http.get(`${environment.endpoint}/manufactures/getManufacturesId/${id}`).map((response: Response) => response);
    }
    create(manufactures) {
        return this.http.post(`${environment.endpoint}/manufactures/create`, manufactures).map((response: Response) => response);
    }
    update(manufactures) {
        return this.http.put(`${environment.endpoint}/manufactures/update`, manufactures).map((response: Response) => response);
    }
    deleteManufacturesId(id) {
        return this.http.delete(`${environment.endpoint}/manufactures/delete/${id}`).map((response: Response) => response);
    }
}


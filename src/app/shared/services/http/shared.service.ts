import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../../environments/environment';
import { DmCauseLoss } from '../../models/dm-cause-loss';
import { Observable } from '../../../../../node_modules/rxjs/Rx';
import { catchError } from 'rxjs/operators';
import { of } from 'rxjs/internal/observable/of';

@Injectable()
export class SharedService {
  constructor(protected http: HttpClient) {
  }

  getAllBusinessUnit(): Observable<DmCauseLoss[]> {
    return this.http.get<DmCauseLoss[]>(`${environment.endpoint}/branch/list`).pipe(
      catchError(() => of([])),
    )
  }
}

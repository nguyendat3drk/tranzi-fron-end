import { Injectable } from '@angular/core';
import { BaseHttpService } from './base-http.service';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../../environments/environment';
import { Setting } from '../../models/Setting';
@Injectable()
export class SettingService extends BaseHttpService<Setting> {
    protected base = 'setting';
    constructor(protected http: HttpClient) {
        super(http);
    }
    getAlls() {
        return this.http.get(`${environment.endpoint}/setting/getAlls`).map((response: Response) => response);
    }
    getAllsPadding(page: number, pageSize: number, columnName: string, typeOrder: string, search: string) {
        const url = `${environment.endpoint}/setting/pagerAllSetting`
            + '?page=' + page + '&pageSize=' + pageSize +
            '&columnName=' + columnName + '&typeOrder=' + typeOrder + '&search=' + search;
        return this.http.get(url).map((response: Response) => response);
    }
    deletebyiId(id){
        return this.http.delete(`${environment.endpoint}/setting/delete/${id}`).map((response: Response) => response);
    }
    create(data){
        return this.http.post(`${environment.endpoint}/setting/add/`,data).map((response: Response) => response);
    }
    update(data){
        return this.http.put(`${environment.endpoint}/setting/update/`,data).map((response: Response) => response);
    }
    
    getProductID(id){
        return this.http.get(`${environment.endpoint}/setting/getById/${id}`).map((response: Response) => response);
    }
}

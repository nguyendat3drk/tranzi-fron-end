
import { Injectable } from '@angular/core';
import { BaseHttpService } from './base-http.service';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from '../../../../environments/environment';
import { Inventory } from '../../models/Inventory';
import { InventoryOrder } from '../../models/InventoryOrder';

@Injectable()
export class InventoryOrderService extends BaseHttpService<InventoryOrder> {
    protected base = 'inventoryorder';
    constructor(protected http: HttpClient) {
        super(http);
    }
    getAlls() {
        return this.http.get(`${environment.endpoint}/inventoryorder/getAlls`).map((response: Response) => response);
    }
    create(inventory) {
        return this.http.post(`${environment.endpoint}/inventoryorder/create`, inventory).map((response: Response) => response);
    }
    update(inventory) {
        return this.http.put(`${environment.endpoint}/inventoryorder/update`, inventory).map((response: Response) => response);
    }
    deleteManufacturesId(id) {
        return this.http.delete(`${environment.endpoint}/inventoryorder/delete/${id}`).map((response: Response) => response);
    }
}


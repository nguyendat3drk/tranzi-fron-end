import { Injectable } from '@angular/core';
import { AdminBom } from '../../models/AdminBom';
import { BaseHttpService } from './base-http.service';
import { HttpClient } from '@angular/common/http';
import {environment} from '../../../../environments/environment';

@Injectable()
export class AdminBomService extends BaseHttpService<AdminBom> {
  protected base = 'adminbom';
  constructor(protected http: HttpClient) {
      super(http);
  }
  
  
  getAllsPadding(page: number, pageSize: number, columnName: string, typeOrder: string, search: string) {
      const url = `${environment.endpoint}/adminbom/pagerAllAdminBom`
          + '?page=' + page + '&pageSize=' + pageSize +
           '&columnName=' + columnName + '&typeOrder=' + typeOrder + '&search=' + search
      return this.http.get(url).map((response: Response) => response);
  }
  getAdminBomID(id) {
      return this.http.get(`${environment.endpoint}/adminbom/getAdminBomId/${id}`).map((response: Response) => response);
  }
  create(adminbom) {
      return this.http.post(`${environment.endpoint}/adminbom/create`, adminbom).map((response: Response) => response);
  }
  update(adminbom) {
      return this.http.put(`${environment.endpoint}/adminbom/update`, adminbom).map((response: Response) => response);
  }
  delete(id) {
      return this.http.delete(`${environment.endpoint}/adminbom/delete/${id}`).map((response: Response) => response);
  }
  
}


import { Injectable } from '@angular/core';
import { BaseHttpService } from './base-http.service';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../../environments/environment';
import { Observable } from 'rxjs/Rx';
import { catchError } from 'rxjs/internal/operators';
import { Page } from '../../models/page';
import { Account } from '../../models/account';

@Injectable()
export class AccountService extends BaseHttpService<Account> {
  protected base = 'account';
  constructor(protected http: HttpClient) {
    super(http);
  }

  getAllsPadding(page: number, pageSize: number, columnName: string, typeOrder: string, search: string) {
    const url = `${environment.endpoint}/account/pagerAllAccount`
      + '?page=' + page + '&pageSize=' + pageSize +
      '&columnName=' + columnName + '&typeOrder=' + typeOrder + '&search=' + search
    return this.http.get(url).map((response: Response) => response);
  }
  create(account: Account): Observable<any> {
    if (account) {
      if (account.blAction) {
        account.active = 1
      } else {
        account.active = 0
      }
      if (account && (!account.id || account.id === 0)) {
        const url = `${environment.endpoint}/account/add`
        return this.http.post(url, account).map((response: Response) => response);
      } else {
        const url = `${environment.endpoint}/account/update`
        return this.http.put(url, account).map((response: Response) => response);
      }
    }
    return null
  }
  deleteAccountById(id: number) {
    if (id) {
      const url = `${environment.endpoint}/account/delete/${id}`
      return this.http.delete(url).map((response: Response) => response);
    }
    return null;
  }

  changePassword(userId: number, oldPassword: string, newPassword: string): Observable<any> {
    return this.http.post(`${environment.endpoint}/${this.base}/changePassword`, {
      userId: userId,
      oldPassword: oldPassword,
      newPassword: newPassword,
    })
  }

  getList(page = 0, size = 20, params: {
    type: number,
    searchKey?: string,
    active?: number,
  }): Observable<Page<Account>> {
    return super.getList(page, size, params);
  }

  update(account: Account): Observable<any> {
    return this.http.put(`${environment.endpoint}/${this.base}/update`, account)
      .pipe(catchError(this.handleError));
  }

  forgotPassword() {
    // TODO
  }

  resetPassword(newPassword: string, token: string) {
    return this.http.put(`${environment.endpoint}/${this.base}/resetPassword`, {
      newPassword: newPassword,
      tokenSecret: token,
    }).pipe(catchError(this.handleError));
  }
  getlimitPriceRole() {
    return this.http.get(`${environment.endpoint}/account/getLimitation`).map((response: Response) => response);
  }
  runUpdateAccout() {
    return this.http.put(`${environment.endpoint}/account/bacthInsertAccount`, null).map((response: Response) => response);
  }
}


import { Injectable } from '@angular/core';
import { BaseHttpService } from './base-http.service';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from '../../../../environments/environment';
import { Articles } from '../../models/articles';

@Injectable()
export class ArticlesService extends BaseHttpService<Articles> {
    protected base = 'articles';
    constructor(protected http: HttpClient) {
        super(http);
    }
    getAlls() {
        return this.http.get(`${environment.endpoint}/articles/getAlls`).map((response: Response) => response);
    }
    
    getAllRespont() {
        return this.http.get(`${environment.endpoint}/articles/getAllRespont`).map((response: Response) => response);
    }
    getAllsPadding(page: number, pageSize: number, columnName: string, typeOrder: string, search: string) {
        const url = `${environment.endpoint}/articles/pagerAllArticles`
            + '?page=' + page + '&pageSize=' + pageSize +
             '&columnName=' + columnName + '&typeOrder=' + typeOrder + '&search=' + search
        return this.http.get(url).map((response: Response) => response);
    }
    
    getvideosID(id) {
        return this.http.get(`${environment.endpoint}/articles/getArticlesId/${id}`).map((response: Response) => response);
    }
    create(data) {
        return this.http.post(`${environment.endpoint}/articles/create`, data).map((response: Response) => response);
    }
    update(videos) {
        return this.http.put(`${environment.endpoint}/articles/update`, videos).map((response: Response) => response);
    }
    deleteId(id) {
        return this.http.delete(`${environment.endpoint}/articles/delete/${id}`).map((response: Response) => response);
        // return this.http.delete(`${environment.endpoint}/videos/deleteALL`).map((response: Response) => response);
    }
}


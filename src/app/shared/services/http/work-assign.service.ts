import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../../environments/environment';

@Injectable()
export class WorkAssignService {

  urlApi: any;
  assignWorkUrlApi: any;

  constructor(private http: HttpClient) {
    this.urlApi = environment.endpoint;
    this.assignWorkUrlApi = this.urlApi.concat('/work');
  }

  searchSurveyor(searchRequest) {
    return new Promise((resolve, reject) => {
      this.http.post(this.assignWorkUrlApi.concat('/seachSurveyor'), searchRequest).subscribe(
        res => {
          if (res) {
            resolve(res);
          }
        },
        (err) => {
          reject(err);
        })
    });
  };

  assignSurveyor(surveyor) {
    return new Promise((resolve, reject) => {
      this.http.post(this.assignWorkUrlApi.concat('/assignSurveyor'), surveyor).subscribe(
        res => {
          if (res) {
            resolve(res);
          }
        },
        (err) => {
          reject(err);
        })
    });
  };

  removeAssignSurveyor(surveyor) {
    return new Promise((resolve, reject) => {
      this.http.post(this.assignWorkUrlApi.concat('/removeAssignSurveyor'), surveyor).subscribe(
        res => {
          if (res) {
            resolve(res);
          }
        },
        (err) => {
          reject(err);
        })
    });
  };

  getIncidentInfo(claimId) {
    return new Promise((resolve, reject) => {
      this.http.get(this.urlApi.concat('/claim/getIncidentInfo?id=', claimId)).subscribe(
        res => {
          if (res) {
            resolve(res);
          }
        },
        (err) => {
          reject(err);
        })
    })
  }

}

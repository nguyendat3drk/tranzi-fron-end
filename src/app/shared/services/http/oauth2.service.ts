import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs/Rx';
import { TokenResponse } from '../../models/token-response';
import { environment } from '../../../../environments/environment';
import { Router } from '@angular/router';
import { AuthenticationUser } from '../../loginDTO/AuthenticationUser';
declare var baseAuth: any;

@Injectable()
export class Oauth2Service {

  token: BehaviorSubject<TokenResponse> = new BehaviorSubject(null);
  tokenAuth: BehaviorSubject<AuthenticationUser> = new BehaviorSubject(null);
  constructor(
    private http: HttpClient,
    private router: Router) {
    this.loadFromLocalStorage();
    // this.loadAuthLocalStorage();
  }

  getToken(username: string, password: string): Observable<TokenResponse> {
    let headers = new HttpHeaders();
    headers = headers.append('Authorization', 'Basic ' + btoa(`${environment.basicAuth.username}:${environment.basicAuth.password}`));
    headers = headers.append('Content-Type', 'application/x-www-form-urlencoded');
    const params = new HttpParams().set('username', username).set('password', password).set('grant_type', 'password');
    return this.http.post<TokenResponse>(`${environment.endpoint}/oauth/token`, {},
      { headers: headers, params: params });
  }
  login(data): Observable<any> {
    return this.http.post(`${environment.endpoint}/share-api/login`, data).map((response: Response) => response);
  }

  authentication(data): Observable<any> {
    return this.http.post(`${environment.endpoint}/authentication/login`, data).map((response: Response) => response);
  }

  saveToLocalStorage(token: TokenResponse) {
    localStorage.setItem('token', JSON.stringify(token));
    baseAuth = token;
    this.token.next(token);
  }
  saveAuthToLocalStorage(token: AuthenticationUser) {
    localStorage.removeItem('token')
    localStorage.setItem('token', JSON.stringify(token));
    this.tokenAuth.next(token);
  }

  loadFromLocalStorage() {
    const token = JSON.parse(localStorage.getItem('token'));
    if (token) {
      this.token.next(token);
    }
  }

  loadAuthLocalStorage() {
    const token = JSON.parse(localStorage.getItem('token'));
    if (token) {
      this.tokenAuth.next(token);
    }
  }

  logout() {
    localStorage.removeItem('token');
    this.router.navigate(['/auth/login']);
  }
}

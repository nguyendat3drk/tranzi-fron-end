import {Injectable} from '@angular/core';
import {Incident} from '../../models/incident';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../environments/environment';
import {Observable} from 'rxjs/Rx';

@Injectable()
export class DmIncidentService {

  protected base = 'incident';

  constructor(private http: HttpClient) {
  }

  getList(): Observable<Incident[]> {
    return this.http.get<Incident[]>(`${environment.endpoint}/${this.base}/list`);
  }

  getDetail() {
    return this.http.get<Incident>(`${environment.endpoint}/${this.base}/detail`);
  }
}


import { Injectable } from '@angular/core';
import { BaseHttpService } from './base-http.service';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from '../../../../environments/environment';
import { Inventory } from '../../models/Inventory';

@Injectable()
export class InventoryService extends BaseHttpService<Inventory> {
    protected base = 'inventory';
    constructor(protected http: HttpClient) {
        super(http);
    }
    getAlls() {
        return this.http.get(`${environment.endpoint}/inventory/getAlls`).map((response: Response) => response);
    }
    getAllsPadding(page: number, pageSize: number, columnName: string, typeOrder: string, search: string) {
        const url = `${environment.endpoint}/inventory/pagerAllInventory`
            + '?page=' + page + '&pageSize=' + pageSize +
            '&columnName=' + columnName + '&typeOrder=' + typeOrder + '&search=' + search
        return this.http.get(url).map((response: Response) => response);
    }
    getAllsPaddingDetail(page: number, pageSize: number, columnName: string, typeOrder: string, search: string, idInventory) {
        const url = `${environment.endpoint}/inventoryorder/pagerAllInventoryOrder`
            + '?page=' + page + '&pageSize=' + pageSize +
            '&columnName=' + columnName + '&typeOrder=' + typeOrder + '&search=' + search + '&idInventory=' + idInventory
        return this.http.get(url).map((response: Response) => response);
    }
    getManufacturesID(id) {
        return this.http.get(`${environment.endpoint}/inventory/getInventoryId/${id}`).map((response: Response) => response);
    }
    create(inventory) {
        return this.http.post(`${environment.endpoint}/inventory/create`, inventory).map((response: Response) => response);
    }
    update(inventory) {
        return this.http.put(`${environment.endpoint}/inventory/update`, inventory).map((response: Response) => response);
    }
    deleteId(id) {
        return this.http.delete(`${environment.endpoint}/inventory/delete/${id}`).map((response: Response) => response);
    }
    createListInventory(list) {
        return this.http.post(`${environment.endpoint}/inventory/createList`, list).map((response: Response) => response);
    }
}


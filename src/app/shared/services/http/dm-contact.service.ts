import { Injectable } from '@angular/core';
import { BaseHttpService } from './base-http.service';
import { DmContact } from '../../models/dm-contact';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../../environments/environment';
import { catchError } from 'rxjs/internal/operators';

@Injectable()
export class DmContactService extends BaseHttpService<DmContact> {

  protected base = 'contact';
  constructor(protected http: HttpClient) {
    super(http);
  }

  create(contact: DmContact) {
    this.http.post(`${environment.endpoint}/${this.base}/create`, contact)
      .pipe(catchError(this.handleError));
  }

  update(contact: DmContact) {
    this.http.put(`${environment.endpoint}/${this.base}/update`, contact)
      .pipe(catchError(this.handleError));
  }

  getGaraForDocument(claimId: number) {
    return new Promise((resolve, reject) => {
      this.http.get(`${environment.endpoint}/${this.base}/garaForDocument?claimID=` + claimId).subscribe(
        res => {
          if (res || res == null) {
            resolve(res);
          }
        },
        (err) => {
          reject(err);
        })
    })
  }
  getGaraDocumentByClaim(claimId: number) {
    return new Promise((resolve, reject) => {
      this.http.get(`${environment.endpoint}/${this.base}/gara/documentByClaim?claimID=` + claimId).subscribe(
        res => {
          if (res || res == null) {
            resolve(res);
          }
        },
        (err) => {
          reject(err);
        })
    })
  }
}

import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {environment} from '../../../../environments/environment';
import {Page} from '../../models/page';
import {DmContact} from '../../models/dm-contact';
import {Observable} from 'rxjs/Rx';

@Injectable()
export class BeneficiaryService {

  protected base = 'contact/beneficiary';

  constructor(private http: HttpClient) {
  }

  getList(searchKey?: string, page = 0, size = 10): Observable<Page<DmContact>> {
    return this.http.get<Page<DmContact>>(`${environment.endpoint}/${this.base}/list`, {
      params: new HttpParams()
        .set('searchKey', encodeURI(searchKey))
        .set('page', page.toString())
        .set('size', size.toString()),
    })
  }

  saveAndGetValue(beneficiary: DmContact): Observable<DmContact> {
    return this.http.post<DmContact>(`${environment.endpoint}/${this.base}/saveAndGetValue`, beneficiary);
  }

  updateAndGetValue(beneficiary: DmContact): Observable<DmContact> {
    return this.http.put<DmContact>(`${environment.endpoint}/${this.base}/updateAndGetValue`, beneficiary);
  }
  getListByName(search): Observable<any> {
    const url = `${environment.endpoint}` + '/contact/beneficiary/list?searchKey=' + encodeURI(search) + '&page=0&size=100'
    return this.http.get(url).map((response: Response) => response);
  }
}

import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs/Rx';
import { Guarantee } from '../../models/guarantee';
import { environment } from '../../../../environments/environment';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class GuaranteeService {

  urlApi: any;
  guaranteeUrlApi: any;

  historyExchangeApproves: any[];

  constructor(private http: HttpClient) {
    this.urlApi = environment.endpoint;
    this.guaranteeUrlApi = this.urlApi.concat('/guarantee');
    this.historyExchangeApproves = [];
  }

  setHistoryExchangeApproves(data) {
    this.historyExchangeApproves = data || [];
  }

  getGuaranteeDetailById(guaranteeId) {
    return new Promise((resolve, reject) => {
      this.http.get(this.guaranteeUrlApi.concat('/detail?guaranteeId=', guaranteeId)).subscribe(
        res => {
          if (res) {
            resolve(res);
          }
        },
        (err) => {
          reject(err);
        })
    })
  }

  getGuaranteeDetailByGaraId(claimId, garaId) {
    return new Promise((resolve, reject) => {
      this.http.get(this.guaranteeUrlApi.concat('/detail/gara?claimId=', claimId, '&garaId=', garaId)).subscribe(
        res => {
          resolve(res);
        },
        (err) => {
          reject(err);
        })
    })
  }

  getListGuaranteeHistory(claimId) {
    return new Promise((resolve, reject) => {
      this.http.get(this.guaranteeUrlApi.concat('/list?claimId=', claimId)).subscribe(
        res => {
          if (res) {
            resolve(res);
          }
        },
        (err) => {
          reject(err);
        })
    })
  }

  getListGuaranteeDetailHistory(claimId) {
    return new Promise((resolve, reject) => {
      this.http.get(this.guaranteeUrlApi.concat('/historyApprove?guaranteeId=', claimId)).subscribe(
        res => {
          if (res) {
            resolve(res);
          }
        },
        (err) => {
          reject(err);
        })
    })
  }

  getListInsuranceCompany(claimId) {
    return new Promise((resolve, reject) => {
      this.http.get(this.urlApi.concat('/branch/getInsuranceCompany?claimId=', claimId)).subscribe(
        res => {
          if (res || res == null) {
            resolve(res);
          }
        },
        (err) => {
          reject(err);
        })
    })
  }

  getDataToCreate(claimId, garaId) {
    return new Promise((resolve, reject) => {
      this.http.get(this.guaranteeUrlApi.concat('/create?claimId=', claimId, '&garaId=', garaId)).subscribe(
        res => {
          if (res) {
            resolve(res);
          }
        },
        (err) => {
          reject(err);
        })
    })
  }

  getDataToAdditional(claimId, garaId) {
    return new Promise((resolve, reject) => {
      this.http.get(this.guaranteeUrlApi.concat('/additional?claimId=', claimId, '&garaId=', garaId)).subscribe(
        res => {
          if (res) {
            resolve(res);
          }
        },
        (err) => {
          reject(err);
        })
    })
  }

  checkAdditionalQuotation(claimId, garaId) {
    return new Promise((resolve, reject) => {
      this.http.get(this.guaranteeUrlApi.concat('/checkAdditional?claimId=', claimId, '&garaId=', garaId)).subscribe(
        res => {
          resolve(res);
        },
        (err) => {
          reject(err);
        })
    })
  }

  saveGuarantee(guarantee) {
    return new Promise((resolve, reject) => {
      this.http.post(this.guaranteeUrlApi.concat('/', 'save'), guarantee).subscribe(
        res => {
          if (res) {
            resolve(res);
          }
        },
        (err) => {
          reject(err);
        })
    });
  };

  updateGuarantee(guarantee) {
    return new Promise((resolve, reject) => {
      this.http.put(this.guaranteeUrlApi.concat('/', 'update'), guarantee).subscribe(
        res => {
          if (res) {
            resolve(res);
          }
        },
        (err) => {
          reject(err);
        })
    });
  };

  approvedGuarantee(guarantee) {
    return new Promise((resolve, reject) => {
      this.http.put(this.guaranteeUrlApi.concat('/', 'approve'), guarantee).subscribe(
        res => {
          if (res) {
            resolve(res);
          }
        },
        (err) => {
          reject(err);
        })
    });
  };

  updateGuaranteeByStatus(guarantee) {
    return new Promise((resolve, reject) => {
      this.http.put(this.guaranteeUrlApi.concat('/', 'updateStatus'), guarantee).subscribe(
        res => {
          if (res) {
            resolve(res);
          }
        },
        (err) => {
          reject(err);
        })
    });
  };

  sendComment(history) {
    return new Promise((resolve, reject) => {
      this.http.post(this.guaranteeUrlApi.concat('/', 'sentComment'), history).subscribe(
        res => {
          if (res) {
            resolve(res);
          }
        },
        (err) => {
          reject(err);
        })
    });
  };

  viewPDF(event): any {
    return this.http.get(this.guaranteeUrlApi.concat('/getFilePDF?guaranteeId=', event), { responseType: 'arraybuffer' as 'json' })
      .map(res => res);
  }

  getListDocumentRequired(claimId, garaId) {
    return new Promise((resolve, reject) => {
      this.http.get(this.urlApi.concat('/document/list/required?claimId=', claimId, '&garaId=', garaId)).subscribe(
        res => {
          if (res) {
            resolve(res);
          }
        },
        (err) => {
          reject(err);
        })
    })
  }

  updateGuaranteeAndSendComment(guarantee) {
    return new Promise((resolve, reject) => {
      this.http.put(this.guaranteeUrlApi.concat('/', 'updateAndSentComment'), guarantee).subscribe(
        res => {
          if (res) {
            resolve(res);
          }
        },
        (err) => {
          reject(err);
        })
    });
  };

}

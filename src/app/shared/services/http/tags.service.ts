import { Injectable } from '@angular/core';
import { BaseEntity } from '../../models/base-entity';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs/Rx';
import { DmContact } from '../../models/dm-contact';
import { environment } from '../../../../environments/environment';
import { Subject } from 'rxjs/Subject';
import { Attributes } from '../../models/Attributes';
import { BaseHttpService } from './base-http.service';
import { Tags } from '../../models/Tags';

@Injectable()
export class TagsService extends BaseHttpService<Tags> {
    protected base = 'tags';
    constructor(protected http: HttpClient) {
        super(http);
    }
    getTagsByProductId(id: number) {
        return this.http.get(`${environment.endpoint}/tags/getTagsByProductId/${id}`).map((response: Response) => response);
    }
}

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../../environments/environment';

@Injectable()
export class WorkBasketService {

  constructor(private http: HttpClient) { }

  searchForAssessors(data) {
    const params = (data.claimNumber ? '?claimNumber=' + data.claimNumber : '')
      + (data.plateNumber ? (data.claimNumber ? '&' : '?') + 'plateNumber=' + data.plateNumber : '')
    return new Promise((resolve, reject) => {
      this.http.get(environment.endpoint.concat('/', 'work/cart/surveyor', encodeURI(params))).subscribe(
        res => {
          if (res || res == null) {
            resolve(res)
          }
        },
        (err) => {
          reject(err)
        })
    })
  }

  assessorsRejectDocument(data) {
    return new Promise((resolve, reject) => {
      this.http.put(environment.endpoint.concat('/', 'work/confirmProcessWork'), data).subscribe(
        res => {
          if (res) {
            resolve(res)
          }
        },
        (err) => {
          reject(err)
        })
    })
  }

  searchForLeader(data) {
    let params = (data.page ? '?page=' + data.page : '')
      + (data.size ? (data.page ? '&' : '?') + 'size=' + data.size : '')
    params = encodeURI(params)
    return new Promise((resolve, reject) => {
      this.http.post(environment.endpoint.concat('/', 'work/cart/manager', params), data).subscribe(
        res => {
          if (res || res == null) {
            resolve(res)
          }
        },
        (err) => {
          reject(err)
        })
    })
  }
}

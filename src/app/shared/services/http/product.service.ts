import { Injectable } from '@angular/core';
import { BaseHttpService } from './base-http.service';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from '../../../../environments/environment';
import { Product } from '../../models/Product';
import { ProductSearch } from '../../entity/ProductSearch';

@Injectable()
export class ProductService extends BaseHttpService<Product> {
    protected base = 'product';
    constructor(protected http: HttpClient) {
        super(http);
    }
    
    getAllsNotDetail() {
        return this.http.get(`${environment.endpoint}/product/getAllsNotDetail`).map((response: Response) => response);
    }
    getAlls() {
        return this.http.get(`${environment.endpoint}/product/getAllProduct`).map((response: Response) => response);
    }
    getAllsPadding(page: number, pageSize: number, columnName: string, typeOrder: string, search: string) {
        const url = `${environment.endpoint}/product/pagerAllProduct`
            + '?page=' + page + '&pageSize=' + pageSize +
             '&columnName=' + columnName + '&typeOrder=' + typeOrder + '&search=' + search
        return this.http.get(url).map((response: Response) => response);
    }
    getAllsPaddingMove(pro: ProductSearch) {
        const url = `${environment.endpoint}/product/searchMoveAllProduct`;
        return this.http.post(url, pro).map((response: Response) => response);
    }
    getProductID(id) {
        return this.http.get(`${environment.endpoint}/product/getProductId/${id}`).map((response: Response) => response);
    }
    checkArticleProeduct(id) {
        return this.http.get(`${environment.endpoint}/product/checkArticleProeduct/${id}`).map((response: Response) => response);
    }
    create(produ) {
        return this.http.post(`${environment.endpoint}/product/create`, produ).map((response: Response) => response);
    }
    update(produ) {
        return this.http.put(`${environment.endpoint}/product/update`, produ).map((response: Response) => response);
    }
    deleteProductId(id) {
        
        // return this.http.delete(`${environment.endpoint}/product/deleteAllProduct`).map((response: Response) => response);
        return this.http.delete(`${environment.endpoint}/product/delete/${id}`).map((response: Response) => response);
    }
    // packageType
    getAllPackageType() {
        return this.http.get(`${environment.endpoint}/packageType/getAlls`).map((response: Response) => response);
    }
    // Trang Chu
    countProduct() {
        return this.http.get(`${environment.endpoint}/product/countProduct`).map((response: Response) => response);
    }
    countOrder() {
        return this.http.get(`${environment.endpoint}/customerorderproduct/countOrder`).map((response: Response) => response);
    }
    getAllsPaddingArea(page: number, pageSize: number, columnName: string, typeOrder: string, search: string, area:number) {
        const url = `${environment.endpoint}/product/pagerAllProductArea`
            + '?page=' + page + '&pageSize=' + pageSize +'&area=' + area +
             '&columnName=' + columnName + '&typeOrder=' + typeOrder + '&search=' + search
        return this.http.get(url).map((response: Response) => response);
    }
}


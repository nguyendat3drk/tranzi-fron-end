import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs/Rx';
import { environment } from '../../../../environments/environment';
import { Subject } from 'rxjs/Subject';
import { Customer } from '../../models/Customer';
import { BaseHttpService } from './base-http.service';
import { CustomerOrder } from '../../models/CustomerOrder';

@Injectable()
export class CustomerOrderService extends BaseHttpService<CustomerOrder> {
    protected base = 'customerorder';
    constructor(protected http: HttpClient) {
        super(http);
    }
    getAll() {
        return this.http.get(`${environment.endpoint}/customerorder/getAlls`).map((response: Response) => response);
    }
    getAllsPadding(page: number, pageSize: number, columnName: string, typeOrder: string, search: string) {
        const url = `${environment.endpoint}/customerorder/pagerAllCustomer`
            + '?page=' + page + '&pageSize=' + pageSize +
            '&columnName=' + columnName + '&typeOrder=' + typeOrder + '&search=' + search
        return this.http.get(url).map((response: Response) => response);
    }
    deleteCustomerOrderId(id) {
        return this.http.delete(`${environment.endpoint}/customerorder/delete/${id}`).map((response: Response) => response);
    }
    getCustomerOrderById(id) {
        return this.http.get(`${environment.endpoint}/customerorder/getById/${id}`).map((response: Response) => response);
    }
    create(customers:CustomerOrder) {
        if (customers && (customers.id) > 0) {
            return this.http.put(`${environment.endpoint}/customerorder/update`, customers).map((response: Response) => response);
        } else {
            return this.http.post(`${environment.endpoint}/customerorder/create`, customers).map((response: Response) => response);
        }
    }
    update(customers) {
        return this.http.put(`${environment.endpoint}/customerorder/update`, customers).map((response: Response) => response);
    }
    // product by customer_order_id
    getAllByCustomerOrderId(id){
        return this.http.get(`${environment.endpoint}/customerorderproduct/getByCustomerOrderID/${id}`).map((response: Response) => response);
    }
    // Trang Chu
    countOrder() {
        return this.http.get(`${environment.endpoint}/customerorder/countOrder`).map((response: Response) => response);
    }
}

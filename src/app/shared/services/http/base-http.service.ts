import {BaseEntity} from '../../models/base-entity';
import {HttpClient, HttpErrorResponse, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs/Rx';
import {Page} from '../../models/page';
import {environment} from '../../../../environments/environment';
import {EclaimHttpError} from '../../models/eclaim-http-error';
import {throwError} from 'rxjs/index';

export class BaseHttpService<E extends BaseEntity> {

  protected base = 'object';
  constructor(protected http: HttpClient) {
  }

  getList(page = 0, size = 20, params?: Object): Observable<Page<E>> {
    let httpParams = new HttpParams();
    // tslint:disable-next-line:forin
    for (const item in params) {
      httpParams = httpParams.set(item, encodeURI(params[item]));
    }

    httpParams = httpParams.set('page', page.toString()).set('size', size.toString());
    return this.http.get<Page<E>>(`${environment.endpoint}/${this.base}/list`, {
      params: httpParams,
    });
  }

  getDetail(id: number): Observable<E> {
    return this.http.get<E>(`${environment.endpoint}/${this.base}/detail/${id}`);
  }

  protected handleError = (err: HttpErrorResponse) => {
    console.error(err);
    const httpError: EclaimHttpError = err.error;
    return throwError(httpError);
  }
}

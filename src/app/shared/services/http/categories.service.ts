import { Injectable } from '@angular/core';
import { BaseHttpService } from './base-http.service';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from '../../../../environments/environment';
import { Categories } from '../../models/Categories';

@Injectable()
export class CategoriesService extends BaseHttpService<Categories> {
    protected base = 'categories';
    constructor(protected http: HttpClient) {
        super(http);
    }
    // test api
    fixAttribute(){
        return this.http.get(`${environment.endpoint}/attributes/fixAttribute`).map((response: Response) => response);
    }
    testAPI() {
        return this.http.get(`${environment.endpoint}/categories/testAPI`).map((response: Response) => response);
    }
    getAlls() {
        return this.http.get(`${environment.endpoint}/categories/getAlls`).map((response: Response) => response);
    }
    updateOrderCategoty(data) {
        return this.http.put(`${environment.endpoint}/categories/updateOrder`, data).map((response: Response) => response);
    }
    getAllRespont() {
        return this.http.get(`${environment.endpoint}/categories/getAllRespont`).map((response: Response) => response);
    }
    getAllsPadding(page: number, pageSize: number, columnName: string, typeOrder: string, search: string) {
        const url = `${environment.endpoint}/categories/pagerAllCategories`
            + '?page=' + page + '&pageSize=' + pageSize +
             '&columnName=' + columnName + '&typeOrder=' + typeOrder + '&search=' + search
        return this.http.get(url).map((response: Response) => response);
    }
    loadCategoriesTree(id:number) {
        const url = `${environment.endpoint}/categories/gettreeTable/`+id
        return this.http.get(url).map((response: Response) => response);
    }
    loadParentCategoryTable() {
        const url = `${environment.endpoint}/categories/getParentTree`
        return this.http.get(url).map((response: Response) => response);
    }
    getParentTreeChildenTable(id: number) {
        const url = `${environment.endpoint}/categories/getParentTreeChilden/`+id
        return this.http.get(url).map((response: Response) => response);
    }
    getCategoriesID(id) {
        return this.http.get(`${environment.endpoint}/categories/getCategoriesId/${id}`).map((response: Response) => response);
    }
    create(categories) {
        return this.http.post(`${environment.endpoint}/categories/create`, categories).map((response: Response) => response);
    }
    update(categories) {
        return this.http.put(`${environment.endpoint}/categories/update`, categories).map((response: Response) => response);
    }
    deleteCategoriId(id) {
        return this.http.delete(`${environment.endpoint}/categories/delete/${id}`).map((response: Response) => response);
        // return this.http.delete(`${environment.endpoint}/categories/deleteALL`).map((response: Response) => response);
    }
    getParentTree() {
        return this.http.get(`${environment.endpoint}/categories/getListParentCategories`).map((response: Response) => response);
    }
    getChildrenTree() {
        return this.http.get(`${environment.endpoint}/categories/getListChildrenCategories`).map((response: Response) => response);
    }
    getTreeCategories(id) {
        return this.http.get(`${environment.endpoint}/categories/getTreeNode/${id}`).map((response: Response) => response);
    }
    // Manufactory
    getAllsPaddingManu(page: number, pageSize: number, columnName: string, typeOrder: string, search: string) {
        const url = `${environment.endpoint}/manufactures/pagerAllManufactures`
            + '?page=' + page + '&pageSize=' + pageSize +
            '&columnName=' + columnName + '&typeOrder=' + typeOrder + '&search=' + search
        return this.http.get(url).map((response: Response) => response);
    }
    getAllsManufact() {
        return this.http.get(`${environment.endpoint}/manufactures/getAlls`).map((response: Response) => response);
    }
    getManufacturesIDManufact(id) {
        return this.http.get(`${environment.endpoint}/manufactures/getManufacturesId/${id}`).map((response: Response) => response);
    }
    createManufact(manufactures) {
        return this.http.post(`${environment.endpoint}/manufactures/create`, manufactures).map((response: Response) => response);
    }
    updateManufact(manufactures) {
        return this.http.put(`${environment.endpoint}/manufactures/update`, manufactures).map((response: Response) => response);
    }
    deleteManufacturesIdManufact(id) {
        return this.http.delete(`${environment.endpoint}/manufactures/delete/${id}`).map((response: Response) => response);
    }
     // packageType
     getAllPackageType() {
        return this.http.get(`${environment.endpoint}/packageType/getAlls`).map((response: Response) => response);
    }
    // media product
    getMediaProductById(id: number) {
        return this.http.get(`${environment.endpoint}/medias/getMediaByProductID/${id}`).map((response: Response) => response);
    }
    // media product
    getMediaProductMediaById(id: number) {
        return this.http.get(`${environment.endpoint}/medias/getMediaByProductMediaID/${id}`).map((response: Response) => response);
    }
}


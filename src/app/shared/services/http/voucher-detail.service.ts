import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../../environments/environment';
import { CommentParams } from '../../models/comment-params';

@Injectable()
export class VoucherDetailService {

  constructor(private http: HttpClient) { }

  getBvDocuments(claimId: number) {
    const params = '?claimId=' + claimId
    return new Promise((resolve, reject) => {
      this.http.get(environment.endpoint.concat('/', 'document/list/bv', params)).subscribe(
        res => {
          if (res || res == null) {
            resolve(res)
          }
        },
        (err) => {
          reject(err)
        })
    })
  }
  getBvDocumentsForGarage(claimId: number, garaId: number, classificationExchange: string) {
    const params = '?claimId=' + claimId + '&garaId=' + garaId + '&classificationExchange=' + classificationExchange
    return new Promise((resolve, reject) => {
      this.http.get(environment.endpoint.concat('/', 'document/listForGarage', params)).subscribe(
        res => {
          if (res || res == null) {
            resolve(res)
          }
        },
        (err) => {
          reject(err)
        })
    })
  }

  getGaraDocuments(claimId: number, garaId: number, classificationExchange: string) {
    const params = '?claimId=' + claimId + '&garaId=' + garaId + '&classificationExchange=' + classificationExchange
    return new Promise((resolve, reject) => {
      this.http.get(environment.endpoint.concat('/', 'document/list/gara', params)).subscribe(
        res => {
          if (res || res == null) {
            resolve(res)
          }
        },
        (err) => {
          reject(err)
        })
    })
  }

  getAttachments(documentId: number) {
    const params = '?documentId=' + documentId
    return new Promise((resolve, reject) => {
      this.http.get(environment.endpoint.concat('/', 'document/getFileAttach', params)).subscribe(
        res => {
          if (res || res == null) {
            resolve(res)
          }
        },
        (err) => {
          reject(err)
        })
    })
  }

  getPrintInfo(claimId: number, imageUrls: Array<string>) {
    const params = '?claimId=' + claimId
    return new Promise((resolve, reject) => {
      this.http.post(environment.endpoint.concat('/', 'document/getPrintInfo', params), imageUrls).subscribe(
        res => {
          if (res || res == null) {
            resolve(res)
          }
        },
        (err) => {
          reject(err)
        })
    })
  }

  downloadFile(url): any {
    return this.http.get(`${environment.endpoint}/document/download?url=${encodeURI(url)}`, { responseType: 'arraybuffer' as 'json' })
      .map(res => res);
  }

  downloadAllFile(params): any {
    return new Promise((resolve, reject) => {
      this.http.post(environment.endpoint.concat('/', 'document/downloadAll'), params, { responseType: 'arraybuffer' as 'json' }).subscribe(
        res => {
          if (res || res == null) {
            resolve(res)
          }
        },
        (err) => {
          reject(err)
        })
    })
  }

  postComment(params: CommentParams) {
    return new Promise((resolve, reject) => {
      this.http.post(environment.endpoint.concat('/', 'bvGaraComment/create'), params).subscribe(
        res => {
          if (res || res == null) {
            resolve(res)
          }
        },
        (err) => {
          reject(err)
        })
    })
  }

  updateStatusDocument(data) {
    const params = '?documentId=' + data.documentId + '&status=' + data.status
    return new Promise((resolve, reject) => {
      this.http.post(environment.endpoint.concat('/', 'document/updateStatus', params), {}).subscribe(
        res => {
          if (res || res == null) {
            resolve(res)
          }
        },
        (err) => {
          reject(err)
        })
    })
  }

  updateAllAttachments(claimId, documentId, data) {
    const params = '?documentId=' + documentId + '&claimId=' + claimId
    return new Promise((resolve, reject) => {
      this.http.post(environment.endpoint.concat('/', 'document/updateAllAttachments', params), data).subscribe(
        res => {
          if (res || res == null) {
            resolve(res)
          }
        },
        (err) => {
          reject(err)
        })
    })
  }

  deleteAttachment(data) {
    const params = '?claimId=' + (data ? data.claimId : '') + '&documentId=' + (data ? data.documentId : '')
      + '&url=' + (data ? encodeURI(data.url) : '')
    return new Promise((resolve, reject) => {
      this.http.delete(environment.endpoint.concat('/', 'document/attachment/delete', params), {}).subscribe(
        res => {
          if (res || res == null) {
            resolve(res)
          }
        },
        (err) => {
          reject(err)
        })
    })
  }
  GetByClaimId(data) {
    return new Promise((resolve, reject) => {
      this.http.get(environment.endpoint.concat('/', 'claim/detail/', data), {}).subscribe(
        res => {
          if (res || res == null) {
            resolve(res)
          }
        },
        (err) => {
          reject(err)
        })
    })
  }
}

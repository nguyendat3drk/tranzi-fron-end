import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from '../../../../environments/environment';
import { BaseHttpService } from './base-http.service';
import { inventoryArea } from '../../models/inventoryArea';

@Injectable()
export class InventoryAreaService extends BaseHttpService<inventoryArea> {
    protected base = 'inventoryArea';
    constructor(protected http: HttpClient) {
        super(http);
    }
    getAll() {
        return this.http.get(`${environment.endpoint}/inventoryArea/getAlls`).map((response: Response) => response);
    }
    getAllsPadding(page: number, pageSize: number, columnName: string, typeOrder: string, search: string) {
        const url = `${environment.endpoint}/inventoryArea/getAllPage`
            + '?page=' + page + '&pageSize=' + pageSize +
            '&columnName=' + columnName + '&typeOrder=' + typeOrder + '&search=' + search
        return this.http.get(url).map((response: Response) => response);
    }
    delete(id) {
        return this.http.delete(`${environment.endpoint}/inventoryArea/delete/${id}`).map((response: Response) => response);
    }
    create(attributes) {
        return this.http.post(`${environment.endpoint}/inventoryArea/addupdate`, attributes).map((response: Response) => response);
    }

    // get list id AttributeValue by id product
    getById(id) {
        return this.http.get(`${environment.endpoint}/inventoryArea/getById/${id}`).map((response: Response) => response);
    }
    
}

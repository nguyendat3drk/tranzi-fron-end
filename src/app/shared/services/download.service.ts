import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { Http, RequestOptions, Response, ResponseContentType } from '@angular/http';
import { HttpClient } from '@angular/common/http';

import { saveAs } from 'file-saver';

import { MessageService } from 'primeng/components/common/messageservice';

@Injectable()
export class DownloadService {


    constructor(private http: Http,
    private httpClient: HttpClient,
    private messageService: MessageService) {
    }

    async downloadFile(url) {
        const options = new RequestOptions({responseType: ResponseContentType.Blob });
        // Process the file downloaded
        try {
            await this.http.get(url, options).subscribe(res => {
                const fileName = this.getFileNameFromUrl(url);
                this.saveFile(res.blob(), fileName);
            });
        } catch (error) {
            this.messageService.add({
                detail: 'Lỗi tải file',
                severity: 'error',
                summary: 'Thông báo',
            });
        }
    }

    downloadFile2(urlResponse):void {
        this.getFile(urlResponse)
        .subscribe(fileData => {
            let b:any = new Blob([fileData], { type: 'image/png' });
            var url= window.URL.createObjectURL(b);
            window.open(url);
        });
    }

   

    /**
     * Saves a file by opening file-save-as dialog in the browser
     * using file-save library.
     * @param blobContent file content as a Blob
     * @param fileName name file should be saved as
     */
    private saveFile = (blobContent: Blob, fileName: string) => {
        const blob = new Blob([blobContent], { type: 'application/octet-stream' });
        saveAs(blob, fileName);
    };

    /**
     * Derives file name from the http response
     * by looking inside content-disposition
     * @param res http Response
     */
    private getFileNameFromResponseContentDisposition = (res: Response) => {
        const contentDisposition = res.headers.get('content-disposition') || '';
        const matches = /filename=([^;]+)/ig.exec(contentDisposition);
        const fileName = (matches[1] || 'untitled').trim();
        return fileName;
    };

    /**
     * Derives file name from the http response
     * by looking inside content-disposition
     * @param url http Response
     */
    private getFileNameFromUrl = (url: String) => {
        const words = url.split('/');
        return words[words.length -1];
    };

    private getFile(path: string):Observable<any> {
        let options = new RequestOptions({responseType: ResponseContentType.Blob});
        return this.http.get(path, options)
        .map((response: Response) => <Blob>response.blob());
    }

}

import { Injectable } from '@angular/core';
import { AuthorizationModel } from './../models/authorization-model';
import { UserTypes } from '../models/userTypes.enum';
import { MessageService } from 'primeng/components/common/messageservice';
import { moment } from '../../../../node_modules/ngx-bootstrap/chronos/test/chain';
import { environment } from '../../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { TranslateService } from '@ngx-translate/core';
import { AuthenticationUser } from '../loginDTO/AuthenticationUser';
@Injectable()
export class Utilities {

  protected base = 'claim';
  public maskDate = [/[1-9]/, /[1-9]/, '/', /[1-9]/, /[1-9]/, '/', /[1-9]/, /[1-9]/, /[1-9]/, /[1-9]/]
  constructor(private messageService: MessageService, protected http: HttpClient, private translateService: TranslateService) {
  }
  getUrlServerHost() {
    return `${environment.endpoint}`
  }
  urlServerFrontEnd() {
    return `${environment.urlSerVer}`;
  }
  
  closeAutoMenu(){
    if (!window.matchMedia("(min-width: 900px)").matches) {
      /* The viewport is at least 400 pixels wide */
      if (document.body.classList.contains('is-collapsed')) {
        document.body.classList.remove('is-collapsed')
      } else {
        document.body.classList.add('is-collapsed')
      }
    } 
  }

  checkNumber(event): boolean {
    const numberExtension = new RegExp('[^0-9]');
    let check = true;
    if (event) {
      if (numberExtension.test(event)) {
        check = false;
      } else {
        check = true;
      }
    }
    return check;
  }
  
  numberPrice(nStr) {
    if (nStr) {
      nStr += '';
      const ncheck = nStr.replace(/\./g, '');
      return ncheck.replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
    } else {
      return 0;
    }
  }

  estimateAmountKeyUp(input, obj) {
    let valueStr = '';
    if (input.value || input.value === 0 || input.value === '0') valueStr = input.value.replace(/,/g, '');

    // tslint:disable-next-line:radix
    const value = parseInt(valueStr);
    if (!value && value !== 0) {
      input.value = '';
      obj.estimateAmount = 0;
      return;
    }

    obj.estimateAmount = value;

    input.value = value.toFixed(0).replace(/./g, function (c, i, a) {
      return i > 0 && c !== ',' && (a.length - i) % 3 === 0 ? ',' + c : c;
    });
    return;
  }
  public amountKeyUp(input, obj) {
    let valueStr = '';
    if (input.value) valueStr = input.value.replace(/,/g, '');

    // tslint:disable-next-line:radix
    const value = parseInt(valueStr);
    if (!value && value !== 0) {
      input.value = '';
      obj = 0;
      return;
    }

    obj = value;

    input.value = value.toFixed(0).replace(/./g, function (c, i, a) {
      return i > 0 && c !== ',' && (a.length - i) % 3 === 0 ? ',' + c : c;
    });
    return;
  }

  estimateAmountKeyDown(event) {
    if ([46, 8, 9, 27, 13, 110, 190].indexOf(event.keyCode) !== -1 ||
      // Allow: Ctrl+A
      (event.keyCode === 65 && (event.ctrlKey || event.metaKey)) ||
      // Allow: Ctrl+C
      (event.keyCode === 67 && (event.ctrlKey || event.metaKey)) ||
      // Allow: Ctrl+V
      (event.keyCode === 86 && (event.ctrlKey || event.metaKey)) ||
      // Allow: Ctrl+X
      (event.keyCode === 88 && (event.ctrlKey || event.metaKey)) ||
      // Allow: home, end, left, right
      (event.keyCode >= 35 && event.keyCode <= 39)) {
      // let it happen, don't do anything
      return;
    }
    // Ensure that it is a number and stop the keypress
    if ((event.shiftKey || (event.keyCode < 48 || event.keyCode > 57)) && (event.keyCode < 96 || event.keyCode > 105)) {
      event.preventDefault();
    }
    if ((event && event.target && event.target.value)) {
      const n = event.target.value.indexOf(',');
      const nlength = event.target.value.split(',');
      if ((n < 0 && event.target.value.length === 15) || (nlength.length === 2 && event.target.value.length === 16)
        || (nlength.length === 3 && event.target.value.length === 17) || (nlength.length === 4 && event.target.value.length === 18)) {
        event.preventDefault();
      }
    }
    const patternExtension = /[0-9\.]/;
    if (!patternExtension.test(event.key)) {
      return false;
    }
  }
  estimateAmountFocusOut(event) {
    if ([46, 8, 9, 27, 13, 110, 190].indexOf(event.keyCode) !== -1 ||
      // Allow: Ctrl+A
      (event.keyCode === 65 && (event.ctrlKey || event.metaKey)) ||
      // Allow: Ctrl+C
      (event.keyCode === 67 && (event.ctrlKey || event.metaKey)) ||
      // Allow: Ctrl+V
      (event.keyCode === 86 && (event.ctrlKey || event.metaKey)) ||
      // Allow: Ctrl+X
      (event.keyCode === 88 && (event.ctrlKey || event.metaKey)) ||
      // Allow: home, end, left, right
      (event.keyCode >= 35 && event.keyCode <= 39)) {
      // let it happen, don't do anything
      return;
    }
    // Ensure that it is a number and stop the keypress
    if ((event.shiftKey || (event.keyCode < 48 || event.keyCode > 57)) && (event.keyCode < 96 || event.keyCode > 105)) {
      event.preventDefault();
    }

    const patternExtension = /[0-9\.]/;
    if (!patternExtension.test(event.key)) {
      return false;
    }
  }
  getPermission() {
    let authen = new AuthorizationModel();
    if (localStorage.getItem('token')) {
      authen = JSON.parse(localStorage.getItem('token'));
    }
    return authen;
  }
  getPermissionAuth() {
    let authen = new AuthenticationUser();
    if (localStorage.getItem('token')) {
      authen = JSON.parse(localStorage.getItem('token'));
    }
    return authen;
  }
  checkRole(textRole) {
    let check = false;
    if (textRole) {
      let authen = new AuthorizationModel();
      if (localStorage.getItem('token')) {
        authen = JSON.parse(localStorage.getItem('token'));
        if (authen && authen.roles) {
          for (let i = 0; i < authen.roles.length; i++) {
            if (textRole === authen.roles[i]) {
              check = true;
              return true;
            }
          }
        }
      }
    }
    return check;
  }
  getToken() {
    let authen = new AuthorizationModel();
    if (localStorage.getItem('token')) {
      authen = JSON.parse(localStorage.getItem('token'));
    }
    return authen.access_token;
  }
  isLeader(): boolean {
    const roleId = this.getRoleId()
    return roleId === UserTypes.TANDPTG || roleId === UserTypes.GD_GDX
      || roleId === UserTypes.TP_GDX || roleId === UserTypes.GDGDBT || roleId === UserTypes.TP_GDBT
  }
  isAssessors(): boolean {
    return this.getRoleId() === UserTypes.GDV
  }
  isZoneLead(): boolean {
    return this.getRoleId() === UserTypes.TZ
  }
  isGarage(): boolean {
    return this.getRoleId() === UserTypes.GARAGE
  }
  getCurrentGaraId() {
    const userInfo = this.getPermission()
    return userInfo ? userInfo.beneficiaryId : null
  }
  getRoleId() {
    const userInfo = this.getPermission()
    return userInfo ? userInfo.roleId : null
  }
  showError(message) {
    this.messageService.clear();
    this.messageService.add({
      detail: message ? message : 'Lỗi',
      severity: 'error',
      summary: 'Thông báo',
    });
  }
  showSuccess(message) {
    this.messageService.clear();
    this.messageService.add({
      detail: message ? message : 'Thành công',
      severity: 'success',
      summary: 'Thông báo',
    });
  }
  public convertDateToStringDateVN(date: Date) {
    if (!date) return null

    return moment(date).format('DD/MM/YYYY')
  }
  convertDateTimeToStringDateVN(date: Date) {
    if (!date) return null

    return ('0' + date.getDate()).slice(-2) + '/' + ('0' + (date.getMonth() + 1)).slice(-2) +
      '/' + date.getFullYear() + ' ' + ('0' + date.getHours()).slice(-2) + ':'
      + ('0' + date.getMinutes()).slice(-2) + ':' + ('0' + date.getSeconds()).slice(-2);
  }

  removeSpecialCharForPlateNumber(plateNumber) {
    let result = '';
    if (plateNumber) {
      result = plateNumber.replace(/[^A-Z+0-9]+/ig, '');
      result = result ? result.toUpperCase() : '';
    }
    return result;
  }
  getEmployeeId() {
    const userInfo = this.getPermission()
    return userInfo ? userInfo.employeeId : null
  }
  getFullName() {
    const userInfo = this.getPermissionAuth()
    return userInfo ? userInfo.firstName + ' ' + userInfo.lastName : 'Users'
  }
  getUserId() {
    const userInfo = this.getPermissionAuth()
    return userInfo ? userInfo.id : 0
  }
  getLanguage(event) {
    let re = '';
    this.translateService.get(event).subscribe(
      data => {
        re = data;
      });
    return re;
  }
  getTypeLogin() {
    const userInfo = this.getPermission()
    return userInfo ? userInfo.type : 0
  }
  changeToSlug(event) {
    if (event) {
      let slug = '';
      // Đổi chữ hoa thành chữ thường
      slug = event.toLowerCase();
      // Đổi ký tự có dấu thành không dấu
      slug = slug.replace(/á|à|ả|ạ|ã|ă|ắ|ằ|ẳ|ẵ|ặ|â|ấ|ầ|ẩ|ẫ|ậ/gi, 'a');
      slug = slug.replace(/é|è|ẻ|ẽ|ẹ|ê|ế|ề|ể|ễ|ệ/gi, 'e');
      slug = slug.replace(/i|í|ì|ỉ|ĩ|ị/gi, 'i');
      slug = slug.replace(/ó|ò|ỏ|õ|ọ|ô|ố|ồ|ổ|ỗ|ộ|ơ|ớ|ờ|ở|ỡ|ợ/gi, 'o');
      slug = slug.replace(/ú|ù|ủ|ũ|ụ|ư|ứ|ừ|ử|ữ|ự/gi, 'u');
      slug = slug.replace(/ý|ỳ|ỷ|ỹ|ỵ/gi, 'y');
      slug = slug.replace(/đ/gi, 'd');
      // Xóa các ký tự đặt biệt
      slug = slug.replace(/\`|\~|\!|\@|\#|\||\$|\%|\^|\&|\*|\(|\)|\+|\=|\,|\.|\/|\?|\>|\<|\'|\'|\:|\;|_/gi, '');
      // Đổi khoảng trắng thành ký tự gạch ngang
      slug = slug.replace(/ /gi, '-');
      // Đổi nhiều ký tự gạch ngang liên tiếp thành 1 ký tự gạch ngang
      // Phòng trường hợp người nhập vào quá nhiều ký tự trắng
      slug = slug.replace(/\-\-\-\-\-/gi, '-');
      slug = slug.replace(/\-\-\-\-/gi, '-');
      slug = slug.replace(/\-\-\-/gi, '-');
      slug = slug.replace(/\-\-/gi, '-');
      // Xóa các ký tự gạch ngang ở đầu và cuối
      slug = '@' + slug + '@';
      slug = slug.replace(/\@\-|\-\@|\@/gi, '');
      // In slug ra textbox có id “slug”
      return slug;

    }
  }
  checkURL(url) {
    // return (url.match(/\.(jpeg|jpg|gif|png|PNG|JPG|JPEG|GIF)$/) != null);

  }
  checkEmail(event) {
    // tslint:disable-next-line:max-line-length
    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(event).toLowerCase());
  }
  testUsername(event) {
    const patternExtension = /^[a-zA-Z0-9]+$/;
    if (!patternExtension.test(event)) {
      return false;
    }
    return true;
  }
  clearNameKytuDacbiet(event) {
    var re = '';
    if (event) {
      for (var i = 0; i < event.length; i++) {
        if (event[i] === '.' || event[i] === '-' || event[i] === '_' || this.testUsername(event[i]))
          re = re + event[i]
      }
    }
    return re;

  }
}

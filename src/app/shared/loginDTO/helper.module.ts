export class Acdw1 {
    type: number;
    access_token: string;
    token_type: string;
    refresh_token: string;
    expires_in: number;
    scope: string;
    employeeId: string
    beneficiaryId: number;
    id: number;
    userName: string;
    isAuth: boolean;
    password: string;
    isRememberPassword: boolean;
    isExternalAccess: boolean;
    userRole: number;
    token: string;
    firstLogin: number;
    state: string;
    email: string;
    activationCode: string;
    rememberCode: string;
    firstName: string;
    lastName: string;
    company: string;
    phone: string;
}

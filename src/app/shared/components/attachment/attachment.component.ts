import { Component, OnInit, Input, AfterViewInit, EventEmitter, Output } from '@angular/core';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import * as moment from 'moment';
import { Utilities } from '../../services/utilities.service';
import { VoucherDetailService } from '../../services/http/voucher-detail.service';
import { LoaderService } from '../../services/loader.service';
import { ConfirmService } from '../../confirm-dialog/ConfirmService';

declare const google: any;

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'attachment',
  templateUrl: './attachment.component.html',
  styleUrls: ['./attachment.component.scss'],
})
export class AttachmentComponent implements OnInit {

  constructor(private voucherService: VoucherDetailService, private loader: LoaderService, private sanitizer: DomSanitizer,
    private ultilities: Utilities, private _confirm: ConfirmService) { }

  ngOnInit() {
  }



}

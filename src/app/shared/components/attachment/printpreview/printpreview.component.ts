import {
  Component, OnInit, Input, Output, EventEmitter, HostListener,
} from '@angular/core';
import * as moment from 'moment';
import { VoucherDetailService } from '../../../services/http/voucher-detail.service';
import { LoaderService } from '../../../services/loader.service';
import { PDFDocumentProxy } from 'ng2-pdf-viewer';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'printpreview',
  templateUrl: './printpreview.component.html',
  styleUrls: ['./printpreview.component.scss'],
})
export class PrintpreviewComponent implements OnInit {
  @Input() claimId: number
  @Input() documentName: string
  @Input() fileName: string
  @Output() onBack = new EventEmitter<boolean>()

  fileSavingTypes = ['PDF', 'WORD']
  fileSavingType = this.fileSavingTypes[0]
  printInfo: any = {}
  pages = []
  currentPage = 0
  pageHeight = 1133
  headerHeight = 61
  notPagingHeight = 10
  isFixHeader = false
  isAnhTonThat = false
  styleForPDF = ''
  pdfUrl = ''
  pdf: any;

  constructor(private voucherService: VoucherDetailService, private loader: LoaderService,) { }

  ngOnInit() {
    this.isAnhTonThat = this.documentName === 'Ảnh tổn thất'
    this.getAttachmentCheckeds()
    this.loadFilePDF()
  }
  back() {
    this.onBack.emit(false)
  }
  print() {
    window.print()
  }
  private loadFilePDF() {
    this.loader.show()
  
  }

  save() {
    if (this.fileSavingType === 'PDF') {
      this.exportToPDF()
    } else if (this.fileSavingType === 'WORD') {
      this.downloadFile('doc')
    }
  }
  pageChange(page) {
    if (this.pages && page >= 0 && page < this.pages.length) {
      this.currentPage = page;
    }
    window.scrollTo(0, this.pageHeight * this.currentPage)
  }
  @HostListener('window:scroll') onScroll() {
    const currentPosition = document.documentElement.scrollTop
    this.isFixHeader = currentPosition >= this.headerHeight
    this.currentPage = Math.floor(currentPosition / this.pageHeight)
  }

  loadPdfComplete(pdf: PDFDocumentProxy) {
    this.pdf = pdf;
    this.pages = this.getPages()
    this.loader.hide()
  }

  loadPdfError() {
    this.loader.hide()
  }

  private getPages(): Array<number> {
    if (!this.pdf || this.pdf.numPages === 0) return [0]

    // tslint:disable-next-line:prefer-const
    let pageArray = []
    for (let i = 0; i < this.pdf.numPages; i++) {
      pageArray.push(i)
    }
    return pageArray
  }

  private exportToPDF() {
    // tslint:disable-next-line:prefer-const
    let a = document.createElement('a');
    document.body.appendChild(a)
    a.setAttribute('style', 'display: none');
    a.href = this.pdfUrl;
    a.download = (this.fileName ? this.fileName : moment().format('DD-MM-YYYY-HH-mm-ss')) + '.pdf';
    a.click();
    window.URL.revokeObjectURL(this.pdfUrl);
    a.remove()
  }

  private downloadFile(type) {
  
  }

  private downloadArrayBuffer(data: ArrayBuffer, fileName: string, fileType: string) {
    const file = new Blob([data], { type: 'application/' + fileType })
    const fileURL = URL.createObjectURL(file)
    // tslint:disable-next-line:prefer-const
    let a = document.createElement('a');
    document.body.appendChild(a)
    a.setAttribute('style', 'display: none');
    a.href = fileURL;
    a.download = fileName + '.' + fileType;
    a.click();
    window.URL.revokeObjectURL(fileURL);
    a.remove()
  }



  private getMaxDateAttach() {


  }

  private getFileName(url) {
    let fileName = url ? decodeURI(url).split('/').pop() : ''
    if (fileName) fileName = fileName.toLowerCase()
    return fileName
  }

  private isImage(url) {
    const images = ['jpg', 'jpeg', 'gif', 'png']
    return images.indexOf(this.getExtension(url)) !== -1
  }

  private getExtension(url) {
    let fileType = url ? decodeURI(url).split('.').pop() : ''
    if (fileType) fileType = fileType.toLowerCase()
    return fileType
  }
  private getAttachmentCheckeds() {

  }

  private getPrintInformation() {
    if (!this.claimId) return

    this.loader.show()
    this.voucherService.getPrintInfo(this.claimId, this.getImageUrls()).then((result: any) => {
      this.printInfo = result
      this.loader.hide()
    })
      .catch(() => {
        this.loader.hide()
      })
  }
  private getImageUrls() {
    // tslint:disable-next-line:prefer-const
    let urls = []
    return urls
  }
}

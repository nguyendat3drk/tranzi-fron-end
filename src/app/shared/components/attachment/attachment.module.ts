import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ModalModule } from 'ngx-bootstrap';

import { TranslateService, TranslateModule } from '@ngx-translate/core';
import { AttachmentComponent } from './attachment.component';
import { ConfirmService } from '../../confirm-dialog/ConfirmService';
import { TextMaskModule } from 'angular2-text-mask';
import { PdfViewerModule } from 'ng2-pdf-viewer';
import { ConfirmDialogModule } from '../../confirm-dialog/confirm-module';
import { PrintpreviewComponent } from './printpreview/printpreview.component';
import { FormsModule } from '@angular/forms';
import { BvDateMaskModule } from '../date-mask/date-mask.module';

@NgModule({
  imports: [
    CommonModule, ModalModule.forRoot(), TextMaskModule, PdfViewerModule,
    ConfirmDialogModule, TranslateModule, FormsModule, BvDateMaskModule,
  ],
  declarations: [
    AttachmentComponent,
    PrintpreviewComponent,
  ],
  providers: [ConfirmService, TranslateService],
  exports: [AttachmentComponent],
})
export class AttachmentModule { }

import { Component, Input, OnInit, ViewChild, ElementRef } from '@angular/core';
import { NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import * as _ from 'lodash'
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import { MenuItem } from 'primeng/primeng';
import { Message } from 'primeng/components/common/api';
import { MessageService } from 'primeng/components/common/messageservice';

@Component({
    // tslint:disable-next-line:component-selector
    selector: 'bv-gara',
    templateUrl: './bv-gara.component.html',
    styleUrls: ['./bv-gara.component.scss'],
})
export class BvGaraComponent implements OnInit {
    @ViewChild('scrolledContents') scrolledContents: ElementRef;
    result: any;
    isShowCreateGara: boolean;
    searchGaraParams: {
        query: String,
        city: String,
        district: String,
        page: any,
        size: any,
    };
    garas: any;
    garaLoadmores: any;

    cities: any;
    districts: any;

    isLoading: boolean;
    isScroll: boolean;
    countSearch: any;

    citySearchLoading = false;
    districtSearchLoading = false;

    searchCityModel: any;
    searchDistrictModel: any;

    selectTable: any;
    items: MenuItem[];

    garaExport: any[];
    msgs: Message[] = [];

    firstLoadFrom: boolean = true;
    constructor(
        public activeModal: NgbActiveModal,
        private messageService: MessageService) {
        this.isShowCreateGara = false;
        this.searchGaraParams = {
            query: '',
            city: '',
            district: '',
            page: 0,
            size: 5,
        };
        this.searchCityModel = {
            city: '',
        };
        this.searchDistrictModel = {
            city: '',
        };
    }

    ngOnInit() {
        this.isLoading = false;
        this.isScroll = false;
        this.result = {};
        this.countSearch = 0;
        this.garas = [];
        this.garaExport = [];
        this.garaLoadmores = [];
        this.cities = [];
        this.districts = [];
        // this.loadData();onChangeCity
        this.getCities();

        this.items = [
            { label: 'Chọn', command: (event) => this.onSelectGara(this.selectTable) },
            { label: 'Sửa', command: (event) => this.onSelectGaraUpdate(this.selectTable) },
        ];
        this.items = this.items.slice();
    }

    loadData() {
        this.searchGara();
        this.getCities();
    }

    customSearchFn(term, item) {
        term = term.toLocaleLowerCase();
        return item.city.toLocaleLowerCase().indexOf(term) > -1;
    }

    customSearchDistrictFn(term, item) {
        term = term.toLocaleLowerCase();
        return item.city.toLocaleLowerCase().indexOf(term) > -1;
    }

    onMouseOver(gara) {
        this.selectTable = gara;
    }

    onSelectGara(gara) {
        // console.log('onSelectGara: ', gara);
        this.addGaraToListExport(gara);
    }

    onDoubleClickGara(gara) {
        this.addGaraToListExport(gara);
        this.closeModal();
    }

    onSelectGaraUpdate(gara) {
        this.showCreateGara();
        // console.log('onSelectGaraUpdate: ', gara);
        if (gara.taxCode) {
        }
    }

    addGaraToListExport(gara) {
        const garaExist = _.find(this.garaExport, { id: gara.id })
        if (!garaExist) {
            this.garaExport.push(gara);
        }
    }

    searchGara() {
        this.firstLoadFrom = false;
        this.searchGaraParams.city = (this.searchCityModel == null || !this.searchCityModel.city) ? '' : this.searchCityModel.city;
        this.searchGaraParams.district = (this.searchDistrictModel == null
            || !this.searchDistrictModel.city) ? '' : this.searchDistrictModel.city;
        this.searchGaraParams.page = 0;
    }

    loadMore() {
        // console.log('loadMore...');
        this.isLoading = true;
        this.isScroll = true;
        if (this.searchGaraParams.page * this.searchGaraParams.size + 5 > this.countSearch) {
            this.isLoading = false;
            return;
        }
        this.searchGaraParams.page += 1;
        this.searchGaraParams.city = (this.searchCityModel == null || !this.searchCityModel.city) ? '' : this.searchCityModel.city;
        this.searchGaraParams.district = (this.searchDistrictModel == null
            || !this.searchDistrictModel.city) ? '' : this.searchDistrictModel.city;

    }

    searchReset() {
        this.searchGaraParams = {
            query: '',
            city: '',
            district: '',
            page: 0,
            size: 5,
        };
        this.searchModelReset();
        if (!this.firstLoadFrom) {
            this.searchGara();
        }
    }

    searchModelReset() {
        this.searchCityModel = {};
        this.searchDistrictModel = {};
    }

    getCities() {
        const searchKey = '';

    }

    onChangeCity() {
        this.getDistricts();
    }

    getDistricts() {
        this.searchGaraParams.city = this.searchCityModel ? this.searchCityModel.city : '';

    }

    showCreateGara() {
        this.isShowCreateGara = true;
    }
    closeCreateGara() {
        this.isShowCreateGara = false;
    }

    getEventSaveGara(event) {
        // Only save
        if (event.type === '1') {
            const newGara = event.newGara;
            this.garas.unshift(newGara);
            this.scrollToTopByElements();
        } else if (event.type === '2') {
            // close modal
            const newGara = event.newGara;
            this.closeCreateGara();
            this.garas.unshift(newGara);
            this.scrollToTopByElements();
        } else if (event.type === '3') {
            // close modal
            const oldGara = event.oldGara;
            const dataIndex = _.findIndex(this.garas, { id: oldGara.id });
            if (dataIndex >= 0) {
                // this.garas.splice(dataIndex, 1, oldGara);
                this.garas.splice(dataIndex, 1);
                this.garas.unshift(oldGara);
                this.scrollToTopByElements();
            }
            this.closeCreateGara();
        } else {
            this.closeCreateGara();
        }
    }

    closeModal() {
        // this.activeModal.dismiss({});
        this.activeModal.dismiss(this.garaExport);
    }

    scrollToTopByElements() {
        // const elementId = document.getElementById('scroll-to-top');
        // elementId.scrollTop = 0;
        this.scrolledContents.nativeElement.scrollTo({ left: 0, top: 0, behavior: 'smooth' });
    }

    showMessage(type, content) {
        this.msgs = [];
        this.msgs.push({ severity: type, detail: content });
        setTimeout(() => {
            this.msgs = [];
        }, 3000);
    }

}

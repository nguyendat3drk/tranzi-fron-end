import { Component, Input, Output, OnInit, EventEmitter, OnDestroy } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { catchError, debounceTime, map, switchMap } from 'rxjs/internal/operators';
import { of } from 'rxjs/index';
import { ConfirmationService } from 'primeng/api';
import { Message } from 'primeng/components/common/api';
import { MessageService } from 'primeng/components/common/messageservice';

import { DmContact } from '../../../../shared/models/dm-contact';
import { Address } from '../../../../shared/models/address';

import { DmContactService } from '../../../../shared/services/http/dm-contact.service';
import { AddressService } from '../../../../shared/services/http/address.service';
import { Utilities } from './../../../services/utilities.service';

import * as _ from 'lodash'

@Component({
    // tslint:disable-next-line:component-selector
    selector: 'bv-gara-create',
    templateUrl: './bv-gara-create.component.html',
    styleUrls: ['./bv-gara-create.component.scss'],
    providers: [MessageService, ConfirmationService],
})
export class BvGaraCreateComponent implements OnInit {

    @Output() isClose = new EventEmitter();
    firstLoadAddress: boolean = true;
    result: any;
    dmContact: DmContact;
    accidentPlace: Address;

    isUpdateGara: boolean;
    msgs: Message[] = [];

    // isRequiredName: boolean = false;
    // isRequiredTaxCode: boolean = false;
    isRequiredAddress: boolean = false;
    isRequiredEmail: boolean = false;

    banks: any[];
    bankBanchs: any[];
    searchBankModel: any;
    searchBankBanchModel: any;
    bankSearchLoading: boolean = false;

    // validate
    blTaxCode: boolean = true;
    blFormatTaxCode: boolean = true;
    blNameVN: boolean = true;
    blFormatPhoneNumber: boolean = true;
    blTEmail: boolean = true;
    blFormatEmail: boolean = true;
    blAddress: boolean = true;
    blFormatAccountNumber: boolean = true;

    isRequiredAccountNumber: boolean = false;
    blBank: boolean = true;
    blBranch: boolean = true;
    blAddressAuto: boolean = true;
    mesigeAddressAuto: string = 'Trường bắt buộc nhập';

    subscribeUpdateCurrentGara: any;

    constructor(
        private dmContactService: DmContactService,
        private addressService: AddressService,
        private confirmationService: ConfirmationService,
        private messageService: MessageService,
        private utilities: Utilities) {
    }

    ngOnInit() {
        this.dmContact = new DmContact();
        this.isUpdateGara = false;
        this.banks = [];
        this.bankBanchs = [];
        this.searchBankModel = {
            nameVN: '',
            id: 0,
        };
        this.searchBankBanchModel = {
            nameVN: '',
            id: 0,
            branchName: '',
        };
        this.loadData();
    }

    // tslint:disable-next-line:use-life-cycle-interface
    ngOnDestroy() {
        this.subscribeUpdateCurrentGara.unsubscribe();
    }

    loadData() {
        this.getBanks();
    }

    customSearchFn(term, item) {
        term = term.toLocaleLowerCase();
        return item.nameVN.toLocaleLowerCase().indexOf(term) > -1;
    }

    customSearchBankBranchFn(term, item) {
        term = term.toLocaleLowerCase();
        return item.branchName.toLocaleLowerCase().indexOf(term) > -1;
    }

    getBanks() {
        const searchKey = '';
    }

    onChangeBank() {
        this.getBankBranchs();
    }

    getBankBranchs() {
        const searchKey = (this.searchBankModel == null || !this.searchBankModel.nameVN) ? '' : this.searchBankModel.nameVN;
        const parentId = (this.searchBankModel == null || !this.searchBankModel.id) ? 0 : this.searchBankModel.id;
    }

    searchModelReset() {
        this.searchBankModel = {};
        this.searchBankBanchModel = {};
    }

    onCheckTaxCode() {
        const x = document.getElementById('ngb-typeahead-5');
        if (x) {
            x.style.display = 'block';
        }
        if (this.isUpdateGara) {
            return;
        }
        if (this.dmContact.taxCode) {
            if (this.utilities.checkNumber(this.dmContact.taxCode)) {
            } else {
                this.blTaxCode = true;
            }
        } else {
            this.blTaxCode = false;
        }
    }

    save() {
        if (this.onCheckValidaterequired('', '', 'save')) return;
        this.setAddress();
        this.setBankAndBranchName();
        // console.log('save gara: ', this.dmContact);
        if (this.isUpdateGara) {
            this.updateGara();
        } else {
            this.saveGara();
        }
    }
    validateNull() {
        const check = true;
        if (!this.dmContact || !this.dmContact.taxCode || !this.dmContact.nameVN ||
            !this.dmContact.email || !this.dmContact.address) {
            return false;
        }
        return check;
    }

    saveAndClose() {
        if (this.onCheckValidaterequired('', '', 'save')) return;
        // if (this.invalidateSubmit()) return;
        // if (!this.validateNull()) return;
        this.setAddress();
        this.setBankAndBranchName();
        // console.log('save and close gara: ', this.dmContact);
        if (this.isUpdateGara) {
            this.updateAndCloseGara();
        } else {
            this.saveAndCloseGara();
        }
    }

    reset() {
        this.dmContact = new DmContact();
        this.accidentPlace = new Address;
        this.accidentPlace.commune = '';
        this.accidentPlace.district = '';
        this.accidentPlace.city = '';
        this.searchModelReset();
        this.resetValidateRequired();
        this.searchAddressFormatter = (x: Address) => '';
    }

    resetValidateRequired() {
        this.isRequiredAddress = false;
        this.isRequiredEmail = false;
        this.blTaxCode = true;
        this.blFormatTaxCode = true;
        this.blNameVN = true;
        this.blFormatPhoneNumber = true;
        this.blTEmail = true;
        this.blFormatEmail = true;
        this.blAddress = true;
        this.blFormatAccountNumber = true;
        this.isRequiredAccountNumber = false;
        this.blBank = true;
        this.blBranch = true;
        this.blAddressAuto = true;
        this.mesigeAddressAuto = 'Trường bắt buộc nhập';
    }

    saveGara() {
    }
    viewAddress(event1, event2, event3) {
        if (event1 && event2 && event3) {
            return event1 + ', ' + event2 + ', ' + event3;
        } else {
            return '';
        }
    }
    saveAndCloseGara() {
    }

    updateGara() {
    }

    updateAndCloseGara() {
    }

    closePopupCreate() {
        this.isClose.emit({});
    }

    searchAddressFormatter = (x: Address) => `${x.commune}, ${x.district}, ${x.city}`;
    searchAddress = (text$: Observable<string>) =>
        text$.pipe(
            debounceTime(500),
            switchMap(text => {
                this.searchAddressFormatter = (x: Address) => `${x.commune}, ${x.district}, ${x.city}`;
                if (this.searchAddressFormatter) {

                }
                return this.addressService.search(text)
                    .pipe(
                        // map(x => x.map(y => {
                        //   return `${y.city}, ${y.district}, ${y.commune}`
                        // })),
                        catchError(() => of([])),
                )
            }),
        );

    setAddress() {
        this.dmContact.commune = this.accidentPlace.commune;
        this.dmContact.district = this.accidentPlace.district;
        this.dmContact.city = this.accidentPlace.city;
        this.dmContact.postCode = this.accidentPlace.postCode ? this.accidentPlace.postCode : this.dmContact.postCode;
    }

    setBankAndBranchName() {
        // this.dmContact.bankName = (this.searchBankModel == null || !this.searchBankModel.nameVN) ? '' : this.searchBankModel.nameVN;
        // this.dmContact.branchName = (this.searchBankBanchModel == null ||
        //     !this.searchBankBanchModel.branchName) ? '' : this.searchBankBanchModel.branchName;
        // TODO
        this.dmContact.bankName = (this.searchBankModel == null || !this.searchBankModel.nameVN) ? '' : this.searchBankModel.nameVN;
        this.dmContact.branchName = (this.searchBankBanchModel == null ||
            !this.searchBankBanchModel.nameVN) ? '' : this.searchBankBanchModel.nameVN;

        this.dmContact.bankId = (this.searchBankBanchModel == null ||
            !this.searchBankBanchModel.id) ? null : this.searchBankBanchModel.id;

        // if(this.dmContact.accountNumber) {
        //     this.dmContact.bankAccountNumber = this.dmContact.accountNumber;
        // }

    }

    convertDataBankModelToDisplayUpdateGara(data) {
        // this.searchBankModel.nameVN = data.bankName ? data.bankName : '';
        this.searchBankBanchModel.nameVN = data.nameVN ? data.nameVN : '';
        // this.searchBankBanchModel.branchName = data.branchName ? data.branchName : '';

        if (data.parentBankId) {
            // tslint:disable-next-line:prefer-const
            let bankObject = _.find(this.banks, { id: data.parentBankId });

            if (bankObject) {
                this.searchBankModel = {
                    nameVN: bankObject.nameVN,
                    id: bankObject.id,
                };
            }
        }
    }

    confirm(data) {
        this.confirmationService.confirm({
            message: 'Mã số thuế này đã được tạo trên hệ thống với tên: ' + data.nameVN + '. Bạn có muốn cập nhật gara này không?',
            accept: () => {
                this.isUpdateGara = true;
                this.dmContact = data;
                this.accidentPlace = new Address;
                this.accidentPlace.commune = data.commune;
                this.accidentPlace.district = data.district;
                this.accidentPlace.city = data.city;
                this.convertDataBankModelToDisplayUpdateGara(data);
            },
            reject: () => {
                this.dmContact.taxCode = '';
            },
        });
    }

    updateGaraFromSearch(data) {
        this.blAddressAuto = true;
        this.firstLoadAddress = false;
        this.isUpdateGara = true;
        this.dmContact = data;
        this.accidentPlace = new Address;
        this.accidentPlace.commune = data.commune;
        this.accidentPlace.district = data.district;
        this.accidentPlace.city = data.city;
        this.convertDataBankModelToDisplayUpdateGara(data);
    }

    showMessage(type, content) {
        this.msgs = [];
        this.msgs.push({ severity: type, detail: content });
        // this.messageService.add({severity: type, summary:'Service Message', detail: content});

        setTimeout(() => {
            this.msgs = [];
        }, 5000);
    }

    invalidateSubmit() {
        // this.isRequiredName = (!this.dmContact.nameVN) ? true : false;
        // this.isRequiredTaxCode = (!this.dmContact.taxCode) ? true : false;
        this.isRequiredAddress = (!this.dmContact.address) ? true : false;
        this.isRequiredEmail = (!this.dmContact.email) ? true : false;
        if (!this.dmContact || !this.dmContact.taxCode || !this.dmContact.nameVN || !this.dmContact.email || !this.dmContact.address) {
            // this.showMessage('error', 'Bạn phải nhập hết các trường bắt buộc.!');
            return true;
        } else if (!this.accidentPlace || !this.accidentPlace.commune || !this.accidentPlace.city || !this.accidentPlace.district) {
            this.showMessage('error', 'Định dạng địa chỉ không hợp lệ');
            return true;
        }
    }
    keyPress(event: any, type) {
        if (type && type === 'price') {
            const patternExtension = /[0-9]/;
            const inputChar = String.fromCharCode(event.charCode);
            if (event.keyCode !== 9 && !patternExtension.test(inputChar)) {
                event.preventDefault();
            }
        } else if (type && type === 'date') {
            const patternExtension = /[0-9\/]/;
            const inputChar = String.fromCharCode(event.charCode);
            if (event.keyCode !== 9 && !patternExtension.test(inputChar)) {
                event.preventDefault();
            }
        } else {
            const patternExtension = /[0-9\.]/;
            const inputChar = String.fromCharCode(event.charCode);
            if (event.keyCode !== 9 && !patternExtension.test(inputChar)) {
                event.preventDefault();
            }
        }
    }
    onCheckValidaterequired(key, event, eventcheck) {
        let check = false;
        if (event === 'isRequiredName' || eventcheck === 'save') {
            if (!this.dmContact.nameVN) {
                this.blNameVN = false;
                check = true;
            } else {
                this.blNameVN = true;
            }
        }
        if (event === 'isRequiredTaxCode' || eventcheck === 'save') {
            if (!this.dmContact.taxCode) {
                this.blTaxCode = false;
                check = true;
            } else {
                if (this.utilities.checkNumber(this.dmContact.taxCode)) {
                    this.blFormatTaxCode = true;
                } else {
                    this.blFormatTaxCode = false;
                    check = true;
                }
                this.blTaxCode = true;
            }
        }
        if (event === 'blFormatPhoneNumber' || eventcheck === 'save') {
            if (this.dmContact.phoneNumber && !this.utilities.checkNumber(this.dmContact.phoneNumber)) {
                this.blFormatPhoneNumber = false;
                check = true;
            } else {
                this.blFormatPhoneNumber = true;
            }
        }
        if (event === 'isRequiredEmail' || eventcheck === 'save') {
            if (!this.dmContact.email) {
                this.blTEmail = false;
                check = true;
            } else {
                // tslint:disable-next-line:max-line-length
                const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                // return re.test(String(event).toLowerCase());
                if (re.test(this.dmContact.email.toLowerCase())) {
                    this.blFormatEmail = true;
                } else {
                    this.blFormatEmail = false;
                    this.blFormatEmail = this.blFormatEmail;
                    check = true;
                }
                this.blTEmail = true;
            }
        }
        if (event === 'isRequiredblAddress' || eventcheck === 'save') {
            if (!this.dmContact.address) {
                this.blAddress = false;
                check = true;
            } else {
                this.blAddress = true;
            }
        }
        if (event === 'addressAuto' || eventcheck === 'save') {
            // this.firstLoadAddress = false;
            // if (eventcheck !== 'save') {
            //     if (this.accidentPlace && this.accidentPlace.city) {
            //         this.accidentPlace.city = null;
            //     }
            // }
            // if ((!this.accidentPlace || !this.accidentPlace.commune || !this.accidentPlace.city || !this.accidentPlace.district)) {
            //     const divToChange = document.getElementById('addressAutoID');
            //     // tslint:disable-next-line:quotemark
            //     if (divToChange && divToChange["value"] && divToChange["value"].length > 0) {
            //         this.blAddressAuto = false;
            //         this.mesigeAddressAuto = ' Không tìm thấy trong danh sách';
            //         check = true;
            //     } else {
            //         this.blAddressAuto = false;
            //         this.mesigeAddressAuto = 'Trường bắt buộc nhập';
            //         check = true;
            //     }
            // }
            this.firstLoadAddress = false;
            const divToChange = document.getElementById('addressAutoID');
        }
        if (event === 'accountNumber' || eventcheck === 'save') {
            if (this.dmContact.accountNumber && !this.utilities.checkNumber(this.dmContact.accountNumber)) {
                this.blFormatAccountNumber = false;
                check = true;
            } else {
                this.blFormatAccountNumber = true;
            }
            this.isRequiredAccountNumber = this.dmContact.accountNumber ? true : false;
        }
        if (event === 'isBankNameRequired' || event === 'isBranchNameRequired' || eventcheck === 'save') {
            const bankNameRequired = (this.searchBankModel == null ||
                !this.searchBankModel.nameVN) ? '' : this.searchBankModel.nameVN;
            // const branchNameRequired = (!this.searchBankBanchModel || this.searchBankBanchModel == null
            //     || !this.searchBankBanchModel.branchName) ? '' : this.searchBankBanchModel.branchName;
            // TODO
            const branchNameRequired = (!this.searchBankBanchModel || this.searchBankBanchModel == null
                || !this.searchBankBanchModel.nameVN) ? '' : this.searchBankBanchModel.nameVN;
            if (event === 'isBankNameRequired' || eventcheck === 'save') {
                if (!bankNameRequired) {
                    this.blBank = false;
                    if (this.dmContact.accountNumber) {
                        check = true;
                    }
                } else {
                    this.blBank = true;
                }
            }
            if (event === 'isBranchNameRequired' || eventcheck === 'save') {
                if (!branchNameRequired) {
                    this.blBranch = false;
                    if (this.dmContact.accountNumber) {
                        check = true;
                    }
                } else {
                    this.blBranch = true;
                }
            }
        }
        return check;
    }
    onAccidentChange($event?) {
        this.onCheckValidaterequired('', '', 'save');
        this.blAddressAuto = true;
    }
    blueBank() {
        if (this.dmContact.accountNumber) {
            this.blBank = false;
        } else {
            this.blBank = true;
        }
    }
    blueBranch() {
        if (this.dmContact.accountNumber) {
            this.blBranch = false;
        } else {
            this.blBranch = true;
        }
    }
    blurAddressAuto() {
        if (!this.accidentPlace) {
            this.blAddressAuto = false;
        }

    }
    blurAddressAuto2() {
        this.firstLoadAddress = false;
        if (!this.accidentPlace) {
            this.blAddressAuto = false;
        }
        const divToChange = document.getElementById('addressAutoIDtest');
        // tslint:disable-next-line:quotemark
        if (divToChange && divToChange["value"] && divToChange["value"].length > 0) {
            // tslint:disable-next-line:prefer-const
            let divToChange2 = document.getElementById('addressAutoID');
            if (divToChange2) {
                // tslint:disable-next-line:quotemark
                divToChange2.innerHTML = divToChange["value"];
            }
        }

    }
    blurAddressAuto3(event: any) {
        if (event) {
            this.firstLoadAddress = false;
            setTimeout(() => {
                // tslint:disable-next-line:prefer-const
                let check1 = document.getElementById('addressAutoID');
                if (check1) {
                    check1.focus();
                }
            }, 100);
            if (!this.accidentPlace) {
                this.blAddressAuto = false;
            }
            if (event.key) {
                // tslint:disable-next-line:prefer-const
                let divToChange2 = document.getElementById('addressAutoID');
                if (divToChange2) {
                    divToChange2.innerHTML = event.key;
                    divToChange2.focus();
                }
            }
        }
    }
}

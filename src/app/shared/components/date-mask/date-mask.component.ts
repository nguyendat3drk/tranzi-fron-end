import { Component, Input, OnInit, EventEmitter, Output, ViewChild } from '@angular/core';
import * as moment from 'moment';
import { NgForm } from '@angular/forms';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'bv-datemask',
  templateUrl: './date-mask.component.html',
  styleUrls: ['./date-mask.component.scss'],
})
export class BvDateMaskComponent implements OnInit {
  @ViewChild('dateMaskForm') public dateMaskForm: NgForm;
  @Input() datetime: Date;
  @Input() isRequired: boolean;
  @Input() isInValid: boolean;
  @Input() inValidMes: string;
  @Input() minDate: Date;
  @Input() maxDate: Date;
  @Input() nameInputDate: string;
  @Input() strDate: string;

  @Output() outDate = new EventEmitter();
  @Output() formatDate = new EventEmitter();
  @Output() onChangeDate = new EventEmitter();

  public checkFormatDate: boolean = true;
  maxDateCheck: Date = new Date('2900/09/09');
  minDateCheck: Date = new Date('1901/09/09');
  firstLoad: boolean = true;
  constructor() { }

  ngOnInit() {
    if (this.datetime) {
      this.strDate = ('0' + this.datetime.getDate()).slice(-2) + '' +
        ('0' + (this.datetime.getMonth() + 1)).slice(-2) + this.datetime.getFullYear();
    } else {
      this.strDate = null;
    }
    if (!this.nameInputDate) {
      this.nameInputDate = 'DateFixName'
    }
  }

  addYearToDate(date, yearInt) {
    if (!date) return new Date();
    const year = date.getFullYear();
    const month = date.getMonth();
    const day = date.getDate();
    return new Date(year + yearInt, month, day);
  }

  setdateText() {
    this.firstLoad = false;
    if (this.datetime) {
      this.checkFormatDate = true;
      this.strDate = ('0' + this.datetime.getDate()).slice(-2) + '' +
        ('0' + (this.datetime.getMonth() + 1)).slice(-2) + this.datetime.getFullYear();
    }
    this.outDate.emit(this.datetime);
    this.onChangeDate.emit(this.datetime);
    this.formatDate.emit(this.checkFormatDate);
  }
  changeDate() {
    this.firstLoad = false;
    this.datetime = null;
    if (this.strDate && this.strDate.length === 8) {
      this.checkFormatDate = true;
      this.datetime = moment(this.strDate, 'DDMMYYYY').toDate();
      const yearS = this.strDate.slice(4, 8);
      if (isNaN(this.datetime.getTime()) || Number(yearS) < 1000) {
        this.checkFormatDate = false;
      }
    } else if (this.strDate && this.strDate.length < 8 && this.strDate.length > 0) {
      this.checkFormatDate = false;
    }
    if (!this.strDate) {
      this.checkFormatDate = true;
    }
    this.outDate.emit(this.datetime);
    this.onChangeDate.emit(this.datetime);
    this.formatDate.emit(this.checkFormatDate);
  }
  clearDate() {
    this.strDate = null;
    this.checkFormatDate = true;
    this.datetime = null;
    this.isInValid = false;
    this.outDate.emit(this.datetime);
    this.onChangeDate.emit(this.datetime);
    this.formatDate.emit(this.checkFormatDate);
  }
}

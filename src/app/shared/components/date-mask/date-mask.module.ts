import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ModalModule } from 'ngx-bootstrap';
import { TranslateService, TranslateModule } from '@ngx-translate/core';
import { BvDateMaskComponent } from './date-mask.component';
import { NgxMaskModule } from 'ngx-mask';
import { CalendarModule } from 'primeng/calendar';
import { FormsModule } from '@angular/forms';

@NgModule({
  imports: [CommonModule, ModalModule.forRoot(), NgxMaskModule, CalendarModule, FormsModule, TranslateModule],
  declarations: [
    BvDateMaskComponent,
  ],
  providers: [TranslateService],
  exports: [BvDateMaskComponent]})
export class BvDateMaskModule { }

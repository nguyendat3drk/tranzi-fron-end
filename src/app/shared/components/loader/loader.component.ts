import { Component, OnInit, HostBinding } from '@angular/core';
import { LoaderService } from '../../services/loader.service';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'app-loader',
  templateUrl: './loader.component.html',
  styleUrls: ['./loader.component.css'],
})
export class LoaderComponent implements OnInit {

  @HostBinding('class.is-open')
  isOpen = false;

  constructor(
    private loaderService: LoaderService,
  ) { }

  ngOnInit() {
    this.loaderService.change.subscribe(isOpen => {
      this.isOpen = isOpen;
    });
  }
}

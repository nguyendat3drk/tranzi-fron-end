import { Component, Input, OnInit, Injectable, ViewChild } from '@angular/core';
import { ConfirmService } from './ConfirmService';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { TranslateService } from '@ngx-translate/core';

const KEY_ESC = 27;

@Component({
  selector: 'ngx-app-confirm',
  templateUrl: 'confirm-dialog.html',
  styleUrls: ['confirm-dialog.css'],
})
@Injectable()
export class ConfirmComponent implements OnInit {
  public title: string;
  public message: string;
  public okText: string;
  public cancelText: string;
  public closeText: string;
  public isShow: boolean = false;

  @Input()
  confirmDialog: boolean = true;

  @ViewChild('staticModal') public staticModal: ModalDirective;
  private _defaults = {
    title: 'Thông báo',
    message: 'Confirm the operation?',
    cancelText: 'Không',
    okText: 'Có',
    closeText: 'Close',
  };
  private _confirmElement: any;
  private _cancelButton: any;
  private _okButton: any;
  private _closeButton: any;
  private _offButton: any;

  constructor(confirmService: ConfirmService, public translateService: TranslateService) {
    // assign a function to the property activate of ConfirmService.
    // After this, calling activate on ConfirmService will cause the function activate
    // from ConfirmComponent to be executed.
    confirmService.activate = this.activate.bind(this);
  }

  private setLabels(message = this._defaults.message, title = this._defaults.title) {
    this.title = title;
    this.message = message;
    this.okText = this._defaults.okText;
    this.cancelText = this._defaults.cancelText;
    this.closeText = this._defaults.closeText;
    this.translateService.get('claim.yes').subscribe(
      data => {
        this.okText = data;
      });
    this.translateService.get('claim.no').subscribe(
      data => {
        this.cancelText = data;
      });
    this.translateService.get('wille-bus.common.close').subscribe(
      data => {
        this.closeText = data;
      });
  }

  activate(message = this._defaults.message, title = this._defaults.title) {
    this.setLabels(message, title);
    // console.log("in activate");

    const promise = new Promise<boolean>(resolve => {
      this.show(resolve);
      this.isShow = true;
    });
    return promise;
  }

  private show(resolve: (boolean) => any) {
    document.onkeyup = null;

    const negativeOnClick = (e: any) => resolve(false);
    const positiveOnClick = (e: any) => resolve(true);

    if (!this._confirmElement || !this._cancelButton || !this._okButton || !this._closeButton || !this._offButton) {
      return;
    }
    this._confirmElement.style.opacity = 0;
    this._confirmElement.style.zIndex = 9999;

    this._cancelButton.onclick = ((e: any) => {
      e.preventDefault();
      if (!negativeOnClick(e)) {
        this.hideDialog();
      }
    });

    this._okButton.onclick = ((e: any) => {
      e.preventDefault();
      if (!positiveOnClick(e)) {
        this.hideDialog();
      }
    });

    this._closeButton.onclick = ((e: any) => {
      e.preventDefault();
      if (!negativeOnClick(e)) {
        this.hideDialog();
      }
    });

    this._offButton.onclick = ((e: any) => {
      e.preventDefault();
      if (!negativeOnClick(e)) {
        this.hideDialog();
      }
    });
    // this._confirmElement.onclick = () => {
    //   this.hideDialog();
    //   return negativeOnClick(null);
    // };

    document.onkeyup = (e: any) => {
      if (e.which === KEY_ESC) {
        this.hideDialog();
        return negativeOnClick(null);
      }
    };


    this._confirmElement.style.opacity = 1;
    this._confirmElement.style.display = 'block';
  }

  close() {
    this.hideDialog();
  }

  private hideDialog() {
    document.onkeyup = null;
    this._confirmElement.style.opacity = 0;
    window.setTimeout(() => this._confirmElement.style.zIndex = -1, 400);
    this.isShow = false;
  }

  ngOnInit(): any {
    this._confirmElement = document.getElementById('confirmationModal');
    this._cancelButton = document.getElementById('cancelButton');
    this._okButton = document.getElementById('okButton');
    this._closeButton = document.getElementById('close-x');
    this._offButton = document.getElementById('offButton');
  }
}

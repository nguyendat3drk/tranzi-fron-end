import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ModalModule } from 'ngx-bootstrap';
import { ConfirmComponent } from './ConfirmComponent';
import { ConfirmService } from './ConfirmService';
import { TranslateService } from '@ngx-translate/core';

@NgModule({
  imports: [CommonModule, ModalModule.forRoot()],
  declarations: [
    ConfirmComponent,
  ],
  providers: [ConfirmService, TranslateService],
  exports: [ConfirmComponent]})
export class ConfirmDialogModule { }

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BvWorkBasketPersonalComponent } from './bv-work-basket-personal.component';

describe('BvWorkBasketPersonalComponent', () => {
  let component: BvWorkBasketPersonalComponent;
  let fixture: ComponentFixture<BvWorkBasketPersonalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BvWorkBasketPersonalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BvWorkBasketPersonalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BvWorkBasketAssessorsComponent } from './bv-work-basket-assessors.component';

describe('BvWorkBasketAssessorsComponent', () => {
  let component: BvWorkBasketAssessorsComponent;
  let fixture: ComponentFixture<BvWorkBasketAssessorsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BvWorkBasketAssessorsComponent ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BvWorkBasketAssessorsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

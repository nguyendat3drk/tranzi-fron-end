import { Component, OnInit, ViewChild } from '@angular/core';
import { WorkBasket } from '../../../../shared/models/work-basket';
import { WorkBasketService } from '../../../../shared/services/http/work-basket.service';
import { LoaderService } from '../../../../shared/services/loader.service';
import { MenuItem } from 'primeng/primeng';
import { NgForm } from '@angular/forms';
import { Utilities } from '../../../../shared/services/utilities.service';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'bv-work-basket-assessors',
  templateUrl: './bv-work-basket-assessors.component.html',
  styleUrls: ['./bv-work-basket-assessors.component.scss'],
})
export class BvWorkBasketAssessorsComponent implements OnInit {
  @ViewChild('workBasketAssessors') public form: NgForm;
  searchParams: any = {
    claimNumber: null,
    plateNumber: '',
  }

  listDataBox1: WorkBasket[]
  listDataBox2: WorkBasket[]
  listDataBox3: WorkBasket[]
  listDataBox4: WorkBasket[]
  selectedObject: WorkBasket
  items: MenuItem[]
  cols: any[]

  workConfirm = {
    Reject: 0,
    Accept: 1,
  }

  constructor(
    private workBasketService: WorkBasketService,
    private loaderService: LoaderService,
    private utilities: Utilities) {
  }

  ngOnInit() {
    this.getAndUpdateDatatable();
  }

  rejectClick(obj: WorkBasket) {
    const data = {
      claimNumber: obj.claimNumber,
      status: this.workConfirm.Reject,
    }
    this.workBasketService.assessorsRejectDocument(data)
      .then((result: any) => {
        this.getAndUpdateDatatable()
        this.loaderService.hide()
      })
      .catch(error => {
        this.utilities.showError(error.error.message)
        this.loaderService.hide()
      })
  }

  searchClick() {
    if (this.form.valid) {
      this.getAndUpdateDatatable();
    }
  }

  onlyInputNumber(event) {
    if (!this.utilities.checkNumber(event.key)) {
      event.preventDefault();
    }
  }

  allowRightClick(contextMenu) {
    const isValid = this.selectedObject && this.selectedObject.taskStatus === 1
    if (!isValid) {
      if (contextMenu) contextMenu.hide()
      this.selectedObject = null
    }
  }

  private getAndUpdateDatatable() {
    this.loaderService.show()
    this.initialData()
    this.searchParams.plateNumber = this.utilities.removeSpecialCharForPlateNumber(this.searchParams.plateNumberTemp)
    this.workBasketService.searchForAssessors(this.searchParams).then((result: any) => {
      const workBasets = result as Array<WorkBasket>
      if (!workBasets || workBasets.length === 0) {
        this.loaderService.hide()
        return
      }

      this.update4List(workBasets)
      this.refreshDatatable()
      this.loaderService.hide()
    })
      .catch(error => {
        this.loaderService.hide()
      })
  }

  private refreshDatatable() {
    this.listDataBox1 = this.listDataBox1.slice();
    this.listDataBox2 = this.listDataBox2.slice();
    this.listDataBox3 = this.listDataBox3.slice();
    this.listDataBox4 = this.listDataBox4.slice();
    this.items = this.items.slice();
  }

  private update4List(workBasets) {
    workBasets.forEach((item) => {
      switch (item.group) {
        case 1:
          this.listDataBox1.push(item)
          break
        case 2:
          this.listDataBox2.push(item)
          break;
        case 3:
          this.listDataBox3.push(item)
          break
        case 4:
          this.listDataBox4.push(item)
          break
      }
    });
  }

  private initialData() {
    this.selectedObject = new WorkBasket()
    this.listDataBox1 = []
    this.listDataBox2 = []
    this.listDataBox3 = []
    this.listDataBox4 = []
    this.items = [
      { label: 'Từ chối', icon: 'fa fa-times', command: (event) => this.rejectClick(this.selectedObject) },
    ]
  }
}

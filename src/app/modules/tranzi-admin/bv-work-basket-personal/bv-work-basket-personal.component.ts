import { Component, OnInit } from '@angular/core';
import { Utilities } from '../../../shared/services/utilities.service';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'bv-work-basket-personal',
  templateUrl: './bv-work-basket-personal.component.html',
  styleUrls: ['./bv-work-basket-personal.component.scss'],
})
export class BvWorkBasketPersonalComponent implements OnInit {
  hasRoleAssessors: boolean
  hasRoleLeader: boolean
  hasRoleZone: boolean
  constructor(private utilities: Utilities) { }

  ngOnInit() {
    this.hasRoleLeader = this.utilities.isLeader()
    this.hasRoleZone = this.utilities.isZoneLead()
    this.hasRoleAssessors = this.utilities.isAssessors()
  }
}

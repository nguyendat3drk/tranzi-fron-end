import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BvWorkBasketZoneComponent } from './bv-work-basket-zone.component';

describe('BvWorkBasketZoneComponent', () => {
  let component: BvWorkBasketZoneComponent;
  let fixture: ComponentFixture<BvWorkBasketZoneComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BvWorkBasketZoneComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BvWorkBasketZoneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

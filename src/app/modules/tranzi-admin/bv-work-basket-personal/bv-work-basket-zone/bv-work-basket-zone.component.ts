import { Component, OnInit, ViewChild } from '@angular/core';
import { LoaderService } from '../../../../shared/services/loader.service';
import { ProductService } from '../../../../shared/services/http/product.service';
import { MessageService } from 'primeng/components/common/messageservice';
import { WorkBasket } from '../../../../shared/models/work-basket';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import * as moment from 'moment';
import { Utilities } from '../../../../shared/services/utilities.service';
@Component({
  // tslint:disable-next-line:component-selector
  selector: 'bv-work-basket-zone',
  templateUrl: './bv-work-basket-zone.component.html',
  styleUrls: ['./bv-work-basket-zone.component.scss'],
})
export class BvWorkBasketZoneComponent implements OnInit {
  @ViewChild('workBasketZone') public form: NgForm;
  listDataTableZone: WorkBasket[] = [];
  listDataService: WorkBasket[] = [];
  listDataServices: WorkBasket[] = [];
  listDataWorks: WorkBasket[] = [];
  listDataWork: WorkBasket[] = [];
  fromDate: Date;
  fromDateSend: string = '';
  toDateSend: string = '';
  toDate: Date;
  plateNumber: string = '';
  type: string = '';
  checkPlateNumber: boolean = true;
  isButtonDisabledPC: boolean = false;
  formatfromDateNgbDate: boolean = true;
  blfromDate: boolean = false;
  strfromDate = '';
  formattoDateNgbDate: boolean = true;
  bltoDate: boolean = false;
  strtoDate = '';
  checkBeforeSearch: boolean = true;
  record: any;
  selectRowId: WorkBasket = new WorkBasket();
  claimNumbers: string = '';
  firstFromDate: boolean = true;
  firstToDate: boolean = true;
  constructor(private loaderService: LoaderService, private productService: ProductService, private ultilities: Utilities,
    private router: Router, private messageService: MessageService) { }

  ngOnInit() {
    this.searchZone();
  }
  // convertDate trước khi gửi
  toConvertDate(date: Date, time?: Date): string {
    return `${date.getDate()}/${date.getMonth() + 1}/${date.getFullYear()}`;
  }
  workPc() {
    // this.router.navigate(['/bao-viet/division-job/']);
    const claimId = this.selectRowId.claimId;
    if (claimId) {
      this.router.navigate(['/bao-viet/division-job/' + claimId]);
    }
  }
  getRowId(event) {
    this.record = event.data.taskStatus;
    this.selectRowId = event.data;
    if (event.data.claimId) {
      this.isButtonDisabledPC = true;
    }
  }

  get fromDateNgbDate() {
    if (this.fromDate instanceof Date) {
      return {
        year: this.fromDate.getFullYear(),
        month: this.fromDate.getMonth() + 1,
        day: this.fromDate.getDate(),
      }
    }
  }
  get toDateNgbDate() {
    if (this.fromDate instanceof Date) {
      return {
        year: this.fromDate.getFullYear(),
        month: this.fromDate.getMonth() + 1,
        day: this.fromDate.getDate(),
      }
    }
  }
  // button search trưởng zone
  searchZone() {
    if (this.fromDate) {
      this.fromDateSend = this.toConvertDate(this.fromDate);
    } else {
      this.fromDateSend = '';
    }
    if (this.isInvalidDate('', 'search')) return;
    if (this.toDate) {
      this.toDateSend = this.toConvertDate(this.toDate);
    } else {
      this.toDateSend = '';
    }
    const plateNumberRemovedSpecialChar = this.ultilities.removeSpecialCharForPlateNumber(this.plateNumber)
    this.type = !this.type ? '' : this.type
    this.claimNumbers = !this.claimNumbers ? '' : this.claimNumbers
    this.loaderService.show();
    this.initialData();
  }
  private refreshDatatable() {
    this.listDataServices = this.listDataServices.slice();
    this.listDataWorks = this.listDataWorks.slice();
  }
  private initialData() {
    this.listDataServices = [];
    this.listDataWorks = [];
  }
  private update2List(workZone) {
    workZone.forEach((item) => {
      switch (item.group) {
        case 1:
          this.listDataServices.push(item)
          break
        case 2:
          this.listDataWorks.push(item)
          break;
      }
    });
  }
// check number input form
keyPress(event: any, type) {
  if (type && type === 'price') {
    const patternExtension = /[0-9]/;
    const inputChar = String.fromCharCode(event.charCode);
    if (event.keyCode !== 9 && !patternExtension.test(inputChar)) {
      event.preventDefault();
    }
  }
}
  validatePlateNumber() {
    this.checkPlateNumber = true;
    let check = 1;
    if (this.plateNumber) {
      // const alphaExp = /^[0-9a-zA-Z]+\-+[0-9a-zA-Z]+$/;
      if (this.plateNumber) {
        if (this.plateNumber.length <= 20) {
          check = 1;
        } else {
          check = 0;
        }
      }
      if (check === 0) {
        this.checkPlateNumber = false;
      } else {
        // this.claim.policyInforPlateNumber = this.claim.policyInforPlateNumber.toUpperCase();
      }
    }
  }

  private isFromDateGreatThanToDate() {
    if (this.toDate && this.fromDate) {
      const d1 = moment(this.fromDate)
      const d2 = moment(this.toDate)
      return d1.isAfter(d2)
    }
  }
  isInvalidDate(field, event) {
    let check = false;
    this.blfromDate = false;
    this.strfromDate = '';
    if (this.toDate && this.fromDate) {
      const fromDateS = moment(this.fromDate);
      const toDates = moment(this.toDate);
      if (fromDateS.isAfter(toDates)) {
        if (field === 'fromDate' || event === 'search' || !this.firstFromDate) {
          this.firstFromDate = false;
          this.strfromDate = 'Từ ngày không được lớn hơn đến ngày';
          this.blfromDate = true;
          check = true;
        }
      }
    }
    this.bltoDate = false;
    if (this.toDate && this.fromDate) {
      const fromDateS = moment(this.fromDate);
      const toDates = moment(this.toDate);
      if (fromDateS.isAfter(toDates)) {
        if (field === 'toDate' || event === 'search' || !this.firstToDate) {
          this.firstToDate = false;
          this.strtoDate = 'Từ ngày không được lớn hơn đến ngày';
          this.bltoDate = true;
          check = true;
        }
      }
    }
    return check;
  }
  viewDateAMPM(event) {
    if (event) {
      const dt = moment(event, 'DD/MM/YYYY HH:mm:ss').toDate();
      return dt;
    }
    return null;
  }
  changeColor(value) {
    // console.log(value)
    if (value === 1) {
      return '#007BC5';
    } else if (value === 2) {
      return '#CBA34D';
    } else {
      return 'rgb(75, 71, 74)';
    }
  }
}

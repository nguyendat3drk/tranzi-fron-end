import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BvWorkBasketLeaderComponent } from './bv-work-basket-leader.component';

describe('BvWorkBasketLeaderComponent', () => {
  let component: BvWorkBasketLeaderComponent;
  let fixture: ComponentFixture<BvWorkBasketLeaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BvWorkBasketLeaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BvWorkBasketLeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

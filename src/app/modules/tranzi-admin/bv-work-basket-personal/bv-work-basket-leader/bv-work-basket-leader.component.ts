import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { LoaderService } from '../../../../shared/services/loader.service';
import * as moment from 'moment';
import { UserTypes } from '../../../../shared/models/userTypes.enum';
import { Utilities } from '../../../../shared/services/utilities.service';
import { SharedService } from '../../../../shared/services/http/shared.service';
import { WorkBasketService } from '../../../../shared/services/http/work-basket.service';
import { WorkBasket } from '../../../../shared/models/work-basket';
import { Router } from '@angular/router';
import { Page } from '../../../../shared/models/page';
import { NgForm } from '@angular/forms';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'bv-work-basket-leader',
  templateUrl: './bv-work-basket-leader.component.html',
  styleUrls: ['./bv-work-basket-leader.component.scss'],
})
export class BvWorkBasketLeaderComponent implements OnInit {
  @ViewChild('workBasketLeader') public form: NgForm;
  params = {
    fromDate: null,
    toDate: null,
    type: null,
    plateNumber: null,
    plateNumberTemp: null,

    claimNumber: null,
    buId: null,
    employeeRole: null,
    employeeId: null,
    page: 0,
    size: 10,
  }

  data: Page<WorkBasket>
  listData: WorkBasket[]
  businessUnits = []
  staffTypes = [
    { id: UserTypes.GDV, name: 'Giám định viên' },
    { id: UserTypes.BTV, name: 'Bồi thường viên' },
    { id: UserTypes.TZ, name: 'Trường zone' },
  ]
  employees = []

  constructor(private loaderService: LoaderService
    , private workBasketService: WorkBasketService
    , private sharedService: SharedService
    , private ultilities: Utilities
    , private router: Router) { }

  ngOnInit() {
    this.loadDropdownData()
    this.initialData()
    this.search('search')
  }

  btnSearchClick() {
    if (this.form.valid && !this.isFromDateGreatThanToDate()) {
      this.initialData()
      this.search('search')
    }
  }

  loadMore() {
    if (this.params.page === (this.data.metadata.totalPage - 1)) {
      return
    }
    this.params.page += 1
    this.search('')
  }

  getMessageDate() {
    const isError = this.isFromDateGreatThanToDate()
    if (!isError) return ''

    return 'Từ ngày không được lớn hơn đến ngày'
  }

  onlyInputNumber(event) {
    if (!this.ultilities.checkNumber(event.key)) {
      event.preventDefault();
    }
  }

  cboBUchange() {
    this.params.employeeId = null
    this.getEmployees()
  }

  colorRowIsRed(status) {
  }

  onBlurClaimNumber() {
    if (!this.params.claimNumber) return

    if (!this.ultilities.checkNumber(this.params.claimNumber)) {
      this.params.claimNumber = '';
    }
    if (this.params.claimNumber.length > 10) {
      this.params.claimNumber = this.params.claimNumber.slice(0, 10);
    }
  }

  private search(event) {
    if (event === 'search') {
      this.listData = [];
      this.listData = this.listData.slice()
    }
    this.loaderService.show()
    this.params.plateNumber = this.ultilities.removeSpecialCharForPlateNumber(this.params.plateNumberTemp)
    this.workBasketService.searchForLeader(this.params).then((result: any) => {
      this.data = result
      if (!this.data || this.data.contents.length === 0) {
        this.loaderService.hide()
        return
      }

      this.updateListData(this.data.contents)
      this.loaderService.hide()
    })
      .catch(error => {
        this.loaderService.hide()
      })
  }

  private getEmployees() {
    
  }

  private updateListData(data) {
    this.listData = [...this.listData, ...data]
  }

  private loadDropdownData() {
    this.sharedService.getAllBusinessUnit().subscribe(x => this.businessUnits = x);
  }

  private isFromDateGreatThanToDate() {
    const fromDate = this.params.fromDate;
    const toDate = this.params.toDate;
    if (toDate && fromDate) {
      const d1 = moment(fromDate)
      const d2 = moment(toDate)
      return d1.isAfter(d2)
    }
  }

  private initialData() {
    this.data = new Page<WorkBasket>()
    this.params.page = 0
    this.listData = []
    this.listData = this.listData.slice();
  }
  convertDate(event, type) {
    if (type === 'todate') {
      this.params.toDate = this.ultilities.convertDateToStringDateVN(event)
    }else if (type === 'fromdate') {
      this.params.fromDate = this.ultilities.convertDateToStringDateVN(event)
    }
  }
}

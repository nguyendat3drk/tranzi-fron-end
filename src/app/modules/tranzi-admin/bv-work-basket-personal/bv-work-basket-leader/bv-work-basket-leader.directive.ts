import { Directive, ElementRef, HostListener, Input, Output, EventEmitter } from '@angular/core';

@Directive({
  // tslint:disable-next-line:directive-selector
  selector: '[BvWorkBasketLeaderDirective]',
})
export class BvWorkBasketLeaderDirective {

  constructor(private element: ElementRef) { }

  @Input('BvWorkBasketLeaderDirective') type;
  @Output()
  public onLoadmore: EventEmitter<any> = new EventEmitter<any>();

  @HostListener('scroll')
  public onScroll() {

    const scrolled = this.element.nativeElement.scrollTop;
    const height = this.element.nativeElement.scrollHeight;
    const offsetHeight = this.element.nativeElement.offsetHeight;

    if (this.type === 'bottom') {
      if (height - scrolled - offsetHeight === 0) {
        this.onLoadmore.emit();
      }
    }
  }
}

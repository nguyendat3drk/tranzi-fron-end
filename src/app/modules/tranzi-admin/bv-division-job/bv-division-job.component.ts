import { Component, OnInit } from '@angular/core';
// tslint:disable-next-line:quotemark
import { Router, ActivatedRoute } from "@angular/router";
import * as _ from 'lodash';

import { MenuItem } from 'primeng/primeng';

import { WorkAssignService } from '../../../shared/services/http/work-assign.service';
import { LoaderService } from '../../../shared/services/loader.service';
import { MessageService } from 'primeng/components/common/messageservice';
import { Utilities } from '../../../shared/services/utilities.service';
import { UserTypes } from '../../../shared/models/userTypes.enum';

export const SURVEYOR_STATUS = ['Ngoại tuyến', 'Trực tuyến', 'Mất kết nối'];
declare const google: any;

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'bv-division-job',
  templateUrl: './bv-division-job.component.html',
  styleUrls: ['./bv-division-job.component.scss'],
})
export class BvDivisionJobComponent implements OnInit {

  claimId: number;
  surveyorSearchList: any;
  surveyorAssignList: any;

  isSelectedSurveyor: boolean;
  isDisplayAssignSurveyor: boolean;
  isDisplaySurveyorSearch: boolean;
  selectedSurveyor: any;
  workAssignType: string;

  searchQuery: {
    latitude: number,
    longitude: number,
    distance: number,
    distanceTmp: number,
    status: string,
  }

  assignSurveyorRequest: any;

  isZoneLead: boolean;
  incidentInfo: any;

  subscribeParams: any;
  items: MenuItem[];

  selectedSurveyorCm: any;
  distanceRadio: any;

  map: any
  marker: any

  keyPressOnlyNumber(event: any) {
    const pattern = /[0-9]/;

    // tslint:disable-next-line:prefer-const
    let inputChar = String.fromCharCode(event.charCode);
    if (event.keyCode !== 8 && !pattern.test(inputChar)) {
      event.preventDefault();
    }
  }

  constructor(
    private loaderService: LoaderService,
    private messageService: MessageService,
    private workAssignService: WorkAssignService,
    private ultilities: Utilities,
    private router: Router,
    private activeRouter: ActivatedRoute,
  ) {
    this.isSelectedSurveyor = false;
    this.isDisplayAssignSurveyor = false;
    this.isDisplaySurveyorSearch = false;
    this.searchQuery = {
      latitude: null,
      longitude: null,
      distance: null,
      distanceTmp: null,
      status: '',
    };
    this.workAssignType = '1';
    this.isZoneLead = false;
    this.incidentInfo = {};
    this.surveyorSearchList = [];
    this.surveyorAssignList = [];
    this.assignSurveyorRequest = {};
    this.distanceRadio = 'km';
  }

  ngOnInit() {

    this.subscribeParams = this.activeRouter.params.subscribe((params) => {
      const claimId = <any>params.claimId;
      if (claimId) {
        this.claimId = claimId;
        this.fetchData();
      } else {
        this.loaderService.show();
      }
    });
    this.checkRoleInit();

    this.items = [
      { label: 'Chọn', command: (event) => this.surveyorSearchSelect(event) },
    ];
  }

  initialGoogleMap(address, latitude, longitude) {
    this.map = new google.maps.Map(document.getElementById('map'), {
      zoom: 15,
      center: { lat: latitude, lng: longitude },
    })
    this.marker = new google.maps.Marker({
      position: { lat: latitude, lng: longitude },
      map: this.map,
      title: address,
    })
  }

  // tslint:disable-next-line:use-life-cycle-interface
  ngOnDestroy() {
    this.subscribeParams.unsubscribe();
  }

  checkRoleInit() {
    const checkRoleId = this.ultilities.getRoleId();
    if (checkRoleId && checkRoleId === UserTypes.TZ) {
      this.isZoneLead = true;
    } else {
      // redirect to page-forbidden works
      this.router.navigate(['/bao-viet/page-forbidden']);
    }
  }

  fetchData() {
    this.getIncidentInfo();
  }

  getIncidentInfo() {
    this.loaderService.show();
    this.workAssignService.getIncidentInfo(this.claimId)
      .then(result => {
        this.loaderService.hide();
        if (result) {
          this.incidentInfo = result || {};
          this.incidentInfo.address = this.incidentInfo.inspectionRealPlaceDetail.concat(' - ', this.incidentInfo.inspectionRealPlace);
          this.searchQuery.latitude = this.incidentInfo.latitude;
          this.searchQuery.longitude = this.incidentInfo.longitude;
          this.initialGoogleMap(this.incidentInfo.address,
            this.incidentInfo.latitude, this.incidentInfo.longitude);
        }
      })
      .catch(error => {
        this.loaderService.hide();
        this.incidentInfo = {};
      });
  }

  surveyorSearchSelect(event) {
    // console.log('surveyorSearchSelect: ', event);
    this.isSelectedSurveyor = true;
    this.selectedSurveyor = event.data;
  }

  searchSurveyor() {
    if (this.searchQuery.distanceTmp && this.distanceRadio === 'km') {
      this.searchQuery.distance = this.searchQuery.distanceTmp * 1000;
    } else if (this.searchQuery.distanceTmp && this.distanceRadio === 'm') {
      this.searchQuery.distance = this.searchQuery.distanceTmp;
    } else {
      this.searchQuery.distance = null;
    }
    this.surveyorAssignList = [];
    this.isDisplayAssignSurveyor = false;

    this.loaderService.show();
    this.workAssignService.searchSurveyor(this.searchQuery)
      .then(result => {
        this.loaderService.hide();
        this.isDisplaySurveyorSearch = true;
        this.isSelectedSurveyor = false;
        if (result) {
          // tslint:disable-next-line:prefer-const
          let surveyorSearchListTmp = <any>result || [];
          _.forEach(surveyorSearchListTmp).map(surveyor => {
            // status
            if (surveyor.status == null) {
              surveyor.statusStr = '';
            } else if (surveyor.status === 0) {
              surveyor.statusStr = SURVEYOR_STATUS[0];
            }else if (surveyor.status === 1) {
              surveyor.statusStr = SURVEYOR_STATUS[1];
            } else if (surveyor.status === 2) {
              surveyor.statusStr = SURVEYOR_STATUS[2];
            }
            surveyor.workDone = (surveyor.workDone) ? surveyor.workDone : 0;
            surveyor.workPending = (surveyor.workPending) ? surveyor.workPending : 0;
          });

          this.surveyorSearchList = surveyorSearchListTmp;
        }
      })
      .catch(error => {
        this.isDisplaySurveyorSearch = true;
        this.loaderService.hide();
        this.surveyorSearchList = [];
      });
  }

  assignSurveyor() {

    this.setDataToSubmitAssign();

    this.loaderService.show();
    this.workAssignService.assignSurveyor(this.assignSurveyorRequest)
      .then(result => {
        this.loaderService.hide();
        if (result) {
          this.isDisplaySurveyorSearch = false;
          this.isDisplayAssignSurveyor = true;
          this.surveyorAssignList = [...this.surveyorAssignList, result];
          this.messageService.add({
            detail: 'Gán việc cho Giám định viên thành công',
            severity: 'success',
            summary: 'Thông báo',
          });
        }
      })
      .catch(error => {
        this.loaderService.hide();
        this.surveyorAssignList = [];
        this.messageService.add({
          detail: 'Xảy ra lỗi khi gán việc cho Giám định viên',
          severity: 'error',
          summary: 'Thông báo',
        });
      });
  }

  removeAssignSurveyor() {

    this.setDataToSubmitAssign();

    this.loaderService.show();
    this.workAssignService.removeAssignSurveyor(this.assignSurveyorRequest)
      .then(result => {
        this.loaderService.hide();
        if (result) {
          this.isDisplaySurveyorSearch = false;
          this.isDisplayAssignSurveyor = false;
          this.surveyorAssignList = [];
          this.messageService.add({
            detail: 'Bỏ gán việc cho Giám định viên thành công',
            severity: 'success',
            summary: 'Thông báo',
          });
        }
      })
      .catch(error => {
        this.loaderService.hide();
        this.isDisplayAssignSurveyor = false;
        this.messageService.add({
          detail: 'Có lỗi xảy ra khi bỏ gán việc cho Giám định viên',
          severity: 'error',
          summary: 'Thông báo',
        });
      });
  }

  setDataToSubmitAssign() {
    this.assignSurveyorRequest.claimId = this.claimId;
    this.assignSurveyorRequest.employeeId = this.selectedSurveyor.employeeId;
    this.assignSurveyorRequest.employeeName = this.selectedSurveyor.employeeName;

    this.assignSurveyorRequest.zoneLeadId = this.ultilities.getEmployeeId();
    this.assignSurveyorRequest.zoneLeadName = this.ultilities.getFullName();

    // tslint:disable-next-line:prefer-const
    let workAssignType = (this.workAssignType) ? this.workAssignType : '1';
    // tslint:disable-next-line:radix
    this.assignSurveyorRequest.taskType = parseInt(workAssignType);
  }



}

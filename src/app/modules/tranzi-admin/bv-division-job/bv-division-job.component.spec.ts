import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BvDivisionJobComponent } from './bv-division-job.component';

describe('BvDivisionJobComponent', () => {
  let component: BvDivisionJobComponent;
  let fixture: ComponentFixture<BvDivisionJobComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BvDivisionJobComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BvDivisionJobComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

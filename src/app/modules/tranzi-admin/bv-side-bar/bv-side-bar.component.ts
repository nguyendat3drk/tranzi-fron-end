import { Component, ElementRef, HostListener } from '@angular/core';
import { Router } from '@angular/router';
import { Utilities } from '../../../shared/services/utilities.service';
// import {GlobalState} from '../../../global.state';
// import {layoutSizes} from '../../../theme';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'bv-side-bar',
  templateUrl: './bv-side-bar.component.html',
  styleUrls: ['./bv-side-bar.component.scss'],
})
export class BvSideBarComponent {
  fullName: string = 'USER'
  constructor(private router: Router, private utilities: Utilities) {
    this.fullName = this.utilities.getFullName();
  }
  toggleAccount = false
  toggleSearch = false;

  // tslint:disable-next-line:use-life-cycle-interface
  public ngOnInit(): void {

  }
  toggleMenu() {
    if (document.body.classList.contains('is-collapsed')) {
      document.body.classList.remove('is-collapsed')
    } else {
      document.body.classList.add('is-collapsed')
    }
  }
  public logout() {
    localStorage.removeItem('token');
    this.router.navigate(['/auth/login']);
  }
}

import { Routes } from '@angular/router';

export const MENU_ITEMS: Routes = [
  {
    path: 'pages',
    children: [
      {
        path: '/tranzi-admin/dashboard',
        data: {
          menu: {
            title: 'general.menu.dashboard',
            icon: 'fa fa-home',
            selected: false,
            expanded: false,
            order: 0,
            hidden: false,
          },
        },
      },
      {
        path: '/tranzi-admin/categories',
        data: {
          menu: {
            title: 'general.menu.approval',
            icon: 'fa fa-tachometer',
            selected: false,
            expanded: false,
            order: 0,
            hidden: false,
          },
        },
      },
      {
        path: '/tranzi-admin/product',
        data: {
          menu: {
            title: 'general.menu.search',
            icon: 'fa fa-product-hunt',
            selected: false,
            expanded: false,
            order: 0,
            hidden: false,
          },
        },
        children: [],
      },
      {
        path: '/tranzi-admin/claim/create',
        data: {
          menu: {
            title: 'general.menu.contact',
            icon: 'ion-gear-b',
            selected: false,
            expanded: false,
            order: 0,
            hidden: false,
          },
        },
      },
      {
        path: '/tranzi-admin/document',
        data: {
          menu: {
            title: 'general.menu.document',
            icon: 'ion-gear-b',
            selected: false,
            expanded: true,
            order: 0,
            hidden: false,
          },
        },
        children: [
          {
            path: '/tranzi-admin/view-document',
            data: {
              menu: {
                title: 'general.menu.view_document',
                icon: 'ion-gear-b',
                selected: false,
                expanded: false,
                order: 0,
                hidden: false,
              },
            },
          },
          {
            path: '/tranzi-admin/update-document',
            data: {
              menu: {
                title: 'general.menu.update_document',
                icon: 'ion-gear-b',
                selected: false,
                expanded: false,
                order: 0,
                hidden: false,
              },
            },
          },
        ],
      },
      {
        path: '/tranzi-admin/account',
        data: {
          menu: {
            title: 'general.menu.account',
            icon: 'fa fa-bar-chart',
            selected: false,
            expanded: false,
            order: 0,
            hidden: false,
          },
        },
      },
      {
        path: 'configuration',
        data: {
          menu: {
            title: 'general.menu.configuration',
            icon: 'ion-gear-b',
            pathMatch: 'ewr',
            selected: false,
            expanded: true,
            order: 500,
            hidden: true,
          },
        },
        children: [
          {
            path: 'user-management',
            data: {
              menu: {
                title: 'general.menu.user_management',
                icon: 'fa fa-tachometer',
                selected: false,
                expanded: false,
                order: 0,
                hidden: true,
              },
            },
          },
        ],
      },
    ],
  },
];

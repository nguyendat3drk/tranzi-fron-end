import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BvHelpComponent } from './bv-help.component';

describe('BvHelpComponent', () => {
  let component: BvHelpComponent;
  let fixture: ComponentFixture<BvHelpComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BvHelpComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BvHelpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Utilities } from '../../../../shared/services/utilities.service';
import { Component, OnInit } from '@angular/core';
import { ProductService } from '../../../../shared/services/http/product.service';
import { MessageService } from 'primeng/components/common/messageservice';
import { ActivatedRoute, Router } from '@angular/router';
import { LoaderService } from '../../../../shared/services/loader.service';
import { CategoriesService } from '../../../../shared/services/http/categories.service';
import { Categories } from '../../../../shared/models/Categories';
import { TranslateService } from '@ngx-translate/core';
import { TreeNode } from 'primeng/primeng';
import { ThemeSettingsComponent } from '../../../../@theme/components';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'categories-create',
  templateUrl: './categories-create.component.html',
  styleUrls: ['./categories-create.component.scss'],
})
export class CategoriesCreateComponent implements OnInit {

  selectCategories: Categories = new Categories();
  public editorValue: string = ''
  urlServerFrontEnd: string;

  // Tree
  filesTree2: TreeNode[];
  selectedFile: TreeNode;
  displayOne: boolean = false;
  displayBanner: boolean = false;
  displaySEO: boolean = false;

  listParentTree: Categories[] = [];
  listChildrenTree: Categories[] = [];
  listIDCategoryTree: number[] = [];
  // end tree
  // upload
  urlupload: string = '';
  uploadedFilesSuccess: any[] = []
  uploadedFilesError: any[] = []
  uploadedFilesErrorInvalid: any[] = []
  progressBarPercent = 20
  totalFile: number = 0;
  //Upload Icon
  urluploadIcon: String ='';

  // validatee
  blName: boolean = false;
  blNameEn: boolean = false;
  blOrder: boolean = false;
  blBannerImage: boolean = false;

  constructor(private router: Router, private translateService: TranslateService,
    private utilities: Utilities, private messageService: MessageService,
    private loaderService: LoaderService, private ultilities: Utilities,
    private categoriesService: CategoriesService) {
    this.urlupload = this.ultilities.getUrlServerHost() + '/share-api/uploadImageCategory'
    this.urlServerFrontEnd = this.ultilities.urlServerFrontEnd() + 'assets/images/category/'
    this.urluploadIcon = this.ultilities.getUrlServerHost() + '/share-api/uploadIcon'
  }
  ngOnInit() {
    // this.getParentTree();
    this.testGetTree();
  }
  testGetTree() {
    this.filesTree2 = [];
    this.categoriesService.getTreeCategories(0).subscribe((response: any) => {
      if (response && response.resultCode === 200) {
        this.filesTree2 = response.object;
      }
      this.loaderService.hide()
    }, error => {
      this.loaderService.hide();
    });
  }
  nodeSelect(event) {
    this.selectCategories.parentId = null
    if (event) {
      this.selectCategories.parentId = event.node.data
    }
  }
  changeSlugByname() {
    if (!this.selectCategories.slug && this.selectCategories.name) {
      this.selectCategories.slug = this.selectCategories.name;
      this.setSlugOnchange();
    }
  }
  setSlugOnchange() {
    if (event) {
      this.selectCategories.slug = this.ultilities.changeToSlug(this.selectCategories.slug);
    }
  }
  nodeUnselect(event) {
  }

  createCategori() {
    this.validate();
    this.selectCategories.id = 0;
    if (!this.selectCategories || !this.selectCategories.nameEn || !this.selectCategories.name || this.selectCategories.order === undefined
      || this.selectCategories.order === null) {
      return;
    }
    if (this.selectCategories.blSeoIndex) {
      this.selectCategories.seoIndex = 1
    } else {
      this.selectCategories.seoIndex = 0
    }
    this.categoriesService.create(this.selectCategories).subscribe((response: any) => {
      // tslint:disable-next-line:no-console
      // console.log('responseresponseresponse=', response);
      if (response && response.resultCode === 200) {
        this.router.navigate(['/tranzi-admin/categories']);
        localStorage.setItem('addCategory', 'Thêm mới Thể loại thành công');
      } else if (response && response.resultCode === 204) {
        this.ultilities.showError('Not Update Database')
      }
      window.scrollTo(0, 0);
      this.loaderService.hide();
    }, (error) => {
      // tslint:disable-next-line:no-console
      let mes = '';
      if (error && error.error && error.error.errors && error.error.errors[0] && error.error.errors[0].message) {
        mes = error.error.errors[0].message;
      }
      this.messageService.clear();
      this.messageService.add({
        detail: mes ? mes : 'Có lỗi xảy ra. Liên hệ Admin để kiểm tra lại',
        severity: 'error',
        summary: 'Lỗi',
      });
      this.loaderService.hide();
    });
  }
  tesst(event) {

  }
  bankmanageCategory() {
    this.router.navigate(['/tranzi-admin/categories']);
  }
  getParentTree() {
    this.listParentTree = [];
    this.categoriesService.getParentTree().subscribe((response: any) => {
      if (response && response.resultCode === 200) {
        this.listParentTree = response.object;
        if (this.listParentTree) {
          this.getChildrenTree();
        }
      }
      this.loaderService.hide()
    }, error => {
      this.loaderService.hide();
    });
  }
  getChildrenTree() {
    this.listChildrenTree = [];
    this.categoriesService.getChildrenTree().subscribe((response: any) => {
      if (response && response.resultCode === 200) {
        this.listChildrenTree = response.object;
      }
      this.loaderService.hide()
      this.setTree();
    }, error => {
      this.loaderService.hide();
      this.setTree();
    });
  }
  setTree() {
    this.filesTree2 = [];
    const dtoRoot = {
      label: 'Select Categories',
      data: null,
      // expandedIcon: 'fa fa-folder-open',
      // collapsedIcon: 'fa fa-folder',
      children: null,
      expanded: true,
    }
    this.filesTree2.push(dtoRoot);
    this.listIDCategoryTree = [];
    if (this.listParentTree) {
      for (let i = 0; i < this.listParentTree.length; i++) {
        const chi = this.setChildrenTree(this.listParentTree[i].id);
        let dto = null;
        if (chi) {
          dto = {
            label: this.listParentTree[i].name,
            data: this.listParentTree[i].id,
            // expandedIcon: 'fa fa-folder-open',
            // collapsedIcon: 'fa fa-folder',
            // expanded: true,
            children: [chi],
          }
        } else {
          dto = {
            label: this.listParentTree[i].name,
            data: this.listParentTree[i].id,
            // expandedIcon: 'fa fa-folder-open',
            // collapsedIcon: 'fa fa-folder',
            // expanded: true,
          }
        }
        this.listIDCategoryTree.push(this.listParentTree[i].id);
        this.filesTree2.push(dto);
      }
    }
  }
  setChildrenTree(id) {
    if (id) {
      if (this.listChildrenTree) {
        for (let i = 0; i < this.listChildrenTree.length; i++) {
          if (this.listChildrenTree[i].parentId === id) {
            if (this.listIDCategoryTree) {
              for (let j = 0; j < this.listIDCategoryTree.length; j++) {
                if (this.listIDCategoryTree[j] === this.listChildrenTree[i].parentId) {
                  return null;
                }
              }
            }
            this.listIDCategoryTree.push(this.listChildrenTree[i].id);
            const chi = this.setChildrenTree(this.listChildrenTree[i].id);
            let dto = null;
            if (chi) {
              dto = {
                label: this.listChildrenTree[i].name,
                data: this.listChildrenTree[i].id,
                // expandedIcon: 'fa fa-folder-open',
                // collapsedIcon: 'fa fa-folder',
                // expanded: true,
                children: [chi],
              }
            } else {
              dto = {
                label: this.listChildrenTree[i].name,
                data: this.listChildrenTree[i].id,
                // expandedIcon: 'fa fa-folder-open',
                // collapsedIcon: 'fa fa-folder',
                // expanded: true,
              }
            }

            return dto;
          }
          if (i === (this.listChildrenTree.length - 1)) {
            return null;
          }
        }
      }
    }
  }
  keyPress(event: any, type) {
    if (type && type === 'number') {
      const patternExtension = /[0-9]/;
      const inputChar = String.fromCharCode(event.charCode);
      if (event.keyCode !== 9 && !patternExtension.test(inputChar)) {
        event.preventDefault();
      }
    } else if (type && type === 'date') {
      const patternExtension = /[0-9\/]/;
      const inputChar = String.fromCharCode(event.charCode);
      if (event.keyCode !== 9 && !patternExtension.test(inputChar)) {
        event.preventDefault();
      }
    } else {
      const patternExtension = /[0-9\.]/;
      const inputChar = String.fromCharCode(event.charCode);
      if (event.keyCode !== 9 && !patternExtension.test(inputChar)) {
        event.preventDefault();
      }
    }
  }
  // upload===============================================UPLOAD====================
  btnUploadClick(fileUpload) {
    fileUpload.advancedFileInput.nativeElement.click()
  }
  btnUploadClickIcon(fileUploadIcon){
    fileUploadIcon.advancedFileInput.nativeElement.click()
  }

  onSelectedFiles(modal, fileUpload) {
    this.totalFile = 0;
    this.progressBarPercent = 20
    this.uploadedFilesError = []
    this.uploadedFilesErrorInvalid = [];
    this.uploadedFilesSuccess = []
    if (fileUpload.advancedFileInput.nativeElement.files) {
      this.totalFile = fileUpload.files.length;
      for (let i = 0; i < fileUpload.advancedFileInput.nativeElement.files.length; i++) {
        const element = fileUpload.advancedFileInput.nativeElement.files[i];
        const type = fileUpload.advancedFileInput.nativeElement.files[i].type;
        const strTypeCheck = type.split('/')[0];
        const strTypeTailFile = type.split('/')[1];
        if (('' + strTypeCheck).toUpperCase().trim() === 'VIDEO' && (('' + strTypeTailFile).toUpperCase().trim() !== 'MP4'
          && ('' + strTypeTailFile).toUpperCase().trim() !== '3GP' && ('' + strTypeTailFile).toUpperCase().trim() !== '3GPP')) {
          // tslint:disable-next-line:prefer-const
          let dto = {
            value: fileUpload.advancedFileInput.nativeElement.files[i].name,
            type: fileUpload.advancedFileInput.nativeElement.files[i].type,
          }
          this.uploadedFilesErrorInvalid.push(dto)
        } else if (element.size > 10000000 || element.size === 0) {
          this.uploadedFilesError.push(element)
        }

      }
    }
    if (fileUpload && fileUpload.msgs) {
      for (let i = 0; i < fileUpload.msgs.length; i++) {
        const element = fileUpload.msgs[i];
        if (element) {
          const maxsize = element.detail.indexOf('maximum');
          let valueDetail = '';
          let valueType = '';
          valueDetail = element.summary.split(':')[0];
          if (maxsize < 0) {
            valueType = 'Không đúng định dạng'
            // tslint:disable-next-line:prefer-const
            let dto = {
              value: valueDetail,
              type: valueType,
            }
            this.uploadedFilesErrorInvalid.push(dto)
          }
        }
      }
    }
    const checkNumberError = Number(this.uploadedFilesErrorInvalid ? this.uploadedFilesErrorInvalid.length : 0) +
      Number(this.uploadedFilesError ? this.uploadedFilesError.length : 0);
    if (this.totalFile === checkNumberError) {
      this.progressBarPercent = 100
      this.ultilities.showError('Cập nhật thất bại')
    }
    modal.show()
    fileUpload.upload()
  }
  onBeforUpload(event, fileUpload) {
    // tslint:disable-next-line:prefer-const
    let interval = setInterval(() => {
      if (this.progressBarPercent < 90) {
        this.progressBarPercent = this.progressBarPercent + 10;
      }
      if (this.progressBarPercent >= 100) {
        this.progressBarPercent = 100;
        clearInterval(interval);
      }
    }, 700);
    this.uploadedFilesSuccess = []
    if (fileUpload.files) {
      for (let i = 0; i < fileUpload.files.length; i++) {
        const element = fileUpload.files[i];
        const type = fileUpload.files[i].type;
        // const name = fileUpload.files[i].name;
        // const firstName = this.ultilities.changeToSlug(name.split('.')[0]);
        // const lastName = name.split('.')[1];
        // fileUpload.files[i].name = firstName + '.' + lastName;
        const strTypeCheck = type.split('/')[0];
        const strTypeTailFile = type.split('/')[1];
        if (element.size > 0) {
          // if (('' + strTypeCheck).toUpperCase().trim() === 'VIDEO' && (('' + strTypeTailFile).toUpperCase().trim() !== 'MP4'
          //   && ('' + strTypeTailFile).toUpperCase().trim() !== '3GP' && ('' + strTypeTailFile).toUpperCase().trim() !== '3GPP')) {
          // } else {
          this.uploadedFilesSuccess.push(element)
          // }
        }
      }
    }
    event.xhr.setRequestHeader('Authorization', + this.ultilities.getToken())
    if (event.formData) {
      // event.formData.append('id', 1993)
      // event.formData.papend('nameForder', 'banner')
    }
  }
  onUploadSuccess(event, modal) {
    setTimeout(() => {
      const checkNumberError = Number(this.uploadedFilesErrorInvalid ? this.uploadedFilesErrorInvalid.length : 0) +
        Number(this.uploadedFilesError ? this.uploadedFilesError.length : 0);
      this.progressBarPercent = 100

      if (event && event.xhr && event.xhr.status !== 200 || this.totalFile === checkNumberError) {
        if (modal) modal.hide()
        this.ultilities.showError('Cập nhật thất bại')
      } else {
        // this.ultilities.showSuccess('Cập nhật thành công')
        // let response;
        // if (event.xhr.response) {
        //   response = JSON.parse(event.xhr.response)
        // }
        // let urlImage = ''
        // if (response && response.message) {
        //   urlImage = response.object;
        //   this.selectCategories.bannerImage = this.urlServerFrontEnd + urlImage;
        // }
      }
    }, 1000);

    if (event && event.xhr && event.xhr.response) {
      const document = JSON.parse(event.xhr.response)
      if (document) {
        // this.ultilities.showSuccess('upload thanh cong')
        this.selectCategories.bannerImage = this.urlServerFrontEnd + this.ultilities.clearNameKytuDacbiet(document.object);
      }
    }
  }
  checkorderFormat() {
    this.blOrder = true;
    if (this.selectCategories.order) {
      const check = this.utilities.checkNumber(this.selectCategories.order);
      if (check) {
      } else {
        this.selectCategories.order = null;
      }
    }

  }

  onUploadError(event, modal) {
    setTimeout(() => {
      this.progressBarPercent = 100
      if (modal) modal.hide()
      if (event && event.xhr) {
        let response;
        if (event.xhr.response) {
          response = JSON.parse(event.xhr.response)
        }
        let message = 'Cập nhật không thành công'
        if (response && response.message) {
          message = response.message
        }
        // this.ultilities.showError(message)
      }
    }, 1000)
  }
  // Icon upload
  onSelectedFilesIcon(modal, fileUpload) {
    // modal.show()
    fileUpload.upload()
  }
  onBeforUploadIcon(event, fileUpload) {    
    event.xhr.setRequestHeader('Authorization', + this.ultilities.getToken())
    // if (event.formData) {
    // }
  }
  onUploadSuccessIcon(event, modal) {
    if (event && event.xhr && event.xhr.response) {
      const document = JSON.parse(event.xhr.response)
      if (document) {
        // this.ultilities.showSuccess('upload thanh cong')
        this.selectCategories.iconUrl = this.urlServerFrontEnd + document.object;
      }
    }
  }
  onUploadErrorIcon(event, modal) {
    setTimeout(() => {
      this.progressBarPercent = 100
      if (modal) modal.hide()
      if (event && event.xhr) {
        let response;
        if (event.xhr.response) {
          response = JSON.parse(event.xhr.response)
        }
        let message = 'Cập nhật không thành công'
        if (response && response.message) {
          message = response.message
        }
      }
    }, 1000)
  }
  validate() {
    this.blName = true;
    this.blNameEn = true;
    this.blOrder = true;
    this.blBannerImage = true;
  }
}




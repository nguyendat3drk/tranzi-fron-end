import { Utilities } from '../../../../shared/services/utilities.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ProductService } from '../../../../shared/services/http/product.service';
import { MessageService } from 'primeng/components/common/messageservice';
import { ActivatedRoute, Router } from '@angular/router';
import { LoaderService } from '../../../../shared/services/loader.service';
import { CategoriesService } from '../../../../shared/services/http/categories.service';
import { Categories } from '../../../../shared/models/Categories';
import { PagerData } from '../../../../shared/entity/pagerData';
import { isFulfilled } from 'q';
import { TreeNode } from 'primeng/api';
import { ThemeSettingsComponent } from '../../../../@theme/components';
import { ModalDirective } from 'ngx-bootstrap';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.scss'],
})
export class CategoriesComponent implements OnInit {
  listCategories: Categories[] = [];
  totalPage: number = 0;
  selectCategories: Categories = new Categories();
  txtSearch: string = '';
  listTreeCategory: TreeNode[] = [];

  listTreeCategorySearch: TreeNode[] = [];


  files: TreeNode[];

  cols: any[];
  loading: boolean;
  @ViewChild('confirmDialodDel') public confirmDialodDel: ModalDirective;
  idDelete: Number = 0;
  title: string = '';
  constructor(private utilities: Utilities, private router: Router,
    private activeRouter: ActivatedRoute, private messageService: MessageService,
    private loaderService: LoaderService, private ultilities: Utilities,
    private categoriesService: CategoriesService) {
      this.ultilities.closeAutoMenu();
  }

  ngOnInit() {

    // this.loadCategories();
    // this.loadCategoriesTree();
    setTimeout(() => {
      if (localStorage.getItem('addCategory')) {
        this.ultilities.showSuccess(localStorage.getItem('addCategory'));
        localStorage.removeItem('addCategory')
      } else if (localStorage.getItem('editCategory')) {
        this.ultilities.showSuccess(localStorage.getItem('editCategory'));
        localStorage.removeItem('editCategory')
      }
    }, 300);
  }
  loadNodes(event) {
    this.loading = true;
    setTimeout(() => {
      this.loading = false;
          this.categoriesService.loadParentCategoryTable().subscribe((response: any) => {
            this.totalPage = 0;
            this.listTreeCategory = [];
            this.listTreeCategorySearch = [];
            if (response && response.resultCode === 200 && response.object && response.object.length>0) {
              for(let i = 0; i < event.rows; i++) {
                response.object[event.first  + i].leaf = false;
                this.listTreeCategory.push(response.object[event.first  + i]);
                this.listTreeCategorySearch.push(response.object[event.first  + i]);
              }
              this.totalPage = response.object.length;
              // this.listProduct = response;
            }
            this.loaderService.hide()
          }, error => {
            this.totalPage = 0
            this.listCategories = [];
            this.loaderService.hide();
          });
      }, 100);
  }
  onNodeExpand(event) {
    this.loading = true;
console.log(event)
    setTimeout(() => {
        this.loading = false;
        const node = event.node;
        this.loading = false;
          this.categoriesService.getParentTreeChildenTable(node.data.id?node.data.id:1).subscribe((response: any) => {
            
            if (response && response.resultCode === 200 && response.object) {
              node.children = response.object;
            }
            this.loaderService.hide()
            this.listTreeCategory = [...this.listTreeCategory];
          }, error => {
            this.listCategories = [];
            this.loaderService.hide();
          });
          this.listTreeCategory = [...this.listTreeCategory];
      }, 100);

        
    
  }
  loadCategoriesTree() {
    this.loaderService.show();
    this.categoriesService.loadCategoriesTree(1).subscribe((response: any) => {
      this.listTreeCategory = [];
      this.listTreeCategorySearch = [];
      if (response && response.resultCode === 200) {
        this.listTreeCategory = response.object;
        this.listTreeCategorySearch = response.object;
        // this.listProduct = response;
      }
      this.loaderService.hide()
    }, error => {
      this.listCategories = [];
      this.loaderService.hide();
    });
  }
  searchTree(event) {
    this.listTreeCategory = [];
    if (event) {
      if (this.listTreeCategorySearch) {
        for (let i = 0; i < this.listTreeCategorySearch.length; i++) {
          let check = false;
          if (this.listTreeCategorySearch[i].data) {
            if (this.listTreeCategorySearch[i].data && this.listTreeCategorySearch[i].data.name &&
              this.listTreeCategorySearch[i].data.name.indexOf(event) >= 0) {
              check = true;
            } else {
              // tslint:disable-next-line:prefer-const
              let checkChideln = false;
            }
          }
          if (check) {
            this.listTreeCategorySearch[i].expanded = true;
            this.listTreeCategory.push(this.listTreeCategorySearch[i]);
          }
        }
      }
    } else {
      this.listTreeCategory = this.listTreeCategorySearch;
    }
  }
  loadCategories() {
    this.loaderService.show();
    this.categoriesService.getAllsPadding(1, 10, 'id', 'desc', this.txtSearch).subscribe((response: any) => {
      this.listCategories = [];
      this.totalPage = 0;
      if (response && response.resultCode === 200) {
        let page = new PagerData();
        page = response.object;
        if (page && page.content) {
          this.listCategories = page.content;
          this.totalPage = page.totalRow;
        }
        // this.listProduct = response;
      }
      this.loaderService.hide()
    }, error => {
      this.listCategories = [];
      this.totalPage = 0;
      this.loaderService.hide();
    });
  }
  createCategori() {
    this.router.navigate(['/tranzi-admin/categories-create']);
  }
  paginate(event) {
    if (event) {
      this.listCategories = [];
      // this.totalPage = 0;
      // tslint:disable-next-line:max-line-length
      this.categoriesService.getAllsPadding(event.page ? (event.page + 1) : 1, event.rows ? event.rows : 10, 'id', 'desc', this.txtSearch).subscribe((response: any) => {
        // tslint:disable-next-line:no-console
        // console.log(response)
        if (response && response.resultCode === 200) {
          let page = new PagerData();
          page = response.object;
          if (page && page.content) {
            this.listCategories = page.content;
            this.totalPage = page.totalRow;
            // this.totalPage = this.totalPage.s
          }
          // this.listProduct = response;
        }
        this.loaderService.hide()
      }, error => {
        this.totalPage = 0
        this.loaderService.hide();
      });
    }

  }


  seSelectCategories(event) {
    this.selectCategories = new Categories()
    if (event && event.data) {
      this.selectCategories = event.data;
    }
  }
  routingUpdate(id) {
    if (id) {
      this.router.navigate(['/tranzi-admin/categories-update/' + id]);
    }
  }
  deleteCategori() {
    // this.listCategories = [];
    this.confirmDialodDel.hide();
    if (this.idDelete) {
      this.loaderService.show();
      this.categoriesService.deleteCategoriId(this.idDelete).subscribe((response: any) => {
        if (response && response.resultCode === 200) {
          // Dung table
          // this.loadCategories();
          // Dung tree table
          this.loadCategoriesTree();
          this.ultilities.showSuccess('Xóa thành công');
        } else if (response && response.resultCode === 205) {
          this.ultilities.showError('Thể loại đã liên kết sản phẩm. Không thể xóa.')
        } else if (response && response.resultCode === 204) {
          this.ultilities.showError(response.errorMessage ? response.errorMessage : 'Có lỗi xảy ra. Liên hệ Admin để kiểm tra lại')
        }
        this.loaderService.hide()
        // this.ultilities.showSuccess('Delete Success Categories')
      }, error => {
        this.loaderService.hide();
        this.ultilities.showError('Delete Error Categories')
      });
    }
  }
  deleteCheck(ca: Categories) {
    if (ca) {
      this.title = 'Bạn có muốn xóa Thể loại: ' + ca.name + ' không?';
      this.idDelete = ca.id
      this.confirmDialodDel.show();
    }
  }
  orderCategory(){
    this.router.navigate(['/tranzi-admin/order-category']);
  }
  orderCategoryAttribute(ca: Categories){
    if(ca && ca.id){
      this.router.navigate(['/tranzi-admin/order-category-attribute/'+ca.id]);
    }
    
  }

}

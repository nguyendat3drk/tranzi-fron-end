import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CategoriesComponent } from './categories.component';
import { PageNotFoundComponent } from '../../../../shared/components/page-not-found/page-not-found.component';

const routes: Routes = [
  {
    path: '',
    component: CategoriesComponent,
  },
  { path: '**', component: PageNotFoundComponent },
];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
  ],
  exports: [RouterModule],
})
export class CategoriesRoutingModule { }

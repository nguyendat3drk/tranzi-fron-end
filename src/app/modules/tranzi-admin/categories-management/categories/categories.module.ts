import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../../../shared/shared.module';
import { HttpClientModule } from '@angular/common/http';
import { DataTableModule } from 'primeng/datatable';
import { ContextMenuModule } from 'primeng/contextmenu';
import { CalendarModule } from 'primeng/calendar';
import { TextMaskModule } from 'angular2-text-mask';
import { NgxMaskModule } from 'ngx-mask';
import { CurrencyMaskModule } from 'ng2-currency-mask';
import { CategoriesRoutingModule } from './categories.routing.module';
import { ModalModule } from 'ngx-bootstrap';
import { CategoriesComponent } from './categories.component';
import {PaginatorModule} from 'primeng/paginator';
import {TreeTableModule} from 'primeng/treetable';

@NgModule({
  imports: [
    CategoriesRoutingModule,
    CommonModule,
    SharedModule,
    HttpClientModule,
    DataTableModule,
    ContextMenuModule,
    CalendarModule,
    TextMaskModule,
    ModalModule.forRoot(),
    NgxMaskModule.forRoot(),
    CurrencyMaskModule,
    PaginatorModule,
    TreeTableModule,
  ],
  declarations: [
    CategoriesComponent,
  ],
  // exports: [RouterModule],
  providers: [
  ],
})
export class CategoriesModule {
}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { OrderCategoryAttributeComponent } from './order-category-attribute.component';


describe('OrderCategoryAttributeComponent', () => {
  let component: OrderCategoryAttributeComponent;
  let fixture: ComponentFixture<OrderCategoryAttributeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [OrderCategoryAttributeComponent],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderCategoryAttributeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

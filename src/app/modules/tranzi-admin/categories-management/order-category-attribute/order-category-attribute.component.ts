import { Utilities } from '../../../../shared/services/utilities.service';
import { Component, OnInit } from '@angular/core';
import { ProductService } from '../../../../shared/services/http/product.service';
import { MessageService } from 'primeng/components/common/messageservice';
import { ActivatedRoute, Router } from '@angular/router';
import { LoaderService } from '../../../../shared/services/loader.service';
import { CategoriesService } from '../../../../shared/services/http/categories.service';
import { Categories } from '../../../../shared/models/Categories';
import { TranslateService } from '@ngx-translate/core';
import { TreeNode } from 'primeng/primeng';
import { Attributes } from '../../../../shared/models/Attributes';
import { AttributesService } from '../../../../shared/services/http/attributes.service';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'order-category-attribute',
  templateUrl: './order-category-attribute.component.html',
  styleUrls: ['./order-category-attribute.component.scss'],
})
export class OrderCategoryAttributeComponent implements OnInit {
  categoriesId: number;
  listAttribute: Attributes[] = [];

  constructor(private router: Router, private activeRouter: ActivatedRoute,
    private utilities: Utilities, private attributesService: AttributesService,
    private loaderService: LoaderService, private ultilities: Utilities,
    private categoriesService: CategoriesService) {
      this.activeRouter.params.subscribe((params) => {
        this.categoriesId = +params.id
        if (this.categoriesId) {
          this.getAttibuteByCategoryId();
         
        }
      });
  }
  ngOnInit() {
  }
  getAttibuteByCategoryId() {
    this.listAttribute = [];
    this.attributesService.getAttributeBuCategory(this.categoriesId).subscribe((response: any) => {
      if (response && response.object) {
        this.listAttribute = response.object;
      }
      this.loaderService.hide()
    }, error => {
      this.loaderService.hide();
    });
  }
  updateOrder(){
    if(this.listAttribute){
      for(let i =0;i<this.listAttribute.length;i++){
        this.listAttribute[i].orderby  = (i+1);
      }
    }
    this.loaderService.show()
    this.attributesService.updateOrderAttributes(this.listAttribute).subscribe((response: any) => {
      if (response && response.resultCode === 200) {
        this.loaderService.hide();
        this.ultilities.showSuccess('Cập nhật thành công')
      }else{
        this.ultilities.showError('Có lỗi xảy ra')
      }
      this.loaderService.hide();
      this.loaderService.hide()
    }, error => {
      this.loaderService.hide();
      this.loaderService.hide();
    });
  }
  bankList(){
    this.router.navigate(['/tranzi-admin/categories']);
  }
  
}




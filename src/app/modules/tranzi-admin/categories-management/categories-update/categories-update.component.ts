import { Utilities } from '../../../../shared/services/utilities.service';
import { Component, OnInit } from '@angular/core';
import { ProductService } from '../../../../shared/services/http/product.service';
import { MessageService } from 'primeng/components/common/messageservice';
import { ActivatedRoute, Router } from '@angular/router';
import { LoaderService } from '../../../../shared/services/loader.service';
import { CategoriesService } from '../../../../shared/services/http/categories.service';
import { Categories } from '../../../../shared/models/Categories';
import { TranslateService } from '@ngx-translate/core';
import { TreeNode } from 'primeng/primeng';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'categories-update',
  templateUrl: './categories-update.component.html',
  styleUrls: ['./categories-update.component.scss'],
})
export class CategoriesUpdateComponent implements OnInit {
  selectCategories: Categories = new Categories();
  public editorValue: string = ''
  categoriesId: number;
  urlServerFrontEnd: string;
  // Tree
  filesTree2: TreeNode[];
  selectedFile: TreeNode;
  displayOne: boolean = false;
  displayBanner: boolean = false;
  displaySEO: boolean = false;

  listParentTree: Categories[] = [];
  listChildrenTree: Categories[] = [];
  listIDCategoryTree: number[] = [];
  // end tree
  // upload
  urlupload: string = '';
  uploadedFilesSuccess: any[] = []
  uploadedFilesError: any[] = []
  uploadedFilesErrorInvalid: any[] = []
  progressBarPercent = 20
  totalFile: number = 0;

  urluploadIcon:string ='';

  // validatee
  blName: boolean = false;
  blNameEn: boolean = false;
  blOrder: boolean = false;
  blBannerImage: boolean = false;
  constructor(private router: Router, private utilities: Utilities,
    private activeRouter: ActivatedRoute, private messageService: MessageService,
    private loaderService: LoaderService, private ultilities: Utilities,
    private categoriesService: CategoriesService) {
    this.urlupload = this.ultilities.getUrlServerHost() + '/share-api/uploadImageCategory'
    this.urlServerFrontEnd = this.ultilities.urlServerFrontEnd() + 'assets/images/category/'
    this.urluploadIcon = this.ultilities.getUrlServerHost() + '/share-api/uploadIcon'
    this.activeRouter.params.subscribe((params) => {
      this.categoriesId = +params.id
      if (this.categoriesId) {
        this.getCategoriesId();
        this.testGetTree();

      }
    });
  }
  ngOnInit() {
    // this.getParentTree();
  }
  // getAllCategori(id) {
  //   this.selectedFile = null;
  //   this.categoriesService.getCategoriesID(id).subscribe((response: any) => {
  //     if (response && response.resultCode === 200 && response.object) {
  //       response.object;
  //       this.selectedFile.data = response.object.id
  //       this.selectedFile.label = response.object.name
  //       this.selectedFile.selectable = false
  //     }
  //   }, error => {
  //     this.loaderService.hide();
  //   });
  // }
  testGetTree() {
    this.filesTree2 = [];
    this.categoriesService.getTreeCategories(this.categoriesId).subscribe((response: any) => {
      if (response && response.resultCode === 200) {
        this.filesTree2 = response.object;
        if (this.filesTree2) {
          setTimeout(() => {
            this.filesTree2.forEach(element => {
              if (Number(element.data) === Number(this.selectCategories.parentId)) {
                this.selectedFile = element;
                return;
              }
              if (element.children && element.children.length > 0) {
                this.searchTreeNode(element.children);
              }
            });
          }, 200);

        }
      }
      this.loaderService.hide()
    }, error => {
      this.loaderService.hide();
    });
  }
  searchTreeNode(event) {
    if (event && event.length > 0) {
      event.forEach(element => {
        if (Number(element.data) === Number(this.selectCategories.parentId)) {
          this.selectedFile = element;
          return;
        }
        if (element.children && element.children.length > 0) {
          this.searchTreeNode(element.children.length);
        }
      });
    }
  }
  checkorderFormat() {
    this.blOrder = true;
    if (this.selectCategories.order) {
      const check = this.utilities.checkNumber(this.selectCategories.order);
      if (check) {
      } else {
        this.selectCategories.order = null;
      }
    }

  }
  nodeSelect(event) {
    this.selectCategories.parentId = null
    if (event) {
      this.selectCategories.parentId = event.node.data
    }
    // this.ultilities.showSuccess('event.node.label');
  }

  nodeUnselect(event) {
    // this.ultilities.showSuccess('event.node.label');
  }
  getCategoriesId() {
    this.categoriesService.getCategoriesID(this.categoriesId).subscribe((response: any) => {
      // tslint:disable-next-line:no-console
      console.log(response)
      if (response && response.resultCode === 200) {
        this.selectCategories = response.object;
        if (this.selectCategories && this.selectCategories.seoIndex && this.selectCategories.seoIndex === 1) {
          this.selectCategories.blSeoIndex = true;
        }else{this.selectCategories.blSeoIndex = false;}

      }
      this.loaderService.hide()
    }, error => {
      this.loaderService.hide();
    });
  }
  changeSlugByname() {
    if (!this.selectCategories.slug && this.selectCategories.name) {
      this.selectCategories.slug = this.selectCategories.name;
      this.setSlugOnchange();
    }
  }
  setSlugOnchange() {
    if (event) {
      this.selectCategories.slug = this.ultilities.changeToSlug(this.selectCategories.slug);
    }
  }
  createCategori() {
    this.loaderService.show();
    if (this.selectCategories.blSeoIndex) {
      this.selectCategories.seoIndex = 1
    } else {
      this.selectCategories.seoIndex = 0
    }
    this.categoriesService.create(this.selectCategories).subscribe((response: any) => {
      if (response && response.resultCode === 200) {
        this.router.navigate(['/tranzi-admin/categories']);
      }
      window.scrollTo(0, 0);
      this.loaderService.hide();
    }, (error) => {
      // tslint:disable-next-line:no-console
      let mes = '';
      if (error && error.error && error.error.errors && error.error.errors[0] && error.error.errors[0].message) {
        mes = error.error.errors[0].message;
      }
      this.messageService.clear();
      this.messageService.add({
        detail: mes ? mes : 'Có lỗi xảy ra. Liên hệ Admin để kiểm tra lại',
        severity: 'error',
        summary: 'Lỗi',
      });
      this.loaderService.hide();
    });
  }

  bankmanageCategory() {
    this.router.navigate(['/tranzi-admin/categories']);
  }
  updateCategori() {
    this.validate();
    // this.selectCategories.id = 0;
    if (!this.selectCategories || !this.selectCategories.nameEn || !this.selectCategories.name || this.selectCategories.order === undefined
      || this.selectCategories.order === null) {
      return;
    }
    if (this.selectCategories.blSeoIndex) {
      this.selectCategories.seoIndex = 1
    } else {
      this.selectCategories.seoIndex = 0
    }
    this.loaderService.show();
    this.categoriesService.update(this.selectCategories).subscribe((response: any) => {
      if (response && response.resultCode === 200) {
        this.router.navigate(['/tranzi-admin/categories']);
        localStorage.setItem('editCategory', 'Cập nhật Thể loại thành công');
      } else if (response && response.resultCode === 204) {
        this.ultilities.showError(response.errorMessage ? response.errorMessage : 'Có lỗi xảy ra. Liên hệ support để được hỗ trợ')
      }
      window.scrollTo(0, 0);
      this.loaderService.hide();
    }, (error) => {
      // tslint:disable-next-line:no-console
      let mes = '';
      if (error && error.error && error.error.errors && error.error.errors[0] && error.error.errors[0].message) {
        mes = error.error.errors[0].message;
      }
      this.messageService.clear();
      this.messageService.add({
        detail: mes ? mes : 'Có lỗi xảy ra. Liên hệ Admin để kiểm tra lại',
        severity: 'error',
        summary: 'Lỗi',
      });
      this.loaderService.hide();
    });
  }
  getParentTree() {
    this.listParentTree = [];
    this.categoriesService.getParentTree().subscribe((response: any) => {
      if (response && response.resultCode === 200) {
        this.listParentTree = response.object;
        if (this.listParentTree) {
          this.getChildrenTree();
        }
      }
      this.loaderService.hide()
    }, error => {
      this.loaderService.hide();
    });
  }
  getChildrenTree() {
    this.listChildrenTree = [];
    this.categoriesService.getChildrenTree().subscribe((response: any) => {
      if (response && response.resultCode === 200) {
        this.listChildrenTree = response.object;
      }
      this.loaderService.hide()
      this.setTree();
    }, error => {
      this.loaderService.hide();
      this.setTree();
    });
  }
  setTree() {
    this.filesTree2 = [];
    const dtoRoot = {
      label: '--Selected-- ',
      data: null,
      children: null,
      expanded: true,
    }
    this.filesTree2.push(dtoRoot);
    this.listIDCategoryTree = [];
    if (this.listParentTree) {
      for (let i = 0; i < this.listParentTree.length; i++) {
        const chi = this.setChildrenTree(this.listParentTree[i].id);
        let dto = null;
        if (chi) {
          dto = {
            label: this.listParentTree[i].name,
            data: this.listParentTree[i].id,
            expanded: true,
            children: [chi],
          }
        } else {
          dto = {
            label: this.listParentTree[i].name,
            data: this.listParentTree[i].id,
          }
        }
        this.listIDCategoryTree.push(this.listParentTree[i].id);
        this.filesTree2.push(dto);
      }
    }
    if (this.filesTree2 && this.selectCategories.parentId) {
      for (let i = 0; i < this.filesTree2.length; i++) {
        if (Number(this.filesTree2[i].data) === this.selectCategories.parentId) {
          this.selectedFile = this.filesTree2[i];
        }
      }
    }
  }
  setChildrenTree(id) {
    if (id) {
      if (this.listChildrenTree) {
        for (let i = 0; i < this.listChildrenTree.length; i++) {
          if (this.listChildrenTree[i].parentId === id) {
            if (this.listIDCategoryTree) {
              for (let j = 0; j < this.listIDCategoryTree.length; j++) {
                if (this.listIDCategoryTree[j] === this.listChildrenTree[i].parentId) {
                  return null;
                }
              }
            }
            this.listIDCategoryTree.push(this.listChildrenTree[i].id);
            const chi = this.setChildrenTree(this.listChildrenTree[i].id);
            let dto = null;
            if (chi) {
              dto = {
                label: this.listChildrenTree[i].name,
                data: this.listChildrenTree[i].id,
                expanded: true,
                children: [chi],
              }
            } else {
              dto = {
                label: this.listChildrenTree[i].name,
                data: this.listChildrenTree[i].id,
              }
            }
            return dto;
          }
          if (i === (this.listChildrenTree.length - 1)) {
            return null;
          }
        }
      }
    }
  }
  validate() {
    this.blName = true;
    this.blNameEn = true;
    this.blOrder = true;
    this.blBannerImage = true;
  }
  keyPress(event: any, type) {
    if (type && type === 'number') {
      const patternExtension = /[0-9]/;
      const inputChar = String.fromCharCode(event.charCode);
      if (event.keyCode !== 9 && !patternExtension.test(inputChar)) {
        event.preventDefault();
      }
    } else if (type && type === 'date') {
      const patternExtension = /[0-9\/]/;
      const inputChar = String.fromCharCode(event.charCode);
      if (event.keyCode !== 9 && !patternExtension.test(inputChar)) {
        event.preventDefault();
      }
    } else {
      const patternExtension = /[0-9\.]/;
      const inputChar = String.fromCharCode(event.charCode);
      if (event.keyCode !== 9 && !patternExtension.test(inputChar)) {
        event.preventDefault();
      }
    }
  }
  // upload===============================================UPLOAD====================
  btnUploadClick(fileUpload) {
    fileUpload.advancedFileInput.nativeElement.click()
  }

  onSelectedFiles(modal, fileUpload) {
    this.totalFile = 0;
    this.progressBarPercent = 20
    this.uploadedFilesError = []
    this.uploadedFilesErrorInvalid = [];
    this.uploadedFilesSuccess = []
    if (fileUpload.advancedFileInput.nativeElement.files) {
      this.totalFile = fileUpload.files.length;
      for (let i = 0; i < fileUpload.advancedFileInput.nativeElement.files.length; i++) {
        const element = fileUpload.advancedFileInput.nativeElement.files[i];
        const type = fileUpload.advancedFileInput.nativeElement.files[i].type;
        const strTypeCheck = type.split('/')[0];
        const strTypeTailFile = type.split('/')[1];
        if (('' + strTypeCheck).toUpperCase().trim() === 'VIDEO' && (('' + strTypeTailFile).toUpperCase().trim() !== 'MP4'
          && ('' + strTypeTailFile).toUpperCase().trim() !== '3GP' && ('' + strTypeTailFile).toUpperCase().trim() !== '3GPP')) {
          // tslint:disable-next-line:prefer-const
          let dto = {
            value: fileUpload.advancedFileInput.nativeElement.files[i].name,
            type: fileUpload.advancedFileInput.nativeElement.files[i].type,
          }
          this.uploadedFilesErrorInvalid.push(dto)
        } else if (element.size > 10000000 || element.size === 0) {
          this.uploadedFilesError.push(element)
        }

      }
    }
    if (fileUpload && fileUpload.msgs) {
      for (let i = 0; i < fileUpload.msgs.length; i++) {
        const element = fileUpload.msgs[i];
        if (element) {
          const maxsize = element.detail.indexOf('maximum');
          let valueDetail = '';
          let valueType = '';
          valueDetail = element.summary.split(':')[0];
          if (maxsize < 0) {
            valueType = 'Không đúng định dạng'
            // tslint:disable-next-line:prefer-const
            let dto = {
              value: valueDetail,
              type: valueType,
            }
            this.uploadedFilesErrorInvalid.push(dto)
          }
        }
      }
    }
    const checkNumberError = Number(this.uploadedFilesErrorInvalid ? this.uploadedFilesErrorInvalid.length : 0) +
      Number(this.uploadedFilesError ? this.uploadedFilesError.length : 0);
    if (this.totalFile === checkNumberError) {
      this.progressBarPercent = 100
      this.ultilities.showError('Cập nhật thất bại')
    }
    modal.show()
    fileUpload.upload()
  }
  onBeforUpload(event, fileUpload) {
    // tslint:disable-next-line:prefer-const
    let interval = setInterval(() => {
      if (this.progressBarPercent < 90) {
        this.progressBarPercent = this.progressBarPercent + 10;
      }
      if (this.progressBarPercent >= 100) {
        this.progressBarPercent = 100;
        clearInterval(interval);
      }
    }, 700);
    this.uploadedFilesSuccess = []
    if (fileUpload.files) {
      for (let i = 0; i < fileUpload.files.length; i++) {
        const element = fileUpload.files[i];
        const type = fileUpload.files[i].type;
        const strTypeCheck = type.split('/')[0];
        const strTypeTailFile = type.split('/')[1];
        if (element.size > 0) {
          if (('' + strTypeCheck).toUpperCase().trim() === 'VIDEO' && (('' + strTypeTailFile).toUpperCase().trim() !== 'MP4'
            && ('' + strTypeTailFile).toUpperCase().trim() !== '3GP' && ('' + strTypeTailFile).toUpperCase().trim() !== '3GPP')) {
          } else {
            this.uploadedFilesSuccess.push(element)
          }
        }
      }
    }
    event.xhr.setRequestHeader('Authorization', + this.ultilities.getToken())
    if (event.formData) {
      // event.formData.append('id', 1993)
      // event.formData.papend('nameForder', 'banner')
    }
  }
  onUploadSuccess(event, modal) {
    setTimeout(() => {
      const checkNumberError = Number(this.uploadedFilesErrorInvalid ? this.uploadedFilesErrorInvalid.length : 0) +
        Number(this.uploadedFilesError ? this.uploadedFilesError.length : 0);
      this.progressBarPercent = 100

      if (event && event.xhr && event.xhr.status !== 200 || this.totalFile === checkNumberError) {
        if (modal) modal.hide()
        this.ultilities.showError('Cập nhật thất bại')
      } else {
        // this.ultilities.showSuccess('Cập nhật thành công')
        // let response;
        // if (event.xhr.response) {
        //   response = JSON.parse(event.xhr.response)
        // }
        // let urlImage = ''
        // if (response && response.message) {
        //   urlImage = response.object;
        //   this.selectCategories.bannerImage = this.urlServerFrontEnd + urlImage;
        // }
      }
    }, 1000);

    if (event && event.xhr && event.xhr.response) {
      const document = JSON.parse(event.xhr.response)
      if (document) {
        // this.ultilities.showSuccess('upload thanh cong')
        this.selectCategories.bannerImage = this.urlServerFrontEnd + this.ultilities.clearNameKytuDacbiet(document.object);
      }
    }
  }

  onUploadError(event, modal) {
    setTimeout(() => {
      this.progressBarPercent = 100
      if (modal) modal.hide()
      if (event && event.xhr) {
        let response;
        if (event.xhr.response) {
          response = JSON.parse(event.xhr.response)
        }
        let message = 'Cập nhật không thành công'
        if (response && response.message) {
          message = response.message
        }
        this.ultilities.showError(message)
      }
    }, 1000)
  }
  // Upload Icon
  btnUploadClickIcon(fileUploadIcon){
    fileUploadIcon.advancedFileInput.nativeElement.click()
  }
  onSelectedFilesIcon(modal, fileUploadIcon) {
    // modal.show()
    fileUploadIcon.upload()
  }
  onBeforUploadIcon(event, fileUploadIcon) {    
    event.xhr.setRequestHeader('Authorization', + this.ultilities.getToken())
    // if (event.formData) {
    // }
  }
  onUploadSuccessIcon(event, modal) {
    if (event && event.xhr && event.xhr.response) {
      const document = JSON.parse(event.xhr.response)
      if (document) {
        // this.ultilities.showSuccess('upload thanh cong')
        this.selectCategories.iconUrl = this.urlServerFrontEnd + document.object;
      }
    }
  }
  onUploadErrorIcon(event, modal) {
    setTimeout(() => {
      this.progressBarPercent = 100
      if (modal) modal.hide()
      if (event && event.xhr) {
        let response;
        if (event.xhr.response) {
          response = JSON.parse(event.xhr.response)
        }
        let message = 'Cập nhật không thành công'
        if (response && response.message) {
          message = response.message
        }
      }
    }, 1000)
  }
}




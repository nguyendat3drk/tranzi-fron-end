import { Utilities } from '../../../../shared/services/utilities.service';
import { Component, OnInit } from '@angular/core';
import { MessageService } from 'primeng/components/common/messageservice';
import { ActivatedRoute, Router } from '@angular/router';
import { LoaderService } from '../../../../shared/services/loader.service';
import { CategoriesService } from '../../../../shared/services/http/categories.service';
import { Categories } from '../../../../shared/models/Categories';
import { TranslateService } from '@ngx-translate/core';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'order-category',
  templateUrl: './order-category.component.html',
  styleUrls: ['./order-category.component.scss'],
})
export class OrderCategoryComponent implements OnInit {

  listCategory: Categories[] = [];

  constructor(private router: Router, private translateService: TranslateService,
    private messageService: MessageService,
    private loaderService: LoaderService, private ultilities: Utilities,
    private categoriesService: CategoriesService) {
  }
  ngOnInit() {
    // this.getParentTree();
    this.testGetTree();
  }
  testGetTree() {
    this.listCategory = [];
    this.categoriesService.getAlls().subscribe((response: any) => {
      if (response) {
        this.listCategory = response;
      }
      this.loaderService.hide()
    }, error => {
      this.loaderService.hide();
    });
  }
  updateOrder(){
    if(this.listCategory){
      for(let i =0;i<this.listCategory.length;i++){
        this.listCategory[i].order = (i+1);
      }
    }
    this.loaderService.show()
    this.categoriesService.updateOrderCategoty(this.listCategory).subscribe((response: any) => {
      if (response && response.resultCode === 200) {
        this.loaderService.hide();
        this.ultilities.showSuccess('Cập nhật thành công')
      }else{
        this.ultilities.showError('Có lỗi xảy ra')
      }
      this.loaderService.hide();
      this.loaderService.hide()
    }, error => {
      this.loaderService.hide();
      this.loaderService.hide();
    });
  }
  bankList(){
    this.router.navigate(['/tranzi-admin/categories']);
  }
  
}




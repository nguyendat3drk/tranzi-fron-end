import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BvReportComponent } from './bv-report.component';

describe('BvReportComponent', () => {
  let component: BvReportComponent;
  let fixture: ComponentFixture<BvReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BvReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BvReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Utilities } from '../../../shared/services/utilities.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ProductService } from '../../../shared/services/http/product.service';
import { MessageService } from 'primeng/components/common/messageservice';
import { ActivatedRoute, Router } from '@angular/router';
import { LoaderService } from '../../../shared/services/loader.service';
import { CategoriesService } from '../../../shared/services/http/categories.service';
import { Categories } from '../../../shared/models/Categories';
import { PagerData } from '../../../shared/entity/pagerData';
import { isFulfilled } from 'q';
import { TreeNode } from 'primeng/api';
import { ThemeSettingsComponent } from '../../../@theme/components';
import { ModalDirective } from 'ngx-bootstrap';
import { Setting } from '../../../shared/models/Setting';
import { SettingService } from '../../../shared/services/http/SettingService';
import { moment } from 'ngx-bootstrap/chronos/test/chain';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'setting',
  templateUrl: './setting.component.html',
  styleUrls: ['./setting.component.scss'],
})
export class SettingComponent implements OnInit {
  listSetting: Setting[] = [];
  totalPage: number = 0;
  selectSetting: Setting = new Setting();
  txtSearch: string = '';
  loading: boolean;
  @ViewChild('confirmDialodDel') public confirmDialodDel: ModalDirective;
  idDelete: Number = 0;
  title: string = '';
  constructor(private utilities: Utilities, private router: Router,
    private activeRouter: ActivatedRoute, private messageService: MessageService,
    private loaderService: LoaderService, private ultilities: Utilities,
    private settingService: SettingService) {
      this.ultilities.closeAutoMenu();
  }

  ngOnInit() {
    setTimeout(() => {
      if (localStorage.getItem('addSetting')) {
        this.ultilities.showSuccess(localStorage.getItem('addSetting'));
        localStorage.removeItem('addSetting')
      } else if (localStorage.getItem('editSetting')) {
        this.ultilities.showSuccess(localStorage.getItem('editSetting'));
        localStorage.removeItem('editSetting')
      }
    }, 300);
    this.loadCategories();
  }
  
  loadCategories() {
    this.loaderService.show();
    this.settingService.getAllsPadding(1, 10, 'id', 'desc', this.txtSearch).subscribe((response: any) => {
      this.listSetting = [];
      this.totalPage = 0;
      if (response && response.resultCode === 200) {
        let page = new PagerData();
        page = response.object;
        if (page && page.content) {
          this.listSetting = page.content;
          this.totalPage = page.totalRow;
        }
        // this.listProduct = response;
      }
      this.loaderService.hide()
    }, error => {
      this.listSetting = [];
      this.totalPage = 0;
      this.loaderService.hide();
    });
  }
  createSetting() {
    this.router.navigate(['/tranzi-admin/setting-create']);
  }
  updateSetting(id) {
    if(id){
      this.router.navigate(['/tranzi-admin/setting-update/'+id]);
    }
  }
  paginate(event) {
    if (event) {
      this.listSetting = [];
      // this.totalPage = 0;
      // tslint:disable-next-line:max-line-length
      this.settingService.getAllsPadding(event.page ? (event.page + 1) : 1, event.rows ? event.rows : 10, 'id', 'desc', this.txtSearch).subscribe((response: any) => {
        // tslint:disable-next-line:no-console
        // console.log(response)
        if (response && response.resultCode === 200) {
          let page = new PagerData();
          page = response.object;
          if (page && page.content) {
            this.listSetting = page.content;
            this.totalPage = page.totalRow;
            // this.totalPage = this.totalPage.s
          }
          // this.listProduct = response;
        }
        this.loaderService.hide()
      }, error => {
        this.totalPage = 0
        this.loaderService.hide();
      });
    }

  }


  seSelectCategories(event) {
    this.selectSetting = new Setting()
    if (event && event.data) {
      this.selectSetting = event.data;
    }
  }
  routingUpdate(id) {
    if (id) {
      this.router.navigate(['/tranzi-admin/setting-update/' + id]);
    }
  }
  deleteByID(idSetting) {
    if (idSetting) {
      this.loaderService.show();
      this.settingService.deletebyiId(idSetting).subscribe((response: any) => {
        if (response && response.resultCode === 200) {
          this.loadCategories();
          this.ultilities.showSuccess('Xóa thành công');
        } else {
          this.ultilities.showError(response.errorMessage ? response.errorMessage : 'Có lỗi xảy ra. Liên hệ Admin để kiểm tra lại')
        }
        this.loaderService.hide()
        // this.ultilities.showSuccess('Delete Success Categories')
      }, error => {
        this.loaderService.hide();
        this.ultilities.showError('Delete Error Setting')
      });
    }
  }
  viewDate(event) {
    if (event && Number(event) > 0) {
      let dt = new Date(Number(event) * 1000)
      const d2 = moment(dt, 'DD/MM/YYYY HH:mm')
      return d2;
    }
    return ''
  }
}

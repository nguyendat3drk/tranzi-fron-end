import { Utilities } from '../../../../shared/services/utilities.service';
import { Component, OnInit } from '@angular/core';
import { ProductService } from '../../../../shared/services/http/product.service';
import { MessageService } from 'primeng/components/common/messageservice';
import { ActivatedRoute, Router } from '@angular/router';
import { LoaderService } from '../../../../shared/services/loader.service';
import { CategoriesService } from '../../../../shared/services/http/categories.service';
import { Categories } from '../../../../shared/models/Categories';
import { TranslateService } from '@ngx-translate/core';
import { TreeNode } from 'primeng/primeng';
import { ThemeSettingsComponent } from '../../../../@theme/components';
import { Setting } from '../../../../shared/models/Setting';
import { SettingService } from '../../../../shared/services/http/SettingService';
import { moment } from 'ngx-bootstrap/chronos/test/chain';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'setting-create',
  templateUrl: './setting-create.component.html',
  styleUrls: ['./setting-create.component.scss'],
})
export class SettingCreateComponent implements OnInit {
  selectSetting: Setting = new Setting();

  blKey: boolean = false;
  selectCategories: Categories = new Categories();

  urlApi: string = '';
  urlServerFile: string = '';
  constructor(private router: Router, private translateService: TranslateService,
    private utilities: Utilities, private messageService: MessageService,
    private loaderService: LoaderService, private ultilities: Utilities,
    private settingService: SettingService) {
    this.urlApi = this.ultilities.getUrlServerHost() + '/share-api/uploadSettingImage'
    this.urlServerFile = this.ultilities.urlServerFrontEnd() + 'assets/banner/'
  }
  changeBlurKey(){
    this.blKey = true
  }
  
  ngOnInit() {
    this.selectSetting.type = 'image'
  }

  changeType(){
    this.selectSetting.value='';
  }
  updateSetting() {
    this.selectSetting.id = 0;
    if (!this.selectSetting || !this.selectSetting.key) {
      return;
    }

    this.settingService.create(this.selectSetting).subscribe((response: any) => {
      // tslint:disable-next-line:no-console
      // console.log('responseresponseresponse=', response);
      if (response && response.resultCode === 200) {
        this.router.navigate(['/tranzi-admin/setting']);
        localStorage.setItem('addSetting', 'Thêm mới thành công');
      } else if (response && response.resultCode === 204) {
        this.ultilities.showError('Not Update Database')
      }
      window.scrollTo(0, 0);
      this.loaderService.hide();
    }, (error) => {
      // tslint:disable-next-line:no-console
      let mes = '';
      if (error && error.error && error.error.errors && error.error.errors[0] && error.error.errors[0].message) {
        mes = error.error.errors[0].message;
      }
      this.messageService.clear();
      this.messageService.add({
        detail: mes ? mes : 'Có lỗi xảy ra. Liên hệ Admin để kiểm tra lại',
        severity: 'error',
        summary: 'Lỗi',
      });
      this.loaderService.hide();
    });
  }
  bankListSetting() {
    this.router.navigate(['/tranzi-admin/setting']);
  }
  // upload===============================================UPLOAD====================
  btnUploadClick(fileUpload) {
    fileUpload.advancedFileInput.nativeElement.click()
  }

  onSelectedFiles(modal, fileUpload) {
    if (fileUpload && fileUpload.msgs) {
      for (let i = 0; i < fileUpload.msgs.length; i++) {
        const element = fileUpload.msgs[i];
        if (element) {
          const maxsize = element.detail.indexOf('maximum');
          let valueDetail = '';
          let valueType = '';
          valueDetail = element.summary.split(':')[0];
          if (maxsize < 0) {
            valueType = 'Không đúng định dạng'
            // tslint:disable-next-line:prefer-const
            let dto = {
              value: valueDetail,
              type: valueType,
            }
            // this.uploadedFilesErrorInvalid.push(dto)
          }
        }
      }
    }
    // const checkNumberError = Number(this.uploadedFilesErrorInvalid ? this.uploadedFilesErrorInvalid.length : 0) +
    //   Number(this.uploadedFilesError ? this.uploadedFilesError.length : 0);
    // if (this.totalFile === checkNumberError) {
    //   this.progressBarPercent = 100
    //   this.ultilities.showError('Cập nhật thất bại')
    // }
    // modal.show()
    fileUpload.upload()
  }
  onBeforUpload(event, fileUpload) {
    event.xhr.setRequestHeader('Authorization', + this.ultilities.getToken())
    if (event.formData) {
    }
  }
  viewDate(event) {
    if (event && Number(event) > 0) {
      let dt = new Date(Number(event) * 1000)
      const d2 = moment(dt, 'DD/MM/YYYY HH:mm')
      return d2;
    }
    return ''
  }
  onUploadSuccess(event, modal) {
    if (event && event.xhr && event.xhr.response) {
      const document = JSON.parse(event.xhr.response)
      if (document && document.object) {
        console.log(document.object)
        this.selectSetting.value = this.urlServerFile + document.object
      }
    }
  }
  onUploadError(event, modal) {

  }

  // ===================END UPLOAD==============================
}

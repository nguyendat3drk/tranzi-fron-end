import { Injectable } from '@angular/core';
import {BehaviorSubject} from 'rxjs/Rx';

@Injectable()
export class TranziAdminStateService {

  isShowSideBar: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(true);
  constructor() { }

  toggleSideBar() {
    this.isShowSideBar.next(!this.isShowSideBar.value);
  }
}

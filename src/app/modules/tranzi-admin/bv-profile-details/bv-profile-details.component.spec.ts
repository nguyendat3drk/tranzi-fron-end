import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BvProfileDetailsComponent } from './bv-profile-details.component';

describe('BvProfileDetailsComponent', () => {
  let component: BvProfileDetailsComponent;
  let fixture: ComponentFixture<BvProfileDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BvProfileDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BvProfileDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BvDashboardItemComponent } from './bv-dashboard-item.component';

describe('BvDashboardItemComponent', () => {
  let component: BvDashboardItemComponent;
  let fixture: ComponentFixture<BvDashboardItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BvDashboardItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BvDashboardItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

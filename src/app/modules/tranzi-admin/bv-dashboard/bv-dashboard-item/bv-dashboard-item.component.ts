import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'bv-dashboard-item',
  templateUrl: './bv-dashboard-item.component.html',
  styleUrls: ['./bv-dashboard-item.component.scss']
})
export class BvDashboardItemComponent implements OnInit {

  @Input() title: string;
  @Input() img: string;
  @Input() content: string;
  constructor() { }

  ngOnInit() {
  }

}

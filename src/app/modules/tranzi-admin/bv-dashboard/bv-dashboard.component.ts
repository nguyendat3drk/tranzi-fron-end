import { Component, OnInit } from '@angular/core';
import { CategoriesService } from '../../../shared/services/http/categories.service';
import { ProductService } from '../../../shared/services/http/product.service';
import { CustomerOrderService } from '../../../shared/services/http/customer-order.service';
import { CustomerService } from '../../../shared/services/http/customer.service';
import { Utilities } from '../../../shared/services/utilities.service';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'dashboard',
  templateUrl: './bv-dashboard.component.html',
  styleUrls: ['./bv-dashboard.component.scss'],
})
export class BvDashboardComponent implements OnInit {


  constructor(private categoriesService: CategoriesService, private productService: ProductService,
    private ultilities: Utilities,
    private customerOrderService: CustomerOrderService, private customerService: CustomerService) {
    this.ultilities.closeAutoMenu();
  }
  countProduct: number = 0;
  countOrder: number = 0;
  countCustomer: number = 0;
  ngOnInit() {
    this.productService.countProduct().subscribe((response: any) => {
      if (response && Number(response) >= 0) {
        this.countProduct = Number(response);
      }
    }, error => {
    });
    this.customerOrderService.countOrder().subscribe((response: any) => {
      if (response && Number(response) >= 0) {
        this.countOrder = Number(response);
      }
    }, error => {
    });
    this.customerService.countCustomer().subscribe((response: any) => {
      if (response && Number(response) >= 0) {
        this.countCustomer = Number(response);
      }
    }, error => {
    });
  }

}

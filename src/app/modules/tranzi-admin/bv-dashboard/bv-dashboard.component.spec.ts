import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BvDashboardComponent } from './bv-dashboard.component';

describe('BvDashboardComponent', () => {
  let component: BvDashboardComponent;
  let fixture: ComponentFixture<BvDashboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BvDashboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BvDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

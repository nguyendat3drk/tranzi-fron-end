import { MessageService } from 'primeng/components/common/messageservice';
import { Claim } from '../../../../shared/models/claim';
import { ClaimService } from '../../../../shared/services/http/claim.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ClaimStatus } from '../../../../shared/models/claim-status';
import { ConfirmService } from '../../../../shared/confirm-dialog/ConfirmService';
import { ReservePayment } from '../../../../shared/models/reserve-payment';
import { TypePayment } from '../../../../shared/models/type-payment';
import { BvFormClaimInfoComponent } from './bv-form-claim-info/bv-form-claim-info.component';
import { LoaderService } from '../../../../shared/services/loader.service';
import { TypeStatusOffDriver } from '../../../../shared/models/type-statusOffDriver';
import { TypeDamageLevel } from '../../../../shared/models/type-damageLevel';
import { moment } from 'ngx-bootstrap/chronos/test/chain';
import { Router } from '@angular/router';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'bv-claim-create',
  templateUrl: './bv-claim-create.component.html',
  styleUrls: ['./bv-claim-create.component.scss'],
})
export class BvClaimCreateComponent implements OnInit {

  claim: Claim = new Claim();
  @ViewChild(BvFormClaimInfoComponent) claimInfoForm: BvFormClaimInfoComponent;
  constructor(
    private claimService: ClaimService,
    private messageService: MessageService,
    private _confirm: ConfirmService,
    private loaderService: LoaderService,
    private router: Router) {
  }

  claimGaras: any;

  ShowData(data) {
  }
  ngOnInit() {
    this.claim = new Claim();
  }
  onSubmit(event) {
    this.claimInfoForm.handleSubmit();
  }

}

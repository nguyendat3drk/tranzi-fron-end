import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {BvClaimCreateComponent} from './bv-claim-create.component';

describe('BvClaimCreateComponent', () => {
  let component: BvClaimCreateComponent;
  let fixture: ComponentFixture<BvClaimCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BvClaimCreateComponent ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BvClaimCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

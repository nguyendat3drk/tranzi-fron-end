import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ConfirmService } from './../../../../../shared/confirm-dialog/ConfirmService';
import { Utilities } from './../../../../../shared/services/utilities.service';
// import { ENGINE_METHOD_DIGESTS } from 'constants';
@Component({
  // tslint:disable-next-line:component-selector
  selector: 'bv-form-claim-info',
  templateUrl: './bv-form-claim-info.component.html',
  styleUrls: ['./bv-form-claim-info.component.scss'],
})
export class BvFormClaimInfoComponent implements OnInit {
  constructor(
    private route: ActivatedRoute,
    private _confirm: ConfirmService,
    private utilities: Utilities) {
  }

  ngOnInit() {
  }
  handleSubmit() {

  }
}

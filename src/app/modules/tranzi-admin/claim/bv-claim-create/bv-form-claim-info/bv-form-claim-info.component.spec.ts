import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {BvFormClaimInfoComponent} from './bv-form-claim-info.component';

describe('BvFormClaimInfoViewUpdateComponent', () => {
  let component: BvFormClaimInfoComponent;
  let fixture: ComponentFixture<BvFormClaimInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BvFormClaimInfoComponent ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BvFormClaimInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

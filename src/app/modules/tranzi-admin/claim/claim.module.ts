import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { BvClaimCreateComponent } from './bv-claim-create/bv-claim-create.component';
import { BvFormClaimInfoComponent } from './bv-claim-create/bv-form-claim-info/bv-form-claim-info.component';
import { SharedModule } from '../../../shared/shared.module';
import { HttpClientModule } from '@angular/common/http';
import { DataTableModule } from 'primeng/datatable';
import { ContextMenuModule } from 'primeng/contextmenu';
import { ModalModule } from 'ngx-bootstrap';
import { ConfirmDialogModule } from './../../../shared/confirm-dialog/confirm-module';

import { PdfViewerModule } from 'ng2-pdf-viewer';
import { CalendarModule } from 'primeng/calendar';
import { TextMaskModule } from 'angular2-text-mask';
import {NgxMaskModule} from 'ngx-mask';
import { CurrencyMaskModule } from 'ng2-currency-mask';
import { BvDateComponent } from './../date/date.component';

const routes: Routes = [
  {
    path: 'create',
    component: BvClaimCreateComponent,
    data: { isCreate: true },
  },
  { path: '/:id', component: BvClaimCreateComponent },
];

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    HttpClientModule,
    RouterModule.forChild(routes),
    DataTableModule,
    ContextMenuModule,
    ModalModule.forRoot(),
    ConfirmDialogModule,
    PdfViewerModule,
    CalendarModule,
    TextMaskModule,
    NgxMaskModule.forRoot(),
    CurrencyMaskModule,
  ],
  declarations: [
    BvClaimCreateComponent,
    BvFormClaimInfoComponent,
    BvDateComponent,
  ],
  exports: [RouterModule],
  entryComponents: [],
})
export class ClaimModule {
}

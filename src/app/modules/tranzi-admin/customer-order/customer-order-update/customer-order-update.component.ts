
import { Utilities } from '../../../../shared/services/utilities.service';
import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { MessageService } from 'primeng/components/common/messageservice';
import { ActivatedRoute, Router } from '@angular/router';
import { LoaderService } from '../../../../shared/services/loader.service';
import { Customer } from '../../../../shared/models/Customer';
import { CustomerService } from '../../../../shared/services/http/customer.service';
import { CustomerOrderService } from '../../../../shared/services/http/customer-order.service';
import { CustomerOrder } from '../../../../shared/models/CustomerOrder';
import { CustomerOrderProduct } from '../../../../shared/models/CustomerOrderProduct';
import { Product } from '../../../../shared/models/Product';
import { ProductService } from '../../../../shared/services/http/product.service';
import { CityService } from '../../../../shared/entity/City.service';
import { DistrictService } from '../../../../shared/entity/District.service';
@Component({
  // tslint:disable-next-line:component-selector
  selector: 'customer-order-update',
  templateUrl: './customer-order-update.component.html',
  styleUrls: ['./customer-order-update.component.scss'],
})
export class CustomerOrderUpdateComponent implements OnInit {
  customerId: number = 0;
  customersUser: Customer = new Customer();
  selectCustomerOrder: CustomerOrder = new CustomerOrder();
  listCustomers: Customer[] = [];
  listCustomerOrderProduct: CustomerOrderProduct[] = [];
  listProduct: Product[] = [];
  blUser: boolean = false;
  listCity: any[] = [];
  listDistrict: any[] = [];

  totalTable: number = 0;


  // validate
  blName: boolean = false;
  blOrder: boolean = false;
  blEmail: boolean = false;
  blEmailFomat: boolean = false;
  blPhone: boolean = false;
  blAddress: boolean = false;
  blDistrict: boolean = false;
  blCity: boolean = false;
  blPass: boolean = false;
  blPassFomat: boolean = false;
  typeRole: number = null;

  constructor(private utilities: Utilities, private router: Router, private customerOrderService: CustomerOrderService,
    private activeRouter: ActivatedRoute, private cityService: CityService, private districtService: DistrictService, private productService: ProductService,
    private loaderService: LoaderService, private ultilities: Utilities,
    private customerService: CustomerService) {
    this.listCity = [];
    this.listCity = this.cityService.getCity();
    this.listDistrict = [];
    this.listDistrict = this.districtService.getDistrict();
    this.activeRouter.params.subscribe((params) => {
      this.customerId = +params.id
      if (this.customerId) {
        this.getCustomerSelected();
        this.getlistProductCustomerOrder();
      }
    });
  }

  getlistProductCustomerOrder() {
    this.listCustomerOrderProduct = [];
    this.customerOrderService.getAllByCustomerOrderId(this.customerId).subscribe((response: any) => {
      if (response && response.resultCode === 200) {
        this.listCustomerOrderProduct = response.object;
      }
      this.loaderService.hide()
    }, error => {
      this.loaderService.hide();
    });
  }
  getCustomerUser(key) {
    this.customerService.getCustomerNameId(key).subscribe((response: any) => {
      if (response && response.resultCode === 200) {
        this.customersUser = response.object;
      }
      this.loaderService.hide()
    }, error => {
      this.loaderService.hide();
    });
  }
  getCustomerSelected() {
    this.customerOrderService.getCustomerOrderById(this.customerId).subscribe((response: any) => {
      if (response && response.resultCode === 200) {
        this.selectCustomerOrder = response.object;
        if (this.selectCustomerOrder && this.selectCustomerOrder.customerId && Number(this.selectCustomerOrder.customerId) > 0) {
          this.getCustomerUser(this.selectCustomerOrder.customerId);
        }
        if (this.selectCustomerOrder && Number(this.selectCustomerOrder.status) === 0) {
          this.typeRole = 0;
        } else if (this.selectCustomerOrder && Number(this.selectCustomerOrder.status) === 1) {
          this.typeRole = 1;
        } else if (this.selectCustomerOrder && Number(this.selectCustomerOrder.status) === 2) {
          this.typeRole = 2;
        } else if (this.selectCustomerOrder && Number(this.selectCustomerOrder.status) === 3) {
          this.typeRole = 3;
        }
        this.selectCustomerOrder.listProductOrderDelete = [];
      }
      this.loaderService.hide()
    }, error => {
      this.loaderService.hide();
    });
  }

  ngOnInit() {
    // this.resetData();
    this.searchAddressWithoutObject();
  }
  resetData() {
    this.selectCustomerOrder = new CustomerOrder();
    this.selectCustomerOrder.shipmentStatus = 0
    this.selectCustomerOrder.paymentMethod = 0;
    this.selectCustomerOrder.paymentStatus = '0';
    this.selectCustomerOrder.status = 0;
    this.listCustomerOrderProduct = [];
  }
  searchAddressWithoutObject() {
    this.listProduct = [];
    this.productService.getAllsNotDetail().subscribe((response: any) => {
      this.listProduct = response;
    }, error => {
      return null;
    });
  }
  addRow() {
    if(!this.listCustomerOrderProduct){
      this.listCustomerOrderProduct = []
    }
    let dtoOrderProduct = new CustomerOrderProduct()
    dtoOrderProduct.id = 0;
    dtoOrderProduct.customerOrderId = this.customerId;
    this.listCustomerOrderProduct.push(dtoOrderProduct);
  }
  deleteRow(index) {
    if (this.listCustomerOrderProduct && this.listCustomerOrderProduct[index]) {
      this.selectCustomerOrder.listProductOrderDelete.push(this.listCustomerOrderProduct[index])
      this.listCustomerOrderProduct.splice(index, 1);
    }
  }
  changeNumber(event) {
    if (event === 'phone') {
      if (this.selectCustomerOrder.shippingMobile && !isNaN(Number(this.selectCustomerOrder.shippingMobile))) {
      } else {
        this.selectCustomerOrder.shippingMobile = '0';
      }
    }
  }
  validateCustomer(event) {
    let check = true;
    if (event === 'blName' || event === 'save') {
      this.blName = false;
      if (!this.selectCustomerOrder || !this.selectCustomerOrder.shippingName) {
        this.blName = true;
        check = false;
      }
    }
    if (event === 'blOrder' || event === 'save') {
      this.blOrder = false;
      if (!this.selectCustomerOrder || !this.selectCustomerOrder.orderCode) {
        this.blOrder = true;
        check = false;
      }
    }
    if (event === 'email' || event === 'save') {
      this.blEmail = false;
      this.blEmailFomat = false;
      if (!this.selectCustomerOrder || !this.selectCustomerOrder.shippingEmail || !this.selectCustomerOrder.shippingEmail.trim()) {
        this.blEmail = true;
        check = false;
      } else if (!this.ultilities.checkEmail(this.selectCustomerOrder.shippingEmail.trim())) {
        this.blEmailFomat = true;
        check = false;
      }
    }
    if (event === 'blPhone' || event === 'save') {
      this.blPhone = false;
      if (!this.selectCustomerOrder || this.selectCustomerOrder.shippingMobile === undefined || this.selectCustomerOrder.shippingMobile === null) {
        this.blPhone = true;
        check = false;
      }
    }
    if (event === 'blAddress' || event === 'save') {
      this.blAddress = false;
      if (!this.selectCustomerOrder || !this.selectCustomerOrder.shippingAddress) {
        this.blAddress = true;
        check = false;
      }
    }
    if (event === 'blCity' || event === 'save') {
      this.blCity = false;
      if (!this.selectCustomerOrder || !this.selectCustomerOrder.shippingCity) {
        this.blCity = true;
        check = false;
      }
    }
    if (event === 'blDistrict' || event === 'save') {
      this.blDistrict = false;
      if (!this.selectCustomerOrder || !this.selectCustomerOrder.shippingDistrict) {
        this.blDistrict = true;
        check = false;
      }
    }
    return check;
  }
  onChange($event, index) {
    console.log('change', $event);
    if (this.listCustomerOrderProduct && this.listCustomerOrderProduct[index]) {
      this.listCustomerOrderProduct[index].unitPrice = 0;
    }
    this.updateTotalPrice();
  }
  onBlur($event: Event) {
    console.log('onBlur', $event);
  }
  updateTotalPrice() {
    this.totalTable = 0;
    if (this.listCustomerOrderProduct) {
      for (let i = 0; i < this.listCustomerOrderProduct.length; i++) {
        this.listCustomerOrderProduct[i].total = (this.listCustomerOrderProduct[i].quantity ? Number(this.listCustomerOrderProduct[i].quantity) : 0) *
          (this.listCustomerOrderProduct[i].unitPrice ? Number(this.listCustomerOrderProduct[i].unitPrice) : 0)
        this.totalTable = this.totalTable + this.listCustomerOrderProduct[i].total;
      }
    }
    this.selectCustomerOrder.subTotal = this.totalTable
    this.updateTotalPriceOrder();
  }
  onAdd($event, index) {

    if ($event) {
      this.listCustomerOrderProduct[index].blProduct = false
      let productDTO = new Product();
      productDTO = $event;

      let count = 0;
      if (this.listCustomerOrderProduct) {
        for (let i = 0; i < this.listCustomerOrderProduct.length; i++) {
          if (this.listCustomerOrderProduct[i].productId === productDTO.id) {
            count++;
          }
        }
      }
      if (count < 1) {
        let dt = new Date();
        let startDate = new Date();
        let endDate = new Date();
        if (productDTO.saleStart && productDTO.saleEnd) {
          startDate.setTime(Number(productDTO.saleStart) * 1000)
          startDate.setHours(0)
          startDate.setMinutes(0)
          startDate.setSeconds(0)
          endDate.setTime(Number(productDTO.saleEnd) * 1000)
          endDate.setHours(23)
          endDate.setMinutes(59)
          endDate.setSeconds(59)
          if (startDate <= dt && dt <= endDate) {
            console.log(true);
            this.listCustomerOrderProduct[index].unitPrice = productDTO.salePrice
          } else {
            console.log(true);
            this.listCustomerOrderProduct[index].unitPrice = productDTO.normalPrice
          }
        } else if (!productDTO.saleStart && !productDTO.saleEnd && Number(productDTO.salePrice) > 0) {
          this.listCustomerOrderProduct[index].unitPrice = productDTO.salePrice
        } else if ((!productDTO.saleStart || productDTO.saleEnd)) {
          endDate.setTime(Number(productDTO.saleEnd) * 1000)
          endDate.setHours(23)
          endDate.setMinutes(59)
          endDate.setSeconds(59)
          if (dt <= endDate) {
            this.listCustomerOrderProduct[index].unitPrice = productDTO.salePrice
          } else {
            this.listCustomerOrderProduct[index].unitPrice = productDTO.normalPrice
          }
        } else if ((productDTO.saleStart || !productDTO.saleEnd)) {
          startDate.setTime(Number(productDTO.saleStart) * 1000)
          startDate.setHours(0)
          startDate.setMinutes(0)
          startDate.setSeconds(0)
          if (dt >= startDate) {
            this.listCustomerOrderProduct[index].unitPrice = productDTO.salePrice
          } else {
            this.listCustomerOrderProduct[index].unitPrice = productDTO.normalPrice
          }
        }
      } else {
        setTimeout(() => {
          this.listCustomerOrderProduct[index].productId = 0;
        }, 100);
        this.ultilities.showError('Sản phẩm đã được chọn')
      }

    }
    this.updateTotalPrice();
  }
  changeCity(event) {
    if (event && event.code) {
      this.listDistrict = this.districtService.getDistrictByCity(event.code);
    } else {
      this.listDistrict = this.districtService.getDistrict();
    }
  }
  changeDistrict(event) {
    this.blCity = false
    if (event && event.parent_code) {
      this.selectCustomerOrder.shippingCity = event.parent_code;
    } else {
      this.selectCustomerOrder.shippingCity = null;
    }
  }
  updateTotalPriceOrder() {
    if (this.selectCustomerOrder) {
      this.selectCustomerOrder.total = ((this.selectCustomerOrder.subTotal ? Number(this.selectCustomerOrder.subTotal) : 0) +
        (this.selectCustomerOrder.shippingFee ? Number(this.selectCustomerOrder.shippingFee) : 0))
    }
  }
  keyPress(event: any, type) {
    if (type && type === 'number') {
      const patternExtension = /[0-9]/;
      const inputChar = String.fromCharCode(event.charCode);
      if (event.keyCode !== 9 && !patternExtension.test(inputChar)) {
        event.preventDefault();
      }
    } else if (type && type === 'date') {
      const patternExtension = /[0-9\/]/;
      const inputChar = String.fromCharCode(event.charCode);
      if (event.keyCode !== 9 && !patternExtension.test(inputChar)) {
        event.preventDefault();
      }
    } else {
      const patternExtension = /[0-9\.]/;
      const inputChar = String.fromCharCode(event.charCode);
      if (event.keyCode !== 9 && !patternExtension.test(inputChar)) {
        event.preventDefault();
      }
    }
  }
  validateListTable(index, type) {
    let check = true;
    if (index !== undefined && index !== null && Number(index) >= 0) {
      if (this.listCustomerOrderProduct[index] && type === 'product') {
        this.listCustomerOrderProduct[index].blProduct = false;
        if (!this.listCustomerOrderProduct[index].productId) {
          this.listCustomerOrderProduct[index].blProduct = true;
          check = false;
        }
      }
      if (this.listCustomerOrderProduct[index] && type === 'quantity') {
        this.listCustomerOrderProduct[index].blQuantity = false;
        if (this.listCustomerOrderProduct[index].quantity === undefined || this.listCustomerOrderProduct[index].quantity === null) {
          this.listCustomerOrderProduct[index].blQuantity = true;
          check = false;
        }
      }
    } else if (type === 'save') {
      if (this.listCustomerOrderProduct) {
        for (let i = 0; i < this.listCustomerOrderProduct.length; i++) {
          this.listCustomerOrderProduct[i].blProduct = false;
          if (!this.listCustomerOrderProduct[i].productId) {
            this.listCustomerOrderProduct[i].blProduct = true;
            check = false;
          }
          this.listCustomerOrderProduct[i].blQuantity = false;
          if (this.listCustomerOrderProduct[i].quantity === undefined || this.listCustomerOrderProduct[i].quantity === null) {
            this.listCustomerOrderProduct[i].blQuantity = true;
            check = false;
          }
        }
      }
    }
    return check;
  }
  backManagement() {
    this.router.navigate(['/tranzi-admin/customer-order']);
  }
  submitOk() {
    let check = true;
    check = this.validateCustomer('save')
    if (!check) {
      return;
    }
    let checkTable = true;
    if (!this.listCustomerOrderProduct || this.listCustomerOrderProduct.length <= 0) {
      this.ultilities.showError('Chưa có chi tiết sản phẩm của đơn hàng');
      return;
    }
    checkTable = this.validateListTable(null, 'save');
    if (!checkTable) {
      return;
    }
    this.loaderService.show();
    this.selectCustomerOrder.listProductOrder = this.listCustomerOrderProduct;
    this.customerOrderService.create(this.selectCustomerOrder).subscribe((response: any) => {
      if (response && response.resultCode === 200) {
        localStorage.removeItem('addOrder')
        localStorage.setItem('addOrder', 'Cập nhật thành công')
        this.router.navigate(['/tranzi-admin/customer-order']);
      } else if (response && response.resultCode === 201) {
        this.ultilities.showError('Mã đơn hàng ' + this.selectCustomerOrder.orderCode + ' đã tồn tại trong hệ thống.');
      } else {
        const mes = response.errorMessage;
        this.ultilities.showError(mes ? mes : 'Có lỗi xảy ra. Liên hệ Admin để kiểm tra lại');
      }
      this.loaderService.hide()
    }, error => {
      this.loaderService.hide();
      this.ultilities.showError('Có lỗi xảy ra. Liên hệ Admin để kiểm tra lại');
    });

  }
  viewDate(event) {
    if (event && Number(event) > 0) {
      return this.ultilities.convertDateToStringDateVN(new Date(Number(event) * 1000));
    }
    return ''
  }

}








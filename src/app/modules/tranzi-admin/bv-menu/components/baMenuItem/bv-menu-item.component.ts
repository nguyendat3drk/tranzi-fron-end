import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { Router } from '@angular/router';
import 'rxjs/add/operator/switchMap';
import { Utilities } from './../../../../../shared/services/utilities.service';
// import { ConfirmService } from '../../../../../pages/confirm-dialog/ConfirmService';
// import { ModalDirective } from 'ng2-bootstrap/modal';

const KEY_ESC = 27;

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'bv-menu-item',
  templateUrl: './bv-menu-item.component.html',
  styleUrls: ['./bv-menu-item.component.scss'],
})
// tslint:disable-next-line:component-class-suffix
export class BvMenuItem implements OnInit {
  public confirmDialog: boolean = true;
  public title: string;
  public message: string;
  public okText: string;
  public cancelText: string;
  public closeText: string;
  public isShow: boolean = false;
  public isOn: boolean = false;
  @Input() menuItem: any;
  @Input() child: boolean = false;
  @Input() isLastItem: boolean = false;

  @Output() itemHover = new EventEmitter<any>();
  @Output() toggleSubMenu = new EventEmitter<any>();

  // @ViewChild('staticModal') public staticModal: ModalDirective;
  private _defaults = {
    title: 'Confirm',
    message: 'Confirm the operation?',
    cancelText: 'Không',
    okText: 'Có',
    closeText: 'Đóng',
  };
  private _confirmElement: any;
  private _cancelButton: any;
  private _okButton: any;
  private _closeButton: any;
  private _offButton: any;
  roleId: number = 0;
  constructor(
    private router: Router,
    private translateService: TranslateService,
    private utilities: Utilities,
    // private _confirm: ConfirmService
  ) {
    this.confirmDialog = true;
    this.roleId = utilities.getRoleId();
    // console.log('RoleId=', this.roleId );
  }

  public onHoverItem($event): void {
    this.itemHover.emit($event);
  }

  public onToggleSubMenu($event, item): boolean {
    $event.item = item;
    this.toggleSubMenu.emit($event);
    return false;
  }

  public onClickMenu() {
    // console.log('clickMenu');
    localStorage.removeItem('selectedMenu');
    let routingAddress = '';
    const isInputingForm = localStorage.getItem('isInputingForm');
    // tslint:disable-next-line:prefer-const
    for (let i of this.menuItem.route.paths) {
      if (i === '/') {
        routingAddress += i;
      } else {
        routingAddress += i + '/';
      }
    }
    if (isInputingForm === 'true') {
      this.showDialog(routingAddress);
    } else {
      this.router.navigate([routingAddress]);
    }
    // this.router.navigate([routingAddress]);
  }

  public showDialog(routingAddress: string) {
    this.translateService.get('usermessage.error.navigate_when_inputing', {}).subscribe(
      data => {
        const translation = data;
        this.activate(null, null)
          .then(result => {
            if (result === true) {
              localStorage.removeItem('isInputingForm');
              localStorage.setItem('isInputingForm', 'false');
              this.router.navigate([routingAddress]);
            }
          }).catch(error => {
            // console.log(error);
          });
      });
  }

  private setLabels(message: string, title: string) {
    this.title = title;
    this.message = message;
  }

  activate(message: string, title: string) {
    const promise = new Promise<boolean>(resolve => {
      this.isOn = true;
      this.isShow = true;
      this.show(resolve);
    });
    // this.setLabels(message, title);
    // console.log('in activate');
    return promise;
  }

  private show(resolve: (boolean) => any) {
    document.onkeyup = null;

    const negativeOnClick = (e: any) => resolve(false);
    const positiveOnClick = (e: any) => resolve(true);

    if (!this._confirmElement || !this._cancelButton || !this._okButton || !this._closeButton || !this._offButton) {
      return;
    }
    this._confirmElement.style.opacity = 0;
    this._confirmElement.style.zIndex = 9999;
    this._confirmElement.style.height = '100%';

    this._cancelButton.onclick = ((e: any) => {
      e.preventDefault();
      if (!negativeOnClick(e)) {
        this.hideDialog();
      }
    });

    this._okButton.onclick = ((e: any) => {
      e.preventDefault();
      if (!positiveOnClick(e)) {
        this.hideDialog();
      }
    });

    this._closeButton.onclick = ((e: any) => {
      e.preventDefault();
      if (!negativeOnClick(e)) {
        this.hideDialog();
      }
    });

    this._offButton.onclick = ((e: any) => {
      e.preventDefault();
      if (!negativeOnClick(e)) {
        this.hideDialog();
      }
    });

    document.onkeyup = (e: any) => {
      if (e.which === KEY_ESC) {
        this.hideDialog();
        return negativeOnClick(null);
      }
    };


    this._confirmElement.style.opacity = 1;
    this._confirmElement.style.display = 'block';
  }

  close() {
    this.hideDialog();
  }

  private hideDialog() {
    document.onkeyup = null;
    this._confirmElement.style.opacity = 0;
    this._confirmElement.style.height = '0px';
    window.setTimeout(() => this._confirmElement.style.zIndex = -1111, 400);
    this.isShow = false;
    this.isOn = false;
  }

  ngOnInit(): any {
    this._confirmElement = document.getElementById('confirmationModalNavigate');
    this._cancelButton = document.getElementById('cancelButtonNavigate');
    this._okButton = document.getElementById('okButtonNavigate');
    this._closeButton = document.getElementById('close-x-navigate');
    this._offButton = document.getElementById('offButtonNavigate');
    this.isShow = false;
    this.isOn = false;
    // console.log('menuItem=', this.menuItem );
    // if (this.menuItem && (this.roleId === 6 || this.roleId === 9)) {

    // }
  }
}

import {Component, OnInit} from '@angular/core';
import {MENU_ITEMS} from '../tranzi-admin-menu';
import {BvMenuService} from '../../../shared/services/bv-menu.service';
import {TranziAdminStateService} from '../tranzi-admin-state.service';
import {NgbDatepickerConfig} from '@ng-bootstrap/ng-bootstrap';
import { trigger, transition, state, animate, style } from '@angular/animations';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'bv-app-shell',
  templateUrl: './bv-app-shell.component.html',
  styleUrls: ['./bv-app-shell.component.scss'],
  providers: [NgbDatepickerConfig],
  animations: [
    trigger('childAnimation', [
      // ...
      state('open', style({
        width: '250px',
        opacity: 1,
        backgroundColor: 'yellow',
      })),
      state('closed', style({
        width: '100px',
        opacity: 0.5,
        backgroundColor: 'green',
      })),
      transition('* => *', [
        animate('1s'),
      ]),
    ]),
  ],
})
export class BvAppShellComponent implements OnInit {

  menu = MENU_ITEMS;
  checkDialog: boolean = true;
  constructor(
    private menuService: BvMenuService,
    private bvState: TranziAdminStateService) {
  }

  ngOnInit() {
    console.log('test menu')
    // this.menuService.updateMenuByRoutes(this.menu);
  }

  get isShowSideBar() {
    return this.bvState.isShowSideBar;
  }

}

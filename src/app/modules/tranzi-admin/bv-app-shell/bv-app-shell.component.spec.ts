import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BvAppShellComponent } from './bv-app-shell.component';

describe('BvAppShellComponent', () => {
  let component: BvAppShellComponent;
  let fixture: ComponentFixture<BvAppShellComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BvAppShellComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BvAppShellComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

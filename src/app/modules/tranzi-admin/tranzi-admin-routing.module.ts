import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BvDashboardComponent } from './bv-dashboard/bv-dashboard.component';
import { BvAppShellComponent } from './bv-app-shell/bv-app-shell.component';
import { PageNotFoundComponent } from '../../shared/components/page-not-found/page-not-found.component';
import { BvWorkBasketPersonalComponent } from './bv-work-basket-personal/bv-work-basket-personal.component';
import { BvProfileDetailsComponent } from './bv-profile-details/bv-profile-details.component';
import { BvDivisionJobComponent } from './bv-division-job/bv-division-job.component';
import { PageForbiddenComponent } from '../../shared/components/page-forbidden/page-forbidden.component';
import { AccountComponent } from './account/account.component';
import { CategoriesComponent } from './categories-management/categories/categories.component';
import { CategoriesCreateComponent } from './categories-management/categories-create/categories-create.component';
import { CategoriesUpdateComponent } from './categories-management/categories-update/categories-update.component';
import { ManufacturesCreateComponent } from './manufactures-management/manufactures-create/manufactures-create.component';
import { ManufacturesUpdateComponent } from './manufactures-management/manufactures-update/manufactures-update.component';
import { ManufacturesComponent } from './manufactures-management/manufactures/manufactures.component';
import { ProductCreateComponent } from './products-management/product-create/product-create.component';
import { ProductUpdateComponent } from './products-management/product-update/product-update.component';
import { ProductComponent } from './products-management/product/product.component';
import { AttributeComponent } from './attribute-management/attribute-management.component';
import { CustomerComponent } from './customer-management/customer.component';
import { CustomerOrderComponent } from './customer-order/customer-order.component';
import { CustomerOrderDetailComponent } from './customer-order/customer-order-detail/customer-order-detail.component';
import { CustomerOrderCreateComponent } from './customer-order/customer-order-create/customer-order-create.component';
import { CustomerOrderUpdateComponent } from './customer-order/customer-order-update/customer-order-update.component';
import { ImportProductComponent } from './ImportProduct/import-product.component';
import { InventoryManagementComponent } from './inventory-management/inventory-management.component';
import { BomManagementomponent } from './bom-management/bom-management.component';
import { InventoryAreaComponent } from './inventory-area/inventory-area.component';
import { AddInventoryAreaComponent } from './add-inventory-area/add-inventory-area.component';
import { OrderCategoryComponent } from './categories-management/order-category/order-category.component';
import { SettingComponent } from './setting/setting.component';
import { SettingCreateComponent } from './setting/setting-create/setting-create.component';
import { SettingUpdateComponent } from './setting/setting-update/setting-update.component';
import { OrderCategoryAttributeComponent } from './categories-management/order-category-attribute/order-category-attribute.component';
import { TermsComponent } from './terms-management/terms/terms.component';
import { TermsCreateComponent } from './terms-management/terms-create/terms-create.component';
import { TermsUpdateComponent } from './terms-management/terms-update/terms-update.component';
import { ArticlesComponent } from './articles-management/articles/articles.component';
import { ArticlesCreateComponent } from './articles-management/articles-create/articles-create.component';
import { ArticlesUpdateComponent } from './articles-management/articles-update/articles-update.component';

const routes: Routes = [
  {
    path: '',
    component: BvAppShellComponent,
    children: [
      { path: '', pathMatch: 'full', redirectTo: 'dashboard' },
      { path: 'dashboard', component: BvDashboardComponent },
      { path: 'account', component: AccountComponent },
      { path: 'import', component: ImportProductComponent },
      { path: 'categories', component: CategoriesComponent },
      { path: 'categories-create', component: CategoriesCreateComponent },
      { path: 'categories-update/:id', component: CategoriesUpdateComponent },
      { path: 'order-category', component: OrderCategoryComponent },
      { path: 'manufactures', component: ManufacturesComponent },
      { path: 'manufactures-create', component: ManufacturesCreateComponent },
      { path: 'manufactures-update/:id', component: ManufacturesUpdateComponent },
      { path: 'product', component: ProductComponent },
      { path: 'product-create', component: ProductCreateComponent },
      { path: 'product-update/:id', component: ProductUpdateComponent },
      { path: 'customer', component: CustomerComponent },
      { path: 'inventory-management', component: InventoryManagementComponent },
      { path: 'work-basket-personal', component: BvWorkBasketPersonalComponent },
      { path: 'customer-order', component: CustomerOrderComponent },
      { path: 'customer-order-detail/:id', component: CustomerOrderDetailComponent },
      { path: 'customer-order-update/:id', component: CustomerOrderUpdateComponent },
      { path: 'customer-order-create', component: CustomerOrderCreateComponent },
      { path: 'attribute', component: AttributeComponent },
      { path: 'bom-management', component: BomManagementomponent },
      { path: 'inventory-area', component: InventoryAreaComponent },
      { path: 'add-inventory-area', component: AddInventoryAreaComponent },
      { path: 'detail-profile', component: BvProfileDetailsComponent },
      { path: 'division-job/:claimId', component: BvDivisionJobComponent },
      { path: 'page-forbidden', component: PageForbiddenComponent },
      { path: 'setting', component: SettingComponent },
      { path: 'setting-create', component: SettingCreateComponent },
      { path: 'setting-update/:id', component: SettingUpdateComponent },
      { path: 'order-category-attribute/:id', component: OrderCategoryAttributeComponent },
      { path: 'terms', component: TermsComponent },
      { path: 'terms-create', component: TermsCreateComponent },
      { path: 'terms-update/:id', component: TermsUpdateComponent },
      { path: 'articles', component: ArticlesComponent },
      { path: 'article-create', component: ArticlesCreateComponent },
      { path: 'article-update/:id', component: ArticlesUpdateComponent },
    ],
  },
  { path: '**', component: PageNotFoundComponent },
];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
  ],
  exports: [RouterModule],
})
export class TranziAdminRoutingModule { }

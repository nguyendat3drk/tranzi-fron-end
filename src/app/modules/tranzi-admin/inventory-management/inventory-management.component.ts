import { Utilities } from '../../../shared/services/utilities.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { LoaderService } from '../../../shared/services/loader.service';
import { ManufactService } from '../../../shared/services/http/manufact.service';
import { Inventory } from '../../../shared/models/Inventory';
import { PagerData } from '../../../shared/entity/pagerData';
import { CategoriesComponent } from '../categories-management/categories/categories.component';
import { CategoriesService } from '../../../shared/services/http/categories.service';
import { InventoryService } from '../../../shared/services/http/inventory.service';
import { ModalDirective } from 'ngx-bootstrap';
import { fromJS } from 'immutable';
import { InventoryOrder } from '../../../shared/models/InventoryOrder';
import { InventoryOrderService } from '../../../shared/services/http/Inventoryorder.service';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'inventory-management',
  templateUrl: './inventory-management.component.html',
  styleUrls: ['./inventory-management.component.scss'],
})
export class InventoryManagementComponent implements OnInit {
  page: number = 1;
  listInventory: Inventory[] = [];
  txtSearch: string = ''
  totalPage: number = 0;
  totalPageDetail: number = 0;
  selectInventory: Inventory = new Inventory();
  selectInventoryOrder: InventoryOrder = new InventoryOrder();
  titleAdd: string = ''
  @ViewChild('confirmDialodDel') public confirmDialodDel: ModalDirective;
  idDelete: Number = 0;
  title: string = '';
  listDetailInventoryOrder: InventoryOrder[]= [];
  @ViewChild('createInventory') public createInventory: ModalDirective;
  @ViewChild('viewdetail') public viewdetail: ModalDirective;
  @ViewChild('createInventoryMaster') public createInventoryMaster: ModalDirective;
  
  idDetailSelect: number = 0;
  constructor(private router: Router, private inventoryOrderService: InventoryOrderService,
    private categoriesService: CategoriesService, private inventoryService: InventoryService,
    private loaderService: LoaderService, private ultilities: Utilities) {
      this.ultilities.closeAutoMenu();
  }

  ngOnInit() {
    this.loadInventory();
  }
  viewDate(event) {
    if (event && Number(event) > 0) {
      return this.ultilities.convertDateToStringDateVN(new Date(Number(event) * 1000));
    }
    return ''
  }
  loadInventory() {
    this.loaderService.show();
    this.inventoryService.getAllsPadding(1, 10, 'id', 'desc', this.txtSearch).subscribe((response: any) => {
      // tslint:disable-next-line:no-console
      // console.log(response)
      if (response && response.resultCode === 200) {
        let page = new PagerData();
        this.listInventory = []
        this.totalPage = 0;
        page = response.object;
        if (page && page.content) {
          this.listInventory = page.content;
          this.totalPage = page.totalRow;
        }
        // this.listProduct = response;
      }
      this.loaderService.hide()
    }, error => {
      this.listInventory = []
      this.totalPage = 0;
      this.loaderService.hide();
    });
  }
  createInventoryDetail(event, type) {
    if (event) {
      if (type === 'add') {
        this.titleAdd = 'Tạo phiếu Nhập kho'
        this.selectInventoryOrder.type = 1
      } else {
        this.titleAdd = 'Tạo phiếu xuất kho'
        this.selectInventoryOrder.type = 2
      }
      this.createInventory.show();
      const guaranteeNew = fromJS(event)
      event = guaranteeNew.toJS();
      this.selectInventory = event;
      this.selectInventoryOrder.id = 0;
      this.selectInventoryOrder.productId = this.selectInventory.productId;
      this.selectInventoryOrder.productName = this.selectInventory.productName
      this.selectInventoryOrder.inventoryId = this.selectInventory.id

      this.selectInventoryOrder.sku = this.selectInventory.sku;
      const dt = new Date()
      this.selectInventoryOrder.updatedAt = (dt.getTime() / 1000)
      this.selectInventoryOrder.createdAt = (dt.getTime() / 1000)
      this.selectInventoryOrder.comment = '';
      this.selectInventoryOrder.serialNumbers = '';
      this.selectInventoryOrder.unitPrice = null
      this.selectInventoryOrder.totalPrice = null;
      this.selectInventoryOrder.quantity = null
      this.selectInventoryOrder.blQuantity = false
      this.selectInventoryOrder.blUnitPrice = false
      this.selectInventoryOrder.maxQuatity = false;
    }
  }
  createCategori() {
    // this.router.navigate(['/tranzi-admin/manufactures-create']);
    // this.ultilities.showError('chưa làm')
    this.createInventoryMaster.show()
  }
  paginate(event) {
    if (event) {
      this.listInventory = [];
      this.totalPage = 0;
      // tslint:disable-next-line:max-line-length
      this.loaderService.show();
      this.inventoryService.getAllsPadding(event.page ? (event.page + 1) : 1, event.rows ? event.rows : 10, 'id', 'desc', this.txtSearch).subscribe((response: any) => {
        // tslint:disable-next-line:no-console
        // console.log(response)
        if (response && response.resultCode === 200) {
          let page = new PagerData();
          page = response.object;
          if (page && page.content) {
            this.listInventory = page.content;
            this.totalPage = page.totalRow;
          }
          // this.listProduct = response;
        }
        this.loaderService.hide()
      }, error => {
        this.loaderService.hide();
      });
    }

  }

  setSelectInventory(event) {
    this.selectInventory = new Inventory()
    if (event && event.data) {
      this.selectInventory = event.data;
    }
  }
  routingUpdate(id) {
    if (id) {
      this.router.navigate(['/tranzi-admin/manufactures-update/' + id]);
    }
  }
  submitInventory() {
    let check = true;
    check = this.validateDetail('save');
    if (this.selectInventoryOrder && check) {
      this.inventoryOrderService.create(this.selectInventoryOrder).subscribe((response: any) => {
        if (response && response.resultCode === 200) {
          this.ultilities.showSuccess('Thành công');
        } else if (response && response.resultCode === 204) {
          const mes = response.errorMessage;
          this.ultilities.showError(mes ? mes : 'Có lỗi xảy ra. Liên hệ Admin để kiểm tra lại');
        }
        this.loaderService.hide()
        this.createInventory.hide();
      }, error => {
        this.loaderService.hide();
        this.ultilities.showError('Có lỗi xảy ra. Liên hệ Admin để kiểm tra lại');
      });
    }

  }
  validateDetail(event) {
    let check = true;
    if (this.selectInventoryOrder) {
      if (event === 'save' || event === 'price') {
        this.selectInventoryOrder.blUnitPrice = false;
        if (!this.selectInventoryOrder || this.selectInventoryOrder.unitPrice === undefined || Number(this.selectInventoryOrder.unitPrice) === 0) {
          this.selectInventoryOrder.blUnitPrice = true;
          check = false;
        } else if (Number(this.selectInventoryOrder.unitPrice) < 0) {
          this.selectInventoryOrder.unitPrice = (Number(this.selectInventoryOrder.unitPrice) * (-1));
        }
      }
      if (event === 'save' || event === 'quatity') {
        this.selectInventoryOrder.blQuantity = false;
        if (!this.selectInventoryOrder || this.selectInventoryOrder.quantity === undefined || Number(this.selectInventoryOrder.quantity) === 0) {
          this.selectInventoryOrder.blQuantity = true;
          check = false;
        } else if (Number(this.selectInventoryOrder.quantity) < 0) {
          this.selectInventoryOrder.quantity = (Number(this.selectInventoryOrder.quantity) * (-1));
        }
      }
      this.selectInventoryOrder.maxQuatity = false;
      if (this.selectInventoryOrder.type === 2 && this.selectInventoryOrder.quantity && this.selectInventory
        && this.selectInventory.currentStock < this.selectInventoryOrder.quantity){
          this.selectInventoryOrder.maxQuatity = true;
          check = false;
        }
    }
    this.totalPrice();
    return check;
  }
  deleteInventory() {
    if (this.idDelete) {
      this.loaderService.show();
      this.confirmDialodDel.hide();
      this.inventoryService.deleteId(this.idDelete).subscribe((response: any) => {
        if (response && response.resultCode === 200) {
          this.loadInventory();
          this.ultilities.showSuccess('Xóa thành công');
        } else if (response && response.resultCode === 205) {
          this.ultilities.showError('Sản phẩm đã tạo phiếu xuất nhập kho. Không thể xóa.')
        } else if (response && response.resultCode === 204) {
          this.ultilities.showError(response.errorMessage ? response.errorMessage : 'Có lỗi xảy ra. Liên hệ Admin để kiểm tra lại')
        }
        this.loaderService.hide()
        // this.ultilities.showSuccess('Delete Success Manufactures')
      }, error => {
        this.loaderService.hide();
        this.ultilities.showError('Delete Error Inventory')
      });
    }

  }

  deleteCheck(pro: Inventory) {
    if (pro) {
      this.title = 'Bạn có muốn xóa sản phẩm : ' + pro.productName + ' không?';
      this.idDelete = pro.id
      this.confirmDialodDel.show();
    }
  }
  totalPrice() {
    if (this.selectInventoryOrder) {
      const total = (this.selectInventoryOrder.quantity ? this.selectInventoryOrder.quantity : 0) *
        (this.selectInventoryOrder.unitPrice ? this.selectInventoryOrder.unitPrice : 0);
      this.selectInventoryOrder.totalPrice = total;
    }
  }
  keyPress(event: any, type) {
    if (type && type === 'number') {
      const patternExtension = /[0-9]/;
      const inputChar = String.fromCharCode(event.charCode);
      if (event.keyCode !== 9 && !patternExtension.test(inputChar)) {
        event.preventDefault();
      }
    } else if (type && type === 'date') {
      const patternExtension = /[0-9\/]/;
      const inputChar = String.fromCharCode(event.charCode);
      if (event.keyCode !== 9 && !patternExtension.test(inputChar)) {
        event.preventDefault();
      }
    } else {
      const patternExtension = /[0-9\.]/;
      const inputChar = String.fromCharCode(event.charCode);
      if (event.keyCode !== 9 && !patternExtension.test(inputChar)) {
        event.preventDefault();
      }
    }
  }
  showDetail(idDetail,event){
    if(event){
      this.selectInventory = event;
      this.selectInventoryOrder.productId = this.selectInventory.productId;
      this.selectInventoryOrder.productName = this.selectInventory.productName
      this.selectInventoryOrder.inventoryId = this.selectInventory.id
      this.selectInventoryOrder.sku = this.selectInventory.sku;
    }
    if(idDetail){
      this.idDetailSelect = idDetail;
      this.viewdetail.show();
      this.inventoryService.getAllsPaddingDetail(1, 10, 'id', 'desc', '', idDetail).subscribe((response: any) => {
        // tslint:disable-next-line:no-console
        // console.log(response)
        if (response && response.resultCode === 200) {
          let page = new PagerData();
          this.listDetailInventoryOrder = []
          this.totalPageDetail = 0;
          page = response.object;
          if (page && page.content) {
            this.listDetailInventoryOrder = page.content;
            this.totalPageDetail = page.totalRow;
          }
          // this.listProduct = response;
        }
        this.loaderService.hide()
      }, error => {
        this.listDetailInventoryOrder = []
        this.totalPageDetail = 0;
        this.loaderService.hide();
      });
    }
  }
  paginateDetail(event) {
    if (event && this.idDetailSelect > 0) {
      this.listDetailInventoryOrder = [];
      this.totalPageDetail = 0;
      // tslint:disable-next-line:max-line-length
      this.inventoryService.getAllsPaddingDetail(event.page ? (event.page + 1) : 1, event.rows ? event.rows : 10, 'id', 'desc', '', this.idDetailSelect).subscribe((response: any) => {
        // tslint:disable-next-line:no-console
        // console.log(response)
        if (response && response.resultCode === 200) {
          let page = new PagerData();
          page = response.object;
          if (page && page.content) {
            this.listDetailInventoryOrder = page.content;
            this.totalPageDetail = page.totalRow;
          }
          // this.listProduct = response;
        }
        this.loaderService.hide()
      }, error => {
        this.loaderService.hide();
      });
    }

  }
  viewPrice(event){
    this.ultilities.numberPrice(event)
  }
  addProductInventoryArea(){
    this.router.navigate(['/tranzi-admin/add-inventory-area']);
  }

}

import { NgModule } from '@angular/core';
import { TranziAdminRoutingModule } from './tranzi-admin-routing.module';
import { BvDashboardComponent } from './bv-dashboard/bv-dashboard.component';
import { ThemeModule } from '../../@theme/theme.module';
import { BvAppShellComponent } from './bv-app-shell/bv-app-shell.component';
import { BvMenuItem } from './bv-menu/components/baMenuItem/bv-menu-item.component';
import { BvHeaderComponent } from './bv-header/bv-header.component';
import { BvSideBarComponent } from './bv-side-bar/bv-side-bar.component';
import { BvMenuComponent } from './bv-menu/bv-menu.component';
import { BvWorkBasketPersonalComponent } from './bv-work-basket-personal/bv-work-basket-personal.component';
import { AttributeComponent } from './attribute-management/attribute-management.component';
import { BvDashboardItemComponent } from './bv-dashboard/bv-dashboard-item/bv-dashboard-item.component';
import { SharedModule } from '../../shared/shared.module';
import { HttpClientModule } from '@angular/common/http';
import { TranziAdminStateService } from './tranzi-admin-state.service';
import { NgbDateAdapter, NgbDateParserFormatter } from '@ng-bootstrap/ng-bootstrap';
import { NgbDateNativeAdapter } from '../../shared/common/ngb-date-native-adapter';
import { NgbCustomFormatter } from '../../shared/common/ngb-custom-formatter';
import { ConfirmService } from '../../shared/confirm-dialog/ConfirmService';
import { LoaderComponent } from '../../shared/components/loader/loader.component';
import { LoaderService } from '../../shared/services/loader.service';
import { Utilities } from '../../shared/services/utilities.service';
import { ContextMenuModule } from 'primeng/contextmenu';
import { DataTableModule } from 'primeng/datatable';
import { RadioButtonModule } from 'primeng/primeng';
import { BvProfileDetailsComponent } from './bv-profile-details/bv-profile-details.component';
import { BvWorkBasketAssessorsComponent } from './bv-work-basket-personal/bv-work-basket-assessors/bv-work-basket-assessors.component';
import { BvWorkBasketZoneComponent } from './bv-work-basket-personal/bv-work-basket-zone/bv-work-basket-zone.component';
import { BvDateCartWorkComponent } from './date-cartwork/date-cartwork.component';
import { NgxMaskModule } from 'ngx-mask';
import { BvDivisionJobComponent } from './bv-division-job/bv-division-job.component';
import { BvWorkBasketLeaderComponent } from './bv-work-basket-personal/bv-work-basket-leader/bv-work-basket-leader.component';
import { PdfViewerModule } from 'ng2-pdf-viewer';
import { ModalModule } from 'ngx-bootstrap';
import { FileUploadModule } from 'primeng/fileupload';
import { ProgressBarModule } from 'primeng/progressbar';
import { BvWorkBasketLeaderDirective } from './bv-work-basket-personal/bv-work-basket-leader/bv-work-basket-leader.directive';
import { CurrencyMaskModule } from 'ng2-currency-mask';
import { AutoCompleteModule } from 'primeng/primeng';
import { AccessaryService } from '../../shared/services/http/accessary.service';
import { AccountComponent } from './account/account.component';
import { CKEditorModule } from 'ng2-ckeditor';
import { CategoriesModule } from './categories-management/categories/categories.module';
import { CategoriesCreateComponent } from './categories-management/categories-create/categories-create.component';
import { CategoriesUpdateComponent } from './categories-management/categories-update/categories-update.component';

import { ManufacturesModule } from './manufactures-management/manufactures/manufactures.module';
import { ManufacturesCreateComponent } from './manufactures-management/manufactures-create/manufactures-create.component';
import { ManufacturesUpdateComponent } from './manufactures-management/manufactures-update/manufactures-update.component';
import { ProductCreateComponent } from './products-management/product-create/product-create.component';
import { ProductUpdateComponent } from './products-management/product-update/product-update.component';
import { ProductComponent } from './products-management/product/product.component';
import {PaginatorModule} from 'primeng/paginator';
import {TreeModule} from 'primeng/tree';
import {CalendarModule} from 'primeng/calendar';
import { NgSelectModule } from '@ng-select/ng-select';
import { TagsInputModule } from 'ngx-tags-input/dist';
import {MultiSelectModule} from 'primeng/multiselect';
import { CustomerComponent } from './customer-management/customer.component';
import { CustomerOrderComponent } from './customer-order/customer-order.component';
import { CustomerOrderDetailComponent } from './customer-order/customer-order-detail/customer-order-detail.component';
import { CustomerOrderUpdateComponent } from './customer-order/customer-order-update/customer-order-update.component';
import { CustomerOrderCreateComponent } from './customer-order/customer-order-create/customer-order-create.component';
import { DistrictService } from '../../shared/entity/District.service';
import { CityService } from '../../shared/entity/City.service';
import { ImportProductComponent } from './ImportProduct/import-product.component';
import { InventoryManagementComponent } from './inventory-management/inventory-management.component';
import { BomManagementomponent } from './bom-management/bom-management.component';
import { BvHelpComponent } from './bv-help/bv-help.component';
import { BvClaimDocumentUpdateComponent } from './bv-claim-document-update/bv-claim-document-update.component';
import { BvReportComponent } from './bv-report/bv-report.component';
import { InventoryAreaComponent } from './inventory-area/inventory-area.component';
import { AddInventoryAreaComponent } from './add-inventory-area/add-inventory-area.component';
import {CheckboxModule} from 'primeng/checkbox';
import { OrderCategoryComponent } from './categories-management/order-category/order-category.component';
import {OrderListModule} from 'primeng/orderlist';
import { SettingComponent } from './setting/setting.component';
import { SettingCreateComponent } from './setting/setting-create/setting-create.component';
import { SettingUpdateComponent } from './setting/setting-update/setting-update.component';
import { OrderCategoryAttributeComponent } from './categories-management/order-category-attribute/order-category-attribute.component';
import { TermsComponent } from './terms-management/terms/terms.component';
import { TermsCreateComponent } from './terms-management/terms-create/terms-create.component';
import { TermsUpdateComponent } from './terms-management/terms-update/terms-update.component';
import { ArticlesUpdateComponent } from './articles-management/articles-update/articles-update.component';
import { ArticlesCreateComponent } from './articles-management/articles-create/articles-create.component';
import { ArticlesComponent } from './articles-management/articles/articles.component';
import { DownloadService } from '../../shared/services/download.service';

@NgModule({
  imports: [
    TranziAdminRoutingModule,
    ThemeModule,
    SharedModule,
    HttpClientModule,
    DataTableModule,
    ContextMenuModule,
    RadioButtonModule,
    AutoCompleteModule,
    MultiSelectModule,
    ModalModule.forRoot(),
    NgxMaskModule.forRoot(),
    PdfViewerModule,
    ProgressBarModule,
    CurrencyMaskModule,
    CategoriesModule,
    ManufacturesModule,
    CKEditorModule,
    PaginatorModule,
    FileUploadModule,
    TreeModule,
    CalendarModule,
    NgSelectModule,
    CheckboxModule,
    OrderListModule,
    TagsInputModule.forRoot(),
  ],
  declarations: [
    BvDashboardComponent,
    BvAppShellComponent,
    BvHeaderComponent,
    BvSideBarComponent,
    BvMenuComponent,
    BvMenuItem,
    CustomerComponent,
    BvWorkBasketPersonalComponent,
    CustomerOrderComponent,
    AttributeComponent,
    BvDashboardItemComponent,
    LoaderComponent,
    BvProfileDetailsComponent,
    BvWorkBasketAssessorsComponent,
    BvWorkBasketZoneComponent,
    BvDateCartWorkComponent,
    BvDivisionJobComponent,
    BvWorkBasketLeaderComponent,
    AccountComponent,
    BvWorkBasketLeaderDirective,
    CategoriesCreateComponent,
    CategoriesUpdateComponent,
    ManufacturesCreateComponent,
    ManufacturesUpdateComponent,
    ProductCreateComponent,
    ProductUpdateComponent,
    ProductComponent,
    CustomerOrderDetailComponent,
    CustomerOrderCreateComponent,
    CustomerOrderUpdateComponent,
    ImportProductComponent,
    InventoryManagementComponent,
    BomManagementomponent,
    BvHelpComponent,
    BvClaimDocumentUpdateComponent,
    BvReportComponent,
    InventoryAreaComponent,
    AddInventoryAreaComponent,
    OrderCategoryComponent,
    OrderCategoryAttributeComponent,
    SettingComponent,
    SettingCreateComponent,
    SettingUpdateComponent,
    TermsComponent,
    TermsCreateComponent,
    TermsUpdateComponent,
    ArticlesComponent,
    ArticlesComponent,
    ArticlesCreateComponent,
    ArticlesUpdateComponent
  ],
  providers: [
    TranziAdminStateService,
    ConfirmService,
    LoaderService,
    Utilities,
    DownloadService,
    AccessaryService,
    CityService,
    DistrictService,
    // Date picker transform date to native
    { provide: NgbDateAdapter, useClass: NgbDateNativeAdapter },
    { provide: NgbDateParserFormatter, useClass: NgbCustomFormatter },
  ],
})
export class TranziAdminModule { }

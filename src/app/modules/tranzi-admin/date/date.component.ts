import { Component, Input, OnInit, ViewChild, EventEmitter, Output } from '@angular/core';
import { MenuItem } from 'primeng/primeng';
import { NgForm } from '@angular/forms';
import { moment } from 'ngx-bootstrap/chronos/test/chain';

@Component({
  selector: 'ngx-date-time',
  templateUrl: './date.component.html',
  styleUrls: ['./date.component.scss'],
})
export class BvDateComponent implements OnInit {
  @Input() datetime: Date;
  @Input() isRequired: boolean;
  @Input() isInValid: boolean;
  @Input() inValidMes: string;
  @Output() outDate = new EventEmitter();
  @Output() formatDate = new EventEmitter();
  @Input() minDate: Date;
  @Input() maxDate: Date;
  @Output() onChangeDate = new EventEmitter();
  public checkFormatDate: boolean = true;
  testDate: string = '';
  maxDateCheck: Date = new Date('2900/09/09');
  minDateCheck: Date = new Date('1901/09/09');
  firstLoad: boolean = true;
  constructor() { }

  ngOnInit() {
    if (this.datetime) {
      this.testDate = ('0' + this.datetime.getDate()).slice(-2) + '' +
        ('0' + (this.datetime.getMonth() + 1)).slice(-2) + this.datetime.getFullYear();
    }
  }

  addYearToDate(date, yearInt) {
    if (!date) return new Date();
    const year = date.getFullYear();
    const month = date.getMonth();
    const day = date.getDate();
    return new Date(year + yearInt, month, day);
  }

  setdateText() {
    this.firstLoad = false;
    if (this.datetime) {
      this.checkFormatDate = true;
      this.testDate = ('0' + this.datetime.getDate()).slice(-2) + '' +
        ('0' + (this.datetime.getMonth() + 1)).slice(-2) + this.datetime.getFullYear();
    }
    this.outDate.emit(this.datetime);
    this.onChangeDate.emit(this.datetime);
    this.formatDate.emit(this.checkFormatDate);
  }
  changeDate() {
    this.firstLoad = false;
    this.datetime = null;
    if (this.testDate && this.testDate.length === 8) {
      this.checkFormatDate = true;
      this.datetime = moment(this.testDate, 'DDMMYYYY').toDate();
      const yearS = this.testDate.slice(4, 8);
      if (isNaN(this.datetime.getTime()) || Number(yearS) < 1000) {
        this.checkFormatDate = false;
      }
    } else if (this.testDate && this.testDate.length < 8 && this.testDate.length > 0) {
      this.checkFormatDate = false;
    }
    if (!this.testDate) {
      this.checkFormatDate = true;
    }
    this.outDate.emit(this.datetime);
    this.onChangeDate.emit(this.datetime);
    this.formatDate.emit(this.checkFormatDate);
  }
}

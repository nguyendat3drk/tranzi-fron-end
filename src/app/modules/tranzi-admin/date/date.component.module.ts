import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ModalModule } from 'ngx-bootstrap';
import { TranslateService } from '@ngx-translate/core';

@NgModule({
  imports: [CommonModule, ModalModule.forRoot()],
  declarations: [
  ],
  providers: [TranslateService],
  exports: []})
export class DateModule { }

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../../../shared/shared.module';
import { HttpClientModule } from '@angular/common/http';
import { CurrencyMaskModule } from 'ng2-currency-mask';
import {AccountComponent} from './account.component';
// import {BvDateCartWorkComponent} from '../date-cartwork/date-cartwork.component';

const routes: Routes = [
  {
    path: 'account',
    component: AccountComponent,
    data: { isCreate: true },
  },
];

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    HttpClientModule,
    RouterModule.forChild(routes),
    CurrencyMaskModule,
  ],
  declarations: [
    // BvSearchComponent,
  ],
  exports: [RouterModule],
})
export class BvSearchModule {
}

import { Utilities } from '../../../shared/services/utilities.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ProductService } from '../../../shared/services/http/product.service';
import { MessageService } from 'primeng/components/common/messageservice';
import { ActivatedRoute, Router } from '@angular/router';
import { LoaderService } from '../../../shared/services/loader.service';
import { CategoriesService } from '../../../shared/services/http/categories.service';
import { Categories } from '../../../shared/models/Categories';
import { PagerData } from '../../../shared/entity/pagerData';
import { isFulfilled } from 'q';
import { ThemeSettingsComponent } from '../../../@theme/components';
import { AccountService } from '../../../shared/services/http/account.service';
import { Account } from '../../../shared/models/account';
import { ModalDirective } from 'ngx-bootstrap';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.scss'],
})
export class AccountComponent implements OnInit {
  listAccount: Account[] = [];
  totalPage: number = 0;
  selectAccount: Account = new Account();
  @ViewChild('addAccount') public addAccount: ModalDirective;
  // validate
  blUsername: boolean = false;
  blUsernameFormat: boolean = false;

  blEmail: boolean = false;
  blEmailFormat: boolean = false;

  blPass: boolean = false;
  blLastName: boolean = false;
  blFirstName: boolean = false;
  @ViewChild('confirmDialodDel') public confirmDialodDel: ModalDirective;
  idDelete: number = 0;
  title: string = '';
  txtSearch: string = '';
  constructor(private utilities: Utilities, private router: Router,
    private activeRouter: ActivatedRoute, private accountService: AccountService,
    private loaderService: LoaderService, private ultilities: Utilities,
    private categoriesService: CategoriesService) {
      this.ultilities.closeAutoMenu();
  }
  ngOnInit() {
    this.listAccount = [];
    this.loadAccount();
  }
  viewDate(event) {
    if (event && Number(event) > 0) {
      return this.ultilities.convertDateToStringDateVN(new Date(Number(event) * 1000));
    }
    return ''
  }
  loadAccount() {
    this.accountService.getAllsPadding(1, 10, 'id', 'desc', this.txtSearch).subscribe((response: any) => {
      this.listAccount = [];
      if (response && response.resultCode === 200) {
        let page = new PagerData();
        page = response.object;
        if (page && page.content) {
          this.listAccount = page.content;
          this.totalPage = page.totalRow;
        }
        // this.listProduct = response;
      }
      this.loaderService.hide()
    }, error => {
      this.listAccount = [];
      this.loaderService.hide();
    });
  }
  submitOk() {
    // tslint:disable-next-line:prefer-const
    let check = this.validate('save');
    if (!check) {
      return;
    }
    if (this.selectAccount) {
      this.accountService.create(this.selectAccount).subscribe((response: any) => {
        if (response && response.resultCode === 200) {
          this.loadAccount();
          this.addAccount.hide();
          this.ultilities.showSuccess('Thành công')
        } else if (response && response.resultCode === 201) {
          this.ultilities.showError('Email đã tồn tại. Xin hay kiểm tra lại.')
        } else if (response && response.resultCode === 202) {
          this.ultilities.showError('Tài khoản đã tồn tại. Xin hay kiểm tra lại.')
        } else if (response && response.resultCode === 204) {
          this.ultilities.showError(response.errorMessage ? response.errorMessage : 'Có lỗi xảy ra')
        }
        this.loaderService.hide()
      }, error => {
        this.loaderService.hide();
      });
    }

  }
  paginate(event) {
    if (event) {
      this.accountService.getAllsPadding(event.page ? (event.page + 1) : 1, event.rows ?
        event.rows : 10, 'id', 'desc', this.txtSearch).subscribe((response: any) => {
          this.listAccount = [];
          if (response && response.resultCode === 200) {
            let page = new PagerData();
            page = response.object;
            if (page && page.content) {
              this.listAccount = page.content;
              this.totalPage = page.totalRow;
            }
          }
          this.loaderService.hide()
        }, error => {
          this.listAccount = [];
          this.totalPage = 0
          this.loaderService.hide();
        });
    }

  }
  viewDialog(event) {
    this.blUsername = false;
    this.blUsernameFormat = false;
    this.blEmailFormat = false;
    this.blEmail = false;
    this.blPass = false;
    this.blFirstName = false;
    this.blLastName = false;
    if (!event) {
      this.selectAccount = new Account();
      this.selectAccount.id = 0
      this.selectAccount.ipAddress = '192.168.1.1';
      this.selectAccount.blAction = true
    } else {
      // tslint:disable-next-line:prefer-const
      let newCourse = Object.assign({}, event);
      this.selectAccount = newCourse
      if (this.selectAccount.active && Number(this.selectAccount.active) === 1) {
        this.selectAccount.blAction = true;
      }
    }
    this.addAccount.show()
  }

  deleteAccount() {
    if (this.idDelete) {
      this.loaderService.show();
      this.confirmDialodDel.hide();
      this.accountService.deleteAccountById(this.idDelete).subscribe((response: any) => {
        if (response && response.resultCode === 200) {
          this.loadAccount();
          this.ultilities.showSuccess('Xóa tài khoản thành công')
          this.addAccount.hide();
        } else if (response && response.resultCode === 205) {
          this.ultilities.showError('Tài khoản đã liên kết. Không thể xóa.')
        } else if (response && response.resultCode === 204) {
          this.ultilities.showError(response.errorMessage ? response.errorMessage : 'Có lỗi xảy ra')
        }
        this.loaderService.hide()

      }, error => {
        this.loaderService.hide();
        this.ultilities.showError('Delete Error Categories')
      });
    }
  }
  keyPress(event: any, type) {
    if (type && type === 'number') {
      const patternExtension = /[0-9]/;
      const inputChar = String.fromCharCode(event.charCode);
      if (event.keyCode !== 9 && !patternExtension.test(inputChar)) {
        event.preventDefault();
      }
    } else if (type && type === 'date') {
      const patternExtension = /[0-9\/]/;
      const inputChar = String.fromCharCode(event.charCode);
      if (event.keyCode !== 9 && !patternExtension.test(inputChar)) {
        event.preventDefault();
      }
    } else {
      const patternExtension = /[0-9\.]/;
      const inputChar = String.fromCharCode(event.charCode);
      if (event.keyCode !== 9 && !patternExtension.test(inputChar)) {
        event.preventDefault();
      }
    }
  }
  validate(event) {
    let check = true;
    if (event === 'name' || event === 'save') {
      this.blUsername = false;
      this.blUsernameFormat = false;
      if (!this.selectAccount || !this.selectAccount.username) {
        this.blUsername = true;
        check = false;
      } else if (!this.ultilities.testUsername(this.selectAccount.username)) {
        this.blUsernameFormat = true;
        check = false;
      }
    }
    if (event === 'email' || event === 'save') {
      this.blEmail = false;
      this.blEmailFormat = false;
      if (!this.selectAccount || !this.selectAccount.email) {
        this.blEmail = true;
        check = false;
      } else if (!this.ultilities.checkEmail(this.selectAccount.email)) {
        this.blEmailFormat = true;
        check = false;
      }
    }
    if (event === 'blPass' || event === 'save') {
      this.blPass = false;
      if (!this.selectAccount || !this.selectAccount.password) {
        this.blPass = true;
        check = false;
      }
    }
    if (event === 'blFirstName' || event === 'save') {
      this.blFirstName = false;
      if (!this.selectAccount || !this.selectAccount.firstName) {
        this.blFirstName = true;
        check = false;
      }
    }
    if (event === 'blLastName' || event === 'save') {
      this.blLastName = false;
      if (!this.selectAccount || !this.selectAccount.lastName) {
        this.blLastName = true;
        check = false;
      }
    }
    return check;
  }
  deleteCheck(pro: Account) {
    if (pro) {
      this.title = 'Bạn có muốn xóa sản phẩm: ' + pro.username + ' không?';
      this.idDelete = pro.id
      this.confirmDialodDel.show();
    }
  }
}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BvSearchComponent } from './bv-search.component';

describe('BvSearchComponent', () => {
  let component: BvSearchComponent;
  let fixture: ComponentFixture<BvSearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BvSearchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BvSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

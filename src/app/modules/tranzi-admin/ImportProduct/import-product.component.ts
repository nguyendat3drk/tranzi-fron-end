import { Utilities } from '../../../shared/services/utilities.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ProductService } from '../../../shared/services/http/product.service';
import { MessageService } from 'primeng/components/common/messageservice';
import { ActivatedRoute, Router } from '@angular/router';
import { LoaderService } from '../../../shared/services/loader.service';
import { CategoriesService } from '../../../shared/services/http/categories.service';
import { Categories } from '../../../shared/models/Categories';
import { PagerData } from '../../../shared/entity/pagerData';
import { isFulfilled } from 'q';
import { ThemeSettingsComponent } from '../../../@theme/components';
import { AttributesService } from '../../../shared/services/http/attributes.service';
import { Attributes } from '../../../shared/models/Attributes';
import { ModalDirective } from 'ngx-bootstrap';
import { Attribute } from '@angular/compiler';
import { AttributeValues } from '../../../shared/models/AttributeValues';
import { fromJS } from 'immutable';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'import',
  templateUrl: './import-product.component.html',
  styleUrls: ['./import-product.component.scss'],
})
export class ImportProductComponent implements OnInit {
  blImport: boolean = false;
  blImport2: boolean = false;
  blImport3: boolean = false;
  blImport4: boolean = false;


  typeImport: number = 1;
  // upload
  urlupload: string = '';
  uploadedFilesSuccess: any[] = []
  uploadedFilesError: any[] = []
  uploadedFilesErrorInvalid: any[] = []
  progressBarPercent = 20
  totalFile: number = 0;
  serverPath: string = '';
  urlProductFile: string = '';



  @ViewChild('addAttribute') public addAttribute: ModalDirective;
  @ViewChild('addAttributeValues') public addAttributeValues: ModalDirective;
  constructor(private utilities: Utilities, private router: Router,
    private activeRouter: ActivatedRoute, private messageService: MessageService,
    private loaderService: LoaderService, private ultilities: Utilities,
    private categoriesService: CategoriesService) {
    this.urlupload = this.ultilities.getUrlServerHost() + '/share-api/uploadxls'
    this.ultilities.closeAutoMenu();
  }

  ngOnInit() {
    this.categoriesService.testAPI().subscribe((response: any) => {
    }, error => {
    });
  }

  changeType() {
    if (Number(this.typeImport) === 1) {
      this.urlupload = this.ultilities.getUrlServerHost() + '/share-api/uploadxls'
    } else if (Number(this.typeImport) === 2) {
      this.urlupload = this.ultilities.getUrlServerHost() + '/share-api/importCategory'
    } else if (Number(this.typeImport) === 3) {
      this.urlupload = this.ultilities.getUrlServerHost() + '/share-api/updateProduct'
    }else if (Number(this.typeImport) === 4) {
      this.urlupload = this.ultilities.getUrlServerHost() + '/listPrice/uploadxls'
    }
  }


  // Upload
  // upload===============================================UPLOAD====================
  btnUploadClick(fileUpload, urlType) {

    fileUpload.advancedFileInput.nativeElement.click()
    if (urlType === 1) {
      this.urlupload = this.ultilities.getUrlServerHost() + '/share-api/uploadxls'
    } else if (urlType === 2) {
      this.urlupload = this.ultilities.getUrlServerHost() + '/share-api/importCategory'
    } else if (urlType === 3) {
      this.urlupload = this.ultilities.getUrlServerHost() + '/share-api/updateProduct'
    } else if (urlType === 4) {
      this.urlupload = this.ultilities.getUrlServerHost() + '/listPrice/uploadxls'
    } else {
      this.urlupload = this.ultilities.getUrlServerHost() + '/share-api/uploadxls'
    }
  }

  onSelectedFiles(modal, fileUpload) {
    this.totalFile = 0;
    this.progressBarPercent = 20
    this.uploadedFilesError = []
    this.uploadedFilesErrorInvalid = [];
    this.uploadedFilesSuccess = []
    if (fileUpload.advancedFileInput.nativeElement.files) {
      this.totalFile = fileUpload.files.length;
      for (let i = 0; i < fileUpload.advancedFileInput.nativeElement.files.length; i++) {
        const element = fileUpload.advancedFileInput.nativeElement.files[i];
        const type = fileUpload.advancedFileInput.nativeElement.files[i].type;
        const strTypeCheck = type.split('/')[0];
        const strTypeTailFile = type.split('/')[1];
        if (('' + strTypeCheck).toUpperCase().trim() === 'VIDEO' && (('' + strTypeTailFile).toUpperCase().trim() !== 'MP4'
          && ('' + strTypeTailFile).toUpperCase().trim() !== '3GP' && ('' + strTypeTailFile).toUpperCase().trim() !== '3GPP')) {
          // tslint:disable-next-line:prefer-const
          let dto = {
            value: fileUpload.advancedFileInput.nativeElement.files[i].name,
            type: fileUpload.advancedFileInput.nativeElement.files[i].type,
          }
          this.uploadedFilesErrorInvalid.push(dto)
        } else if (element.size > 10000000 || element.size === 0) {
          this.uploadedFilesError.push(element)
        }

      }
    }
    if (fileUpload && fileUpload.msgs) {
      for (let i = 0; i < fileUpload.msgs.length; i++) {
        const element = fileUpload.msgs[i];
        if (element) {
          const maxsize = element.detail.indexOf('maximum');
          let valueDetail = '';
          let valueType = '';
          valueDetail = element.summary.split(':')[0];
          if (maxsize < 0) {
            valueType = 'Không đúng định dạng'
            // tslint:disable-next-line:prefer-const
            let dto = {
              value: valueDetail,
              type: valueType,
            }
            this.uploadedFilesErrorInvalid.push(dto)
          }
        }
      }
    }
    const checkNumberError = Number(this.uploadedFilesErrorInvalid ? this.uploadedFilesErrorInvalid.length : 0) +
      Number(this.uploadedFilesError ? this.uploadedFilesError.length : 0);
    if (this.totalFile === checkNumberError) {
      this.progressBarPercent = 100
      this.ultilities.showError('Cập nhật thất bại')
    }
    // modal.show()
    fileUpload.upload()
  }
  onBeforUpload(event, fileUpload) {
    this.loaderService.show();
    if (fileUpload.files) {
      for (let i = 0; i < fileUpload.files.length; i++) {
        const element = fileUpload.files[i];
        const type = fileUpload.files[i].type;
        if (element.size > 0) {
          this.uploadedFilesSuccess.push(element)
        }
      }
    }
    event.xhr.setRequestHeader('Authorization', + this.ultilities.getToken())
    if (event.formData) {
    }
  }
  onUploadSuccess(event, modal) {
    this.loaderService.hide();
    if (event && event.xhr && event.xhr.response) {
      const document = JSON.parse(event.xhr.response)
      if (document && document.resultCode === 200) {
        if(Number(document.object) > 0){
          this.ultilities.showSuccess('Import Thành công :' + Number(document.object) + ' Bản ghi');
        }else{
          this.ultilities.showSuccess('Import Thành công ');
        }
        
      } else {
        this.ultilities.showError('Template Import lỗi hoặc không có bản ghi hợp lệ. xin hãy kiểm tra lại')
      }
    }
  }
  onUploadError(event, modal) {
    this.loaderService.hide();
    setTimeout(() => {
      this.progressBarPercent = 100
      if (modal) modal.hide()
      if (event && event.xhr) {
        let response;
        if (event.xhr.response) {
          response = JSON.parse(event.xhr.response)
        }
        let message = 'Import Thất bại'
        if (response && response.message) {
          message = response.message
        }
        this.ultilities.showError(message)
      }
    }, 1000)
  }
  fixAttribute(){
    this.loaderService.show();
    this.categoriesService.fixAttribute().subscribe((response: any) => {
      this.loaderService.hide();
      this.ultilities.showSuccess('Thành Công!');
    }, error => {
      this.loaderService.hide();
    });
  }

  // ===================END UPLOAD==============================
  // export template


}


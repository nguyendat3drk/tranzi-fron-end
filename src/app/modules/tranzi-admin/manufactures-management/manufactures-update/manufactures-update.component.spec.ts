import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ManufacturesUpdateComponent } from './manufactures-update.component';


describe('BvCompulsoryInsuranceViewUpdateComponent', () => {
  let component: ManufacturesUpdateComponent;
  let fixture: ComponentFixture<ManufacturesUpdateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ManufacturesUpdateComponent],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManufacturesUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

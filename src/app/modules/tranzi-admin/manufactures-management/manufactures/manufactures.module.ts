import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../../../shared/shared.module';
import { HttpClientModule } from '@angular/common/http';
import { DataTableModule } from 'primeng/datatable';
import { ContextMenuModule } from 'primeng/contextmenu';
import { ConfirmDialogModule } from '../../../../shared/confirm-dialog/confirm-module';
import { CalendarModule } from 'primeng/calendar';
import { TextMaskModule } from 'angular2-text-mask';
import { NgxMaskModule } from 'ngx-mask';
import { CurrencyMaskModule } from 'ng2-currency-mask';
import { ManufacturesRoutingModule } from './manufactures.routing.module';
import { ModalModule } from 'ngx-bootstrap';
import { ManufacturesComponent } from './manufactures.component';
import {PaginatorModule} from 'primeng/paginator';
import { ManufactService } from '../../../../shared/services/http/manufact.service';

@NgModule({
  imports: [
    ManufacturesRoutingModule,
    CommonModule,
    SharedModule,
    HttpClientModule,
    DataTableModule,
    ContextMenuModule,
    ConfirmDialogModule,
    CalendarModule,
    TextMaskModule,
    ModalModule.forRoot(),
    NgxMaskModule.forRoot(),
    CurrencyMaskModule,
    PaginatorModule,
  ],
  declarations: [
    ManufacturesComponent,
  ],
  // exports: [RouterModule],
  providers: [
    ManufactService,
  ],
})
export class ManufacturesModule {
}

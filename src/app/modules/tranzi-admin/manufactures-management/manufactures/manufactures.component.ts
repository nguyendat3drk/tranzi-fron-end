import { Utilities } from '../../../../shared/services/utilities.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { LoaderService } from '../../../../shared/services/loader.service';
import { ManufactService } from '../../../../shared/services/http/manufact.service';
import { Manufactures } from '../../../../shared/models/Manufactures';
import { PagerData } from '../../../../shared/entity/pagerData';
import { CategoriesComponent } from '../../categories-management/categories/categories.component';
import { CategoriesService } from '../../../../shared/services/http/categories.service';
import { ModalDirective } from 'ngx-bootstrap';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'manufactures',
  templateUrl: './manufactures.component.html',
  styleUrls: ['./manufactures.component.scss'],
})
export class ManufacturesComponent implements OnInit {
  page: number =  1;
  txtSearch: string = '';
  listManufactures: Manufactures[] = [];
  totalPage: number = 0;
  selectManufactures: Manufactures = new Manufactures();

  @ViewChild('confirmDialodDel') public confirmDialodDel: ModalDirective;
  idDelete: Number = 0;
  title: string = '';
  constructor(private router: Router,
    private categoriesService: CategoriesService,
    private loaderService: LoaderService, private ultilities: Utilities,
    private manufacturesService: ManufactService) {
      this.ultilities.closeAutoMenu();
  }

  ngOnInit() {
    this.loadManufactures();
  }
  loadManufactures() {
    this.loaderService.show();
    this.categoriesService.getAllsPaddingManu(1, 10, 'id', 'desc', this.txtSearch).subscribe((response: any) => {
      // tslint:disable-next-line:no-console
      // console.log(response)
      if (response && response.resultCode === 200) {
        let page = new PagerData();
        this.listManufactures = []
        this.totalPage = 0;
        page = response.object;
        if (page && page.content) {
          this.listManufactures = page.content;
          this.totalPage = page.totalRow;
        }
        // this.listProduct = response;
      }
      this.loaderService.hide()
    }, error => {
      this.listManufactures = []
        this.totalPage = 0;
      this.loaderService.hide();
    });
  }
  createCategori() {
    this.router.navigate(['/tranzi-admin/manufactures-create']);
  }
  paginate(event) {
    if (event) {
      this.listManufactures = [];
      this.totalPage = 0;
      this.loaderService.show()
      this.categoriesService.getAllsPaddingManu(event.page ? (event.page + 1) : 1,
       event.rows ? event.rows : 10, 'id', 'desc', this.txtSearch).subscribe((response: any) => {
        // tslint:disable-next-line:no-console
        // console.log(response)
        if (response && response.resultCode === 200) {
          let page = new PagerData();
          page = response.object;
          if (page && page.content) {
            this.listManufactures = page.content;
            this.totalPage = page.totalRow;
          }
          // this.listProduct = response;
        }
        this.loaderService.hide()
      }, error => {
        this.loaderService.hide();
      });
    }

  }

  seSelectManufactures(event) {
    this.selectManufactures = new Manufactures()
    if (event && event.data) {
      this.selectManufactures = event.data;
    }
  }
  routingUpdate(id) {
    if (id) {
      this.router.navigate(['/tranzi-admin/manufactures-update/' + id]);
    }
  }
  deleteManufact() {
    this.confirmDialodDel.hide();
    if (this.idDelete) {
      this.loaderService.show();
      this.categoriesService.deleteManufacturesIdManufact(this.idDelete).subscribe((response: any) => {
        if (response && response.resultCode === 200) {
          this.loadManufactures();
          this.ultilities.showSuccess('Xóa thành công');
        } else if (response && response.resultCode === 205) {
          this.ultilities.showError('Nhà sản xuất đã liên kết sản phẩm. Không thể xóa.')
        } else if (response && response.resultCode === 204) {
          this.ultilities.showError(response.errorMessage ? response.errorMessage : 'Có lỗi xảy ra. Liên hệ Admin để kiểm tra lại')
        }
        this.loaderService.hide();
        // this.ultilities.showSuccess('Delete Success Manufactures')
      }, error => {
        this.loaderService.hide();
        this.ultilities.showError('Delete Error Manufactures')
      });
    }
  }
  deleteTest(pro: Manufactures) {
    if (pro) {
      this.title = 'Bạn có muốn xóa nhà sản xuất: ' + pro.name + ' không?';
      this.idDelete = pro.id
      this.confirmDialodDel.show();
    }
  }

}

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ManufacturesComponent } from './manufactures.component';
import { PageNotFoundComponent } from '../../../../shared/components/page-not-found/page-not-found.component';

const routes: Routes = [
  {
    path: '',
    component: ManufacturesComponent,
  },
  { path: '**', component: PageNotFoundComponent },
];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
  ],
  exports: [RouterModule],
})
export class ManufacturesRoutingModule { }

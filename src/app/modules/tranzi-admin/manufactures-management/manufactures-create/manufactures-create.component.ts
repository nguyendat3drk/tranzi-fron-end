import { Utilities } from '../../../../shared/services/utilities.service';
import { Component, OnInit } from '@angular/core';
import { MessageService } from 'primeng/components/common/messageservice';
import { ActivatedRoute, Router } from '@angular/router';
import { LoaderService } from '../../../../shared/services/loader.service';
import { ManufactService } from '../../../../shared/services/http/manufact.service';
import { Manufactures } from '../../../../shared/models/Manufactures';
import { TranslateService } from '@ngx-translate/core';
import { CategoriesService } from '../../../../shared/services/http/categories.service';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'manufactures-create',
  templateUrl: './manufactures-create.component.html',
  styleUrls: ['./manufactures-create.component.scss'],
})
export class ManufacturesCreateComponent implements OnInit {

  selectManufactures: Manufactures = new Manufactures();
  public editorValue: string = ''

  // upload
  urlupload: string = '';
  urlServerFrontEnd: string = '';
  uploadedFilesSuccess: any[] = []
  uploadedFilesError: any[] = []
  uploadedFilesErrorInvalid: any[] = []
  progressBarPercent = 20
  totalFile: number = 0;

  constructor(private router: Router, private translateService: TranslateService, private categoriesService: CategoriesService,
    private activeRouter: ActivatedRoute, private messageService: MessageService,
    private loaderService: LoaderService, private ultilities: Utilities,
    private manufacturesService: ManufactService) {
    this.urlupload = this.ultilities.getUrlServerHost() + '/share-api/uploadImageManufactures'
    this.urlServerFrontEnd = this.ultilities.urlServerFrontEnd() + 'assets/images/manufactures/'
  }
  ngOnInit() {
  }
  changeSlugByname() {
    if (!this.selectManufactures.slug && this.selectManufactures.name) {
      this.selectManufactures.slug = this.selectManufactures.name;
      this.setSlugOnchange();
    }
  }
  setSlugOnchange() {
    if (event) {
      this.selectManufactures.slug = this.ultilities.changeToSlug(this.selectManufactures.slug);
    }
  }
  createManufactures() {
    this.selectManufactures.id = 0;
    if (this.selectManufactures.blSeoIndex) {
      this.selectManufactures.seoIndex = 1
    } else {
      this.selectManufactures.seoIndex = 0
    }
    const check = this.validate('save');
    if (!check) {
      return;
    }

    this.categoriesService.createManufact(this.selectManufactures).subscribe((response: any) => {
      // tslint:disable-next-line:no-console
      // console.log('responseresponseresponse=', response);
      if (response && response.resultCode === 200) {
        this.router.navigate(['/tranzi-admin/manufactures']);
      } else if (response && response.resultCode === 204) {
        this.ultilities.showError('Not Update Database')
      }
      window.scrollTo(0, 0);
      this.loaderService.hide();
    }, (error) => {
      // tslint:disable-next-line:no-console
      let mes = '';
      if (error && error.error && error.error.errors && error.error.errors[0] && error.error.errors[0].message) {
        mes = error.error.errors[0].message;
      }
      this.messageService.clear();
      this.messageService.add({
        detail: mes ? mes : 'Có lỗi xảy ra. Liên hệ Admin để kiểm tra lại',
        severity: 'error',
        summary: 'Lỗi',
      });
      this.loaderService.hide();
    });
  }
  tesst(event) {

  }

  validate(event) {
    let check = true;
    if (event === 'name' || event === 'save') {
      this.selectManufactures.blname = false;
      if (!this.selectManufactures || !this.selectManufactures.name) {
        this.selectManufactures.blname = true;
        check = false;
      }
    }
    if (event === 'seoTitle' || event === 'save') {
      this.selectManufactures.blSeoTitle = false;
      if (!this.selectManufactures || !this.selectManufactures.seoTitle) {
        this.selectManufactures.blSeoTitle = true;
        check = false;
      }
    }
    if (event === 'seoDescription' || event === 'save') {
      this.selectManufactures.blSeoDepcription = false;
      if (!this.selectManufactures || !this.selectManufactures.seoDescription) {
        this.selectManufactures.blSeoDepcription = true;
        check = false;
      }
    }
    if (event === 'seoKeywords' || event === 'save') {
      this.selectManufactures.blSeoKeywork = false;
      if (!this.selectManufactures || !this.selectManufactures.seoKeywords) {
        this.selectManufactures.blSeoKeywork = true;
        check = false;
      }
    }
    return check;
  }
  bankmanagemanufact() {
    this.router.navigate(['/tranzi-admin/manufactures']);
  }
  keyPress(event: any, type) {
    if (type && type === 'number') {
      const patternExtension = /[0-9]/;
      const inputChar = String.fromCharCode(event.charCode);
      if (event.keyCode !== 9 && !patternExtension.test(inputChar)) {
        event.preventDefault();
      }
    } else if (type && type === 'date') {
      const patternExtension = /[0-9\/]/;
      const inputChar = String.fromCharCode(event.charCode);
      if (event.keyCode !== 9 && !patternExtension.test(inputChar)) {
        event.preventDefault();
      }
    } else {
      const patternExtension = /[0-9\.]/;
      const inputChar = String.fromCharCode(event.charCode);
      if (event.keyCode !== 9 && !patternExtension.test(inputChar)) {
        event.preventDefault();
      }
    }
  }
  // upload===============================================UPLOAD====================
  btnUploadClick(fileUpload) {
    fileUpload.advancedFileInput.nativeElement.click()
  }

  onSelectedFiles(modal, fileUpload) {
    this.totalFile = 0;
    this.progressBarPercent = 20
    this.uploadedFilesError = []
    this.uploadedFilesErrorInvalid = [];
    this.uploadedFilesSuccess = []
    if (fileUpload.advancedFileInput.nativeElement.files) {
      this.totalFile = fileUpload.files.length;
      for (let i = 0; i < fileUpload.advancedFileInput.nativeElement.files.length; i++) {
        const element = fileUpload.advancedFileInput.nativeElement.files[i];
        const type = fileUpload.advancedFileInput.nativeElement.files[i].type;
        const strTypeCheck = type.split('/')[0];
        const strTypeTailFile = type.split('/')[1];
        if (('' + strTypeCheck).toUpperCase().trim() === 'VIDEO' && (('' + strTypeTailFile).toUpperCase().trim() !== 'MP4'
          && ('' + strTypeTailFile).toUpperCase().trim() !== '3GP' && ('' + strTypeTailFile).toUpperCase().trim() !== '3GPP')) {
          // tslint:disable-next-line:prefer-const
          let dto = {
            value: fileUpload.advancedFileInput.nativeElement.files[i].name,
            type: fileUpload.advancedFileInput.nativeElement.files[i].type,
          }
          this.uploadedFilesErrorInvalid.push(dto)
        } else if (element.size > 10000000 || element.size === 0) {
          this.uploadedFilesError.push(element)
        }

      }
    }
    if (fileUpload && fileUpload.msgs) {
      for (let i = 0; i < fileUpload.msgs.length; i++) {
        const element = fileUpload.msgs[i];
        if (element) {
          const maxsize = element.detail.indexOf('maximum');
          let valueDetail = '';
          let valueType = '';
          valueDetail = element.summary.split(':')[0];
          if (maxsize < 0) {
            valueType = 'Không đúng định dạng'
            // tslint:disable-next-line:prefer-const
            let dto = {
              value: valueDetail,
              type: valueType,
            }
            this.uploadedFilesErrorInvalid.push(dto)
          }
        }
      }
    }
    const checkNumberError = Number(this.uploadedFilesErrorInvalid ? this.uploadedFilesErrorInvalid.length : 0) +
      Number(this.uploadedFilesError ? this.uploadedFilesError.length : 0);
    if (this.totalFile === checkNumberError) {
      this.progressBarPercent = 100
      this.ultilities.showError('Cập nhật thất bại')
    }
    modal.show()
    fileUpload.upload()
  }
  onBeforUpload(event, fileUpload) {
    // tslint:disable-next-line:prefer-const
    let interval = setInterval(() => {
      if (this.progressBarPercent < 90) {
        this.progressBarPercent = this.progressBarPercent + 10;
      }
      if (this.progressBarPercent >= 100) {
        this.progressBarPercent = 100;
        clearInterval(interval);
      }
    }, 700);
    this.uploadedFilesSuccess = []
    if (fileUpload.files) {
      for (let i = 0; i < fileUpload.files.length; i++) {
        const element = fileUpload.files[i];
        const type = fileUpload.files[i].type;
        const strTypeCheck = type.split('/')[0];
        const strTypeTailFile = type.split('/')[1];
        if (element.size > 0) {
          // if (('' + strTypeCheck).toUpperCase().trim() === 'VIDEO' && (('' + strTypeTailFile).toUpperCase().trim() !== 'MP4'
          //   && ('' + strTypeTailFile).toUpperCase().trim() !== '3GP' && ('' + strTypeTailFile).toUpperCase().trim() !== '3GPP')) {
          // } else {
            this.uploadedFilesSuccess.push(element)
          // }
        }
      }
    }
    event.xhr.setRequestHeader('Authorization', + this.ultilities.getToken())
    if (event.formData) {
      // event.formData.append('id', 1993)
      // event.formData.papend('nameForder', 'banner')
    }
  }
  onUploadSuccess(event, modal) {
    setTimeout(() => {
      const checkNumberError = Number(this.uploadedFilesErrorInvalid ? this.uploadedFilesErrorInvalid.length : 0) +
        Number(this.uploadedFilesError ? this.uploadedFilesError.length : 0);
      this.progressBarPercent = 100

      if (event && event.xhr && event.xhr.status !== 200 || this.totalFile === checkNumberError) {
        if (modal) modal.hide()
        this.ultilities.showError('Cập nhật thất bại')
      } else {
      }
    }, 1000);

    if (event && event.xhr && event.xhr.response) {
      const document = JSON.parse(event.xhr.response)
      if (document) {
        // this.ultilities.showSuccess('upload thanh cong')
        this.selectManufactures.logoImage = this.urlServerFrontEnd + this.ultilities.clearNameKytuDacbiet(document.object);
        
      }
    }
  }
  onUploadError(event, modal) {
    setTimeout(() => {
      this.progressBarPercent = 100
      if (modal) modal.hide()
      if (event && event.xhr) {
        let response;
        if (event.xhr.response) {
          response = JSON.parse(event.xhr.response)
        }
        let message = 'Cập nhật không thành công'
        if (response && response.message) {
          message = response.message
        }
        // this.ultilities.showError(message)
      }
    }, 1000)
  }

}




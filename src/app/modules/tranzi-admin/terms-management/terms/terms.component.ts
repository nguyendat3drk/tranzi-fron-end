import { Utilities } from '../../../../shared/services/utilities.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ProductService } from '../../../../shared/services/http/product.service';
import { MessageService } from 'primeng/components/common/messageservice';
import { ActivatedRoute, Router } from '@angular/router';
import { LoaderService } from '../../../../shared/services/loader.service';
import { TermsService } from '../../../../shared/services/http/TermsService';
import { Terms } from '../../../../shared/models/Terms';
import { PagerData } from '../../../../shared/entity/pagerData';
import { isFulfilled } from 'q';
import { TreeNode } from 'primeng/api';
import { ThemeSettingsComponent } from '../../../../@theme/components';
import { ModalDirective } from 'ngx-bootstrap';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'terms',
  templateUrl: './terms.component.html',
  styleUrls: ['./terms.component.scss'],
})
export class TermsComponent implements OnInit {
  listTerms: Terms[] = [];
  totalPage: number = 0;
  selectTerms: Terms = new Terms();
  txtSearch: string = '';

  cols: any[];
  loading: boolean;
  @ViewChild('confirmDialodDel') public confirmDialodDel: ModalDirective;
  idDelete: Number = 0;
  title: string = '';
  //  list danh sach the loai con
  listTermsChild: Terms[] = [];
  totalPageChild: number = 0;
  selectTermsChild: Terms = new Terms();
  colsChild: any[];
  loadingChild: boolean;
  idDeleteChild: Number = 0;
  constructor(private utilities: Utilities, private router: Router,
    private activeRouter: ActivatedRoute, private messageService: MessageService,
    private loaderService: LoaderService, private ultilities: Utilities,
    private termsService: TermsService) {
      this.ultilities.closeAutoMenu();
  }

  ngOnInit() {

    this.loadterms();
    setTimeout(() => {
      if (localStorage.getItem('addVideo')) {
        this.ultilities.showSuccess(localStorage.getItem('addVideo'));
        localStorage.removeItem('addVideo')
      } else if (localStorage.getItem('editVideos')) {
        this.ultilities.showSuccess(localStorage.getItem('editVideos'));
        localStorage.removeItem('editVideos')
      }
    }, 300);
  }
  loadterms() {
    this.loaderService.show();
    this.termsService.getAllsPadding(1, 10, 'id', 'desc', this.txtSearch, 0).subscribe((response: any) => {
      this.listTerms = [];
      this.totalPage = 0;
      if (response && response.resultCode === 200) {
        let page = new PagerData();
        page = response.object;
        if (page && page.content) {
          this.listTerms = page.content;
          this.totalPage = page.totalRow;
        }
        // this.listProduct = response;
      }
      this.loaderService.hide()
    }, error => {
      this.listTerms = [];
      this.totalPage = 0;
      this.loaderService.hide();
    });
  }
  create() {
    this.router.navigate(['/tranzi-admin/terms-create']);
  }

  paginate(event) {
    if (event) {
      this.listTerms = [];
      // this.totalPage = 0;
      // tslint:disable-next-line:max-line-length
      this.termsService.getAllsPadding(event.page ? (event.page + 1) : 1, event.rows ? event.rows : 10, 'id', 'desc', this.txtSearch, 0).subscribe((response: any) => {
        // tslint:disable-next-line:no-console
        // console.log(response)
        if (response && response.resultCode === 200) {
          let page = new PagerData();
          page = response.object;
          if (page && page.content) {
            this.listTerms = page.content;
            this.totalPage = page.totalRow;
          }
        }
        this.loaderService.hide()
      }, error => {
        this.totalPage = 0
        this.loaderService.hide();
      });
    }

  }
  routingUpdate(id) {
    if (id) {
      this.router.navigate(['/tranzi-admin/terms-update/' + id]);
    }
  }
  delete() {
    // this.listterms = [];
    this.confirmDialodDel.hide();
    if (this.idDelete) {
      this.loaderService.show();
      this.termsService.deleteId(this.idDelete).subscribe((response: any) => {
        if (response && response.resultCode === 200) {
          // Dung table
          this.loadterms();
          this.ultilities.showSuccess('Xóa thành công');
        } else if (response && response.resultCode === 205) {
          this.ultilities.showError('Thể loại đã liên kết. Không thể xóa!')
        } else if (response && response.resultCode === 204) {
          this.ultilities.showError(response.errorMessage ? response.errorMessage : 'Có lỗi xảy ra. Liên hệ Admin để kiểm tra lại')
        }
        this.loaderService.hide()
        // this.ultilities.showSuccess('Delete Success terms')
      }, error => {
        this.loaderService.hide();
        this.ultilities.showError('Delete Error terms')
      });
    }
  }
  deleteCheck(ca: Terms) {
    if (ca) {
      this.title = 'Bạn có muốn xóa Thể loại: ' + ca.name + ' không?';
      this.idDelete = ca.id
      this.confirmDialodDel.show();
    }
  }
  paginateChild(event) {
    if (event) {
      this.listTermsChild = [];
      // tslint:disable-next-line:max-line-length
      this.termsService.getAllsPadding( 1,1000, 'id', 'desc', this.txtSearch, event).subscribe((response: any) => {
        // tslint:disable-next-line:no-console
        // console.log(response)
        if (response && response.resultCode === 200) {
          let page = new PagerData();
          page = response.object;
          if (page && page.content) {
            this.listTermsChild = page.content;
            this.totalPageChild = page.totalRow;
          }
        }
        this.loaderService.hide()
      }, error => {
        this.totalPageChild = 0
        this.loaderService.hide();
      });
    }

  }
  selectTerm(event,){
    if (event !== undefined && event !== null) {
      if (this.listTerms && this.listTerms.length > 0) {
        for (let i = 0; i < this.listTerms.length; i++) {
          if (Number(event) === i) {
            this.listTerms[i].blSelectedRow = true;
            this.selectTerms = this.listTerms[i];
            this.paginateChild(this.selectTerms.id);
          } else {
            this.listTerms[i].blSelectedRow = false;
          }
        }
      }
    }
  }
  createChild(){
    localStorage.removeItem("idTermChild");
    if(this.selectTerms && this.selectTerms.id > 0){
      localStorage.setItem("idTermChild", this.selectTerms.id+'');
    }
    this.router.navigate(['/tranzi-admin/terms-create']);
  }
  
}

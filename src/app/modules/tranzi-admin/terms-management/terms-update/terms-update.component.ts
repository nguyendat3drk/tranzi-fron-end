import { Utilities } from '../../../../shared/services/utilities.service';
import { Component, OnInit } from '@angular/core';
import { MessageService } from 'primeng/components/common/messageservice';
import { ActivatedRoute, Router } from '@angular/router';
import { LoaderService } from '../../../../shared/services/loader.service';
import { TermsService } from '../../../../shared/services/http/TermsService';
import { Terms } from '../../../../shared/models/Terms';
import { TranslateService } from '@ngx-translate/core';
import { TermsManagmentService } from '../../../../shared/services/http/terms-managment.service';
import { Categories } from '../../../../shared/models/Categories';
import { CategoriesService } from '../../../../shared/services/http/categories.service';
import { PagerData } from '../../../../shared/entity/pagerData';


@Component({
  // tslint:disable-next-line:component-selector
  selector: 'terms-update',
  templateUrl: './terms-update.component.html',
  styleUrls: ['./terms-update.component.scss'],
})
export class TermsUpdateComponent implements OnInit {
  termsId: number =0;
  selectListCategory: number[] = [];
  listItemCategory: Categories[] = [];
  // listCategory: Categories[] = []

  selectTerms: Terms = new Terms();
  public editorValue: string = ''
  urlServerFrontEnd: string;


  // upload
  urlupload: string = '';
  uploadedFilesSuccess: any[] = []
  uploadedFilesError: any[] = []
  uploadedFilesErrorInvalid: any[] = []
  progressBarPercent = 20
  totalFile: number = 0;

  // validatee
  blName: boolean = false;
  blBannerImage: boolean = false;

  constructor(private router: Router, private translateService: TranslateService, private activeRouter: ActivatedRoute,
    private messageService: MessageService, private termsService: TermsManagmentService,
    private loaderService: LoaderService, private ultilities: Utilities,private categoriesService: CategoriesService,
    private termsServiceChild: TermsService) {
    this.urlupload = this.ultilities.getUrlServerHost() + '/share-api/upload'
    this.urlServerFrontEnd = this.ultilities.urlServerFrontEnd() + 'assets/banner/'
  }
  ngOnInit() {
    this.activeRouter.params.subscribe((params) => {
      this.termsId = +params.id
      if (this.termsId) {
        this.getTermsId();
      }
    });
    this.getAllCategori();
  }
  getTermsId() {
    this.termsService.getTermsID(this.termsId).subscribe((response: any) => {
      // tslint:disable-next-line:no-console
      console.log(response)
      if (response && response.resultCode === 200) {
        this.selectTerms = response.object;
        if (this.selectTerms && this.selectTerms.seoIndex && this.selectTerms.seoIndex === 1) {
          this.selectTerms.blSeoIndex = true;
        } else {
          this.selectTerms.blSeoIndex = false;
        }
        if (this.selectTerms && this.selectTerms.listCategory && this.selectTerms.listCategory.length > 0) {
          this.selectListCategory = this.selectTerms.listCategory;
        } else {
          this.selectListCategory = [];
        }
      }
      this.loaderService.hide()
    }, error => {
      this.loaderService.hide();
    });
  }

  getAllCategori() {
    this.loaderService.show();
    this.listItemCategory = [];
    this.termsServiceChild.getAllsPadding(1,1000, 'id', 'desc','',0).subscribe((response: any) => {
      if (response && response.resultCode === 200) {
        let page = new PagerData();
        page = response.object;
        if (page && page.content) {
          this.listItemCategory = page.content;
        }
      }
      this.loaderService.hide();
    }, error => {
      this.loaderService.hide();
    });
  }
  changeSlugByname() {
    if (!this.selectTerms.slug && this.selectTerms.name) {
      this.selectTerms.slug = this.selectTerms.name;
      this.setSlugOnchange();
    }
  }
  setSlugOnchange() {
    if (event) {
      this.selectTerms.slug = this.ultilities.changeToSlug(this.selectTerms.slug);
    }
  }
  createTerms() {
    this.validate();
    if (!this.selectTerms || !this.selectTerms.name) {
      return;
    }
    if (this.selectTerms.blSeoIndex) {
      this.selectTerms.seoIndex = 1
    } else {
      this.selectTerms.seoIndex = 0
    }
    this.selectTerms.listCategory = this.selectListCategory;
    this.termsService.update(this.selectTerms).subscribe((response: any) => {
      // tslint:disable-next-line:no-console
      // console.log('responseresponseresponse=', response);
      if (response && response.resultCode === 200) {
        this.router.navigate(['/tranzi-admin/terms']);
        localStorage.setItem('editVideos', 'Cập nhật bài viết thành công');
      } else if (response && response.resultCode === 204) {
        this.ultilities.showError('Not Update Database')
      }
      window.scrollTo(0, 0);
      this.loaderService.hide();
    }, (error) => {
      // tslint:disable-next-line:no-console
      let mes = '';
      if (error && error.error && error.error.errors && error.error.errors[0] && error.error.errors[0].message) {
        mes = error.error.errors[0].message;
      }
      this.messageService.clear();
      this.messageService.add({
        detail: mes ? mes : 'Có lỗi xảy ra. Liên hệ Admin để kiểm tra lại',
        severity: 'error',
        summary: 'Lỗi',
      });
      this.loaderService.hide();
    });
  }
  tesst(event) {

  }
  bankmanager() {
    this.router.navigate(['/tranzi-admin/terms']);
  }
  keyPress(event: any, type) {
    if (type && type === 'number') {
      const patternExtension = /[0-9]/;
      const inputChar = String.fromCharCode(event.charCode);
      if (event.keyCode !== 9 && !patternExtension.test(inputChar)) {
        event.preventDefault();
      }
    } else if (type && type === 'date') {
      const patternExtension = /[0-9\/]/;
      const inputChar = String.fromCharCode(event.charCode);
      if (event.keyCode !== 9 && !patternExtension.test(inputChar)) {
        event.preventDefault();
      }
    } else {
      const patternExtension = /[0-9\.]/;
      const inputChar = String.fromCharCode(event.charCode);
      if (event.keyCode !== 9 && !patternExtension.test(inputChar)) {
        event.preventDefault();
      }
    }
  }
  // upload===============================================UPLOAD====================
  btnUploadClick(fileUpload) {
    fileUpload.advancedFileInput.nativeElement.click()
  }
  btnUploadClickIcon(fileUploadIcon) {
    fileUploadIcon.advancedFileInput.nativeElement.click()
  }

  onSelectedFiles(fileUpload) {
    fileUpload.upload()
  }
  onBeforUpload(event, fileUpload) {
    event.xhr.setRequestHeader('Authorization', + this.ultilities.getToken())
    if (event.formData) {
    }
  }
  onUploadSuccess(event) {
    setTimeout(() => {
      if (event && event.xhr && event.xhr.response) {
        const document = JSON.parse(event.xhr.response)
        if (document) {
          // this.ultilities.showSuccess('upload thanh cong')
          this.selectTerms.bannerImage = this.urlServerFrontEnd + this.ultilities.clearNameKytuDacbiet(document.object);
        }
      }
    }, 100)
  }

  onUploadError(event) {
    setTimeout(() => {
      this.progressBarPercent = 100
      if (event && event.xhr) {
        let response;
        if (event.xhr.response) {
          response = JSON.parse(event.xhr.response)
        }
        let message = 'Cập nhật không thành công'
        if (response && response.message) {
          message = response.message
        }
        // this.ultilities.showError(message)
      }
    }, 1000)
  }

  validate() {
    this.blName = true;
    this.blBannerImage = true;
  }
}
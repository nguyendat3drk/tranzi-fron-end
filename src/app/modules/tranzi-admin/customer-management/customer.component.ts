import { Utilities } from '../../../shared/services/utilities.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ProductService } from '../../../shared/services/http/product.service';
import { MessageService } from 'primeng/components/common/messageservice';
import { ActivatedRoute, Router } from '@angular/router';
import { LoaderService } from '../../../shared/services/loader.service';
import { CategoriesService } from '../../../shared/services/http/categories.service';
import { Categories } from '../../../shared/models/Categories';
import { PagerData } from '../../../shared/entity/pagerData';
import { isFulfilled } from 'q';
import { ThemeSettingsComponent } from '../../../@theme/components';
import { AttributesService } from '../../../shared/services/http/attributes.service';
import { Attributes } from '../../../shared/models/Attributes';
import { ModalDirective } from 'ngx-bootstrap';
import { Attribute } from '@angular/compiler';
import { AttributeValues } from '../../../shared/models/AttributeValues';
import { Customer } from '../../../shared/models/Customer';
import { CustomerService } from '../../../shared/services/http/customer.service';
import { fromJS } from 'immutable';
@Component({
  // tslint:disable-next-line:component-selector
  selector: 'customer',
  templateUrl: './customer.component.html',
  styleUrls: ['./customer.component.scss'],
})
export class CustomerComponent implements OnInit {
  txtSearch:string = ''

  listCustomers: any[] = [];
  selectCustomer: Customer = new Customer();
  totalPage: number = 0;
  blName: boolean = false;
  blEmail: boolean = false;
  blEmailFomat: boolean = false;
  blPass: boolean = false;
  blPassFomat: boolean = false;
  blActive: boolean = true;
  @ViewChild('addAttribute') public addAttribute: ModalDirective;
  @ViewChild('addAttributeValues') public addAttributeValues: ModalDirective;
  @ViewChild('confirmDialodDel') public confirmDialodDel: ModalDirective;
  idDelete: Number = 0;
  title: string = '';
  constructor(private utilities: Utilities, private router: Router,
    private activeRouter: ActivatedRoute, private messageService: MessageService,
    private loaderService: LoaderService, private ultilities: Utilities,
    private customerService: CustomerService) {
      this.ultilities.closeAutoMenu();
  }

  ngOnInit() {
    this.loadCustomer();
  }
  loadCustomer() {
    this.listCustomers = []
    this.customerService.getAllsPadding(1, 10, 'id', 'desc', this.txtSearch).subscribe((response: any) => {
      if (response && response.resultCode === 200) {
        let page = new PagerData();
        page = response.object;
        if (page && page.content) {
          this.listCustomers = page.content;
          this.totalPage = page.totalRow;
        }
      }
      this.loaderService.hide()
    }, error => {
      this.loaderService.hide();
    });
  }
  paginate(event) {
    if (event) {
      this.customerService.getAllsPadding(event.page ? (event.page + 1) : 1, event.rows ?
       event.rows : 10, 'id', 'desc', this.txtSearch).subscribe((response: any) => {
        if (response && response.resultCode === 200) {
          let page = new PagerData();
          this.listCustomers = [];
          page = response.object;
          if (page && page.content) {
            this.listCustomers = page.content;
            this.totalPage = page.totalRow;
          }
        }
        this.loaderService.hide()
      }, error => {
        this.listCustomers = [];
        this.totalPage = 0
        this.loaderService.hide();
      });
    }
  }
  selectRowCustomers(event) {
    if (event !== undefined && event !== null) {
      if (this.listCustomers && this.listCustomers.length > 0) {
        for (let i = 0; i < this.listCustomers.length; i++) {
          if (Number(event) === i) {
            this.listCustomers[i].blSelectedRow = true;
            this.selectCustomer = this.listCustomers[i];
            // TO-DO
            // this.loadAttributeValuesById(this.selectAttributes.id);
          } else {
            this.listCustomers[i].blSelectedRow = false;
          }
        }
      }
    }
  }
  createCustomers(event) {
    this.blName = false;
    this.blEmail = false;
    this.blEmailFomat = false;
    if (!event) {
      this.blActive = true
      this.blName = false;
      this.selectCustomer = new Customer();
    } else {
      const guaranteeNew = fromJS(event)
      event = guaranteeNew.toJS();
      this.selectCustomer = event;
      if (this.selectCustomer && this.selectCustomer.status === 1) {
        this.blActive = true;
      } else {
        this.blActive = false;
      }
    }
    this.addAttribute.show()
  }
  validateCustomer(event) {
    let check = true;
    if (event === 'name' || event === 'save') {
      this.blName = false;
      if (!this.selectCustomer || !this.selectCustomer.fullName || !this.selectCustomer.fullName.trim()) {
        this.blName = true;
        check = false;
      }
    }
    if (event === 'email' || event === 'save') {
      this.blEmail = false;
      this.blEmailFomat = false;
      if (!this.selectCustomer || !this.selectCustomer.email || !this.selectCustomer.email.trim()) {
        this.blEmail = true;
        check = false;
      } else if (!this.ultilities.checkEmail(this.selectCustomer.email.trim())) {
        this.blEmailFomat = true;
        check = false;
      }
    }
    if (event === 'pass' || event === 'save') {
      this.blPass = false;
      this.blPassFomat = false;
      if (!this.selectCustomer || !this.selectCustomer.password) {
        this.blPass = true;
        check = false;
      } else if (!this.selectCustomer.password.trim()) {
        this.blPassFomat = true;
        check = false;
      }
    }
    return check;
  }
  submitOk() {
    let check = true;
    check = this.validateCustomer('save')
    if (!check) {
      return;
    }
    if (this.blActive) {
      this.selectCustomer.status = 1
    } else {
      this.selectCustomer.status = 0
    }
    this.customerService.create(this.selectCustomer).subscribe((response: any) => {
      if (response && response.resultCode === 200) {
        this.loadCustomer();
        this.ultilities.showSuccess('Thành công');
        this.addAttribute.hide();
      } else if (response && response.resultCode === 201) {
        this.ultilities.showError('Email ' + this.selectCustomer.email + ' đã tồn tại trong hệ thống.');
      } else {
        const mes = response.errorMessage;
        this.ultilities.showError(mes ? mes : 'Có lỗi xảy ra. Liên hệ Admin để kiểm tra lại');
      }
      this.loaderService.hide()
    }, error => {
      this.loaderService.hide();
      this.ultilities.showError('Có lỗi xảy ra. Liên hệ Admin để kiểm tra lại');
    });

  }
  viewDate(event) {
    if (event && Number(event) > 0) {
      return this.ultilities.convertDateToStringDateVN(new Date(Number(event) * 1000));
    }
    return ''
  }
  submitnotOk() {
    this.addAttribute.hide();
  }

  deleteCustomers() {
    if (this.idDelete) {
      this.loaderService.show();
      this.confirmDialodDel.hide();
      this.customerService.deleteAttributeId(this.idDelete).subscribe((response: any) => {
        if (response && response.resultCode === 200) {
          this.loadCustomer();
          this.ultilities.showSuccess('Xóa thành công')
        } else if (response && response.resultCode === 205) {
          this.ultilities.showError('Khách hàng đã có lịch sử giao dịch. Không thể xóa.')
        } else if (response && response.resultCode === 204) {
          this.ultilities.showError(response.errorMessage ? response.errorMessage : 'Có lỗi xảy ra. Liên hệ Admin để kiểm tra lại')
        }
        this.loaderService.hide()

      }, error => {
        this.loaderService.hide();
        this.ultilities.showError('Delete Error Attributes')
      });
    }
  }
  deleteCheck(pro: Customer) {
    if (pro) {
      this.title = 'Bạn có muốn xóa Khách hàng: ' + pro.fullName + ' không?';
      this.idDelete = pro.id
      this.confirmDialodDel.show();
    }
  }


}


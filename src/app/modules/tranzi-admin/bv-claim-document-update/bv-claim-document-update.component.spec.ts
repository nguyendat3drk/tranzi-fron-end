import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BvClaimDocumentUpdateComponent } from './bv-claim-document-update.component';

describe('BvClaimDocumentUpdateComponent', () => {
  let component: BvClaimDocumentUpdateComponent;
  let fixture: ComponentFixture<BvClaimDocumentUpdateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BvClaimDocumentUpdateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BvClaimDocumentUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

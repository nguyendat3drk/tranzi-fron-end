import { Component, OnInit } from '@angular/core';
import { Utilities } from '../../../shared/services/utilities.service';
import { Router } from '@angular/router';

@Component({
   // tslint:disable-next-line:component-selector
  selector: 'bv-claim-document-update',
  templateUrl: './bv-claim-document-update.component.html',
  styleUrls: ['./bv-claim-document-update.component.scss'],
})
export class BvClaimDocumentUpdateComponent implements OnInit {

  constructor(private utilities: Utilities, private router: Router) {
    // if (utilities.getRoleId() === 6 || utilities.getRoleId() === 9) {
    //   this.router.navigate(['/bao-viet/dashboard']);
    // }
  }

  ngOnInit() {
  }

}

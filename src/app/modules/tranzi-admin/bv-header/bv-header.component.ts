import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Router} from '@angular/router';
import 'rxjs/add/operator/switchMap';
import {TranslateService} from '@ngx-translate/core';
import {Oauth2Service} from '../../../shared/services/http/oauth2.service';
import {TranziAdminStateService} from '../tranzi-admin-state.service';
declare var baseAuth: any;
const KEY_ESC = 27;

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'bv-header',
  templateUrl: './bv-header.component.html',
  styleUrls: ['./bv-header.component.scss'],
})
export class BvHeaderComponent implements OnInit {
  private focusSelected: string;

  public user: Account;
  public isScrolled: boolean = false;
  public isMenuCollapsed: boolean = false;
  public userLogin: string;
  public iconStatus = 'assets/images/answer_call.png';
  public isAvailable: boolean = true;
  public isAuxiliary: boolean = false;
  public isOffDuty: boolean = false;
  public isAgent: boolean = false;
  public loginPopup: boolean = false;
  public pickedState: string = 'AVAILABLE';
  // public isAgentOffDuty = false;
  public confirmDialog: boolean = true;
  public title: string;
  public message: string;
  public okText: string;
  public cancelText: string;
  public closeText: string;
  public isShow: boolean = false;
  public isOn: boolean = false;
  public authRoleType: any;
  // public logsChange: LogsChangeState;
  typeLogin: string;

  @Input() child: boolean = false;

  @Output() itemHover = new EventEmitter<any>();
  @Output() toggleSubMenu = new EventEmitter<any>();

  // @ViewChild('staticModal') public staticModal1: ModalDirective;
  private _defaults = {
    title: 'Confirm',
    message: 'Confirm the operation?',
    cancelText: 'Không',
    okText: 'Có',
    closeText: 'Đóng',
  };

  public messYes: string = 'Có';
  public messNo: string = 'Không';
  private _confirmElement: any;
  private _cancelButton: any;
  private _okButton: any;
  private _closeButton: any;
  private _offButton: any;
  public checkDialog: boolean;
  toggleMenu1 = false

  constructor(
    // private _state: GlobalState,
    // private _confirm: ConfirmService,
    // private authenticationService: AuthenticationService,
    // private userService: AccountService,
    private router: Router,
    private translateService: TranslateService,
    private auth: Oauth2Service,
    private bvState: TranziAdminStateService,
    // private logStateService: LogsChangeStateService
  ) {
    console.log('baseAuth1====',baseAuth)
    // this._state.subscribe('menu.isCollapsed', (isCollapsed) => {
    //   this.isMenuCollapsed = isCollapsed;
    // });
    this.userLogin = 'Guest';
  }

  get token() {
    return this.auth.token;
  }
  public toggleMenu() {
    this.bvState.toggleSideBar();
  }

  public scrolledChanged(isScrolled) {
    this.isScrolled = isScrolled;
  }

  public changeStatus(status: string) {
    if (status === 'Logout') {
      localStorage.clear();
      localStorage.removeItem('token');
      this.router.navigate(['/login']);
    }

  }

  public logout() {
    this.auth.logout();
  }
  public setClass() {
    const classes = {
      'fa-check-circle': this.isAvailable,
      'fa-minus-circle': this.isAuxiliary,
      'fa-power-off': this.isOffDuty,
    };
    return classes;
  }

  public onClickMenu(link: string) {

    let routingAddress = '';
    if (link === 'changePass') {
      routingAddress = '/change-pass';
    } else if (link === 'home') {
      routingAddress = '/pages/dashboard';
    }
    const isInputingForm = localStorage.getItem('isInputingForm');
    if (isInputingForm === 'true') {
      setTimeout(() => { this.showDialog(routingAddress, null); }, 100);

    } else {
      this.router.navigate([routingAddress]);
    }
  }
  public showDialog(routingAddress: string, user: Account) {
    this.translateService.get('usermessage.error.navigate_when_inputing', {}).subscribe(
      data => {
        const translation = data;
        this.activate(null, null, 'Navigate')
          .then(result => {
            if (result === true) {
              if (routingAddress === '/login') {
                setTimeout(() => { this.changeStatus('Logout'); }, 200);
              } else {
                localStorage.removeItem('isInputingForm');
                localStorage.setItem('isInputingForm', 'false');
                this.router.navigate([routingAddress]);
              }
            }
          }).catch(error => {

          });
      });
  }

  public showDialogConfirmPopup() {
    this.translateService.get('usermessage.confirm_state_when_login', {}).subscribe(
      data => {
        const translation = data;
        this.activate(null, null, 'LoginPopup')
          .then(result => {
            if (result === true) {
              // console.log(this.user.state);
              // this.user.state = this.pickedState;

              localStorage.removeItem('loginPopup');
              localStorage.setItem('loginPopup', 'false');
              this.loginPopup = false;
            } else {
              this.loginPopup = false;
              localStorage.removeItem('loginPopup');
              localStorage.setItem('loginPopup', 'false');
            }
          }).catch(error => {
          });
      });
  }

  private changeLoginPickedState(typeState: string) {

  }

  private setLabels(message: string, title: string) {
    this.title = title;
    this.message = message;
  }

  activate(message: string, title: string, typeDialog: string) {
    const promise = new Promise<boolean>(resolve => {
      this.isOn = true;
      this.isShow = true;
      this.show(resolve);
    });
    return promise;
  }

  private show(resolve: (boolean) => any) {
    document.onkeyup = null;
    const negativeOnClick = (e: any) => resolve(false);
    const positiveOnClick = (e: any) => resolve(true);

    if (!this._confirmElement || !this._cancelButton || !this._okButton || !this._closeButton || !this._offButton) {
      return;
    }
    this._confirmElement.style.opacity = 0;
    this._confirmElement.style.zIndex = 9999;
    this._confirmElement.style.height = '100%';

    this._cancelButton.onclick = ((e: any) => {
      e.preventDefault();
      if (!negativeOnClick(e)) {
        this.hideDialog();
      }
    });

    this._okButton.onclick = ((e: any) => {
      e.preventDefault();
      if (!positiveOnClick(e)) {
        this.hideDialog();
      }
    });

    this._closeButton.onclick = ((e: any) => {
      e.preventDefault();
      if (!negativeOnClick(e)) {
        this.hideDialog();
      }
    });

    this._offButton.onclick = ((e: any) => {
      e.preventDefault();
      if (!negativeOnClick(e)) {
        this.hideDialog();
      }
    });

    document.onkeyup = (e: any) => {
      if (e.which === KEY_ESC) {
        this.hideDialog();
        return negativeOnClick(null);
      }
    };


    this._confirmElement.style.opacity = 1;
    this._confirmElement.style.display = 'block';
  }

  close() {
    this.hideDialog();
  }

  private hideDialog() {
    document.onkeyup = null;
    this._confirmElement.style.opacity = 0;
    this._confirmElement.style.height = '0px';
    window.setTimeout(() => this._confirmElement.style.zIndex = -1111, 400);
    this.isShow = false;
    this.isOn = false;
  }

  ngOnInit() {
    if (localStorage.getItem('selectedMenu')) {
      this.focusSelected = localStorage.getItem('selectedMenu');
    }else {
      // this.focusSelected = 'home';
    }

    if (localStorage.getItem('role') && localStorage.getItem('role') === 'GARA' ) {
      this.typeLogin = 'GARA';
    }else {
      this.typeLogin = 'BAOVIET';
    }
  }

  // public submitLoginState( state: string) {
  //
  // }
  public routtingPage(path, nameSelected) {
    this.focusSelected = nameSelected;
    localStorage.removeItem('selectedMenu');
    localStorage.setItem('selectedMenu', nameSelected);
    this.router.navigate([path]);
  }
  dropdownMenu() {
    if (document.body.classList.contains('is-collapsed')) {
      document.body.classList.remove('is-collapsed')
    } else {
      document.body.classList.add('is-collapsed')
    }
  }
}

import { Utilities } from '../../../shared/services/utilities.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { LoaderService } from '../../../shared/services/loader.service';
import { CategoriesService } from '../../../shared/services/http/categories.service';
import { Categories } from '../../../shared/models/Categories';
import { PagerData } from '../../../shared/entity/pagerData';
import { AttributesService } from '../../../shared/services/http/attributes.service';
import { Attributes } from '../../../shared/models/Attributes';
import { ModalDirective } from 'ngx-bootstrap';
import { AttributeValues } from '../../../shared/models/AttributeValues';
import { fromJS } from 'immutable';
import { inventoryArea } from '../../../shared/models/inventoryArea';
import { InventoryAreaService } from '../../../shared/services/http/inventoryArea.service';
import { Product } from '../../../shared/models/Product';
import { ProductService } from '../../../shared/services/http/product.service';
import { Inventory } from '../../../shared/models/Inventory';
import { InventoryService } from '../../../shared/services/http/inventory.service';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'add-inventory-area',
  templateUrl: './add-inventory-area.component.html',
  styleUrls: ['./add-inventory-area.component.scss'],
})
export class AddInventoryAreaComponent implements OnInit {
  listComboInventoryArea: inventoryArea[] = [];
  inventoryAreaId: number = 0;

  listProduct: Product[] = [];
  checkAll: boolean = false;
  
  selectInventory: inventoryArea = new inventoryArea();
  blName: boolean = false;
  blNameEn: boolean = false;
  txtSearch: string = ''
  totalPage: number = 0;

  listInventorySubmit: Inventory[] = [];

  @ViewChild('addAttribute') public addAttribute: ModalDirective;
  @ViewChild('confirmDialodDel') public confirmDialodDel: ModalDirective;
  idDelete: Number = 0;
  title: string = '';
  constructor(private utilities: Utilities, private router: Router,
    private inventoryService: InventoryService, private productService: ProductService,
    private loaderService: LoaderService, private ultilities: Utilities,
    private inventoryAreaService: InventoryAreaService) {
    this.ultilities.closeAutoMenu();
  }

  ngOnInit() {
    this.loadInventoryArea();

  }
  changeCheckAll() {
    if (this.listProduct) {
      for (let i = 0; i < this.listProduct.length; i++) {
        if (this.checkAll) {
          this.listProduct[i].blActived = true;
        } else {
          this.listProduct[i].blActived = false;
        }
      }
    }
  }
  checkSelectedAll() {
    if (this.listProduct) {
      let check = true;
      for (let i = 0; i < this.listProduct.length; i++) {
        if (!this.listProduct[i].blActived) {
          check = false
        }
      }
      if (!check) {
        this.checkAll = false
      } else {
        this.checkAll = true
      }
    } else {
      this.checkAll = false;
    }
  }
  backListInventory() {
    this.router.navigate(['/tranzi-admin/inventory-management']);
  }
  loadInventoryArea() {
    // this.listProduct = []
    this.inventoryAreaService.getAll().subscribe((response: any) => {
      this.listComboInventoryArea = [];
      if (response && response.resultCode === 200) {
        this.listComboInventoryArea = response.object;
        if (this.listComboInventoryArea && this.listComboInventoryArea.length > 0) {
          this.inventoryAreaId = this.listComboInventoryArea[0].id
          this.loadProductByArea();
        }

      }
      this.loaderService.hide()
    }, error => {
      this.listComboInventoryArea = [];
      this.loaderService.hide();
    });
  }
  loadProductByArea() {
    this.loaderService.show()
    this.checkSelectedAll();
    this.productService.getAllsPaddingArea(1, 10, 'id', 'desc', this.txtSearch, this.inventoryAreaId).subscribe((response: any) => {
      this.listProduct = [];
      if (response && response.resultCode === 200) {
        let page = new PagerData();
        page = response.object;
        if (page && page.content) {
          this.listProduct = page.content;
          this.totalPage = page.totalRow;
        }
        this.checkSelectedAll();
      }
      this.loaderService.hide()
    }, error => {
      this.listProduct = [];
      this.loaderService.hide();
    });
  }



  submitOk() {
    this.listInventorySubmit = [];
    if (this.listProduct) {
      let check = false;
      
      for (let i = 0; i < this.listProduct.length; i++) {
        if (this.listProduct[i].blActived) {
          let inve = new Inventory();
          check = true;
          inve.id = 0;
          inve.productId = this.listProduct[i].id
          inve.stock = 0
          inve.currentStock = 0;
          inve.inventoryAreaId = this.inventoryAreaId ? this.inventoryAreaId : 0
          let dt = new Date();
          inve.createdAt = dt.getTime() / 1000
          inve.updatedAt = dt.getTime() / 1000
          this.listInventorySubmit.push(inve);
        }
      }
      if (check) {
        this.inventoryService.createListInventory(this.listInventorySubmit).subscribe((response: any) => {
          if (response && response.resultCode === 200) {
            this.loadProductByArea();
            this.ultilities.showSuccess('Thành công');
          } else if (response && response.resultCode === 201) {
            const mes = response.errorMessage;
            this.ultilities.showError(mes ? mes : 'Có lỗi xảy ra. Liên hệ Admin để kiểm tra lại');
          }
          this.loaderService.hide()
          this.addAttribute.hide();
        }, error => {
          this.loaderService.hide();
          this.ultilities.showError('Có lỗi xảy ra. Liên hệ Admin để kiểm tra lại');
        });

      } else {
        this.utilities.showError('Bắt buộc chọn sản phẩm')
      }
    } else {
      this.utilities.showError('Bắt buộc chọn sản phẩm')
    }
    if (!this.selectInventory || !this.selectInventory.name) {
      return;
    }
  }
  submitnotOk() {
    this.addAttribute.hide();
  }
  paginate(event) {
    if (event) {
      // tslint:disable-next-line:max-line-length
      this.productService.getAllsPaddingArea(event.page ? (event.page + 1) : 1, event.rows ? event.rows : 10, 'id', 'desc', this.txtSearch, this.inventoryAreaId).subscribe((response: any) => {
        // tslint:disable-next-line:no-console
        // console.log(response)
        this.listProduct = [];
        this.totalPage = 0;
        if (response && response.resultCode === 200) {
          let page = new PagerData();
          page = response.object;
          if (page && page.content) {
            this.listProduct = page.content;
            this.totalPage = page.totalRow;
          }
          // this.listProduct = response;
        }
        this.loaderService.hide()
      }, error => {
        this.listProduct = [];
        this.totalPage = 0;
        this.loaderService.hide();
      });
    }
  }

}


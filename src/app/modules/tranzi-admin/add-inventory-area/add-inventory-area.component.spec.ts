import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddInventoryAreaComponent } from './add-inventory-area.component';

describe('AddInventoryAreaComponent', () => {
  let component: AddInventoryAreaComponent;
  let fixture: ComponentFixture<AddInventoryAreaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddInventoryAreaComponent ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddInventoryAreaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

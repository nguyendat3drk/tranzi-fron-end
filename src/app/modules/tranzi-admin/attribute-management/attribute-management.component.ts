import { Utilities } from '../../../shared/services/utilities.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ProductService } from '../../../shared/services/http/product.service';
import { MessageService } from 'primeng/components/common/messageservice';
import { ActivatedRoute, Router } from '@angular/router';
import { LoaderService } from '../../../shared/services/loader.service';
import { CategoriesService } from '../../../shared/services/http/categories.service';
import { Categories } from '../../../shared/models/Categories';
import { PagerData } from '../../../shared/entity/pagerData';
import { isFulfilled } from 'q';
import { ThemeSettingsComponent } from '../../../@theme/components';
import { AttributesService } from '../../../shared/services/http/attributes.service';
import { Attributes } from '../../../shared/models/Attributes';
import { ModalDirective } from 'ngx-bootstrap';
import { Attribute } from '@angular/compiler';
import { AttributeValues } from '../../../shared/models/AttributeValues';
import { fromJS } from 'immutable';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'attribute',
  templateUrl: './attribute-management.component.html',
  styleUrls: ['./attribute-management.component.scss'],
})
export class AttributeComponent implements OnInit {
  listAttributes: Attributes[] = [];
  totalPage: number = 0;
  totalPageValuse: number = 0;
  selectAttributes: Attributes = new Attributes();
  blName: boolean = false;
  blNameEn: boolean = false;
  txtSearch: string  = ''

  blNameValues: boolean = false;
  selectAttributeValues: AttributeValues = new AttributeValues();
  listAttributeValues: AttributeValues[] = [];
  listItemCategory: Categories[] = []

  @ViewChild('addAttribute') public addAttribute: ModalDirective;
  @ViewChild('addAttributeValues') public addAttributeValues: ModalDirective;
  @ViewChild('confirmDialodDel') public confirmDialodDel: ModalDirective;
  idDelete: Number = 0;
  title: string = '';
  constructor(private utilities: Utilities, private router: Router,
    private activeRouter: ActivatedRoute, private categoriesService: CategoriesService,
    private loaderService: LoaderService, private ultilities: Utilities,
    private attributesService: AttributesService) {
      this.ultilities.closeAutoMenu();
  }

  ngOnInit() {
    this.loadAttributes();
    this.loadcategory();
  }
  submitOk() {
    this.blName = true;
    this.blNameEn =  true;
    if (!this.selectAttributes || !this.selectAttributes.name || !this.selectAttributes.nameEn) {
      return;
    }
    this.attributesService.create(this.selectAttributes).subscribe((response: any) => {
      if (response && response.resultCode === 200) {
        this.loadAttributes();
        this.ultilities.showSuccess('Thành công');
      } else if (response && response.resultCode === 204) {
        const mes = response.errorMessage;
        this.ultilities.showError(mes ? mes : 'Có lỗi xảy ra. Liên hệ Admin để kiểm tra lại');
      }
      this.loaderService.hide()
      this.addAttribute.hide();
    }, error => {
      this.loaderService.hide();
      this.ultilities.showError('Có lỗi xảy ra. Liên hệ Admin để kiểm tra lại');
    });

  }
  submitnotOk() {
    this.addAttribute.hide();
  }
  selectRowAttribute(event) {
    if (event !== undefined && event !== null) {
      if (this.listAttributes && this.listAttributes.length > 0) {
        for (let i = 0; i < this.listAttributes.length; i++) {
          if (Number(event) === i) {
            this.listAttributes[i].blSelectedRow = true;
            this.selectAttributes = this.listAttributes[i];
            this.loadAttributeValuesById(this.selectAttributes.id);
          } else {
            this.listAttributes[i].blSelectedRow = false;
          }
        }
      }
    }
  }
  loadAttributeValuesById(id) {
    this.listAttributeValues = []
    this.attributesService.getAllsPaddingValuesByAttributeId(1, 10, 'id', 'desc', '', id).subscribe((response: any) => {
      if (response && response.resultCode === 200) {
        let page = new PagerData();
        page = response.object;
        if (page && page.content) {
          this.listAttributeValues = page.content;
          this.totalPageValuse = page.totalRow;
        }
      }
      this.loaderService.hide()
    }, error => {
      this.loaderService.hide();
    });
  }
  loadcategory() {
    this.listItemCategory = []
    this.categoriesService.getAllRespont().subscribe((response: any) => {
      if (response && response.resultCode === 200) {
        this.listItemCategory  = response.object;
      }
      this.loaderService.hide()
    }, error => {
      this.listItemCategory = []
      this.loaderService.hide();
    });
  }
  loadAttributes() {
    // this.listAttributes = []
    this.attributesService.getAllsPadding(1, 10, 'id', 'desc', this.txtSearch).subscribe((response: any) => {
      this.listAttributes = [];
      if (response && response.resultCode === 200) {
        let page = new PagerData();
        page = response.object;
        if (page && page.content) {
          this.listAttributes = page.content;
          this.totalPage = page.totalRow;
        }
      }
      this.loaderService.hide()
    }, error => {
      this.listAttributes = [];
      this.loaderService.hide();
    });
  }
  createAttributes(event) {
    if (!event) {
      this.blName = false;
      this.blNameEn = false;
      this.selectAttributes = new Attributes();
    } else {
      this.blName = false;
      this.blNameEn = false;
      const guaranteeNew = fromJS(event)
      event = guaranteeNew.toJS();
      this.selectAttributes = event;
      this.selectAttributes.selectListCategory = []
      this.attributesService.getListCategoryByAttribute(event.id).subscribe((response: any) => {
        if (response) {
          this.selectAttributes.selectListCategory = response;
        }
        this.loaderService.hide()
      }, error => {
        this.totalPage = 0
        this.loaderService.hide();
      });

    }
    this.addAttribute.show()
  }
  createAttributeValues(event) {
    this.blNameValues = false;
    if (!event) {
      this.selectAttributeValues = new AttributeValues();
      if (this.selectAttributeValues) {
        this.selectAttributeValues.id = 0;
        this.selectAttributeValues.attributeId = this.selectAttributes.id;
      }
    } else {
      const guaranteeNew = fromJS(event)
      event = guaranteeNew.toJS();
      this.selectAttributeValues = event;
    }
    this.addAttributeValues.show()
  }

  paginate(event) {
    if (event) {
      // tslint:disable-next-line:max-line-length
      this.attributesService.getAllsPadding(event.page ? (event.page + 1) : 1, event.rows ? event.rows : 10, 'id', 'desc', this.txtSearch).subscribe((response: any) => {
        this.listAttributes = [];
        if (response && response.resultCode === 200) {
          let page = new PagerData();
          page = response.object;
          if (page && page.content) {
            this.listAttributes = page.content;
            this.totalPage = page.totalRow;
          }
        }
        this.loaderService.hide()
      }, error => {
        this.listAttributes = [];
        this.totalPage = 0
        this.loaderService.hide();
      });
    }
  }
  paginateValues(event) {
    if (event && this.selectAttributes) {
      this.listAttributeValues = [];
      // tslint:disable-next-line:max-line-length
      this.attributesService.getAllsPaddingValuesByAttributeId(event.page ? (event.page + 1) : 1, event.rows ? event.rows : 10, 'id', 'desc', '', this.selectAttributes.id).subscribe((response: any) => {
        if (response && response.resultCode === 200) {
          let page = new PagerData();
          page = response.object;
          if (page && page.content) {
            this.listAttributeValues = page.content;
            this.totalPageValuse = page.totalRow;
          }
        }
        this.loaderService.hide()
      }, error => {
        this.totalPageValuse = 0
        this.loaderService.hide();
      });
    }

  }
  seSelectCategories(event) {
    this.selectAttributes = new Attributes()
    this.selectAttributes.id = 0;
    if (event && event.data) {
      this.selectAttributes = event.data;
    }
  }
  deleteAttribues() {
    if (this.idDelete) {
      this.loaderService.show();
      this.confirmDialodDel.hide();
      this.attributesService.deleteAttributeId(this.idDelete).subscribe((response: any) => {
        if (response && response.resultCode === 200) {
          this.loadAttributes();
          this.ultilities.showSuccess('Xóa thành công')
        } else if (response && response.resultCode === 205) {
          this.ultilities.showError('Thuộc tính đã có bản ghi chi tiết. Không thể xóa.')
        } else if (response && response.resultCode === 204) {
          this.ultilities.showError(response.errorMessage ? response.errorMessage : 'Có lỗi xảy ra. Liên hệ Admin để kiểm tra lại')
        }
        this.loaderService.hide()

      }, error => {
        this.loaderService.hide();
        this.ultilities.showError('Delete Error Attributes')
      });
    }
  }
  deleteAttributeValues(id) {
    if (id) {
      this.loaderService.show();
      this.attributesService.deleteAttributeValuesId(id).subscribe((response: any) => {
        if (response && response.resultCode === 200) {
          if (this.selectAttributes) {
            this.loadAttributeValuesById(this.selectAttributes.id);
          }
          this.ultilities.showSuccess('Xóa thành công');
        } else if (response && response.resultCode === 205) {
          this.ultilities.showError('Chi tiế thuộc tính đã liên kết sản phẩm. Không thể xóa.')
        } else if (response && response.resultCode === 204) {
          this.ultilities.showError(response.errorMessage ? response.errorMessage : 'Có lỗi xảy ra. Liên hệ Admin để kiểm tra lại')
        }
        this.loaderService.hide()
      }, error => {
        this.loaderService.hide();
        this.ultilities.showError('Delete Error Attributes')
      });
    }
  }
  submitOkValues() {
    this.blNameValues = true;
    if (!this.selectAttributeValues || !this.selectAttributeValues.value) {
      return;
    }
    this.attributesService.createValues(this.selectAttributeValues).subscribe((response: any) => {
      if (response && response.resultCode === 200) {
        if (this.selectAttributes) {
          this.loadAttributeValuesById(this.selectAttributes.id);
        }
        this.ultilities.showSuccess('Thêm mới thành công');
      } else if (response && response.resultCode === 204) {
        const mes = response.errorMessage;
        this.ultilities.showError(mes ? mes : 'Có lỗi xảy ra. Liên hệ Admin để kiểm tra lại');
      }
      this.loaderService.hide()
      this.addAttributeValues.hide();
    }, error => {
      this.loaderService.hide();
      this.ultilities.showError('Có lỗi xảy ra. Liên hệ Admin để kiểm tra lại');
    });

  }
  deleteCheck(pro: Attributes) {
    if (pro) {
      this.title = 'Bạn có muốn xóa Thuộc tính: ' + pro.name + ' không?';
      this.idDelete = pro.id
      this.confirmDialodDel.show();
    }
  }

}


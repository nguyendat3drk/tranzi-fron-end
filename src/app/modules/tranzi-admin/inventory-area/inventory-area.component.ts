import { Utilities } from '../../../shared/services/utilities.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { LoaderService } from '../../../shared/services/loader.service';
import { CategoriesService } from '../../../shared/services/http/categories.service';
import { Categories } from '../../../shared/models/Categories';
import { PagerData } from '../../../shared/entity/pagerData';
import { AttributesService } from '../../../shared/services/http/attributes.service';
import { Attributes } from '../../../shared/models/Attributes';
import { ModalDirective } from 'ngx-bootstrap';
import { AttributeValues } from '../../../shared/models/AttributeValues';
import { fromJS } from 'immutable';
import { inventoryArea } from '../../../shared/models/inventoryArea';
import { InventoryAreaService } from '../../../shared/services/http/inventoryArea.service';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'inventory-area',
  templateUrl: './inventory-area.component.html',
  styleUrls: ['./inventory-area.component.scss'],
})
export class InventoryAreaComponent implements OnInit {
  listInventoryArea: inventoryArea[] = [];
  totalPage: number = 0;
  selectInventory: inventoryArea = new inventoryArea();
  blName: boolean = false;
  blNameEn: boolean = false;
  txtSearch: string  = ''

  blNameValues: boolean = false;
  selectAttributeValues: AttributeValues = new AttributeValues();
  listAttributeValues: AttributeValues[] = [];
  listItemCategory: Categories[] = []

  @ViewChild('addAttribute') public addAttribute: ModalDirective;
  @ViewChild('confirmDialodDel') public confirmDialodDel: ModalDirective;
  idDelete: Number = 0;
  title: string = '';
  constructor(private utilities: Utilities, private router: Router,
    private activeRouter: ActivatedRoute, private categoriesService: CategoriesService,
    private loaderService: LoaderService, private ultilities: Utilities,
    private inventoryAreaService: InventoryAreaService) {
      this.ultilities.closeAutoMenu();
  }

  ngOnInit() {
    this.loadInventoryArea();
  }
  loadInventoryArea() {
    // this.listInventoryArea = []
    this.inventoryAreaService.getAllsPadding(1, 10, 'id', 'desc', this.txtSearch).subscribe((response: any) => {
      this.listInventoryArea = [];
      if (response && response.resultCode === 200) {
        let page = new PagerData();
        page = response.object;
        if (page && page.content) {
          this.listInventoryArea = page.content;
          this.totalPage = page.totalRow;
        }
      }
      this.loaderService.hide()
    }, error => {
      this.listInventoryArea = [];
      this.loaderService.hide();
    });
  }
  submitOk() {
    this.blName = true;
    this.blNameEn =  true;
    if (!this.selectInventory || !this.selectInventory.name) {
      return;
    }
    this.inventoryAreaService.create(this.selectInventory).subscribe((response: any) => {
      if (response && response.resultCode === 200) {
        this.loadInventoryArea();
        this.ultilities.showSuccess('Thành công');
      } else if (response && response.resultCode === 204) {
        const mes = response.errorMessage;
        this.ultilities.showError(mes ? mes : 'Có lỗi xảy ra. Liên hệ Admin để kiểm tra lại');
      }
      this.loaderService.hide()
      this.addAttribute.hide();
    }, error => {
      this.loaderService.hide();
      this.ultilities.showError('Có lỗi xảy ra. Liên hệ Admin để kiểm tra lại');
    });

  }
  submitnotOk() {
    this.addAttribute.hide();
  }
  createAttributes(event) {
    if (!event) {
      this.blName = false;
      this.blNameEn = false;
      this.selectInventory = new inventoryArea();
    } else {
      this.blName = false;
      this.blNameEn = false;
      const guaranteeNew = fromJS(event)
      event = guaranteeNew.toJS();
      this.selectInventory = event;
    }
    this.addAttribute.show()
  }
  
  paginate(event) {
    if (event) {
      // tslint:disable-next-line:max-line-length
      this.inventoryAreaService.getAllsPadding(event.page ? (event.page + 1) : 1, event.rows ? event.rows : 10, 'id', 'desc', this.txtSearch).subscribe((response: any) => {
        this.listInventoryArea = [];
        if (response && response.resultCode === 200) {
          let page = new PagerData();
          page = response.object;
          if (page && page.content) {
            this.listInventoryArea = page.content;
            this.totalPage = page.totalRow;
          }
        }
        this.loaderService.hide()
      }, error => {
        this.listInventoryArea = [];
        this.totalPage = 0
        this.loaderService.hide();
      });
    }
  }
  
  deleteAttribues() {
    if (this.idDelete) {
      this.loaderService.show();
      this.confirmDialodDel.hide();
      this.inventoryAreaService.delete(this.idDelete).subscribe((response: any) => {
        if (response && response.resultCode === 200) {
          this.loadInventoryArea();
          this.ultilities.showSuccess('Xóa thành công')
        } else if (response && response.resultCode === 205) {
          this.ultilities.showError('Thuộc tính đã có bản ghi chi tiết. Không thể xóa.')
        } else if (response && response.resultCode === 204) {
          this.ultilities.showError(response.errorMessage ? response.errorMessage : 'Có lỗi xảy ra. Liên hệ Admin để kiểm tra lại')
        }
        this.loaderService.hide()

      }, error => {
        this.loaderService.hide();
        this.ultilities.showError('Delete Error Attributes')
      });
    }
  }
  deleteCheck(pro: Attributes) {
    if (pro) {
      this.title = 'Bạn có muốn xóa Địa chỉ kho : ' + pro.name + ' không?';
      this.idDelete = pro.id
      this.confirmDialodDel.show();
    }
  }

}


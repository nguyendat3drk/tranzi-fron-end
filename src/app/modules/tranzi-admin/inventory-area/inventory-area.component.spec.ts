import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InventoryAreaComponent } from './inventory-area.component';

describe('InventoryAreaComponent', () => {
  let component: InventoryAreaComponent;
  let fixture: ComponentFixture<InventoryAreaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InventoryAreaComponent ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InventoryAreaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

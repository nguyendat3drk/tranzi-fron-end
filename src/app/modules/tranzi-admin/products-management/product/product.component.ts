import { Utilities } from '../../../../shared/services/utilities.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ProductService } from '../../../../shared/services/http/product.service';
import { MessageService } from 'primeng/components/common/messageservice';
import { ActivatedRoute, Router } from '@angular/router';
import { LoaderService } from '../../../../shared/services/loader.service';
import { CategoriesService } from '../../../../shared/services/http/categories.service';
import { Product } from '../../../../shared/models/Product';
import { Categories } from '../../../../shared/models/Categories';
import { PagerData } from '../../../../shared/entity/pagerData';
import { moment } from 'ngx-bootstrap/chronos/test/chain';
import { ModalDirective } from 'ngx-bootstrap';
import { ProductsPrices } from '../../../../shared/models/ProductsPrices';
import { ProductPriceService } from '../../../../shared/services/http/product-service.service';
import { ConfirmService } from '../../../../shared/confirm-dialog/ConfirmService';
import { ProductSearch } from '../../../../shared/entity/ProductSearch';
import { iif } from 'rxjs';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss'],
})
export class ProductComponent implements OnInit {
  productSearch: ProductSearch;
  listProductPrice: ProductsPrices[] = [];
  txtSearch: string = '';
  listProduct: Product[] = [];
  hiddenDeleteAll: boolean = false;
  selectProduct: Product = new Product();
  totalPage: number = 0;
  idProduct: number = 0;
  @ViewChild('productPriceDetail') public productPriceDetail: ModalDirective;
  @ViewChild('confirmDialodDel') public confirmDialodDel: ModalDirective;

  idDelete: Number = 0;
  title: string = '';
  pageRow: number = 10;
  // Search
  selectListCategory: number[] = [];
  listItemCategory: Categories[] = [];
  blSaleSearch: boolean = false;
  blSalePrice: boolean = false;
  priceStart: number;
  priceEnd: number;
  blshowTitle: boolean = false;
  blInsufficient: boolean = false;
  blCheckAllDelete: boolean = false;
  // end search
  constructor(private productService: ProductService, private router: Router, private productPriceService: ProductPriceService,
    private activeRouter: ActivatedRoute, private messageService: MessageService,
    private loaderService: LoaderService, private ultilities: Utilities, private _confirm: ConfirmService,
    private categoriesService: CategoriesService) {
    this.ultilities.closeAutoMenu();
  }

  ngOnInit() {
    // this.loadProduct();
    this.loadProductMore();
    setTimeout(() => {
      if (localStorage.getItem('addProduct')) {
        this.ultilities.showSuccess(localStorage.getItem('addProduct'));
        localStorage.removeItem('addProduct')
      } else if (localStorage.getItem('updateProduct')) {
        this.ultilities.showSuccess(localStorage.getItem('updateProduct'));
        localStorage.removeItem('updateProduct')
      }
    }, 300);
    this.getAllCategori();
  }
  getAllCategori() {
    this.listItemCategory = [];
    this.categoriesService.getAllRespont().subscribe((response: any) => {
      if (response && response.resultCode === 200) {
        this.listItemCategory = response.object;
      }
    }, error => {
    });
  }
  loadProduct() {
    this.setSearchProduct();
    this.loaderService.show();
    this.productService.getAllsPadding(1, this.pageRow>10?this.pageRow:10, 'id', 'desc', this.txtSearch).subscribe((response: any) => {
      // tslint:disable-next-line:no-console
      // console.log(response)
      this.listProduct = [];
      this.totalPage = 0;
      if (response && response.resultCode === 200) {
        let page = new PagerData();
        page = response.object;
        if (page && page.content) {
          this.listProduct = page.content;
          this.totalPage = page.totalRow;
        }
        // this.listProduct = response;
      }
      this.checkShowBtnDeleteAll()
      this.loaderService.hide()
    }, error => {
      this.checkShowBtnDeleteAll()
      this.listProduct = [];
      this.totalPage = 0;
      this.loaderService.hide();
    });
  }
  viewDate(event) {
    if (event && Number(event) > 0) {
      return this.ultilities.convertDateToStringDateVN(new Date(Number(event) * 1000));
    }
    return ''
  }
  viewPrice(idProductSet, dto) {
    this.selectProduct = dto;
    this.productPriceDetail.show()
    this.listProductPrice = [];
    this.idProduct = idProductSet;
    if (idProductSet && Number(idProductSet) > 0) {
      this.loaderService.show()
      this.productPriceService.getProductPriceByProductID(idProductSet).subscribe((response: any) => {
        if (response) {
          this.listProductPrice = response
        }
        this.loaderService.hide()
      }, error => {
        this.loaderService.hide();
      });
    }

  }
  deleteAll() {
    if (this.listProduct) {
      let listId = [];
      for (let i = 0; i < this.listProduct.length; i++) {
        if (this.listProduct[i].blCheckDel) {
          listId.push(this.listProduct[i].id)
        }
      }
    }
  }
  paginate(event) {
    if (event) {
      this.pageRow =  event.rows ? event.rows : 10;
      // tslint:disable-next-line:max-line-length
      this.productService.getAllsPadding(event.page ? (event.page + 1) : 1, event.rows ? event.rows : 10, 'id', 'desc', '').subscribe((response: any) => {
        // tslint:disable-next-line:no-console
        // console.log(response)
        this.listProduct = [];
        this.totalPage = 0;
        if (response && response.resultCode === 200) {
          let page = new PagerData();
          page = response.object;
          if (page && page.content) {
            this.listProduct = page.content;
            this.totalPage = page.totalRow;
          }
          // this.listProduct = response;
        }
        this.checkShowBtnDeleteAll()
        this.loaderService.hide()
      }, error => {
        this.listProduct = [];
        this.checkShowBtnDeleteAll()
        this.totalPage = 0;
        this.loaderService.hide();
      });
    }
  }
  createProduct() {
    this.router.navigate(['/tranzi-admin/product-create']);
  }

  seSelectCategories(event) {
    this.selectProduct = new Product()
    if (event && event.data) {
      this.selectProduct = event.data;
    }
  }
  routingUpdate(id) {
    if (this.selectProduct) {
      this.router.navigate(['/tranzi-admin/product-update/' + id]);
    }
  }
  newArticles(item: Product){
    localStorage.removeItem("nameProduct")
    localStorage.setItem("nameProduct",item.id+'')
    localStorage.removeItem("IDProduct")
    localStorage.setItem("IDProduct",item.id+'')
    this.productService.checkArticleProeduct(item.id).subscribe((response: any) => {
      // tslint:disable-next-line:no-console
      console.log(response)
      this.loaderService.hide()
      if (response && response.resultCode === 200) {
        if(Number(response.object)>0){
          this.router.navigate(['/tranzi-admin/article-update/' + Number(response.object)]);
        }else{
          this.router.navigate(['/tranzi-admin/article-create']);
        }
      }else{
        this.router.navigate(['/tranzi-admin/article-create']);
      }
      
    }, error => {
      this.loaderService.hide();
    });
    
  }

  checkShowBtnDeleteAll(){
    if(this.listProduct){
      let check = false;
      for(let i =0;i<this.listProduct.length;i++){
        if(this.listProduct[i].blCheckDel && (this.listProduct[i].blCheckDel[0]=='true' || this.listProduct[i].blCheckDel=='true')){
          check =true;
        }
      }
      if(check){
        this.hiddenDeleteAll =  true;  
      }else{
        this.hiddenDeleteAll =  false;  
      }
    }else{
      this.hiddenDeleteAll =  false;
    }
  }
  deleteProduct() {
    this.confirmDialodDel.hide();
    if (this.idDelete && this.idDelete > 0) {
      this.confirmDialodDel.hide();
      this.loaderService.show();
      this.productService.deleteProductId(this.idDelete).subscribe((response: any) => {
        if (response && response.resultCode === 200) {
          this.loadProduct();
          this.ultilities.showSuccess('Xóa sản phẩm thành công')
        }else if(response && response.resultCode === 201) {
          let mes = null;
          if (response.data && response.data.data) {
                mes = response.data.name + ':' + response.data.data;
          }
          this.ultilities.showError(mes? mes:'Xóa không thành công')
        }
        this.loaderService.hide()
        this.messageService.clear();
        
      }, error => {
        this.loaderService.hide();
        this.messageService.clear();
        this.ultilities.showError('Xóa thất bại. Liên hệ Admin để kiểm tra lại')
      });
    } else {
      this.ultilities.showError('Chọn sản phẩm cần xóa')
      this.confirmDialodDel.hide();
      this.confirmDialodDel.hide();
    }

  }
  imgError(image) {
    image.onerror = '';
    image.src = '/assets/images/error-image.png';
    return true;
  }
  deleteRowPrice(index) {
    if (this.listProductPrice && this.listProductPrice[index]) {
      this.listProductPrice.splice(index, 1);
    }
  }
  addRowPrice() {
    // tslint:disable-next-line:prefer-const
    let dto = new ProductsPrices();
    dto.id = 0;
    dto.minQuantity = null
    dto.price = null
    dto.salePrice = null
    if (!this.listProductPrice) {
      this.listProductPrice = [];
      this.listProductPrice.push(dto)
    } else {
      this.listProductPrice.push(dto)
    }
  }
  keyPress(event: any, type) {
    if (type && type === 'number') {
      const patternExtension = /[0-9]/;
      const inputChar = String.fromCharCode(event.charCode);
      if (event.keyCode !== 9 && !patternExtension.test(inputChar)) {
        event.preventDefault();
      }
    } else if (type && type === 'date') {
      const patternExtension = /[0-9\/]/;
      const inputChar = String.fromCharCode(event.charCode);
      if (event.keyCode !== 9 && !patternExtension.test(inputChar)) {
        event.preventDefault();
      }
    } else {
      const patternExtension = /[0-9\.]/;
      const inputChar = String.fromCharCode(event.charCode);
      if (event.keyCode !== 9 && !patternExtension.test(inputChar)) {
        event.preventDefault();
      }
    }
  }
  submitSavePrice() {
    let check = true;
    check = this.validateForm('save', 0);
    if (!check) {
      return;
    }
    if (this.listProductPrice && this.listProductPrice.length > 0) {
      // lien ket  price product
      this.loaderService.show();
      this.productPriceService.addListProductPrice(this.listProductPrice).subscribe((response: any) => {
        if (response && response.resultCode === 200) {
          this.ultilities.showSuccess('Cập nhật thành công')
          if (response.object) {
            this.selectProduct = response.object
          }
          this.productPriceDetail.hide();
        } else {
          this.ultilities.showError('Cập nhật thất bại. Liên hệ Admin để kiểm tra lại')
        }
        this.messageService.clear();
        this.loaderService.hide()


      }, error => {
        this.messageService.clear();
        this.loaderService.hide();
        this.ultilities.showError('Cập nhật thất bại. Liên hệ Admin để kiểm tra lại')
      });
    } else {
      // xoa het price product
      if (this.idProduct) {
        this.loaderService.show();
        this.productPriceService.deleteProductId(this.idProduct).subscribe((response: any) => {
          if (response && response.resultCode === 200) {
            this.ultilities.showSuccess('Cập nhật thành công')
            this.productPriceDetail.hide();
          } else {
            this.ultilities.showError('Cập nhật thất bại. Liên hệ Admin để kiểm tra lại')
          }
          this.messageService.clear();
          this.loaderService.hide()


        }, error => {
          this.messageService.clear();
          this.loaderService.hide();
          this.ultilities.showError('Cập nhật thất bại. Liên hệ Admin để kiểm tra lại')
        });
      }
    }
  }
  changeNumber(event, index) {
    if (event === 'blminQuantity' && this.listProductPrice && this.listProductPrice[index]) {
      if (this.listProductPrice[index].minQuantity && !isNaN(this.listProductPrice[index].minQuantity)) {
        this.listProductPrice[index].minQuantity = Math.abs(Number(this.listProductPrice[index].minQuantity))
      } else {
        this.listProductPrice[index].minQuantity = 0;
      }
    }
  }
  validateForm(event, index) {
    let check = true;
    if (event === 'save') {
      if (this.listProductPrice && this.listProductPrice.length) {
        for (let i = 0; i < this.listProductPrice.length; i++) {
          this.listProductPrice[i].productId = this.idProduct;
          this.listProductPrice[i].blminQuantity = false;
          if (!this.listProductPrice[i] || this.listProductPrice[i].minQuantity === undefined ||
            this.listProductPrice[i].minQuantity === null) {
            this.listProductPrice[i].blminQuantity = true;
            check = false;
          }
          this.listProductPrice[i].blprice = false;
          if (!this.listProductPrice[i] || this.listProductPrice[i].price === undefined || this.listProductPrice[i].price === null) {
            this.listProductPrice[i].blprice = true;
            check = false;
          }
          this.listProductPrice[i].blsalePrice = false;
          if (!this.listProductPrice[i] || this.listProductPrice[i].salePrice === undefined ||
            this.listProductPrice[i].salePrice === null) {
            this.listProductPrice[i].blsalePrice = true;
            check = false;
          }

        }
      }
    } else {
      if (this.listProductPrice && this.listProductPrice[index]) {
        if (event === 'blminQuantity') {
          this.listProductPrice[index].blminQuantity = false;
          if (!this.listProductPrice[index] || this.listProductPrice[index].minQuantity === undefined ||
            this.listProductPrice[index].minQuantity === null) {
            this.listProductPrice[index].blminQuantity = true;
            check = false;
          }
        }
        if (event === 'blprice') {
          this.listProductPrice[index].blprice = false;
          if (!this.listProductPrice[index] || this.listProductPrice[index].price === undefined ||
            this.listProductPrice[index].price === null) {
            this.listProductPrice[index].blprice = true;
            check = false;
          }
        }
        if (event === 'blsalePrice') {
          this.listProductPrice[index].blsalePrice = false;
          if (!this.listProductPrice[index] || this.listProductPrice[index].salePrice === undefined ||
            this.listProductPrice[index].salePrice === null) {
            this.listProductPrice[index].blsalePrice = true;
            check = false;
          }
        }

      }

    }

    return check;
  }
  viewPriceSale(cate: Product) {

    if (cate) {
      const star = this.viewDate(cate.saleStart);
      const en = this.viewDate(cate.saleEnd);
      if (star && en) {
        return star + ' - ' + en
      } else {
        return star ? star : en
      }

    }
    return ''
  }
  deleteTest(pro: Product) {
    if (pro) {
      this.title = 'Bạn có muốn xóa sản phẩm: ' + pro.name + ' không?';
      this.idDelete = pro.id
      this.confirmDialodDel.show();
    }
  }
  validatePriceSearch() {
    this.blSalePrice = false;
    let check = true;
    if (this.priceEnd && this.priceStart && (Number(this.priceStart) > Number(this.priceEnd))) {
      this.blSalePrice = true;
      check = false;
    }
    return check;
  }
  setSearchProduct() {
    this.productSearch = new ProductSearch();
    if (this.blSaleSearch) {
      this.productSearch.blSale = true;
    } else {
      this.productSearch.blSale = false;
    }
    if (this.blInsufficient) {
      this.productSearch.blInsufficient = true;
    } else {
      this.productSearch.blInsufficient = false;
    }

    if (this.selectListCategory && this.selectListCategory.length > 0) {
      this.productSearch.listCategoriID = this.selectListCategory;
    } else {
      this.productSearch.listCategoriID = [];
    }
    if (this.priceStart && this.priceStart > 0) {
      this.productSearch.priceStart = this.priceStart
    } else {
      this.productSearch.priceStart = 0;
    }
    if (this.priceEnd && this.priceEnd > 0) {
      this.productSearch.priceEnd = this.priceEnd
    } else {
      this.productSearch.priceEnd = 0;
    }
    if (this.txtSearch) {
      this.productSearch.search = this.txtSearch
    } else {
      this.productSearch.search = '';
    }
  }
  loadProductMore() {
    if (!this.validatePriceSearch()) {
      return '';
    }
    this.setSearchProduct();
    this.productSearch.page = 1;
    this.productSearch.pageSize = 10;
    this.productSearch.columnName = 'id'
    this.productSearch.typeOrder = 'desc'
    this.loaderService.show();
    this.productService.getAllsPaddingMove(this.productSearch).subscribe((response: any) => {
      // tslint:disable-next-line:no-console
      // console.log(response)
      this.listProduct = [];
      this.totalPage = 0;
      if (response && response.resultCode === 200) {
        let page = new PagerData();
        page = response.object;
        if (page && page.content) {
          this.listProduct = page.content;
          this.totalPage = page.totalRow;
        }
        // this.listProduct = response;
      }
      this.checkShowBtnDeleteAll()
      this.loaderService.hide()
    }, error => {
      this.listProduct = [];
      this.totalPage = 0;
      this.checkShowBtnDeleteAll()
      this.loaderService.hide();
    });
  }
  paginateMove(event) {
    if (!this.validatePriceSearch()) {
      return '';
    }
    this.setSearchProduct();
    this.productSearch.page = event.page ? (event.page + 1) : 1;
    this.productSearch.pageSize = event.rows ? event.rows : 10;
    this.productSearch.columnName = 'id'
    this.productSearch.typeOrder = 'desc'
    if (event) {
      // tslint:disable-next-line:max-line-length
      this.productService.getAllsPaddingMove(this.productSearch).subscribe((response: any) => {
        // tslint:disable-next-line:no-console
        // console.log(response)
        this.listProduct = [];
        this.totalPage = 0;
        if (response && response.resultCode === 200) {
          let page = new PagerData();
          page = response.object;
          if (page && page.content) {
            this.listProduct = page.content;
            this.totalPage = page.totalRow;
          }
          // this.listProduct = response;
        }
        this.checkShowBtnDeleteAll()
        this.loaderService.hide()
      }, error => {
        this.listProduct = [];
        this.totalPage = 0;
        this.loaderService.hide();
        this.checkShowBtnDeleteAll()
      });
    }
  }
  deleteListId() {
    let listId = [];
    if (this.listProduct) {
      for (let i = 0; i < this.listProduct.length; i++) {
        if (this.listProduct[i].blCheckDel && (this.listProduct[i].blCheckDel[0]=='true' || this.listProduct[i].blCheckDel=='true')) {
          listId.push(this.listProduct[i].id)
        }
      }
    }

    if (listId && listId.length > 0) {
      this.loaderService.show();
      this.blCheckAllDelete = false;
      this.productPriceService.deleteListProductId(listId).subscribe((response: any) => {
        if (response && response.resultCode === 200) {
          let mes = null;
          if (response.data && response.data.length > 0) {
            for (let i = 0; i < response.data.length; i++) {
              if(!mes){
                mes = response.data[i].name + ':' + response.data[i].data;
              }else{
                mes = mes + '; ' + response.data[i].name + ':' + response.data[i].data;
              }
              
            }
          }
          this.ultilities.showSuccess(mes);
          this.loadProduct();
        } else {
          this.ultilities.showError('Cập nhật thất bại. Liên hệ Admin để kiểm tra lại')
        }
        this.loaderService.hide()
      }, error => {
        this.loaderService.hide();
        this.ultilities.showError('Cập nhật thất bại. Liên hệ Admin để kiểm tra lại')
      });
    }else {
      this.ultilities.showError('Chọn sản phẩm cần xóa')
    }
  }
  checkAll(){
    if(this.blCheckAllDelete){
      if(this.listProduct){
        for(let i = 0;i<this.listProduct.length;i++){
          this.listProduct[i].blCheckDel = 'true';
        }
      }
    }else{
      if(this.listProduct){
        for(let i = 0;i<this.listProduct.length;i++){
          this.listProduct[i].blCheckDel = 'false';
        }
      }
    }
    this.checkShowBtnDeleteAll();
  }
}

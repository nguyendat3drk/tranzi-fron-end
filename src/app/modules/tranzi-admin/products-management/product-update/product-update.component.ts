import { Utilities } from '../../../../shared/services/utilities.service';
import { Component, OnInit, Output, Input, EventEmitter, ViewChild } from '@angular/core';
import { ProductService } from '../../../../shared/services/http/product.service';
import { MessageService } from 'primeng/components/common/messageservice';
import { ActivatedRoute, Router } from '@angular/router';
import { LoaderService } from '../../../../shared/services/loader.service';
import { CategoriesService } from '../../../../shared/services/http/categories.service';
import { TranslateService } from '@ngx-translate/core';
import { Product } from '../../../../shared/models/Product';
import { Manufactures } from '../../../../shared/models/Manufactures';
import { PackageTypes } from '../../../../shared/models/PackageTypes';
import { moment } from 'ngx-bootstrap/chronos/test/chain';
import { Categories } from '../../../../shared/models/Categories';
import { Media } from '../../../../shared/models/Media';
import { Countries } from '../../../../shared/models/countries';
import { CountriesService } from '../../../../shared/entity/CountriesService';
import { AttributesService } from '../../../../shared/services/http/attributes.service';
import { Attributes } from '../../../../shared/models/Attributes';
import { PagerData } from '../../../../shared/entity/pagerData';
import { AttributeValues } from '../../../../shared/models/AttributeValues';
import { Tags } from '../../../../shared/models/Tags';
import { TagsService } from '../../../../shared/services/http/tags.service';
import { ListAttributeAndValues } from '../../../../shared/models/ListAttributeAndValues';
import { ModalDirective } from 'ngx-bootstrap';
import { ArticlesManagmentService } from '../../../../shared/services/http/videos-managment.service';
import { ArticleLinkUpload } from '../../../../shared/models/ArticleLinkUpload';
import { SelectLinkFile } from '../../../../shared/models/SelectLinkFile';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'product-update',
  templateUrl: './product-update.component.html',
  styleUrls: ['./product-update.component.scss'],
})
export class ProductUpdateComponent implements OnInit {
  selectListCategory: number[] = [];
  listItemCategory: Categories[] = [];
  tags: Tags[] = [];
  @Input() removeLastOnBackspace: boolean = false;
  @Output() onTagsChanged = new EventEmitter();
  typeFile: string = 'IMAGE';
  productId: number;
  selectProduct: Product = new Product();
  listManufactures: Manufactures[] = []
  listCategory: Categories[] = []
  listMedia: Media[] = [];
  linkMediafile: string = '';
  listCountries: Countries[] = [];
  listAttributes: Attributes[] = [];
  listAttributesShow: Attributes[] = [];
  atrributeId: number;
  listAttributeValues: AttributeValues[] = []
  checkAll: boolean = false;
  // tree
  // upload
  urlupload: string = '';
  urluploadMedia: string = '';
  uploadedFilesSuccess: any[] = []
  uploadedFilesError: any[] = []
  uploadedFilesErrorInvalid: any[] = []
  progressBarPercent = 20
  totalFile: number = 0;
  serverPath: string = '';
  urlProductFile: string = '';
  listPackage: PackageTypes[] = []

  // end upload
  name = 'ng2-ckeditor';
  ckeConfig: any;
  mycontent: string;
  log: string = '';

  // date
  startDate: Date = null
  endDate: Date = null

  // Validate
  blName: boolean = false;
  blNormalPrice: boolean = false;
  blSku: boolean = false;
  blOrginSku: boolean = false;
  blInStock: boolean = false
  blMinorder: boolean = false
  blThumbnailImage: boolean = false
  blCategory: boolean = false;
  blDate: boolean = false
  blSalePrice: boolean = false;
  // validate
  blMediaFile: boolean = false;
  blTag: boolean = false;
  blSeo: boolean = false;
  blAttributes: boolean = false;
  // Thuoc tinh
  listAttributeAndValues: ListAttributeAndValues[] = [];

  // anh thumbnail
  listMediaThumbnail: Media[] = [];
  blSale: boolean = false;
  // upload file RIP
  urluploadRIP: string = ''
  listMediaRIP: string[] = [];
  @ViewChild('viewImage') public viewImage: ModalDirective;
  strImageUrl: string = '';
  listUploadFileByID: ArticleLinkUpload[] = [];
  listUploadFileBySelected: ArticleLinkUpload[] = [];
  @ViewChild('addlinkUpload') public addlinkUpload: ModalDirective;
  selectLinkFile: SelectLinkFile = {};
  linkSeleced: string = '';
  constructor(private router: Router, private translateService: TranslateService, private countriesService: CountriesService,
    private activeRouter: ActivatedRoute, private messageService: MessageService,
    private loaderService: LoaderService, private ultilities: Utilities, private productService: ProductService,
    private categoriesService: CategoriesService, private attributesService: AttributesService,
    private articleService: ArticlesManagmentService, private tagsService: TagsService) {
    this.listCountries = [];
    this.listCountries = this.countriesService.getCountries();
    this.listMedia = [];
    this.getListAttributes();
    this.urluploadRIP = this.ultilities.getUrlServerHost() + '/share-api/uploadRIP'
    this.urlupload = this.ultilities.getUrlServerHost() + '/share-api/uploadProduct'
    this.urluploadMedia = this.ultilities.getUrlServerHost() + '/share-api/uploadProductMedia'
    this.serverPath = this.ultilities.urlServerFrontEnd() + '/assets/images/product/media/'
    this.urlProductFile = this.ultilities.urlServerFrontEnd() + 'assets/images/product/file/'
    this.getShowlocalStore();
    this.activeRouter.params.subscribe((params) => {
      this.productId = +params.id
      if (this.productId) {
        this.getProductId();
        this.getMediaProductById();
        this.getListAttributeValueID();
        this.getListTagsByProductID();
        this.getMediaProductFileById();
        this.getListFileArticleUpload(this.productId);
      }
    });
  }
  getListTagsByProductID() {
    this.tags = []
    this.tagsService.getTagsByProductId(this.productId).subscribe((response: any) => {
      if (response && response.resultCode === 200) {
        this.tags = response.object;
      }
    }, error => {
    });
  }
  getListAttributeValueID() {
    this.listAttributeAndValues = []
    this.attributesService.getListAttributeValueID(this.productId).subscribe((response: any) => {
      if (response && response.resultCode === 200) {
        this.listAttributeAndValues = response.object;
        this.sortValue();
        // if (this.listAttributeValueID && this.listAttributeValueID.length > 0) {
        //   this.atrributeId = this.listAttributeValueID[0].attributeId;
        // }
      }
    }, error => {
    });
  }
  getListAttributes() {
    this.listAttributes = []
    this.attributesService.getAll().subscribe((response: any) => {
      if (response && response.resultCode === 200) {
        this.listAttributes = response.object;
        this.getListAttributesShow();
      }
    }, error => {
    });
  }
  getMediaProductFileById() {
    this.listMediaThumbnail = []
    this.categoriesService.getMediaProductMediaById(this.productId).subscribe((response: any) => {
      if (response && response.resultCode === 200) {
        this.listMediaThumbnail = response.object;
        setTimeout(() => {
          if (this.listMediaThumbnail && this.selectProduct && this.selectProduct.thumbnailImage) {
            this.listMediaThumbnail.forEach(element => {
              if (element.urlPath === this.selectProduct.thumbnailImage) {
                element.selectImage = true;
              } else {
                element.selectImage = false;
              }
            });
          }
        }, 1000);
      }
    }, error => {
    });
  }
  getMediaProductById() {
    this.listMedia = []
    this.categoriesService.getMediaProductById(this.productId).subscribe((response: any) => {
      if (response && response.resultCode === 200) {
        this.listMedia = response.object;

      }
    }, error => {
    });
  }
  getPackageType() {
    this.listPackage = []
    this.categoriesService.getAllPackageType().subscribe((response: any) => {
      if (response && response.resultCode === 200) {
        this.listPackage = response.object;

      }
    }, error => {
    });
  }
  validateForm(event) {
    let check = true;
    if (event === 'name' || event === 'save') {
      this.blName = false;
      if (!this.selectProduct || !this.selectProduct.name) {
        this.blName = true;
        check = false;
      }
    }
    if (event === 'blNormalPrice' || event === 'save') {
      this.blNormalPrice = false;
      if (!this.selectProduct || this.selectProduct.normalPrice === undefined || this.selectProduct.normalPrice === null) {
        this.blNormalPrice = true;
        check = false;
      }
    }
    if (event === 'blSku' || event === 'save') {
      this.blSku = false;
      if (!this.selectProduct || !this.selectProduct.sku) {
        this.blSku = true;
        check = false;
      }
    }
    if (event === 'blOrginSku' || event === 'save') {
      this.blOrginSku = false;
      if (!this.selectProduct || !this.selectProduct.orginSku) {
        this.blOrginSku = true;
        check = false;
      }
    }
    if (event === 'blInStock' || event === 'save') {
      this.blInStock = false;
      if (!this.selectProduct || this.selectProduct.inStock === undefined || this.selectProduct.inStock === null) {
        this.blInStock = true;
        check = false;
      }
    }
    if (event === 'blMinorder' || event === 'save') {
      this.blMinorder = false;
      if (!this.selectProduct || this.selectProduct.minOrder === undefined || this.selectProduct.minOrder === null) {
        this.blMinorder = true;
        check = false;
      }
    }
    if (event === 'blThumbnailImage' || event === 'save') {
      this.blThumbnailImage = false;
      if ((!this.selectProduct || !this.selectProduct.thumbnailImage) && (!this.listMediaThumbnail || this.listMediaThumbnail.length <= 0)) {
        this.blThumbnailImage = true;
        check = false;
      }
    }
    if (event === 'blCategory' || event === 'save') {
      this.blCategory = false;
      if (!this.selectListCategory || this.selectListCategory.length <= 0) {
        this.blCategory = true;
        check = false;
      }
    }

    if (event === 'blSalePrice' || event === 'save') {
      this.blSalePrice = false;
      if (this.selectProduct && this.selectProduct.salePrice && (Number(this.selectProduct.salePrice)) > Number(this.selectProduct.normalPrice)) {
        this.blSalePrice = true;
        check = false;
      }
    }
    if (event === 'blDate' || event === 'save') {
      this.blDate = false;
      if (this.selectProduct && this.startDate && this.endDate) {
        this.startDate.setHours(0)
        this.startDate.setMinutes(0)
        this.startDate.setSeconds(0)
        this.startDate.setMilliseconds(0);
        this.endDate.setHours(23)
        this.endDate.setMinutes(59)
        this.endDate.setSeconds(59)
        this.endDate.setMilliseconds(0);
        if (this.startDate > this.endDate) {
          this.blDate = true;
          check = false;
        }
      }
    }

    return check;
  }
  getProductId() {
    this.productService.getProductID(this.productId).subscribe((response: any) => {
      // tslint:disable-next-line:no-console
      console.log(response)
      if (response && response.resultCode === 200) {
        this.selectProduct = response.object;
        if (this.selectProduct && this.selectProduct.featured && this.selectProduct.featured === 1) {
          this.selectProduct.blFeatured = true;
        } else {
          this.selectProduct.blFeatured = false;
        }
        if (this.selectProduct && this.selectProduct.actived && this.selectProduct.actived === 1) {
          this.selectProduct.blActived = true;
        } else {
          this.selectProduct.blActived = false;
        }
        if (this.selectProduct && this.selectProduct.seoIndex && this.selectProduct.seoIndex === 1) {
          this.selectProduct.blSeoIndex = true;
        } else {
          this.selectProduct.blSeoIndex = false;
        }
        if (this.selectProduct && this.selectProduct.listCategory && this.selectProduct.listCategory.length > 0) {
          this.selectListCategory = this.selectProduct.listCategory;
        } else {
          this.selectListCategory = [];
        }
        if (this.selectProduct && this.selectProduct.saleStart) {
          this.startDate = new Date(Number(this.selectProduct.saleStart) * 1000)
          this.blSale = true;
        }
        if (this.selectProduct && this.selectProduct.saleEnd) {
          this.endDate = new Date(Number(this.selectProduct.saleEnd) * 1000)
          this.blSale = true;
        }
        if (this.selectProduct && this.selectProduct.salePrice && Number(this.selectProduct.salePrice) > 0) {
          this.blSale = true;
        }
      }
      this.loaderService.hide()
    }, error => {
      this.loaderService.hide();
    });
  }
  ngOnInit() {
    this.getlistManufactures();
    this.getPackageType();
    this.getAllCategori();

  }
  getAllCategori() {
    this.listCategory = [];
    this.listItemCategory = [];
    this.categoriesService.getAllRespont().subscribe((response: any) => {
      if (response && response.resultCode === 200) {
        this.listCategory = response.object;
        if (this.listCategory) {
          let newCourse = Object.assign([], this.listCategory);
          this.listItemCategory = newCourse;
        }
      }
    }, error => {
    });
  }


  getlistManufactures() {
    this.listManufactures = [];
    this.categoriesService.getAllsManufact().subscribe((response: any) => {
      if (response && response.resultCode === 200) {
        this.listManufactures = response.object;

      }
    }, error => {
    });

  }

  changeSlugByname() {
    if (!this.selectProduct.slug && this.selectProduct.name) {
      this.selectProduct.slug = this.selectProduct.name;
      this.setSlugOnchange();
    }
  }
  setSlugOnchange() {
    if (event) {
      this.selectProduct.slug = this.ultilities.changeToSlug(this.selectProduct.slug);
    }
  }
  updateProduct() {
    let check = true
    check = this.validateForm('save');
    if (!check) {
      window.scrollTo(0, 0);
      return false;
    }
    if (!this.selectProduct.thumbnailImage) {
      if (this.listMediaThumbnail && this.listMediaThumbnail.length > 0) {
        this.selectProduct.thumbnailImage = this.listMediaThumbnail[0].urlPath;
      }
    }
    let checkAttribue = this.validaeAttribue()
    if (!checkAttribue) {
      window.scrollTo(0, 0);
      this.ultilities.showError('Giá trị thuộc tính không được bỏ trống.')
      return false;
    }
    if (this.selectProduct.blFeatured) {
      this.selectProduct.featured = 1
    } else {
      this.selectProduct.featured = 0
    }
    if (this.selectProduct.blActived) {
      this.selectProduct.actived = 1
    } else {
      this.selectProduct.actived = 0
    }
    if (this.selectProduct.blSeoIndex) {
      this.selectProduct.seoIndex = 1
    } else {
      this.selectProduct.seoIndex = 0
    }
    if (this.startDate) {
      this.selectProduct.saleStart = (this.startDate.getTime() / 1000)
    } else {
      this.selectProduct.saleStart = null
    }
    if (this.endDate) {
      this.selectProduct.saleEnd = (this.endDate.getTime() / 1000)
    } else {
      this.selectProduct.saleEnd = null
    }
    this.selectProduct.listMedia = this.listMedia;
    this.selectProduct.listTags = this.tags;
    this.selectProduct.listAttributeAndValues = this.listAttributeAndValues;
    this.selectProduct.listMediaThumbnail = this.listMediaThumbnail;
    this.selectProduct.listCategory = this.selectListCategory;
    if (this.selectProduct.listCategory && this.selectProduct.listCategory.length > 0) {
      this.selectProduct.categoryId = this.selectProduct.listCategory[0];
    } else {
      this.selectProduct.categoryId = 0;
    }
    this.loaderService.show();
    setTimeout(() => {
      this.loaderService.hide();
    }, 3000);
    this.selectProduct.listArticleLinkUpload = this.listUploadFileByID;
    this.productService.update(this.selectProduct).subscribe((response: any) => {
      // tslint:disable-next-line:no-console
      // console.log('responseresponseresponse=', response);
      if (response && response.resultCode === 200) {
        this.router.navigate(['/tranzi-admin/product']);
        localStorage.setItem('updateProduct', 'Sửa Sản phẩm thành công');
      } else if (response && response.resultCode === 204) {
        this.ultilities.showError(response.errorMessage ? response.errorMessage : 'Có lỗi xảy ra. Liên hệ Admin để kiểm tra lại')
      }
      window.scrollTo(0, 0);
      this.loaderService.hide();
    }, (error) => {
      // tslint:disable-next-line:no-console
      let mes = '';
      if (error && error.error && error.error.errors && error.error.errors[0] && error.error.errors[0].message) {
        mes = error.error.errors[0].message;
      }
      this.messageService.clear();
      this.messageService.add({
        detail: mes ? mes : 'Có lỗi xảy ra. Liên hệ Admin để kiểm tra lại',
        severity: 'error',
        summary: 'Lỗi',
      });
      this.loaderService.hide();
    });
  }

  bankmanageproduct() {
    this.router.navigate(['/tranzi-admin/product']);
  }

  // upload===============================================UPLOAD====================
  btnUploadClick(fileUpload) {
    fileUpload.advancedFileInput.nativeElement.click()
  }

  onSelectedFiles(modal, fileUpload) {
    this.totalFile = 0;
    this.progressBarPercent = 20
    this.uploadedFilesError = []
    this.uploadedFilesErrorInvalid = [];
    this.uploadedFilesSuccess = []
    if (fileUpload.advancedFileInput.nativeElement.files) {
      this.totalFile = fileUpload.files.length;
      for (let i = 0; i < fileUpload.advancedFileInput.nativeElement.files.length; i++) {
        const element = fileUpload.advancedFileInput.nativeElement.files[i];
        const type = fileUpload.advancedFileInput.nativeElement.files[i].type;
        const strTypeCheck = type.split('/')[0];
        const strTypeTailFile = type.split('/')[1];
        if (('' + strTypeCheck).toUpperCase().trim() === 'VIDEO' && (('' + strTypeTailFile).toUpperCase().trim() !== 'MP4'
          && ('' + strTypeTailFile).toUpperCase().trim() !== '3GP' && ('' + strTypeTailFile).toUpperCase().trim() !== '3GPP')) {
          // tslint:disable-next-line:prefer-const
          let dto = {
            value: fileUpload.advancedFileInput.nativeElement.files[i].name,
            type: fileUpload.advancedFileInput.nativeElement.files[i].type,
          }
          this.uploadedFilesErrorInvalid.push(dto)
        } else if (element.size > 10000000 || element.size === 0) {
          this.uploadedFilesError.push(element)
        }

      }
    }
    if (fileUpload && fileUpload.msgs) {
      for (let i = 0; i < fileUpload.msgs.length; i++) {
        const element = fileUpload.msgs[i];
        if (element) {
          const maxsize = element.detail.indexOf('maximum');
          let valueDetail = '';
          let valueType = '';
          valueDetail = element.summary.split(':')[0];
          if (maxsize < 0) {
            valueType = 'Không đúng định dạng'
            // tslint:disable-next-line:prefer-const
            let dto = {
              value: valueDetail,
              type: valueType,
            }
            this.uploadedFilesErrorInvalid.push(dto)
          }
        }
      }
    }
    const checkNumberError = Number(this.uploadedFilesErrorInvalid ? this.uploadedFilesErrorInvalid.length : 0) +
      Number(this.uploadedFilesError ? this.uploadedFilesError.length : 0);
    if (this.totalFile === checkNumberError) {
      this.progressBarPercent = 100
      this.ultilities.showError('Cập nhật thất bại')
    }
    // modal.show()
    fileUpload.upload()
  }
  onBeforUpload(event, fileUpload) {
    // tslint:disable-next-line:prefer-const
    let interval = setInterval(() => {
      if (this.progressBarPercent < 90) {
        this.progressBarPercent = this.progressBarPercent + 10;
      }
      if (this.progressBarPercent >= 100) {
        this.progressBarPercent = 100;
        clearInterval(interval);
      }
    }, 700);
    this.uploadedFilesSuccess = []
    if (fileUpload.files) {
      for (let i = 0; i < fileUpload.files.length; i++) {
        const element = fileUpload.files[i];
        const type = fileUpload.files[i].type;
        const strTypeCheck = type.split('/')[0];
        const strTypeTailFile = type.split('/')[1];
        if (element.size > 0) {
          // if (('' + strTypeCheck).toUpperCase().trim() === 'VIDEO' && (('' + strTypeTailFile).toUpperCase().trim() !== 'MP4'
          //   && ('' + strTypeTailFile).toUpperCase().trim() !== '3GP' && ('' + strTypeTailFile).toUpperCase().trim() !== '3GPP')) {
          // } else {
          this.uploadedFilesSuccess.push(element)
          // }
        }
      }
    }
    event.xhr.setRequestHeader('Authorization', + this.ultilities.getToken())
    if (event.formData) {
      // TODO
    }
  }
  viewDate(event) {
    if (event && Number(event) > 0) {
      // tslint:disable-next-line:prefer-const
      let dt = new Date(Number(event) * 1000)
      const d2 = moment(dt, 'DD/MM/YYYY')
      return d2;
    }
    return ''
  }
  onUploadSuccess(event, modal) {
    setTimeout(() => {
      const checkNumberError = Number(this.uploadedFilesErrorInvalid ? this.uploadedFilesErrorInvalid.length : 0) +
        Number(this.uploadedFilesError ? this.uploadedFilesError.length : 0);
      this.progressBarPercent = 100

      if (event && event.xhr && event.xhr.status !== 200 || this.totalFile === checkNumberError) {
        if (modal) modal.hide()
        this.ultilities.showError('Cập nhật thất bại')
      } else {
      }
    }, 1000);

    if (event && event.xhr && event.xhr.response) {
      const document = JSON.parse(event.xhr.response)
      // if (document) {
      //   if (document.object) {
      //     this.selectProduct.thumbnailImage = this.urlServerFrontEnd + this.ultilities.clearNameKytuDacbiet(document.object);
      //   } else {
      //     this.selectProduct.thumbnailImage = null;
      //   }

      // }
      if (document && document.object && document.object.length > 0) {
        document.object.forEach(name => {
          // tslint:disable-next-line:prefer-const
          let dto = new Media();
          dto.id = 0;
          dto.createdTime = 0;
          let element = this.ultilities.clearNameKytuDacbiet(name);
          // dto.urlPath = element
          dto.urlPath = this.serverPath + element;
          dto.serverPath = this.serverPath + element;
          dto.type = 'THUMBNAIL';
          this.listMediaThumbnail.push(dto);
        });
      } else {
      }
    }
  }
  onUploadError(event, modal) {
    setTimeout(() => {
      this.progressBarPercent = 100
      if (modal) modal.hide()
      if (event && event.xhr) {
        let response;
        if (event.xhr.response) {
          response = JSON.parse(event.xhr.response)
        }
        let message = 'Cập nhật không thành công'
        if (response && response.message) {
          message = response.message
        }
        // this.ultilities.showError(message)
      }
    }, 1000)
  }

  // ===================END UPLOAD==============================

  // upload===============================================UPLOAD MULTI====================
  btnUploadClickMulti(fileUpload) {
    fileUpload.advancedFileInput.nativeElement.click()
  }

  onSelectedFilesMulti(modal, fileUpload) {
    this.totalFile = 0;
    this.progressBarPercent = 20
    this.uploadedFilesError = []
    this.uploadedFilesErrorInvalid = [];
    this.uploadedFilesSuccess = []
    if (fileUpload.advancedFileInput.nativeElement.files) {
      this.totalFile = fileUpload.files.length;
      for (let i = 0; i < fileUpload.advancedFileInput.nativeElement.files.length; i++) {
        const element = fileUpload.advancedFileInput.nativeElement.files[i];
        const type = fileUpload.advancedFileInput.nativeElement.files[i].type;
        const strTypeCheck = type.split('/')[0];
        const strTypeTailFile = type.split('/')[1];
        if (('' + strTypeCheck).toUpperCase().trim() === 'VIDEO' && (('' + strTypeTailFile).toUpperCase().trim() !== 'MP4'
          && ('' + strTypeTailFile).toUpperCase().trim() !== '3GP' && ('' + strTypeTailFile).toUpperCase().trim() !== '3GPP')) {
          // tslint:disable-next-line:prefer-const
          let dto = {
            value: fileUpload.advancedFileInput.nativeElement.files[i].name,
            type: fileUpload.advancedFileInput.nativeElement.files[i].type,
          }
          this.uploadedFilesErrorInvalid.push(dto)
        } else if (element.size > 20000000 || element.size === 0) {
          this.uploadedFilesError.push(element)
        }

      }
    }
    if (fileUpload && fileUpload.msgs) {
      for (let i = 0; i < fileUpload.msgs.length; i++) {
        const element = fileUpload.msgs[i];
        if (element) {
          const maxsize = element.detail.indexOf('maximum');
          let valueDetail = '';
          let valueType = '';
          valueDetail = element.summary.split(':')[0];
          if (maxsize < 0) {
            valueType = 'Không đúng định dạng'
            // tslint:disable-next-line:prefer-const
            let dto = {
              value: valueDetail,
              type: valueType,
            }
            this.uploadedFilesErrorInvalid.push(dto)
          }
        }
      }
    }
    const checkNumberError = Number(this.uploadedFilesErrorInvalid ? this.uploadedFilesErrorInvalid.length : 0) +
      Number(this.uploadedFilesError ? this.uploadedFilesError.length : 0);
    if (this.totalFile === checkNumberError) {
      this.progressBarPercent = 100
      this.ultilities.showError('Cập nhật thất bại')
    }
    // modal.show()
    fileUpload.upload()
  }
  onBeforUploadMulti(event, fileUpload) {
    // tslint:disable-next-line:prefer-const
    let interval = setInterval(() => {
      if (this.progressBarPercent < 90) {
        this.progressBarPercent = this.progressBarPercent + 10;
      }
      if (this.progressBarPercent >= 100) {
        this.progressBarPercent = 100;
        clearInterval(interval);
      }
    }, 700);
    this.uploadedFilesSuccess = []
    if (fileUpload.files) {
      for (let i = 0; i < fileUpload.files.length; i++) {
        const element = fileUpload.files[i];
        const type = fileUpload.files[i].type;
        const strTypeCheck = type.split('/')[0];
        const strTypeTailFile = type.split('/')[1];
        if (element.size > 0) {
          this.uploadedFilesSuccess.push(element);
        }
      }
    }
    event.xhr.setRequestHeader('Authorization', + this.ultilities.getToken())
    if (event.formData) {
    }
  }
  onUploadSuccessMulti(event, modal) {
    setTimeout(() => {
      const checkNumberError = Number(this.uploadedFilesErrorInvalid ? this.uploadedFilesErrorInvalid.length : 0) +
        Number(this.uploadedFilesError ? this.uploadedFilesError.length : 0);
      this.progressBarPercent = 100

      if (event && event.xhr && event.xhr.status !== 200 || this.totalFile === checkNumberError) {
        if (modal) modal.hide()
        this.ultilities.showError('Cập nhật thất bại')
      } else {
      }
    }, 1000);

    if (event && event.xhr && event.xhr.response) {
      const document = JSON.parse(event.xhr.response)
      if (document) {
        // this.ultilities.showSuccess('upload thanh cong')
        if (document.object && document.object.length > 0) {
          document.object.forEach(name => {
            // tslint:disable-next-line:prefer-const
            let dto = new Media();
            dto.id = 0;
            dto.createdTime = 0;
            let element = this.ultilities.clearNameKytuDacbiet(name);
            // dto.urlPath = element
            dto.urlPath = this.urlProductFile + element;
            dto.serverPath = this.urlProductFile + element;
            if (element && (element.indexOf('.png') >= 0 || element.indexOf('.png') >= 0)
              || element.indexOf('.PNG') >= 0
              || element.indexOf('.JPG') >= 0
              || element.indexOf('.jpg') >= 0
              || element.indexOf('.jpeg') >= 0
              || element.indexOf('.JPEG') >= 0
              || element.indexOf('.gif') >= 0
              || element.indexOf('.GIF') >= 0) {
              dto.type = 'IMAGE';
            } else if (element && (element.indexOf('.docx') >= 0 || element.indexOf('.doc') >= 0)
              || element.indexOf('.DOC') >= 0
              || element.indexOf('.DOCX') >= 0) {
              dto.type = 'DOC';
            } else if (element && (element.indexOf('.xls') >= 0 || element.indexOf('.XLS') >= 0)
              || element.indexOf('.xlsx') >= 0
              || element.indexOf('.XLSX') >= 0) {
              dto.type = 'XLS';
            } else if (element && (element.indexOf('.pdf') >= 0 || element.indexOf('.PDF') >= 0)) {
              dto.type = 'PDF';
            } else if (element && (element.indexOf('.mp4') >= 0 || element.indexOf('.MP4') >= 0)
              || element.indexOf('.3gp') >= 0
              || element.indexOf('.3GP') >= 0
              || element.indexOf('.flv') >= 0
              || element.indexOf('.FLV') >= 0) {
              dto.type = 'MEDIA';
            } else {
              dto.type = 'NOT';
            }
            this.listMedia.push(dto);
          });
        } else {
        }
      }
    }
  }
  onUploadErrorMulti(event, modal) {
    setTimeout(() => {
      this.progressBarPercent = 100
      if (modal) modal.hide()
      if (event && event.xhr) {
        let response;
        if (event.xhr.response) {
          response = JSON.parse(event.xhr.response)
        }
        let message = 'Cập nhật không thành công'
        if (response && response.message) {
          message = response.message
        }
        // this.ultilities.showError(message)
      }
    }, 1000)
  }

  // ===================END UPLOAD==============================
  onChange($event: any): void {
    // tslint:disable-next-line:quotemark
    // tslint:disable-next-line:no-console
    // console.log("onChange");
    // this.log += new Date() + "<br />";
  }
  changeNumber(event) {
    if (event === 'inStock') {
      if (this.selectProduct.inStock && !isNaN(this.selectProduct.inStock)) {
        this.selectProduct.inStock = Math.abs(Number(this.selectProduct.inStock))
      } else {
        this.selectProduct.inStock = 0;
      }
    }
    if (event === 'minOrder') {
      if (this.selectProduct.minOrder && !isNaN(this.selectProduct.minOrder)) {
        this.selectProduct.minOrder = Math.abs(Number(this.selectProduct.minOrder))
      } else {
        this.selectProduct.minOrder = 0;
      }
    }

  }
  keyPress(event: any, type) {
    if (type && type === 'number') {
      const patternExtension = /[0-9]/;
      const inputChar = String.fromCharCode(event.charCode);
      if (event.keyCode !== 9 && !patternExtension.test(inputChar)) {
        event.preventDefault();
      }
    } else if (type && type === 'date') {
      const patternExtension = /[0-9\/]/;
      const inputChar = String.fromCharCode(event.charCode);
      if (event.keyCode !== 9 && !patternExtension.test(inputChar)) {
        event.preventDefault();
      }
    } else {
      const patternExtension = /[0-9\.]/;
      const inputChar = String.fromCharCode(event.charCode);
      if (event.keyCode !== 9 && !patternExtension.test(inputChar)) {
        event.preventDefault();
      }
    }
  }
  deleteMedia(index) {
    if (this.listMedia && this.listMedia.length > 0 && Number(index) >= 0) {
      this.listMedia.splice(index, 1);
    }
  }
  setCategoryID(event) {
    if (event && Number(event) > 0) {
      this.selectProduct.categoryId = event;
    }
  }
  addMediaList() {
    if (!this.linkMediafile || !this.linkMediafile.trim()) {
      this.ultilities.showError('Link không được null hoặc trống ');
    } else {
      // tslint:disable-next-line:prefer-const
      let med = new Media();
      med.id = 0;
      med.type = this.typeFile;
      med.urlPath = this.linkMediafile;
      this.listMedia.push(med);
      this.linkMediafile = '';
    }
  }
  changeCheckAll() {
    // console.log('change check akll = ', this.checkAll)
    if (this.checkAll) {
      if (this.listAttributeValues) {
        this.listAttributeValues.forEach(element => {
          element.blSelected = true;
        });
      }
    } else {
      if (this.listAttributeValues) {
        this.listAttributeValues.forEach(element => {
          element.blSelected = false;
        });
      }
    }
  }
  onTagsChanged2(event) {
    // console.log('eventeventeventeventevent=', event)
  }
  addAttributeValues() {
    if (!this.listAttributeAndValues) {
      this.listAttributeAndValues = [];
    }
    const dto = new ListAttributeAndValues();
    dto.attribute = new Attributes;
    dto.listAttributeValues = [];
    let newCourse = Object.assign({}, dto);
    this.listAttributeAndValues.push(newCourse);
  }
  changeAttribute(event, index) {
    this.listAttributeValues = []
    if (this.listAttributeAndValues && event && event.id === undefined) {
      for (let i = 0; i < this.listAttributeAndValues.length; i++) {
        if (this.listAttributeAndValues[i].attribute === event) {
          this.listAttributeAndValues[i].blValidate = false;
          let at = new Attributes();
          at.id = 0
          at.name = event.name ? event.name : (event ? event : 'add')
          let newCourse = Object.assign({}, at)
          this.listAttributeAndValues[i].attribute = newCourse;
        }
      }
    }

    if (event && Number(event.id) > 0) {
      this.attributesService.getAllsPaddingValuesByAttributeId(1, 1000, 'id', 'desc', '', event.id).subscribe((response: any) => {
        if (response && response.resultCode === 200) {
          let page = new PagerData();
          page = response.object;
          if (page && page.content) {
            this.listAttributeValues = page.content;
          }
        }
        this.loaderService.hide()
      }, error => {
        this.loaderService.hide();
      });
    } else if (!event) {
      this.deleteAttribute(index);
    }
  }
  addAttributesValues(event, index) {
    if (this.listAttributeAndValues && event && event.length > 0 && this.listAttributeAndValues[index].listAttributeValues
      && this.listAttributeAndValues[index].listAttributeValues.length > 0) {
      for (let i = 0; i < this.listAttributeAndValues[index].listAttributeValues.length; i++) {
        if (this.listAttributeAndValues[index].listAttributeValues[i] === event[i]) {
          this.listAttributeAndValues[index].blValidate = false;
          let at = new AttributeValues();
          at.id = 0
          at.value = event[i].value ? event[i].value : (event[i] ? event[i] : 'add')
          let newCourse = Object.assign({}, at)
          this.listAttributeAndValues[index].listAttributeValues[i] = newCourse;
        }
      }
    }
  }
  changeAttributeValues(event, index) {
    this.listAttributeValues = []
    if (event && event.attribute && Number(event.attribute.id) > 0) {
      this.attributesService.getAllsPaddingValuesByAttributeId(1, 1000, 'id', 'desc', '', event.attribute.id).subscribe((response: any) => {
        if (response && response.resultCode === 200) {
          let page = new PagerData();
          page = response.object;
          if (page && page.content) {
            this.listAttributeValues = page.content;
          }
        }
        this.loaderService.hide()
      }, error => {
        this.loaderService.hide();
      });
    } else if (!event) {
      this.deleteAttribute(index);
    }
  }
  getListAttributesShow() {
    this.listAttributesShow = [];
    if (this.listAttributes) {
      for (let i = 0; i < this.listAttributes.length; i++) {
        if (this.listAttributeAndValues) {
          let check = false;
          for (let j = 0; j < this.listAttributeAndValues.length; j++) {
            if (this.listAttributes[i] && this.listAttributeAndValues[j].attribute &&
              this.listAttributes[i].id === this.listAttributeAndValues[j].attribute.id
              && this.listAttributes[i].name === this.listAttributeAndValues[j].attribute.name) {
              check = true;
            }
          }
          if (!check) {

            let newCourse = Object.assign({}, this.listAttributes[i])
            this.listAttributesShow.push(newCourse);

          }
        }
      }
    }
    this.sortValue();
  }

  sortValue() {
    if (this.listAttributesShow) {
      this.listAttributesShow.sort(function (a, b) {
        if (a.name < b.name) { return -1; }
        if (a.name > b.name) { return 1; }
        return 0;
      })
    }
    if (this.listAttributeAndValues) {
      this.listAttributeAndValues.sort(function (a, b) {
        if (a.attribute.name < b.attribute.name) { return -1; }
        if (a.attribute.name > b.attribute.name) { return 1; }
        return 0;
      })
    }
  }

  deleteAttribute(event) {
    if (this.listAttributeAndValues && Number(event) >= 0) {
      this.listAttributeAndValues[event].attribute = undefined

    }
  }
  deleteAttributeAndValue(event) {
    if (this.listAttributeAndValues && Number(event) >= 0) {
      this.listAttributeAndValues.splice(event, 1)
    }
  }
  updateListAttribute() {
    if (this.listAttributeAndValues) {
      for (let i = 0; i < this.listAttributeAndValues.length; i++) {
        if (this.listAttributeAndValues[i].attribute) {
          if (this.listAttributeAndValues[i].attribute.name === undefined) {
            let at = new Attributes();
            at.id = 0
            at.name = this.listAttributeAndValues[i].attribute + '';
            let newCourse = Object.assign({}, at)
            this.listAttributeAndValues[i].attribute = newCourse;
          } else if (this.listAttributeAndValues[i].attribute.id === undefined) {
            let at = new Attributes();
            at.id = 0
            at.name = this.listAttributeAndValues[i].attribute.name + '';
            let newCourse = Object.assign({}, at)
            this.listAttributeAndValues[i].attribute = newCourse;
          }
        }
        if (this.listAttributeAndValues[i].listAttributeValues) {
          if (this.listAttributeAndValues[i].attribute.name === undefined) {
            let at = new Attributes();
            at.id = 0
            at.name = this.listAttributeAndValues[i].attribute + '';
            let newCourse = Object.assign({}, at)
            this.listAttributeAndValues[i].attribute = newCourse;
          } else if (this.listAttributeAndValues[i].attribute.id === undefined) {
            let at = new Attributes();
            at.id = 0
            at.name = this.listAttributeAndValues[i].attribute.name + '';
            let newCourse = Object.assign({}, at)
            this.listAttributeAndValues[i].attribute = newCourse;
          }
        }
      }
    }
  }
  validaeAttribue() {
    let check = true;
    if (this.listAttributeAndValues) {
      for (let i = 0; i < this.listAttributeAndValues.length; i++) {
        this.listAttributeAndValues[i].blValidate = false;
        if (!this.listAttributeAndValues[i].attribute || !this.listAttributeAndValues[i].attribute.name
          || !this.listAttributeAndValues[i].listAttributeValues || this.listAttributeAndValues[i].listAttributeValues.length <= 0) {
          this.listAttributeAndValues[i].blValidate = true;
          check = false;
        }
      }
    }
    return check;
  }
  deleteProductMeida(index) {
    if (this.listMediaThumbnail && index <= this.listMediaThumbnail.length) {
      if (this.listMediaThumbnail[index].selectImage) {
        this.selectProduct.thumbnailImage = '';
      }
      this.listMediaThumbnail.splice(index, 1)
    }
  }
  setDefauleAnhSanPham(index) {
    if (this.listMediaThumbnail) {
      for (let i = 0; i < this.listMediaThumbnail.length; i++) {
        if (i === index) {
          this.listMediaThumbnail[i].selectImage = true;
          this.selectProduct.thumbnailImage = this.listMediaThumbnail[i].urlPath
        } else {
          this.listMediaThumbnail[i].selectImage = false;
        }
      }
    }
  }
  // luu session store 
  setLocalsto(event) {
    if (event === 'blSeo') {
      this.blSeo = !this.blSeo;
      if (this.blSeo) {
        sessionStorage.removeItem('blSeoEdit')
        sessionStorage.setItem('blSeoEdit', 'true');
      } else {
        sessionStorage.removeItem('blSeoEdit')
        sessionStorage.setItem('blSeoEdit', 'false');
      }
    }
    if (event === 'blTag') {
      this.blTag = !this.blTag;
      if (this.blTag) {
        sessionStorage.removeItem('blTagEdit')
        sessionStorage.setItem('blTagEdit', 'true');
      } else {
        sessionStorage.removeItem('blTagEdit')
        sessionStorage.setItem('blTagEdit', 'false');
      }
    }
    if (event === 'blMediaFile') {
      this.blMediaFile = !this.blMediaFile;
      if (this.blMediaFile) {
        sessionStorage.removeItem('blMediaFileEdit')
        sessionStorage.setItem('blMediaFileEdit', 'true');
      } else {
        sessionStorage.removeItem('blMediaFileEdit')
        sessionStorage.setItem('blMediaFileEdit', 'false');
      }
    }
  }
  getShowlocalStore() {
    if (sessionStorage.getItem('blSeoEdit') && sessionStorage.getItem('blSeoEdit') === 'true') {
      this.blSeo = true;
    } else {
      this.blSeo = false;
    }
    if (sessionStorage.getItem('blMediaFileEdit') && sessionStorage.getItem('blMediaFileEdit') === 'true') {
      this.blMediaFile = true;
    } else {
      this.blMediaFile = false;
    }
    if (sessionStorage.getItem('blTagEdit') && sessionStorage.getItem('blTagEdit') === 'true') {
      this.blTag = true;
    } else {
      this.blTag = false;
    }
  }
  changeSale() {
    if (this.blSale) {
      // console.log('true')
    } else {
      // console.log('false')
      this.startDate = null;
      this.endDate = null;
    }
  }
  showImage(event) {
    if (event) {
      this.strImageUrl = event;
      this.viewImage.show();
    }
  }
  //Upload file RIP
  btnUploadClickRIP(fileUploadRIP) {
    fileUploadRIP.advancedFileInput.nativeElement.click()
  }

  onSelectedFilesRIP(fileUploadRIP) {
    fileUploadRIP.upload();
    this.loaderService.show();
  }
  onBeforUploadRIP(event, fileUploadRIP) {
    event.xhr.setRequestHeader('Authorization', + this.ultilities.getToken())
    if (event.formData) {
    }
  }
  onUploadSuccessRIP(event) {
    setTimeout(() => {
      if (event && event.xhr && event.xhr.response) {
        this.loaderService.hide();
        const document = JSON.parse(event.xhr.response)
        if (document) {
          if (document.data && document.data.length > 0) {
            for (let i = 0; i < document.data.length; i++) {
              this.listMediaRIP.push(document.data[i]);
              let dto = new ArticleLinkUpload();
              dto.type = 2
              dto.value = document.data[i]
              dto.status = 1;
              dto.articleId = 0;
              this.listUploadFileByID.push(dto);
            }
          }
        }
      }
    }, 100)
  }

  onUploadErrorRIP(event) {
    setTimeout(() => {
      this.progressBarPercent = 100
      if (event && event.xhr) {
        let response;
        this.loaderService.hide();
        if (event.xhr.response) {
          response = JSON.parse(event.xhr.response)
        }
        let message = 'Cập nhật không thành công'
        if (response && response.message) {
          message = response.message
        }
        // this.ultilities.showError(message)
      }
    }, 1000)
  }
  showListFileUpload() {
    this.addlinkUpload.show();
    this.selectFile();
  }
  selectFile() {
    this.listUploadFileBySelected = [];
    this.articleService.getSearchArticleLinkUpload(this.selectLinkFile).subscribe((response: any) => {
      if (response && response.resultCode === 200) {
        this.listUploadFileBySelected = response.object;
      }
      this.loaderService.hide();
    }, (error) => {
      // tslint:disable-next-line:no-console
      let mes = '';
      if (error && error.error && error.error.errors && error.error.errors[0] && error.error.errors[0].message) {
        mes = error.error.errors[0].message;
      }
      this.messageService.clear();
      this.messageService.add({
        detail: mes ? mes : 'Có lỗi xảy ra. Liên hệ Admin để kiểm tra lại',
        severity: 'error',
        summary: 'Lỗi',
      });
      this.loaderService.hide();
    });

  }
  getListFileArticleUpload(id) {
    this.listUploadFileByID = [];
    this.listMediaRIP = [];
    this.articleService.getByIdProductLinkUpload(id).subscribe((response: any) => {
      console.log(response)
      if (response && response.resultCode === 200) {
        this.listUploadFileByID = response.object;
        if(this.listUploadFileByID!==null && this.listUploadFileByID.length>0){
          for(let i =0;i<this.listUploadFileByID.length;i++){
            this.listMediaRIP.push(this.listUploadFileByID[i].value);
          }
        }
        
      }
      this.loaderService.hide()
    }, error => {
      this.loaderService.hide();
    });
  }
}

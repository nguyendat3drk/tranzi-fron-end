import { Utilities } from '../../../../shared/services/utilities.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MessageService } from 'primeng/components/common/messageservice';
import { ActivatedRoute, Router } from '@angular/router';
import { LoaderService } from '../../../../shared/services/loader.service';
import { TermsService } from '../../../../shared/services/http/TermsService';
import { Terms } from '../../../../shared/models/Terms';
import { TranslateService } from '@ngx-translate/core';
import { TermsManagmentService } from '../../../../shared/services/http/terms-managment.service';
import { Categories } from '../../../../shared/models/Categories';
import { CategoriesService } from '../../../../shared/services/http/categories.service';
import { Articles } from '../../../../shared/models/articles';
import { ArticlesManagmentService } from '../../../../shared/services/http/videos-managment.service';
import { PagerData } from '../../../../shared/entity/pagerData';
import { ArticleLinkUpload } from '../../../../shared/models/ArticleLinkUpload';
import { ModalDirective } from 'ngx-bootstrap';
import { SelectLinkFile } from '../../../../shared/models/SelectLinkFile';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'article-create',
  templateUrl: './articles-create.component.html',
  styleUrls: ['./articles-create.component.scss'],
})
export class ArticlesCreateComponent implements OnInit {
  idTermSelect: number = 0;
  listItemTerms: Terms[] = [];
  listURLUpload: string[] = [];
  // listTerms: Terms[] = []

  selectArticles: Articles = new Articles();
  public editorValue: string = ''
  urlServerFrontEnd: string;


  // upload
  urlupload: string = '';
  urluploadRIP: string = '';
  listMediaRIP: string [] = [];
  uploadedFilesSuccess: any[] = []
  uploadedFilesError: any[] = []
  uploadedFilesErrorInvalid: any[] = []
  progressBarPercent = 20
  totalFile: number = 0;

  // validatee
  blName: boolean = false;
  blBannerImage: boolean = false;
  listUploadFileByID: ArticleLinkUpload [] = [];
  listUploadFileBySelected: ArticleLinkUpload [] = [];
  @ViewChild('addlinkUpload') public addlinkUpload: ModalDirective;
  selectLinkFile: SelectLinkFile={};
  linkSeleced: string = '';
  constructor(private router: Router, private translateService: TranslateService,
    private messageService: MessageService, private articleService: ArticlesManagmentService, private termsService: TermsService,
    private loaderService: LoaderService, private ultilities: Utilities, private categoriesService: CategoriesService,
    private termsServiceChild: TermsService) {
    this.urlupload = this.ultilities.getUrlServerHost() + '/share-api/uploadArticles'
    this.urluploadRIP = this.ultilities.getUrlServerHost() + '/share-api/uploadRIP'
    this.urlServerFrontEnd = this.ultilities.urlServerFrontEnd() + 'assets/articles/'
  }
  ngOnInit() {
    this.getAllCategori();
    this.selectArticles = new Articles();
    this.selectArticles.type = 0;
    let idProduct = localStorage.getItem("IDProduct");
    let nameProduct = localStorage.getItem("nameProduct");
    if (idProduct && Number(idProduct) > 0) {
      this.selectArticles.productId = Number(idProduct);
      this.selectArticles.productName = nameProduct;
    }
  }
  getAllCategori() {
    this.loaderService.show();
    this.listItemTerms = [];
    this.termsServiceChild.getAllsPadding(1, 1000, 'id', 'desc', '', -1).subscribe((response: any) => {
      if (response && response.resultCode === 200) {
        let page = new PagerData();
        page = response.object;
        if (page && page.content) {
          this.listItemTerms = page.content;
        }
      }
      this.loaderService.hide();
    }, error => {
      this.loaderService.hide();
    });
  }
  changeSlugByname() {
    if (!this.selectArticles.slug && this.selectArticles.name) {
      this.selectArticles.slug = this.selectArticles.name;
      this.setSlugOnchange();
    }
  }
  setSlugOnchange() {
    if (event) {
      this.selectArticles.slug = this.ultilities.changeToSlug(this.selectArticles.slug);
    }
  }
  createTerms() {
    this.validate();
    this.selectArticles.id = 0;
    if (!this.selectArticles || !this.selectArticles.name) {
      return;
    }
    if (this.selectArticles.blSeoIndex) {
      this.selectArticles.seoIndex = 1
    } else {
      this.selectArticles.seoIndex = 0
    }
    this.selectArticles.listTerms = [];
    this.selectArticles.listTerms.push(this.idTermSelect);
    this.selectArticles.listArticleLinkUpload = this.listUploadFileByID;
    this.articleService.create(this.selectArticles).subscribe((response: any) => {
      // tslint:disable-next-line:no-console
      // console.log('responseresponseresponse=', response);
      if (response && response.resultCode === 200) {
        this.router.navigate(['/tranzi-admin/articles']);
        localStorage.setItem('addArticles', 'Thêm mới bài viết thành công');
      } else if (response && response.resultCode === 204) {
        this.ultilities.showError('Not Update Database')
      }
      window.scrollTo(0, 0);
      this.loaderService.hide();
    }, (error) => {
      // tslint:disable-next-line:no-console
      let mes = '';
      if (error && error.error && error.error.errors && error.error.errors[0] && error.error.errors[0].message) {
        mes = error.error.errors[0].message;
      }
      this.messageService.clear();
      this.messageService.add({
        detail: mes ? mes : 'Có lỗi xảy ra. Liên hệ Admin để kiểm tra lại',
        severity: 'error',
        summary: 'Lỗi',
      });
      this.loaderService.hide();
    });
  }
  tesst(event) {

  }
  bankmanager() {
    this.router.navigate(['/tranzi-admin/articles']);
  }
  keyPress(event: any, type) {
    if (type && type === 'number') {
      const patternExtension = /[0-9]/;
      const inputChar = String.fromCharCode(event.charCode);
      if (event.keyCode !== 9 && !patternExtension.test(inputChar)) {
        event.preventDefault();
      }
    } else if (type && type === 'date') {
      const patternExtension = /[0-9\/]/;
      const inputChar = String.fromCharCode(event.charCode);
      if (event.keyCode !== 9 && !patternExtension.test(inputChar)) {
        event.preventDefault();
      }
    } else {
      const patternExtension = /[0-9\.]/;
      const inputChar = String.fromCharCode(event.charCode);
      if (event.keyCode !== 9 && !patternExtension.test(inputChar)) {
        event.preventDefault();
      }
    }
  }
  // upload===============================================UPLOAD====================
  btnUploadClick(fileUpload) {
    fileUpload.advancedFileInput.nativeElement.click()
  }
  btnUploadClickIcon(fileUploadIcon) {
    fileUploadIcon.advancedFileInput.nativeElement.click()
  }

  onSelectedFiles(fileUpload) {
    fileUpload.upload()
  }
  onBeforUpload(event, fileUpload) {
    event.xhr.setRequestHeader('Authorization', + this.ultilities.getToken())
    if (event.formData) {
    }
  }
  onUploadSuccess(event) {
    setTimeout(() => {
      if (event && event.xhr && event.xhr.response) {
        const document = JSON.parse(event.xhr.response)
        if (document) {
          // this.ultilities.showSuccess('upload thanh cong')
          // this.selectArticles.url = this.urlServerFrontEnd + this.ultilities.clearNameKytuDacbiet(document.object);
          if (document.object && document.object.length > 0) {
            this.selectArticles.url = this.urlServerFrontEnd + this.ultilities.clearNameKytuDacbiet(document.object[0]);
            document.object.forEach(name => {
              let element = this.ultilities.clearNameKytuDacbiet(name);
              this.listURLUpload.push(this.urlServerFrontEnd + element);
            });
          }
        }
      }
    }, 100)
  }

  onUploadError(event) {
    setTimeout(() => {
      this.progressBarPercent = 100
      if (event && event.xhr) {
        let response;
        if (event.xhr.response) {
          response = JSON.parse(event.xhr.response)
        }
        let message = 'Cập nhật không thành công'
        if (response && response.message) {
          message = response.message
        }
        // this.ultilities.showError(message)
      }
    }, 1000)
  }

  validate() {
    this.blName = true;
    this.blBannerImage = true;
  }
  //Upload file RIP
  btnUploadClickRIP(fileUploadRIP) {
    fileUploadRIP.advancedFileInput.nativeElement.click()
  }

  onSelectedFilesRIP(fileUploadRIP) {
    fileUploadRIP.upload();
    this.loaderService.show();
  }
  onBeforUploadRIP(event, fileUploadRIP) {
    event.xhr.setRequestHeader('Authorization', + this.ultilities.getToken())
    if (event.formData) {
    }
  }
  onUploadSuccessRIP(event) {
    setTimeout(() => {
      if (event && event.xhr && event.xhr.response) {
        this.loaderService.hide();
        const document = JSON.parse(event.xhr.response)
        if (document) {
          if(this.listUploadFileByID == null && this.listUploadFileByID == undefined){
            this.listUploadFileByID = [];
          }
          // this.ultilities.showSuccess('upload thanh cong')
          if (document.data && document.data.length > 0) {
            for(let i =0;i<document.data.length;i++){
              this.listMediaRIP.push(document.data[i]);
              let dto  = new ArticleLinkUpload();
              dto.type = 1
              dto.value = document.data[i]
              dto.status = 1;
              dto.articleId =  0;
              this.listUploadFileByID.push(dto);
            }
        }
      }
    }
    }, 100)
  }

  onUploadErrorRIP(event) {
    setTimeout(() => {
      this.progressBarPercent = 100
      if (event && event.xhr) {
        let response;
        this.loaderService.hide();
        if (event.xhr.response) {
          response = JSON.parse(event.xhr.response)
        }
        let message = 'Cập nhật không thành công'
        if (response && response.message) {
          message = response.message
        }
        // this.ultilities.showError(message)
      }
    }, 1000)
  }
  showListFileUpload(){
    this.addlinkUpload.show();
    this.selectFile();
  }
  selectFile(){
    this.listUploadFileBySelected = [];
    // for(let i=0;i<100;i++){
    //   let at = new ArticleLinkUpload();
    //   at.value = 'http://media.tranzi.vn/assets/RIP/test_l28l29l30__dat21.docx';
    //   this.listUploadFileBySelected.push(at);
    // }


    this.articleService.getSearchArticleLinkUpload(this.selectLinkFile).subscribe((response: any) => {
      if (response && response.resultCode === 200) {
        this.listUploadFileBySelected = response.object;
      }
      this.loaderService.hide();
    }, (error) => {
      // tslint:disable-next-line:no-console
      let mes = '';
      if (error && error.error && error.error.errors && error.error.errors[0] && error.error.errors[0].message) {
        mes = error.error.errors[0].message;
      }
      this.messageService.clear();
      this.messageService.add({
        detail: mes ? mes : 'Có lỗi xảy ra. Liên hệ Admin để kiểm tra lại',
        severity: 'error',
        summary: 'Lỗi',
      });
      this.loaderService.hide();
    });
    
  }
  
}




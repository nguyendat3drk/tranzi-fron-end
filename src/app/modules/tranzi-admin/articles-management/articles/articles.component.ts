import { Utilities } from '../../../../shared/services/utilities.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ProductService } from '../../../../shared/services/http/product.service';
import { MessageService } from 'primeng/components/common/messageservice';
import { ActivatedRoute, Router } from '@angular/router';
import { LoaderService } from '../../../../shared/services/loader.service';
import { TermsService } from '../../../../shared/services/http/TermsService';
import { Terms } from '../../../../shared/models/Terms';
import { PagerData } from '../../../../shared/entity/pagerData';
import { isFulfilled } from 'q';
import { TreeNode } from 'primeng/api';
import { ThemeSettingsComponent } from '../../../../@theme/components';
import { ModalDirective } from 'ngx-bootstrap';
import { ArticlesService } from '../../../../shared/services/http/ArticlesService';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'articles',
  templateUrl: './articles.component.html',
  styleUrls: ['./articles.component.scss'],
})
export class ArticlesComponent implements OnInit {
  listTerms: Terms[] = [];
  totalPage: number = 0;
  selectTerms: Terms = new Terms();
  txtSearch: string = '';

  cols: any[];
  loading: boolean;
  @ViewChild('confirmDialodDel') public confirmDialodDel: ModalDirective;
  idDelete: Number = 0;
  title: string = '';
  constructor(private utilities: Utilities, private router: Router,
    private activeRouter: ActivatedRoute, private messageService: MessageService,
    private loaderService: LoaderService, private ultilities: Utilities,
    private articlesService: ArticlesService) {
      this.ultilities.closeAutoMenu();
  }

  ngOnInit() {

    this.loadterms();
    setTimeout(() => {
      if (localStorage.getItem('addArticles')) {
        this.ultilities.showSuccess(localStorage.getItem('addArticles'));
        localStorage.removeItem('addArticles')
      } else if (localStorage.getItem('editArticles')) {
        this.ultilities.showSuccess(localStorage.getItem('editArticles'));
        localStorage.removeItem('editArticles')
      }
    }, 300);
  }
  loadterms() {
    this.loaderService.show();
    this.articlesService.getAllsPadding(1, 10, 'id', 'desc', this.txtSearch).subscribe((response: any) => {
      this.listTerms = [];
      this.totalPage = 0;
      if (response && response.resultCode === 200) {
        let page = new PagerData();
        page = response.object;
        if (page && page.content) {
          this.listTerms = page.content;
          this.totalPage = page.totalRow;
        }
        // this.listProduct = response;
      }
      this.loaderService.hide()
    }, error => {
      this.listTerms = [];
      this.totalPage = 0;
      this.loaderService.hide();
    });
  }
  create() {
    this.router.navigate(['/tranzi-admin/article-create']);
  }
  paginate(event) {
    if (event) {
      this.listTerms = [];
      // this.totalPage = 0;
      // tslint:disable-next-line:max-line-length
      this.articlesService.getAllsPadding(event.page ? (event.page + 1) : 1, event.rows ? event.rows : 10, 'id', 'desc', this.txtSearch).subscribe((response: any) => {
        // tslint:disable-next-line:no-console
        // console.log(response)
        if (response && response.resultCode === 200) {
          let page = new PagerData();
          page = response.object;
          if (page && page.content) {
            this.listTerms = page.content;
            this.totalPage = page.totalRow;
            // this.totalPage = this.totalPage.s
          }
          // this.listProduct = response;
        }
        this.loaderService.hide()
      }, error => {
        this.totalPage = 0
        this.loaderService.hide();
      });
    }

  }
  seSelectterms(event) {
    this.selectTerms = new Terms()
    if (event && event.data) {
      this.selectTerms = event.data;
    }
  }
  routingUpdate(id) {
    if (id) {
      this.router.navigate(['/tranzi-admin/article-update/' + id]);
    }
  }
  delete() {
    // this.listterms = [];
    this.confirmDialodDel.hide();
    if (this.idDelete) {
      this.loaderService.show();
      this.articlesService.deleteId(this.idDelete).subscribe((response: any) => {
        if (response && response.resultCode === 200) {
          // Dung table
          this.loadterms();
          this.ultilities.showSuccess('Xóa thành công');
        } else if (response && response.resultCode === 205) {
          this.ultilities.showError('Thể loại đã liên kết sản phẩm. Không thể xóa.')
        } else if (response && response.resultCode === 204) {
          this.ultilities.showError(response.errorMessage ? response.errorMessage : 'Có lỗi xảy ra. Liên hệ Admin để kiểm tra lại')
        }
        this.loaderService.hide()
        // this.ultilities.showSuccess('Delete Success terms')
      }, error => {
        this.loaderService.hide();
        this.ultilities.showError('Delete Error terms')
      });
    }
  }
  deleteCheck(ca: Terms) {
    if (ca) {
      this.title = 'Bạn có muốn xóa Thể loại: ' + ca.name + ' không?';
      this.idDelete = ca.id
      this.confirmDialodDel.show();
    }
  }
  
}

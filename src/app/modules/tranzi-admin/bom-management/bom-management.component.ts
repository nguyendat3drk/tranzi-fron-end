import { Component, OnInit, Input, ViewChild, ElementRef } from '@angular/core';
import { FileUploadModule } from 'primeng/fileupload';
import { Utilities } from './../../../shared/services/utilities.service';
import { Router } from '@angular/router';
import { Manufactures } from '../../../shared/models/Manufactures';
import { CategoriesService } from '../../../shared/services/http/categories.service';
import { LoaderService } from '../../../shared/services/loader.service';
import { ManufactService } from '../../../shared/services/http/manufact.service';
import { PagerData } from '../../../shared/entity/pagerData';
import { AdminBom } from '../../../shared/models/AdminBom';
import { AdminBomService } from '../../../shared/services/http/adminBom.service';
import { ConfirmService } from '../../../shared/confirm-dialog/ConfirmService';
import { ModalDirective } from 'ngx-bootstrap';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'bom-management',
  templateUrl: './bom-management.component.html',
  styleUrls: ['./bom-management.component.scss'],
})
// tslint:disable-next-line:component-class-suffix
export class BomManagementomponent implements OnInit {
  page: number = 1;
  txtSearch: string = '';
  listAdminBom: AdminBom[] = [];
  totalPage: number = 0;
  selectAdminBom: AdminBom = new AdminBom();
  @ViewChild('confirmDialodDel') public confirmDialodDel: ModalDirective;
  idDelete: Number = 0;
  title: string = '';
  constructor(private router: Router, private _confirm: ConfirmService,
    private loaderService: LoaderService, private ultilities: Utilities,
    private adminBomService: AdminBomService) {
      this.ultilities.closeAutoMenu();
  }
  viewDate(event) {
    if (event && Number(event) > 0) {
      return this.ultilities.convertDateToStringDateVN(new Date(Number(event) * 1000));
    }
    return ''
  }
  ngOnInit() {
    this.loadAdminBom();
  }
  loadAdminBom() {
    this.loaderService.show();
    this.adminBomService.getAllsPadding(1, 10, 'id', 'desc', this.txtSearch).subscribe((response: any) => {
      if (response && response.resultCode === 200) {
        let page = new PagerData();
        this.listAdminBom = []
        this.totalPage = 0;
        page = response.object;
        if (page && page.content) {
          this.listAdminBom = page.content;
          this.totalPage = page.totalRow;
        }
      }
      this.loaderService.hide()
    }, error => {
      this.listAdminBom = []
      this.totalPage = 0;
      this.loaderService.hide();
    });
  }
  createCategori() {
    this.router.navigate(['/tranzi-admin/manufactures-create']);
  }
  paginate(event) {
    if (event) {
      this.listAdminBom = [];
      this.totalPage = 0;
      this.loaderService.show()
      this.adminBomService.getAllsPadding(event.page ? (event.page + 1) : 1, event.rows ?
        event.rows : 10, 'id', 'desc', this.txtSearch).subscribe((response: any) => {
          // tslint:disable-next-line:no-console
          // console.log(response)
          if (response && response.resultCode === 200) {
            let page = new PagerData();
            page = response.object;
            if (page && page.content) {
              this.listAdminBom = page.content;
              this.totalPage = page.totalRow;
            }
            // this.listProduct = response;
          }
          this.loaderService.hide()
        }, error => {
          this.loaderService.hide();
        });
    }

  }

  setSelectAdminBom(event) {
    this.selectAdminBom = new AdminBom()
    if (event && event.data) {
      this.selectAdminBom = event.data;
    }
  }
  routingUpdate(id) {
    if (id) {
      this.router.navigate(['/tranzi-admin/manufactures-update/' + id]);
    }
  }
  deleteAdminBom() {
    if (this.idDelete) {
      this.loaderService.show();
      this.confirmDialodDel.hide();
      this.adminBomService.delete(this.idDelete).subscribe((response: any) => {
        if (response && response.resultCode === 200) {
          this.loadAdminBom();
          this.ultilities.showSuccess('Xóa thành công');
        } else if (response && response.resultCode === 205) {
          this.ultilities.showError('Bom đã liên kết. Không thể xóa.')
        } else if (response && response.resultCode === 204) {
          this.ultilities.showError(response.errorMessage ? response.errorMessage : 'Có lỗi xảy ra. Liên hệ Admin để kiểm tra lại')
        }
        this.loaderService.hide()
        // this.ultilities.showSuccess('Delete Success Manufactures')
      }, error => {
        this.loaderService.hide();
        this.ultilities.showError('Delete Error Manufactures')
      });
    }
  }
  deleteCheck(pro: AdminBom) {
    if (pro) {
      this.title = 'Bạn có muốn xóa Bom: ' + pro.name + ' không?';
      this.idDelete = pro.id
      this.confirmDialodDel.show();
    }
  }

}

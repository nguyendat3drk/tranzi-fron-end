import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BomManagementomponent } from './bom-management.component';

describe('BomManagementomponent', () => {
  let component: BomManagementomponent;
  let fixture: ComponentFixture<BomManagementomponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BomManagementomponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BomManagementomponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

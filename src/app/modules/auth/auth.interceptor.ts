import { Injectable } from '@angular/core';
import { HttpErrorResponse, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Oauth2Service } from '../../shared/services/http/oauth2.service';
import { catchError } from 'rxjs/internal/operators';
import { throwError } from 'rxjs/index';
import { Location } from '@angular/common';
import { MessageService } from 'primeng/components/common/messageservice';
import { Router } from '@angular/router';
// declare var keyApp: any;
@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  constructor(
    private auth: Oauth2Service,
    private location: Location, private router: Router,
    private messageService: MessageService) {
  }

  intercept(req: HttpRequest<any>, next: HttpHandler) {
    // Get the auth token from the service.
    const authToken = this.auth.token.value;

    // Clone the request and replace the original headers with
    // cloned headers, updated with the authorization.
    if (this.location.path().indexOf('auth/') !== -1) {
      return next.handle(req);
    }
    const authReq = req.clone({
      headers: req.headers.set('Authorization', `${authToken.token}`),
    });

    // send cloned request with header to the next handler.
    return next.handle(authReq).pipe(
      catchError((err: HttpErrorResponse) => {
        console.error(err);
        if (err.status === 403 || err.status === 405) {
          this.messageService.add({ detail: 'Phiên hết hạn, xin hãy đăng nhập lại', severity: 'warn' });
          this.auth.logout();
        }
        this.auth.logout();
        return throwError(err);
      }),
    );
    // return next.handle(authReq).pipe(
    //   catchError((error: HttpErrorResponse | any) => {
    //     console.error(error);
    //     if (error.status === 403) {
    //       this.messageService.add({ detail: 'Phiên hết hạn, xin hãy đăng nhập lại', severity: 'warn' });
    //       this.auth.logout();
    //     }
    //     return throwError(error);
    //   }),
    // );
  }
}

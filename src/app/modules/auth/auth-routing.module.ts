import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { RedirectIfAuthenticatedGuard } from '../../shared/guards/redirect-if-authenticated.guard';

const routes: Routes = [
  {
    path: 'auth',
    children: [
      { path: 'login', component: LoginComponent, canActivate: [RedirectIfAuthenticatedGuard] },
      { path: 'register', component: RegisterComponent },
    ],
  },
];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
  ],
  declarations: [],
  exports: [RouterModule],
  providers: [RedirectIfAuthenticatedGuard],
})
export class AuthRoutingModule { }

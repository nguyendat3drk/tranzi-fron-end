import { Component, OnInit } from '@angular/core';
import { Oauth2Service } from '../../../shared/services/http/oauth2.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
@Component({
  // tslint:disable-next-line:component-selector
  selector: 'login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  isShowLoader: boolean = false;
  loginForm: FormGroup;
  blCheck: boolean = false;
  errerM: string = 'Tên tài khoản hoặc mật khẩu không chính xác';

  constructor(
    private oauth2: Oauth2Service,
    private fb: FormBuilder,
    private router: Router) {
    this.loginForm = this.fb.group({
      username: ['', Validators.required],
      password: ['', Validators.required],
    });
    localStorage.clear();
  }

  get username() {
    return this.loginForm.get('username');
  }

  get password() {
    return this.loginForm.get('password');
  }

  onSubmit() {
    this.isShowLoader = true;
    // tslint:disable-next-line:prefer-const
    let da = {
      'accountName': this.username.value,
      'password': this.password.value,
      'isRememberPassword': true,
    };
    setTimeout(() => {
      if (this.isShowLoader) {
        this.isShowLoader = false;
      }
    }, 10000);
    this.blCheck = false;
    this.oauth2.login(da).subscribe((response: any) => {
      this.isShowLoader = false;
      // tslint:disable-next-line:no-console
      // console.log('responseresponseresponse=', response);
      if (response && response.resultCode === 200) {
        // console.log(response);
        this.oauth2.saveAuthToLocalStorage(response.object);
        // this.router.navigate(['/tranzi-admin']);
        location.reload();
      } else {
        this.blCheck = true;
        this.errerM = 'Đăng nhập thất bại'
      }
    }, (error) => {
      this.isShowLoader = false;
      // tslint:disable-next-line:no-console
      console.log(error)
      this.blCheck = true;
      this.errerM = 'Đăng nhập thất bại'
    });
  }

  login() {
    this.isShowLoader = true;
    this.oauth2.getToken(this.username.value, this.password.value).subscribe(
      token => {
        this.oauth2.saveToLocalStorage(token);
        this.isShowLoader = false;
        // TODO What about garage?
        // if (token && token.type === 1) {
        //   this.router.navigate(['/tranzi-admin']);
        // } else if (token && token.type === 2) {
        //   this.router.navigate(['/garage']);
        // }
        // localStorage.setItem('permission-role', JSON.stringify(token));
      }, error => {
        this.isShowLoader = false;
        this.blCheck = true;
        this.oauth2.logout();
      });
  }

  ngOnInit() {
  }
}

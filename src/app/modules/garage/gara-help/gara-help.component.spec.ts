import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GaraHelpComponent } from './gara-help.component';

describe('BvHelpComponent', () => {
  let component: GaraHelpComponent;
  let fixture: ComponentFixture<GaraHelpComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GaraHelpComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GaraHelpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

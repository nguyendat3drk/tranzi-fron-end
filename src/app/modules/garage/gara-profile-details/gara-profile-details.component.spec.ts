import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GaraProfileDetailsComponent } from './gara-profile-details.component';

describe('BvProfileDetailsComponent', () => {
  let component: GaraProfileDetailsComponent;
  let fixture: ComponentFixture<GaraProfileDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GaraProfileDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GaraProfileDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

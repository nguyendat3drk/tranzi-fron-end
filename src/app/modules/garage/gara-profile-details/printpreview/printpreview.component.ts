import {
  Component, OnInit, Input, Output, EventEmitter
  , ElementRef, ViewChild, HostListener,
} from '@angular/core';
import { LoaderService } from '../../../../shared/services/loader.service';
import { VoucherDetailService } from '../../../../shared/services/http/voucher-detail.service';
import { Utilities } from '../../../../shared/services/utilities.service';
import { PDFDocumentProxy } from 'ng2-pdf-viewer';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'printpreview',
  templateUrl: './printpreview.component.html',
  styleUrls: ['./printpreview.component.scss'],
})
export class PrintpreviewComponent implements OnInit {
  @Output() onBack = new EventEmitter<boolean>()
  @Input() type = '';
  @Input() title = '';
  @Input() printViewData: any;
  @ViewChild('printCotent') public printPanel: ElementRef
  fileSavingTypes = ['PDF', 'WORD', 'EXCEL']
  fileSavingType = this.fileSavingTypes[0]
  pages = []
  currentPage = 0
  pageHeight = 1133
  headerHeight = 61
  notPagingHeight = 10
  isFixHeader = false
  styleForPDF = ''
  logoUrl = ''
  pdfUrl = ''
  pdf: any;
  format: string;

  constructor(private voucherService: VoucherDetailService, private loader: LoaderService,
    private utilities: Utilities) { }

  ngOnInit() {
    this.loadFilePDF()
  }
  back() {
    this.onBack.emit(false)
  }
  print() {
    window.print()
  }
  save() {
    const currentPath = this.getCurrentPathApi();
    if (this.fileSavingType === 'PDF') {
      this.exportToPDF()
    } else if (this.fileSavingType === 'WORD') {
      this.downloadFile(currentPath, 'doc')
    } else if (this.fileSavingType === 'EXCEL') {
      this.downloadFile(currentPath, 'xls')
    }
  }
  pageChange(page) {
    if (this.pages && page >= 0 && page < this.pages.length) {
      this.currentPage = page;
    }
    window.scrollTo(0, this.pageHeight * this.currentPage)
  }
  @HostListener('window:scroll') onScroll() {
    const currentPosition = document.documentElement.scrollTop
    this.isFixHeader = currentPosition >= this.headerHeight
    this.currentPage = Math.floor(currentPosition / this.pageHeight)
  }

  loadPdfComplete(pdf: PDFDocumentProxy) {
    this.pdf = pdf;
    this.pages = this.getPages()
    this.loader.hide()
  }

  loadPdfError() {
    this.loader.hide()
  }

  private getPages(): Array<number> {
    if (!this.pdf || this.pdf.numPages === 0) return [0]

    // tslint:disable-next-line:prefer-const
    let pageArray = []
    for (let i = 0; i < this.pdf.numPages; i++) {
      pageArray.push(i)
    }
    return pageArray
  }

  private exportToPDF() {
    // tslint:disable-next-line:prefer-const
    let a = document.createElement('a');
    document.body.appendChild(a)
    a.setAttribute('style', 'display: none');
    a.href = this.pdfUrl;
    a.download = this.title + '.pdf';
    a.click();
    window.URL.revokeObjectURL(this.pdfUrl);
    a.remove()
  }

  private loadFilePDF() {
    this.loader.show()
    const currentPath = this.getCurrentPathApi();
    this.format = 'pdf';
    
  }

  private downloadFile(path, type) {
   
  }

  private getCurrentPathApi() {
    let path = ''
    switch (this.type) {
      case '1':
        path = 'claimRequestQuotation'
        break;
      case '2':
        path = 'getClaimWaitingApprovalQuotation'
        break;
      case '3':
        path = 'getClaimWaitingGuarantee'
        break;
      case '4':
        path = 'getClaimNotExportInvoice'
        break;
      case '5':
        path = 'getClaimWaitingPayment'
        break;
      case '6':
        path = 'getClaimPaid'
        break;
    }
    return path;
  }

  private downloadArrayBuffer(data: ArrayBuffer, fileName: string, fileType: string) {
    const file = new Blob([data], { type: 'application/' + fileType })
    const fileURL = URL.createObjectURL(file)
    // tslint:disable-next-line:prefer-const
    let a = document.createElement('a');
    document.body.appendChild(a)
    a.setAttribute('style', 'display: none');
    a.href = fileURL;
    a.download = fileName + '.' + fileType;
    a.click();
    window.URL.revokeObjectURL(fileURL);
    a.remove()
  }
}

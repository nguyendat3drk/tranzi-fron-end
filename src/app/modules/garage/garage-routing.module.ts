import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { GaraDashboardComponent } from './gara-dashboard/gara-dashboard.component';
import { GaraAppShellComponent } from './gara-app-shell/gara-app-shell.component';
import { PageNotFoundComponent } from '../../shared/components/page-not-found/page-not-found.component';
import { GaraHelpComponent } from './gara-help/gara-help.component';
import { GaraProfileDetailsComponent } from './gara-profile-details/gara-profile-details.component';
import { GaraDivisionJobComponent } from './gara-division-job/gara-division-job.component';
import { PageForbiddenComponent } from '../../shared/components/page-forbidden/page-forbidden.component';
import { GaraDocumentDetailsComponent } from './gara-document-details/gara-document-details.component';

const routes: Routes = [
  {
    path: '',
    component: GaraAppShellComponent,
    children: [
      { path: '', pathMatch: 'full', redirectTo: 'gara-dashboard' },
      { path: 'gara-dashboard', component: GaraDashboardComponent },
      { path: 'detail-profile', component: GaraProfileDetailsComponent },
      { path: 'detail-profile/:type/:total', component: GaraProfileDetailsComponent },
      { path: 'document-gara/:claimId', component: GaraDocumentDetailsComponent },
      { path: 'document-gara/:claimId/:type', component: GaraDocumentDetailsComponent },
      { path: 'gara-help', component: GaraHelpComponent },
      { path: 'gara-division-job', component: GaraDivisionJobComponent },
      { path: 'page-forbidden', component: PageForbiddenComponent },
    ],
  },
  { path: '**', component: PageNotFoundComponent },
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  declarations: []
})
export class GarageRoutingModule { }

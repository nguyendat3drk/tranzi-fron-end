import { Routes } from '@angular/router';

export const MENU_ITEMS: Routes = [
  {
    path: 'pages',
    children: [
      {
        path: '/gara/search',
        data: {
          menu: {
            title: 'general.menu.search',
            icon: '',
            selected: false,
            expanded: false,
            order: 0,
            hidden: false,
          },
        },
        children: [],
      },
      {
        path: '/gara/approve',
        data: {
          menu: {
            title: 'general.menu.approval',
            icon: '',
            selected: false,
            expanded: false,
            order: 0,
            hidden: false,
          },
        },
      },
      {
        path: '/gara/claim/create',
        data: {
          menu: {
            title: 'general.menu.contact',
            icon: '',
            selected: false,
            expanded: false,
            order: 0,
            hidden: false,
          },
        },
      },
      {
        path: '/gara/document',
        data: {
          menu: {
            title: 'general.menu.document',
            icon: '',
            selected: false,
            expanded: true,
            order: 0,
            hidden: false,
          },
        },
        children: [
          {
            path: '/gara/view-document',
            data: {
              menu: {
                title: 'general.menu.view_document',
                icon: 'fa fa-circle',
                selected: false,
                expanded: false,
                order: 0,
                hidden: false,
              },
            },
          },
          {
            path: '/gara/update-document',
            data: {
              menu: {
                title: 'general.menu.update_document',
                icon: 'fa fa-circle',
                selected: false,
                expanded: false,
                order: 0,
                hidden: false,
              },
            },
          },
        ],
      },
      {
        path: '/gara/dashboard',
        data: {
          menu: {
            title: 'general.menu.dashboard',
            icon: 'fa fa-tachometer',
            selected: false,
            expanded: false,
            order: 0,
            hidden: true,
          },
        },
      },
      {
        path: 'configuration',
        data: {
          menu: {
            title: 'general.menu.configuration',
            icon: 'ion-gear-b',
            pathMatch: 'ewr',
            selected: false,
            expanded: true,
            order: 500,
            hidden: true,
          },
        },
        children: [
          {
            path: 'user-management',
            data: {
              menu: {
                title: 'general.menu.user_management',
                icon: 'fa fa-tachometer',
                selected: false,
                expanded: false,
                order: 0,
                hidden: true,
              },
            },
          },
        ],
      },
    ],
  },
];

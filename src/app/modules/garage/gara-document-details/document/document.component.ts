import { Component, OnInit, Output, Input, EventEmitter, ElementRef, ViewChild } from '@angular/core';
import { VoucherDetailService } from '../../../../shared/services/http/voucher-detail.service';
import { LoaderService } from '../../../../shared/services/loader.service';
import { Utilities } from '../../../../shared/services/utilities.service';
import { CommentParams } from '../../../../shared/models/comment-params';
import { DmContactService } from '../../../../shared/services/http/dm-contact.service';
import { moment } from '../../../../../../node_modules/ngx-bootstrap/chronos/test/chain';

import { Router, ActivatedRoute } from '@angular/router';
import { ClaimCustomer } from '../../../../shared/models/claimCustomer';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'document',
  templateUrl: './document.component.html',
  styleUrls: ['./document.component.scss'],
})
export class DocumentComponent implements OnInit {
  @ViewChild('inputMessage') private inputMessage: ElementRef
  // Chat
  @Input() claimId: number
  @Input() claimCustomer: ClaimCustomer
  cateDocumentId: number
  garaId: number

  @Output()
  public documentId: number = 1
  showAttachmentDetail: boolean = false
  documentName: string
  totalFile: number = 0;

  @Output()
  public focusTabInfoLost = new EventEmitter();

  classificationExchange = {
    Chung_Tu: '1', // Loại trao đổi là: Chứng từ
    Bao_Gia: '2', // Loại trao đổi là Báo giá
  }

  classificationSending = {
    gara_send: '1',
    bv_send: '2',
  }
  urlupload: string = '';
  documentStatsuses = [
    { id: '0', name: 'Không chấp nhận' },
    { id: '1', name: 'Chấp nhận' },
  ]


  garaDocuments: Array<Document> = []
  insuredName: string = '';
  plateNumber: string = '';
  totalStatus: string = '';
  garaChats = []
  documents: Array<Document> = []
  commentParams = new CommentParams()

  isGarage = false
  canEdit = false
  uploadedFilesSuccess: any[] = []
  uploadedFilesError: any[] = []
  uploadedFilesErrorInvalid: any[] = []
  progressBarPercent = 20
  subscribeParams: any;

  constructor(
    private voucherDetailService: VoucherDetailService,
    private loaderService: LoaderService,
    private ultilities: Utilities,
    private activeRouter: ActivatedRoute) {
    this.urlupload = this.ultilities.getUrlServerHost() + '/document/fileUpload'
  }

  ngOnInit() {
    this.garaId = this.ultilities.getCurrentGaraId()
    this.subscribeParams = this.activeRouter.params.subscribe((params) => {
      // const garaId = <any>params.garaId;

    });

    this.initialCommentParams()
    this.getGaraDocument(this.claimId, this.garaId, this.classificationExchange.Chung_Tu)
    this.isGarage = this.ultilities.isGarage()
    this.canEdit = this.ultilities.checkRole('ROLE_DOCUMENT_SEND_MESSAGE')
  }

  btnUploadClick(documentId, fileUpload) {
    if (!this.canEdit) return

    this.documentId = documentId
    fileUpload.advancedFileInput.nativeElement.click()
  }

  onSelectedFiles(modal, fileUpload) {
    this.totalFile = 0;
    this.progressBarPercent = 20
    this.uploadedFilesError = []
    this.uploadedFilesErrorInvalid = [];
    this.uploadedFilesSuccess = []
    if (fileUpload.advancedFileInput.nativeElement.files) {
      this.totalFile = fileUpload.files.length;
      for (let i = 0; i < fileUpload.advancedFileInput.nativeElement.files.length; i++) {
        const element = fileUpload.advancedFileInput.nativeElement.files[i];
        const type = fileUpload.advancedFileInput.nativeElement.files[i].type;
        const strTypeCheck = type.split('/')[0];
        const strTypeTailFile = type.split('/')[1];
        if (('' + strTypeCheck).toUpperCase().trim() === 'VIDEO' && (('' + strTypeTailFile).toUpperCase().trim() !== 'MP4'
          && ('' + strTypeTailFile).toUpperCase().trim() !== '3GP' && ('' + strTypeTailFile).toUpperCase().trim() !== '3GPP')) {
          // tslint:disable-next-line:prefer-const
          let dto = {
            value: fileUpload.advancedFileInput.nativeElement.files[i].name,
            type: fileUpload.advancedFileInput.nativeElement.files[i].type,
          }
          this.uploadedFilesErrorInvalid.push(dto)
        } else if (element.size > 10000000 || element.size === 0) {
          this.uploadedFilesError.push(element)
        }

      }
    }
    if (fileUpload && fileUpload.msgs) {
      for (let i = 0; i < fileUpload.msgs.length; i++) {
        const element = fileUpload.msgs[i];
        if (element) {
          const maxsize = element.detail.indexOf('maximum');
          let valueDetail = '';
          let valueType = '';
          valueDetail = element.summary.split(':')[0];
          if (maxsize < 0) {
            valueType = 'Không đúng định dạng'
            // tslint:disable-next-line:prefer-const
            let dto = {
              value: valueDetail,
              type: valueType,
            }
            this.uploadedFilesErrorInvalid.push(dto)
          }
        }
      }
    }
    const checkNumberError = Number(this.uploadedFilesErrorInvalid ? this.uploadedFilesErrorInvalid.length : 0) +
      Number(this.uploadedFilesError ? this.uploadedFilesError.length : 0);
    if (this.totalFile === checkNumberError) {
      this.progressBarPercent = 100
      this.ultilities.showError('Cập nhật thất bại')
    }
    modal.show()
    fileUpload.upload()
  }

  getTotalDocuments() {
    let totalUpdated = 0
    let total = 0
    for (let i = 0; i < this.documents.length; i++) {
      total += 1
    }
    return totalUpdated + '/' + total
  }

  onBeforUpload(event, fileUpload) {
    // tslint:disable-next-line:prefer-const
    let interval = setInterval(() => {
      if (this.progressBarPercent < 90) {
        this.progressBarPercent = this.progressBarPercent + 10;
      }
      if (this.progressBarPercent >= 100) {
        this.progressBarPercent = 100;
        clearInterval(interval);
      }
    }, 100);
    this.uploadedFilesSuccess = []
    if (fileUpload.files) {
      for (let i = 0; i < fileUpload.files.length; i++) {
        const element = fileUpload.files[i];
        const type = fileUpload.files[i].type;
        const strTypeCheck = type.split('/')[0];
        const strTypeTailFile = type.split('/')[1];
        if (element.size > 0) {
          if (('' + strTypeCheck).toUpperCase().trim() === 'VIDEO' && (('' + strTypeTailFile).toUpperCase().trim() !== 'MP4'
            && ('' + strTypeTailFile).toUpperCase().trim() !== '3GP' && ('' + strTypeTailFile).toUpperCase().trim() !== '3GPP')) {
          } else {
            this.uploadedFilesSuccess.push(element)
          }
        }
      }
    }
    event.xhr.setRequestHeader('Authorization', 'Bearer ' + this.ultilities.getToken())
    if (event.formData) {
      event.formData.append('documentId', this.documentId)
      event.formData.append('claimId', this.claimId)
    }
  }

  onUploadSuccess(event, modal) {
    setTimeout(() => {
      const checkNumberError = Number(this.uploadedFilesErrorInvalid ? this.uploadedFilesErrorInvalid.length : 0) +
        Number(this.uploadedFilesError ? this.uploadedFilesError.length : 0);
      this.progressBarPercent = 100

      if (event && event.xhr && event.xhr.status !== 200 || this.totalFile === checkNumberError) {
        if (modal) modal.hide()
        this.ultilities.showError('Cập nhật thất bại')
      } else {
        this.ultilities.showSuccess('Cập nhật thành công')
      }
    }, 1000);

    if (event && event.xhr && event.xhr.response) {
      const document = JSON.parse(event.xhr.response)
      if (document)
        this.getGaraDocument(this.claimId, this.garaId, this.classificationExchange.Chung_Tu)
    }
  }

  onUploadError(event, modal) {
    setTimeout(() => {
      this.progressBarPercent = 100
      if (modal) modal.hide()
      if (event && event.xhr) {
        const response = JSON.parse(event.xhr.response)
        let message = 'Cập nhật không thành công'
        if (response && response.message) {
          message = response.message
        }
        this.ultilities.showError(message)
      }
    }, 1000)
  }

  postComment() {
    if (!this.canEdit) return

    if (!this.commentParams.content || !this.commentParams.content.trim()) {
      this.inputMessage.nativeElement.focus()
      return
    }

    this.commentParams.beneficiaryId = this.garaId
    this.loaderService.show()
    this.voucherDetailService.postComment(this.commentParams).then(() => {
      this.getGaraDocument(this.claimId, this.garaId, this.classificationExchange.Chung_Tu)
      this.commentParams.content = '';
      this.loaderService.hide();
      this.inputMessage.nativeElement.focus();
    })
      .catch(() => {
        this.loaderService.hide();
      })
  }
  formatTextDateVN(obj) {
    if (!obj) return ''

    return moment(obj, 'DD/MM/YYYY HH:mm:ss').format('DD/MM/YYYY HH:mm')
  }
  changeTabTonThat() {
    this.focusTabInfoLost.emit('btnTonthat')
  }

  viewAttachmentDetail(docId, cateDocumentId,  documentName) {
    this.documentId = docId
    this.documentName = documentName
    this.cateDocumentId = cateDocumentId
    this.showAttachmentDetail = true
  }
  attachmentBack(event) {
    this.showAttachmentDetail = event
    this.getGaraDocument(this.claimId, this.garaId, this.classificationExchange.Chung_Tu)
  }

  private getGaraDocument(claimId, garaId, classificationExchange) {
    this.loaderService.show()
    this.garaDocuments = []
    this.garaChats = []
    // console.log("agdfdfdfd ", claimId, garaId)
    this.voucherDetailService.getBvDocumentsForGarage(claimId, garaId, classificationExchange).then((result: any) => {

      if (!result) {
        this.loaderService.hide()
        return
      }

      if (result.documentList && result.documentList.length > 0) {
        this.garaDocuments = result.documentList
      }

      if (result.commentList && result.commentList.length > 0)
        this.garaChats = result.commentList
      this.insuredName = result.insuredName
      this.plateNumber = result.plateNumber;
      this.updateListData()
      this.loaderService.hide()
    })
      .catch(() => {
        this.updateListData();
        this.loaderService.hide();
      })
  }
  private updateListData() {
    this.documents = [
      ...this.garaDocuments,
    ]
  }
  private initialCommentParams() {
    this.commentParams.claimId = this.claimId
    this.commentParams.beneficiaryId = this.garaId
    this.commentParams.classificationExchange = this.classificationExchange.Chung_Tu
    this.commentParams.classificationSending = this.classificationSending.gara_send
  }
}

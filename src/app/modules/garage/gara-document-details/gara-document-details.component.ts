import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ConfirmService } from '../../../shared/confirm-dialog/ConfirmService';
import { AccountService } from '../../../shared/services/http/account.service';
import { TranslateService } from '@ngx-translate/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { Utilities } from '../../../shared/services/utilities.service';
import { ClaimCustomer } from '../../../shared/models/claimCustomer';
import { ClaimService } from '../../../shared/services/http/claim.service';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'gara-document-details',
  templateUrl: './gara-document-details.component.html',
  styleUrls: ['./gara-document-details.component.scss'],
})
export class GaraDocumentDetailsComponent implements OnInit {

  focusSelected: string;
  claimId: number

  tabId = {
    Information_Loss: '1',
    Document_Gara: '2',
    Proposed_Repair: '3',
    Guarantee: '4',
  }

  tabName = {
    Information_Loss: 'btnInformation',
    Document_Gara: 'btnDocument',
    Proposed_Repair: 'btnRepair',
    Guarantee: 'btnGuarantee',
  }
  isGetList = false;
  claimCustomer: ClaimCustomer = new ClaimCustomer();
  isRejectedQuote = false

  constructor(private router: Router,
    private claimService: ClaimService,
    private ultilities: Utilities,
    private activeRouter: ActivatedRoute,
    private location: Location) {
  }

  ngOnInit() {
    this.activeRouter.params.subscribe((params) => {
      this.claimId = +params.claimId
      const type = params.type

      if (this.claimId) {
        this.getClaimCustomer();
      }

      this.isGetList = this.ultilities.checkRole('ROLE_DOCUMENT_GARA_LIST')
      if (!this.isGetList) {
        this.router.navigate(['/bao-viet/page-forbidden']);
      }
      switch (type) {
        case this.tabId.Document_Gara:
          this.focusSelected = this.tabName.Document_Gara;
          break;

        case this.tabId.Proposed_Repair:
          this.focusSelected = this.tabName.Proposed_Repair;
          break;

        case this.tabId.Guarantee:
          this.focusSelected = this.tabName.Guarantee;
          break;
        default:
          this.focusSelected = this.tabName.Information_Loss;
          break;
      }
    });
  }

  onTabChange(event) {
  }
  onclickTab(event) {
    this.focusSelected = event;
    let tab = null
    switch (event) {
      case this.tabName.Document_Gara:
        tab = this.tabId.Document_Gara;
        break;

      case this.tabName.Proposed_Repair:
        tab = this.tabId.Proposed_Repair;
        break;

      case this.tabName.Guarantee:
        tab = this.tabId.Guarantee;
        break;

      case this.tabName.Information_Loss:
        tab = this.tabId.Information_Loss;
        break;
    }
    const newUrl = this.getUrl(tab)
    this.location.go(newUrl)
  }
  private getUrl(tab) {
    return '/garage/document-gara/' + this.claimId + (tab ? '/' + tab : '')
  }
  private getClaimCustomer() {
    this.claimCustomer = new ClaimCustomer();
    this.claimService.getClaimCustomer(this.claimId).subscribe((response: any) => {
      if (response) {
        this.claimCustomer = response;
      }
    }, (error) => {

    });
  }
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../../../shared/shared.module';
import { HttpClientModule } from '@angular/common/http';
import { DataTableModule } from 'primeng/datatable';
import { ContextMenuModule } from 'primeng/contextmenu';
import { ModalModule } from 'ngx-bootstrap';
import { ConfirmDialogModule } from './../../../shared/confirm-dialog/confirm-module';
import { PdfViewerModule } from 'ng2-pdf-viewer';
import { CalendarModule } from 'primeng/calendar';
import { TextMaskModule } from 'angular2-text-mask';
import { NgxMaskModule } from 'ngx-mask';
import { DocumentComponent } from './document/document.component';
import { FileUploadModule } from 'primeng/primeng';
import { CurrencyMaskModule } from 'ng2-currency-mask';
import { InformationlostComponent } from './informationlost/informationlost.component';
import { GaraDocumentDetailsComponent } from './gara-document-details.component';
import { GuaranteeComponent } from './guarantee/guarantee.component';
import { RepairComponent } from './Repair/repair.component';
import { BvDateMaskModule } from '../../../shared/components/date-mask/date-mask.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    HttpClientModule,
    DataTableModule,
    ContextMenuModule,
    ModalModule.forRoot(),
    ConfirmDialogModule,
    PdfViewerModule,
    CalendarModule,
    TextMaskModule,
    NgxMaskModule.forRoot(),
    FileUploadModule,
    CurrencyMaskModule,
    BvDateMaskModule,
  ],
  declarations: [
    GaraDocumentDetailsComponent,
    InformationlostComponent,
    DocumentComponent,
    GuaranteeComponent,
    RepairComponent,
  ],
  exports: [RouterModule],
  entryComponents: [],
})
export class GaraDocumentDetailsModule { }

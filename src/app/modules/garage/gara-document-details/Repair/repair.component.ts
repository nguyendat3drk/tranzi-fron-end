import { Component, OnInit, Input, ElementRef, ViewChild, Output, EventEmitter } from '@angular/core';
import { VoucherDetailService } from '../../../../shared/services/http/voucher-detail.service';
import { LoaderService } from '../../../../shared/services/loader.service';
import { Utilities } from '../../../../shared/services/utilities.service';
import { CommentParams } from '../../../../shared/models/comment-params';
import { moment } from 'ngx-bootstrap/chronos/test/chain';
import { TranslateService } from '@ngx-translate/core';
import { ModalDirective } from 'ngx-bootstrap';
import { QuotationDetailService } from '../../../../shared/services/http/QuotationDetailService';
import { ClaimCustomer } from '../../../../shared/models/claimCustomer';
import { ConfirmService } from '../../../../shared/confirm-dialog/ConfirmService';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/internal/Observable';
import { catchError, debounceTime, map, switchMap } from 'rxjs/internal/operators';
import { of } from 'rxjs';
import { Accessary } from '../../../../shared/models/Accessary';
import { AccessaryService } from '../../../../shared/services/http/accessary.service';
import { MenuItem } from 'primeng/primeng';


@Component({
    // tslint:disable-next-line:component-selector
    selector: 'repair',
    templateUrl: './repair.component.html',
    styleUrls: ['./repair.component.scss'],
})
export class RepairComponent implements OnInit {
    @ViewChild('inputMessage') private inputMessage: ElementRef
    @Input() claimId: number;
    @Input() claimCustomer: ClaimCustomer
    @Output()
    public onInit = new EventEmitter();
    searchParams: string = '';
    canEdit = true
    commentParams = new CommentParams()
    garaChats = []
    garaId: number;
    items: MenuItem[];
    classificationExchange = {
        Chung_Tu: '1', // Loại trao đổi là: Chứng từ
        Bao_Gia: '2', // Loại trao đổi là Báo giá
    }
    classificationSending = {
        gara_send: '1',
        bv_send: '2',
    }
    // DETAIL QUOTATION
    currentImage: any = null;
    currentImageIndex = 0

    searchString: string = '';
    totalPriveAffter: number = 0;
    totalAmount: number = 0;
    additionalAmount: number = 0;
    additionalBvAmount: number = 0;
    quotationStatus = {
        waitingQuote: 1,
        approve: 6,
        reject: 2,
    }
    isRejected = false
    isCreate = false
    isAdditional = false
    additionalClicked = false
    inputSearchAccessary = x => x.nameVN;
    expectedDeliveryDateMessageError = ''
    expectedDeliveryDate: Date = null
    strExpectedDeliveryDate: string = ''
    fieldsNeedValidate = {
        'quantity': 'blQuantity',
        'priceOfGara': 'blPriceOfGara',
        'discountPercentage': 'blDiscountPercentage',
    }
    typeDialog: string = '';
    contentQuotationDetail: string = '';
    quotationDetailId: number = 0;
    requetContentQuotationDetail: boolean = false;

    @ViewChild('staticModal') public staticModal: ModalDirective;
    constructor(
        private voucherDetailService: VoucherDetailService,
        private loaderService: LoaderService,
        private ultilities: Utilities,
        private translateService: TranslateService,
        private quotationDetailService: QuotationDetailService,
        private _confirm: ConfirmService,
        private router: Router, private accessaryService: AccessaryService) {
    }

    ngOnInit() {

    }

}

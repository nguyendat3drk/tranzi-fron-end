import { Component, ElementRef, HostListener } from '@angular/core';
// import {GlobalState} from '../../../global.state';
// import {layoutSizes} from '../../../theme';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'gara-side-bar',
  templateUrl: './gara-side-bar.component.html',
  styleUrls: ['./gara-side-bar.component.scss'],
})
export class GaraSideBarComponent {
  public menuHeight: number;
  public isMenuCollapsed: boolean = false;
  public isMenuShouldCollapsed: boolean = false;

  constructor(
    private _elementRef: ElementRef,
    // private _state:GlobalState
  ) {

    // this._state.subscribe('menu.isCollapsed', (isCollapsed) => {
    //   this.isMenuCollapsed = isCollapsed;
    // });
  }

  // tslint:disable-next-line:use-life-cycle-interface
  public ngOnInit(): void {
    if (this._shouldMenuCollapse()) {
      this.menuCollapse();
    }
  }

  // tslint:disable-next-line:use-life-cycle-interface
  public ngAfterViewInit(): void {
    setTimeout(() => this.updateSidebarHeight());
  }

  @HostListener('window:resize')
  public onWindowResize(): void {

    // tslint:disable-next-line:prefer-const
    const isMenuShouldCollapsed = this._shouldMenuCollapse();

    if (this.isMenuShouldCollapsed !== isMenuShouldCollapsed) {
      this.menuCollapseStateChange(isMenuShouldCollapsed);
    }
    this.isMenuShouldCollapsed = isMenuShouldCollapsed;
    this.updateSidebarHeight();
  }

  public menuExpand(): void {
    this.menuCollapseStateChange(false);
  }

  public menuCollapse(): void {
    this.menuCollapseStateChange(true);
  }

  public menuCollapseStateChange(isCollapsed: boolean): void {
    this.isMenuCollapsed = isCollapsed;
    // this._state.notifyDataChanged('menu.isCollapsed', this.isMenuCollapsed);
  }

  public updateSidebarHeight(): void {
    // TODO: get rid of magic 84 constant
    this.menuHeight = this._elementRef.nativeElement.childNodes[0].clientHeight - 84;
  }

  private _shouldMenuCollapse(): boolean {
    return true;
    // return window.innerWidth <= layoutSizes.resWidthCollapseSidebar;
  }
}

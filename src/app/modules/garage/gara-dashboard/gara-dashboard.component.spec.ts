import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GaraDashboardComponent } from './gara-dashboard.component';

describe('BvDashboardComponent', () => {
  let component: GaraDashboardComponent;
  let fixture: ComponentFixture<GaraDashboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GaraDashboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GaraDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

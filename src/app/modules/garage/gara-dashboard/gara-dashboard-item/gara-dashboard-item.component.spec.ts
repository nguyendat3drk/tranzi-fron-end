import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GaraDashboardItemComponent } from './gara-dashboard-item.component';

describe('GaraDashboardItemComponent', () => {
  let component: GaraDashboardItemComponent;
  let fixture: ComponentFixture<GaraDashboardItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GaraDashboardItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GaraDashboardItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

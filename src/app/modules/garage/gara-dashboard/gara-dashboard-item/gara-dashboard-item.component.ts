import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'gara-dashboard-item',
  templateUrl: './gara-dashboard-item.component.html',
  styleUrls: ['./gara-dashboard-item.component.scss'],
})
export class GaraDashboardItemComponent implements OnInit {

  @Input() title: string;
  @Input() img: string;
  @Input() content1: string;
  @Input() content2: string;
  @Input() content3: string;
  @Input() content4: string;
  @Input() content5: string;
  @Input() content6: string;
  constructor(private router: Router) { }

  ngOnInit() {

  }

  routerGaraDetail1(event) {
    if (Number(event) !== 0) {
      this.router.navigate(['/garage/detail-profile/1/' + event]);
    }
  }
  routerGaraDetail2(event) {
    if (Number(event) !== 0) {
      this.router.navigate(['/garage/detail-profile/2/' + event]);
    }
  }
  routerGaraDetail3(event) {
    if (Number(event) !== 0) {
      this.router.navigate(['/garage/detail-profile/3/' + event]);
    }
  }
  routerGaraDetail4(event) {
    if (Number(event) !== 0) {
      this.router.navigate(['/garage/detail-profile/4/' + event]);
    }
  }
  routerGaraDetail5(event) {
    if (Number(event) !== 0) {
      this.router.navigate(['/garage/detail-profile/5/' + event]);
    }
  }
  routerGaraDetail6(event) {
    if (Number(event) !== 0) {
      this.router.navigate(['/garage/detail-profile/6/' + event]);
    }
  }
}

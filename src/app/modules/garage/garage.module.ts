import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GarageRoutingModule } from "./garage-routing.module";
import { GarageStateService } from './garage-state.service';
import { GaraDashboardComponent } from './gara-dashboard/gara-dashboard.component';
import { ThemeModule } from '../../@theme/theme.module';
import { GaraAppShellComponent } from './gara-app-shell/gara-app-shell.component';
import { GaraMenuItem } from './gara-menu/components/baMenuItem/gara-menu-item.component';
import { GaraHeaderComponent } from './gara-header/gara-header.component';
import { GaraSideBarComponent } from './gara-side-bar/gara-side-bar.component';
import { GaraMenuComponent } from './gara-menu/gara-menu.component';
import { GaraHelpComponent } from './gara-help/gara-help.component';
import { GaraDashboardItemComponent } from './gara-dashboard/gara-dashboard-item/gara-dashboard-item.component';
import { SharedModule } from '../../shared/shared.module';
import { HttpClientModule } from '@angular/common/http';
import { NgbDateAdapter, NgbDateParserFormatter } from '@ng-bootstrap/ng-bootstrap';
import { NgbDateNativeAdapter } from '../../shared/common/ngb-date-native-adapter';
import { NgbCustomFormatter } from '../../shared/common/ngb-custom-formatter';
import { ConfirmService } from './../../shared/confirm-dialog/ConfirmService';
import { LoaderComponentGara } from '../../shared/components/loaderGara/loader.component';
import { LoaderService } from '../../shared/services/loader.service';
import { Utilities } from './../../shared/services/utilities.service';
import { ContextMenuModule } from 'primeng/contextmenu';
import { DataTableModule } from 'primeng/datatable';
import { RadioButtonModule } from 'primeng/primeng';
import { NgxMaskModule } from 'ngx-mask';
import { GaraDivisionJobComponent } from './gara-division-job/gara-division-job.component';
import { PdfViewerModule } from 'ng2-pdf-viewer';
import { ModalModule } from 'ngx-bootstrap';
import { FileUploadModule } from 'primeng/fileupload';
import { ProgressBarModule } from 'primeng/progressbar';;
import { CurrencyMaskModule } from 'ng2-currency-mask';
import { GaraProfileDetailsComponent } from './gara-profile-details/gara-profile-details.component';
import { GaraDocumentDetailsModule } from './gara-document-details/gara-document-details.module';
import { PrintpreviewComponent } from './gara-profile-details/printpreview/printpreview.component';
@NgModule({
  imports: [
    CommonModule,
    GarageRoutingModule,
    ThemeModule,
    SharedModule,
    HttpClientModule,
    DataTableModule,
    ContextMenuModule,
    RadioButtonModule,
    ModalModule.forRoot(),
    NgxMaskModule.forRoot(),
    PdfViewerModule,
    FileUploadModule,
    ProgressBarModule,
    CurrencyMaskModule,
    GaraDocumentDetailsModule,

  ],
  declarations: [
    GaraDashboardComponent,
    GaraAppShellComponent,
    GaraHeaderComponent,
    GaraSideBarComponent,
    GaraMenuComponent,
    GaraMenuItem,
    GaraDashboardItemComponent,
    LoaderComponentGara,
    GaraProfileDetailsComponent,
    GaraDivisionJobComponent,
    GaraHelpComponent,
    PrintpreviewComponent,
  ],
  providers: [
    GarageStateService,
    ConfirmService,
    LoaderService,
    Utilities,
    // Date picker transform date to native
    { provide: NgbDateAdapter, useClass: NgbDateNativeAdapter },
    { provide: NgbDateParserFormatter, useClass: NgbCustomFormatter },
  ],
})
export class GarageModule { }
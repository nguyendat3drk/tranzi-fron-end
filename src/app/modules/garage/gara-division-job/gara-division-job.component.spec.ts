import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GaraDivisionJobComponent } from './gara-division-job.component';

describe('BvDivisionJobComponent', () => {
  let component: GaraDivisionJobComponent;
  let fixture: ComponentFixture<GaraDivisionJobComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GaraDivisionJobComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GaraDivisionJobComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import {Component, OnInit} from '@angular/core';
import {MENU_ITEMS} from '../bao-viet-menu';
import {BvMenuService} from '../../../shared/services/bv-menu.service';
import {GarageStateService} from '../garage-state.service';
import {NgbDatepickerConfig} from '@ng-bootstrap/ng-bootstrap';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'gara-app-shell',
  templateUrl: './gara-app-shell.component.html',
  styleUrls: ['./gara-app-shell.component.scss'],
  providers: [NgbDatepickerConfig],
})
export class GaraAppShellComponent implements OnInit {

  menu = MENU_ITEMS;
  constructor(
    private menuService: BvMenuService,
    private bvState: GarageStateService) {
  }

  ngOnInit() {
    this.menuService.updateMenuByRoutes(this.menu);
  }

  get isShowSideBar() {
    return this.bvState.isShowSideBar;
  }

}

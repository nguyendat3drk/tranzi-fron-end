import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GaraAppShellComponent } from './gara-app-shell.component';

describe('GaraAppShellComponent', () => {
  let component: GaraAppShellComponent;
  let fixture: ComponentFixture<GaraAppShellComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GaraAppShellComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GaraAppShellComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

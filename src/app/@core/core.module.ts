import { ModuleWithProviders, NgModule, Optional, SkipSelf } from '@angular/core';
import { CommonModule } from '@angular/common';

import { throwIfAlreadyLoaded } from './module-import-guard';
import { Oauth2Service } from '../shared/services/http/oauth2.service';
import { AccountService } from '../shared/services/http/account.service';
import { ClaimService } from '../shared/services/http/claim.service';
import { TermsService } from '../shared/services/http/TermsService';
import { ArticlesService } from '../shared/services/http/ArticlesService';
import { TermsManagmentService } from '../shared/services/http/terms-managment.service';
import { ArticlesManagmentService } from '../shared/services/http/videos-managment.service';
import { DmContactService } from '../shared/services/http/dm-contact.service';
import { DmIncidentService } from '../shared/services/http/dm-incident.service';
import { AddressService } from '../shared/services/http/address.service';
import { MessageService } from 'primeng/components/common/messageservice';
import { AttributesService } from '../shared/services/http/attributes.service';
import { BeneficiaryService } from '../shared/services/http/beneficiary.service';
import { ProductService } from '../shared/services/http/product.service';
import { WorkBasketService } from '../shared/services/http/work-basket.service';
import { SharedService } from '../shared/services/http/shared.service';
import { VoucherDetailService } from '../shared/services/http/voucher-detail.service';
import { QuotationDetailService } from '../shared/services/http/QuotationDetailService';
import { AccessaryService } from '../shared/services/http/accessary.service';
import { WorkAssignService } from '../shared/services/http/work-assign.service';
import { CategoriesService } from '../shared/services/http/categories.service';
import { ManufactService } from '../shared/services/http/manufact.service';
import { CountriesService } from '../shared/entity/CountriesService';
import { TagsService } from '../shared/services/http/tags.service';
import { CustomerService } from '../shared/services/http/customer.service';
import { CustomerOrderService } from '../shared/services/http/customer-order.service';
import { InventoryService } from '../shared/services/http/inventory.service';
import { ProductPriceService } from '../shared/services/http/product-service.service';
import { AdminBomService } from '../shared/services/http/adminBom.service';
import { InventoryOrderService } from '../shared/services/http/Inventoryorder.service';
import { InventoryAreaService } from '../shared/services/http/inventoryArea.service';
import { SettingService } from '../shared/services/http/SettingService';

export const NB_CORE_PROVIDERS = [
  Oauth2Service,
  AccountService,
  ManufactService,
  CategoriesService,
  ClaimService,
  TermsService,
  ArticlesService,
  TermsManagmentService,
  ArticlesManagmentService,
  DmContactService,
  DmIncidentService,
  AddressService,
  MessageService,
  AttributesService,
  BeneficiaryService,
  ProductService,
  WorkBasketService,
  SharedService,
  VoucherDetailService,
  CustomerService,
  ProductPriceService,
  QuotationDetailService,
  AccessaryService,
  SettingService,
  WorkAssignService,
  CountriesService,
  TagsService,
  CustomerOrderService,
  InventoryService,
  AdminBomService,
  InventoryOrderService,
  InventoryAreaService,
];

@NgModule({
  imports: [
    CommonModule,
  ],
  exports: [
    // NbAuthModule,
  ],
  declarations: [],
})
export class CoreModule {
  constructor(@Optional() @SkipSelf() parentModule: CoreModule) {
    throwIfAlreadyLoaded(parentModule, 'CoreModule');
  }

  static forRoot(): ModuleWithProviders {
    return <ModuleWithProviders>{
      ngModule: CoreModule,
      providers: [
        ...NB_CORE_PROVIDERS,
      ],
    };
  }
}

import { ExtraOptions, PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { AuthGuard } from './shared/guards/auth.guard';
import { AuthGuardGara } from './shared/guards/auth.guardGara';

const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: '/auth/login' },
  {
    path: 'tranzi-admin',
    loadChildren: 'app/modules/tranzi-admin/tranzi-admin.module#TranziAdminModule',
    canActivate: [AuthGuard],
  },
  { path: 'garage', loadChildren: 'app/modules/garage/garage.module#GarageModule',
    canActivate: [AuthGuardGara],
  },
];

const config: ExtraOptions = {
  useHash: false,
  preloadingStrategy: PreloadAllModules,
};

@NgModule({
  imports: [RouterModule.forRoot(routes, config)],
  exports: [RouterModule],
})
export class AppRoutingModule {
}

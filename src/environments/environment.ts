// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

// export const environment = {
//   production: false,
//   endpoint:  'http://localhost:8082/EClaim',
//   // endpoint:  'http://10.1.13.29:8888/EClaim',
//   // endpoint:  'http://eclaim.vti.com.vn:8081/EClaim',
//   basicAuth: {
//     username: 'eclaim_app_client',
//     password: '33b17e044ee6a4fa383f46ec6e28ea1d',
//   },
// };

declare var urlPath: any;
declare var basicAuthName: any;
declare var basicAuthPassword: any;
declare var urlSerVer: any;
export const environment = {
  production: false,
  endpoint:  urlPath,
  urlSerVer: urlSerVer,
  basicAuth: {
    username: basicAuthName,
    password: basicAuthPassword,
  },
};

